package cz.vutbr.fit.exampreparation.features.roomPlan;

import cz.vutbr.fit.exampreparation.features.cell.Cell;
import cz.vutbr.fit.exampreparation.features.room.Room;
import cz.vutbr.fit.exampreparation.features.termRun.TermRun;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class RoomPlanTest {
  @Test
  public void testConstructor() {
    ArrayList<TermRun> termRunList = new ArrayList<TermRun>();
    ArrayList<Cell> cellList = new ArrayList<Cell>();
    Room room = new Room();
    RoomPlan actualRoomPlan = new RoomPlan(3, SpacingBetweenStudents.ZERO, termRunList, cellList, room);

    assertEquals(3, actualRoomPlan.getCapacity());
    List<TermRun> termRuns = actualRoomPlan.getTermRuns();
    assertSame(termRunList, termRuns);
    assertEquals(cellList, termRuns);
    assertTrue(termRuns.isEmpty());
    assertEquals(SpacingBetweenStudents.ZERO, actualRoomPlan.getSpacing());
    Room roomReference = actualRoomPlan.getRoomReference();
    assertSame(room, roomReference);
    assertEquals(0, actualRoomPlan.getIdRoomPlan());
    List<Cell> cells = actualRoomPlan.getCells();
    assertSame(cellList, cells);
    assertEquals(termRuns, cells);
    assertTrue(cells.isEmpty());
    assertNull(roomReference.getNumberOfRows());
    assertNull(roomReference.getNumberOfColumns());
    assertNull(roomReference.getName());
    assertEquals(0, roomReference.getIdRoom());
    assertNull(roomReference.getDescription());
    assertNull(roomReference.getAppUserReference());
    assertNull(roomReference.getAbbreviation());
    assertNull(roomReference.getRoomPlans());
    assertSame(termRuns, termRunList);
    assertSame(cells, cellList);
    assertSame(roomReference, room);
  }

  @Test
  public void testConstructor2() {
    Room room = new Room();
    RoomPlan actualRoomPlan = new RoomPlan(SpacingBetweenStudents.ZERO, room);

    assertEquals(0, actualRoomPlan.getCapacity());
    assertNull(actualRoomPlan.getTermRuns());
    assertEquals(SpacingBetweenStudents.ZERO, actualRoomPlan.getSpacing());
    Room roomReference = actualRoomPlan.getRoomReference();
    assertSame(room, roomReference);
    assertEquals(0, actualRoomPlan.getIdRoomPlan());
    assertNull(actualRoomPlan.getCells());
    assertNull(roomReference.getNumberOfRows());
    assertNull(roomReference.getNumberOfColumns());
    assertNull(roomReference.getName());
    assertEquals(0, roomReference.getIdRoom());
    assertNull(roomReference.getDescription());
    assertNull(roomReference.getAppUserReference());
    assertNull(roomReference.getAbbreviation());
    assertNull(roomReference.getRoomPlans());
    assertSame(roomReference, room);
  }
}

