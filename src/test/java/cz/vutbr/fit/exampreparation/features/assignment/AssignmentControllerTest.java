package cz.vutbr.fit.exampreparation.features.assignment;

import cz.vutbr.fit.exampreparation.common.MessageResponseDto;
import cz.vutbr.fit.exampreparation.features.assignment.dto.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {AssignmentController.class})
@ExtendWith(SpringExtension.class)
public class AssignmentControllerTest {
  @Autowired
  private AssignmentController assignmentController;

  @MockBean
  private AssignmentService assignmentService;

  @Test
  public void testGetAllAssignmentsInfo() throws Exception {
    when(this.assignmentService.getAllAssignmentsInfo())
      .thenReturn(new ResponseEntity<List<BasicAssignmentInfoDto>>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/internal/assignments");
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.assignmentController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testGetAssignmentDetails() throws Exception {
    when(this.assignmentService.getAssignmentDetails(anyInt()))
      .thenReturn(new ResponseEntity<AssignmentInfoDto>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/internal/assignments/{id}", 1);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.assignmentController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testDeleteAssignment() throws Exception {
    when(this.assignmentService.deleteAssignment(anyInt()))
      .thenReturn(new ResponseEntity<MessageResponseDto>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
      .delete("/internal/assignments/{assignmentId:[\\d]+}", 123);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.assignmentController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testGetAssignmentFileContent() throws Exception {
    when(this.assignmentService.getAssignmentFileContent(anyInt(), anyString()))
      .thenReturn(new ResponseEntity<FileContentDto>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
      .get("/internal/assignments/{assignmentId:[\\d]+}/file", 123)
      .param("filePath", "foo");
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.assignmentController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testGenerateAssignment() throws Exception {
    when(this.assignmentService.generateAssignment((Integer) any(), (Integer) any(), anyString(), (Integer) any()))
      .thenReturn(new ResponseEntity<MessageResponseDto>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/internal/assignments/generate");
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.assignmentController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(200));
  }

  @Test
  public void testCancelAssignmentGenerating() throws Exception {
    when(this.assignmentService.cancelAssignmentGenerating((Integer) any()))
      .thenReturn(new ResponseEntity<MessageResponseDto>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
      .get("/internal/assignments/cancel-generating");
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.assignmentController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testCreateNewAssignment() throws Exception {
    MockHttpServletRequestBuilder postResult = MockMvcRequestBuilders.post("/internal/assignments");
    MockHttpServletRequestBuilder paramResult = postResult.param("assignmentDto",
      String.valueOf(new BasicAssignmentDto()));
    MockHttpServletRequestBuilder requestBuilder = paramResult.param("assignmentFiles",
      String.valueOf(new MultipartFile[]{null, null, null}));
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.assignmentController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(400));
  }

  @Test
  public void testDownloadFile() throws Exception {
    when(this.assignmentService.downloadAssignmentFile(anyInt(), anyString()))
      .thenReturn(new ResponseEntity<Resource>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
      .get("/internal/assignments/{assignmentId:[\\d]+}/file/download", 123)
      .param("filePath", "foo");
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.assignmentController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testEditAssignment() throws Exception {
    MockHttpServletRequestBuilder putResult = MockMvcRequestBuilders.put("/internal/assignments/{id}", 1);
    MockHttpServletRequestBuilder paramResult = putResult.param("assignmentDto",
      String.valueOf(new EditAssignmentDto()));
    MockHttpServletRequestBuilder requestBuilder = paramResult.param("assignmentFiles",
      String.valueOf(new MultipartFile[]{null, null, null}));
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.assignmentController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(400));
  }
}

