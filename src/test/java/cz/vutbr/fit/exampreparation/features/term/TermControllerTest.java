package cz.vutbr.fit.exampreparation.features.term;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.vutbr.fit.exampreparation.common.MessageResponseDto;
import cz.vutbr.fit.exampreparation.features.appUser.dto.LoginNameDto;
import cz.vutbr.fit.exampreparation.features.roomPlan.SpacingBetweenStudents;
import cz.vutbr.fit.exampreparation.features.term.dto.AllTermsInfoResponseDto;
import cz.vutbr.fit.exampreparation.features.term.dto.StudentsPlacementDto;
import cz.vutbr.fit.exampreparation.features.term.dto.TermDetailDto;
import cz.vutbr.fit.exampreparation.features.term.dto.term.EditTermDto;
import cz.vutbr.fit.exampreparation.features.term.dto.term.EditTermInfoDto;
import cz.vutbr.fit.exampreparation.features.term.dto.term.NewTermDto;
import cz.vutbr.fit.exampreparation.features.termRun.Distribution;
import cz.vutbr.fit.exampreparation.features.termRun.dto.TermRunDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {TermController.class})
@ExtendWith(SpringExtension.class)
public class TermControllerTest {
  @Autowired
  private TermController termController;

  @MockBean
  private TermService termService;

  @Test
  public void testCreateNewTerm() throws Exception {
    NewTermDto newTermDto = new NewTermDto();
    newTermDto.setAdditionalTermManagersEmails(new ArrayList<String>());
    newTermDto.setInformationText("Information Text");
    newTermDto.setDistribution(Distribution.RANDOM);
    newTermDto.setStudentsPlacements(new ArrayList<StudentsPlacementDto>());
    newTermDto.setCourseId(123);
    newTermDto.setTermRuns(new ArrayList<TermRunDto>());
    newTermDto.setEmailMessage("jane.doe@example.org");
    newTermDto.setTermName("Term Name");
    newTermDto.setStudentsList(new ArrayList<LoginNameDto>());
    newTermDto.setSendEmail(true);
    newTermDto.setSpacingBetweenStudents(SpacingBetweenStudents.ZERO);
    String content = (new ObjectMapper()).writeValueAsString(newTermDto);
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/internal/terms")
      .contentType(MediaType.APPLICATION_JSON)
      .content(content);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.termController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(400));
  }

  @Test
  public void testCreateNewTerm2() throws Exception {
    NewTermDto newTermDto = new NewTermDto();
    newTermDto.setAdditionalTermManagersEmails(new ArrayList<String>());
    newTermDto.setInformationText("Information Text");
    newTermDto.setDistribution(Distribution.RANDOM);
    newTermDto.setStudentsPlacements(new ArrayList<StudentsPlacementDto>());
    newTermDto.setCourseId(123);
    newTermDto.setTermRuns(new ArrayList<TermRunDto>());
    newTermDto.setEmailMessage("jane.doe@example.org");
    newTermDto.setTermName("Term Name");
    newTermDto.setStudentsList(new ArrayList<LoginNameDto>());
    newTermDto.setSendEmail(true);
    newTermDto.setSpacingBetweenStudents(SpacingBetweenStudents.ZERO);
    String content = (new ObjectMapper()).writeValueAsString(newTermDto);
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/internal/terms")
      .contentType(MediaType.APPLICATION_JSON)
      .content(content);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.termController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(400));
  }

  @Test
  public void testDeleteTerm() throws Exception {
    when(this.termService.deleteTerm(anyInt())).thenReturn(new ResponseEntity<MessageResponseDto>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/internal/terms/{termId:[\\d]+}", 9);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.termController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testDeleteTerm2() throws Exception {
    when(this.termService.deleteTerm(anyInt())).thenReturn(new ResponseEntity<MessageResponseDto>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/internal/terms/{termId:[\\d]+}", 9);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.termController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testEditTerm2() throws Exception {
    EditTermDto editTermDto = new EditTermDto();
    editTermDto.setDistribution(Distribution.RANDOM);
    editTermDto.setTermRuns(new ArrayList<TermRunDto>());
    editTermDto.setEmailMessage("jane.doe@example.org");
    editTermDto.setOldStudentsList(new ArrayList<LoginNameDto>());
    editTermDto.setAddStudentsAtTheEnd(true);
    editTermDto.setTermName("Term Name");
    editTermDto.setAdditionalTermManagersEmails(new ArrayList<String>());
    editTermDto.setInformationText("Information Text");
    editTermDto.setStudentsPlacements(new ArrayList<StudentsPlacementDto>());
    editTermDto.setCourseId(123);
    editTermDto.setStudentsList(new ArrayList<LoginNameDto>());
    editTermDto.setSendEmail(true);
    editTermDto.setSpacingBetweenStudents(SpacingBetweenStudents.ZERO);
    String content = (new ObjectMapper()).writeValueAsString(editTermDto);
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.put("/internal/terms/{termId:[\\d]+}", 9)
      .contentType(MediaType.APPLICATION_JSON)
      .content(content);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.termController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(400));
  }

  @Test
  public void testGetAllTermsInfoInternal2() throws Exception {
    when(this.termService.getAllTermsInfo(anyBoolean()))
      .thenReturn(new ResponseEntity<List<AllTermsInfoResponseDto>>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/internal/terms");
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.termController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testGetAllTermsInfoPublic2() throws Exception {
    when(this.termService.getAllTermsInfo(anyBoolean()))
      .thenReturn(new ResponseEntity<List<AllTermsInfoResponseDto>>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/public/terms");
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.termController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testGetEditTermDetails() throws Exception {
    when(this.termService.getEditTermDetails(anyInt()))
      .thenReturn(new ResponseEntity<EditTermInfoDto>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/internal/terms/{termId:[\\d]+}/edit",
      9);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.termController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testGetEditTermDetails2() throws Exception {
    when(this.termService.getEditTermDetails(anyInt()))
      .thenReturn(new ResponseEntity<EditTermInfoDto>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/internal/terms/{termId:[\\d]+}/edit",
      9);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.termController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testEditTerm() throws Exception {
    EditTermDto editTermDto = new EditTermDto();
    editTermDto.setDistribution(Distribution.RANDOM);
    editTermDto.setTermRuns(new ArrayList<TermRunDto>());
    editTermDto.setEmailMessage("jane.doe@example.org");
    editTermDto.setOldStudentsList(new ArrayList<LoginNameDto>());
    editTermDto.setAddStudentsAtTheEnd(true);
    editTermDto.setTermName("Term Name");
    editTermDto.setAdditionalTermManagersEmails(new ArrayList<String>());
    editTermDto.setInformationText("Information Text");
    editTermDto.setStudentsPlacements(new ArrayList<StudentsPlacementDto>());
    editTermDto.setCourseId(123);
    editTermDto.setStudentsList(new ArrayList<LoginNameDto>());
    editTermDto.setSendEmail(true);
    editTermDto.setSpacingBetweenStudents(SpacingBetweenStudents.ZERO);
    String content = (new ObjectMapper()).writeValueAsString(editTermDto);
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.put("/internal/terms/{termId:[\\d]+}", 9)
      .contentType(MediaType.APPLICATION_JSON)
      .content(content);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.termController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(400));
  }

  @Test
  public void testGetAllTermsInfoInternal() throws Exception {
    when(this.termService.getAllTermsInfo(anyBoolean()))
      .thenReturn(new ResponseEntity<List<AllTermsInfoResponseDto>>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/internal/terms");
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.termController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testGetAllTermsInfoPublic() throws Exception {
    when(this.termService.getAllTermsInfo(anyBoolean()))
      .thenReturn(new ResponseEntity<List<AllTermsInfoResponseDto>>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/public/terms");
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.termController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testGetTermDetailsInternal() throws Exception {
    when(this.termService.getTermDetails(anyInt(), anyBoolean()))
      .thenReturn(new ResponseEntity<TermDetailDto>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/internal/terms/{termId:[\\d]+}", 9);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.termController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testGetTermDetailsInternal2() throws Exception {
    when(this.termService.getTermDetails(anyInt(), anyBoolean()))
      .thenReturn(new ResponseEntity<TermDetailDto>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/internal/terms/{termId:[\\d]+}", 9);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.termController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testGetTermDetailsPublic() throws Exception {
    when(this.termService.getTermDetails(anyInt(), anyBoolean()))
      .thenReturn(new ResponseEntity<TermDetailDto>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/public/terms/{termId:[\\d]+}", 9);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.termController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testGetTermDetailsPublic2() throws Exception {
    when(this.termService.getTermDetails(anyInt(), anyBoolean()))
      .thenReturn(new ResponseEntity<TermDetailDto>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/public/terms/{termId:[\\d]+}", 9);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.termController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }
}

