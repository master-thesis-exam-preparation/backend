package cz.vutbr.fit.exampreparation.features.assignment.dto;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class BasicAssignmentDtoTest {
  @Test
  public void testCanEqual() {
    assertFalse((new BasicAssignmentDto()).canEqual("Other"));
  }

  @Test
  public void testCanEqual2() {
    BasicAssignmentDto basicAssignmentDto = new BasicAssignmentDto();
    assertTrue(basicAssignmentDto.canEqual(new BasicAssignmentDto()));
  }

  @Test
  public void testConstructor() {
    BasicAssignmentDto actualBasicAssignmentDto = new BasicAssignmentDto();
    ArrayList<String> stringList = new ArrayList<String>();
    actualBasicAssignmentDto.setAdditionalAssignmentManagersEmails(stringList);
    actualBasicAssignmentDto.setName("Name");
    assertSame(stringList, actualBasicAssignmentDto.getAdditionalAssignmentManagersEmails());
    assertEquals("Name", actualBasicAssignmentDto.getName());
    assertEquals("BasicAssignmentDto(name=Name, additionalAssignmentManagersEmails=[])",
      actualBasicAssignmentDto.toString());
  }

  @Test
  public void testConstructor2() {
    ArrayList<String> stringList = new ArrayList<String>();
    BasicAssignmentDto actualBasicAssignmentDto = new BasicAssignmentDto("Name", stringList);
    ArrayList<String> stringList1 = new ArrayList<String>();
    actualBasicAssignmentDto.setAdditionalAssignmentManagersEmails(stringList1);
    actualBasicAssignmentDto.setName("Name");
    List<String> additionalAssignmentManagersEmails = actualBasicAssignmentDto.getAdditionalAssignmentManagersEmails();
    assertSame(stringList1, additionalAssignmentManagersEmails);
    assertEquals(stringList, additionalAssignmentManagersEmails);
    assertEquals("Name", actualBasicAssignmentDto.getName());
    assertEquals("BasicAssignmentDto(name=Name, additionalAssignmentManagersEmails=[])",
      actualBasicAssignmentDto.toString());
  }

  @Test
  public void testEquals() {
    assertNotEquals((new BasicAssignmentDto()), null);
    assertNotEquals((new BasicAssignmentDto()), "Different type to BasicAssignmentDto");
  }

  @Test
  public void testEquals2() {
    BasicAssignmentDto basicAssignmentDto = new BasicAssignmentDto();
    assertEquals(basicAssignmentDto, basicAssignmentDto);
    int expectedHashCodeResult = basicAssignmentDto.hashCode();
    assertEquals(expectedHashCodeResult, basicAssignmentDto.hashCode());
  }

  @Test
  public void testEquals3() {
    BasicAssignmentDto basicAssignmentDto = new BasicAssignmentDto();
    BasicAssignmentDto basicAssignmentDto1 = new BasicAssignmentDto();
    assertEquals(basicAssignmentDto1, basicAssignmentDto);
    int expectedHashCodeResult = basicAssignmentDto.hashCode();
    assertEquals(expectedHashCodeResult, basicAssignmentDto1.hashCode());
  }

  @Test
  public void testEquals4() {
    BasicAssignmentDto basicAssignmentDto = new BasicAssignmentDto("Name", new ArrayList<String>());
    assertNotEquals(new BasicAssignmentDto(), basicAssignmentDto);
  }

  @Test
  public void testEquals5() {
    EditAssignmentDto editAssignmentDto = mock(EditAssignmentDto.class);
    assertNotEquals(new BasicAssignmentDto(), editAssignmentDto);
  }

  @Test
  public void testEquals6() {
    BasicAssignmentDto basicAssignmentDto = new BasicAssignmentDto();
    assertNotEquals(new BasicAssignmentDto("Name", new ArrayList<String>()), basicAssignmentDto);
  }

  @Test
  public void testEquals7() {
    BasicAssignmentDto basicAssignmentDto = new BasicAssignmentDto();
    assertNotEquals(new EditAssignmentDto(), basicAssignmentDto);
  }

  @Test
  public void testEquals8() {
    BasicAssignmentDto basicAssignmentDto = new BasicAssignmentDto();
    EditAssignmentDto editAssignmentDto = mock(EditAssignmentDto.class);
    when(editAssignmentDto.getName()).thenReturn("foo");
    when(editAssignmentDto.canEqual((Object) any())).thenReturn(true);
    assertNotEquals(editAssignmentDto, basicAssignmentDto);
  }

  @Test
  public void testEquals9() {
    BasicAssignmentDto basicAssignmentDto = new BasicAssignmentDto();
    EditAssignmentDto editAssignmentDto = mock(EditAssignmentDto.class);
    when(editAssignmentDto.getAdditionalAssignmentManagersEmails()).thenReturn(new ArrayList<String>());
    when(editAssignmentDto.getName()).thenReturn(null);
    when(editAssignmentDto.canEqual((Object) any())).thenReturn(true);
    assertNotEquals(editAssignmentDto, basicAssignmentDto);
  }

  @Test
  public void testEquals10() {
    ArrayList<String> stringList = new ArrayList<String>();
    stringList.add("foo");

    BasicAssignmentDto basicAssignmentDto = new BasicAssignmentDto();
    basicAssignmentDto.setAdditionalAssignmentManagersEmails(stringList);
    EditAssignmentDto editAssignmentDto = mock(EditAssignmentDto.class);
    when(editAssignmentDto.getAdditionalAssignmentManagersEmails()).thenReturn(new ArrayList<String>());
    when(editAssignmentDto.getName()).thenReturn(null);
    when(editAssignmentDto.canEqual((Object) any())).thenReturn(true);
    assertNotEquals(editAssignmentDto, basicAssignmentDto);
  }
}

