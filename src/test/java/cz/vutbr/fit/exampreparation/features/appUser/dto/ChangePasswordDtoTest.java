package cz.vutbr.fit.exampreparation.features.appUser.dto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ChangePasswordDtoTest {
  @Test
  public void testCanEqual() {
    assertFalse((new ChangePasswordDto("iloveyou", "iloveyou")).canEqual("Other"));
  }

  @Test
  public void testCanEqual2() {
    ChangePasswordDto changePasswordDto = new ChangePasswordDto("iloveyou", "iloveyou");
    assertTrue(changePasswordDto.canEqual(new ChangePasswordDto("iloveyou", "iloveyou")));
  }

  @Test
  public void testConstructor() {
    ChangePasswordDto actualChangePasswordDto = new ChangePasswordDto("iloveyou", "iloveyou");
    actualChangePasswordDto.setCurrentPassword("iloveyou");
    actualChangePasswordDto.setNewPassword("iloveyou");
    assertEquals("iloveyou", actualChangePasswordDto.getCurrentPassword());
    assertEquals("iloveyou", actualChangePasswordDto.getNewPassword());
    assertEquals("ChangePasswordDto(currentPassword=iloveyou, newPassword=iloveyou)",
      actualChangePasswordDto.toString());
  }

  @Test
  public void testEquals() {
    assertNotEquals((new ChangePasswordDto("iloveyou", "iloveyou")), null);
    assertNotEquals((new ChangePasswordDto("iloveyou", "iloveyou")), "Different type to ChangePasswordDto");
  }

  @Test
  public void testEquals2() {
    ChangePasswordDto changePasswordDto = new ChangePasswordDto("iloveyou", "iloveyou");
    assertEquals(changePasswordDto, changePasswordDto);
    int expectedHashCodeResult = changePasswordDto.hashCode();
    assertEquals(expectedHashCodeResult, changePasswordDto.hashCode());
  }

  @Test
  public void testEquals3() {
    ChangePasswordDto changePasswordDto = new ChangePasswordDto("iloveyou", "iloveyou");
    ChangePasswordDto changePasswordDto1 = new ChangePasswordDto("iloveyou", "iloveyou");

    assertEquals(changePasswordDto1, changePasswordDto);
    int expectedHashCodeResult = changePasswordDto.hashCode();
    assertEquals(expectedHashCodeResult, changePasswordDto1.hashCode());
  }

  @Test
  public void testEquals4() {
    ChangePasswordDto changePasswordDto = new ChangePasswordDto(null, "iloveyou");
    assertNotEquals(new ChangePasswordDto("iloveyou", "iloveyou"), changePasswordDto);
  }

  @Test
  public void testEquals5() {
    ChangePasswordDto changePasswordDto = new ChangePasswordDto("Current Password", "iloveyou");
    assertNotEquals(new ChangePasswordDto("iloveyou", "iloveyou"), changePasswordDto);
  }

  @Test
  public void testEquals6() {
    ChangePasswordDto changePasswordDto = new ChangePasswordDto("iloveyou", null);
    assertNotEquals(new ChangePasswordDto("iloveyou", "iloveyou"), changePasswordDto);
  }

  @Test
  public void testEquals7() {
    ChangePasswordDto changePasswordDto = new ChangePasswordDto("iloveyou", "New Password");
    assertNotEquals(new ChangePasswordDto("iloveyou", "iloveyou"), changePasswordDto);
  }

  @Test
  public void testEquals8() {
    ChangePasswordDto changePasswordDto = new ChangePasswordDto(null, "iloveyou");
    ChangePasswordDto changePasswordDto1 = new ChangePasswordDto(null, "iloveyou");

    assertEquals(changePasswordDto1, changePasswordDto);
    int expectedHashCodeResult = changePasswordDto.hashCode();
    assertEquals(expectedHashCodeResult, changePasswordDto1.hashCode());
  }

  @Test
  public void testEquals9() {
    ChangePasswordDto changePasswordDto = new ChangePasswordDto("iloveyou", null);
    ChangePasswordDto changePasswordDto1 = new ChangePasswordDto("iloveyou", null);

    assertEquals(changePasswordDto1, changePasswordDto);
    int expectedHashCodeResult = changePasswordDto.hashCode();
    assertEquals(expectedHashCodeResult, changePasswordDto1.hashCode());
  }
}

