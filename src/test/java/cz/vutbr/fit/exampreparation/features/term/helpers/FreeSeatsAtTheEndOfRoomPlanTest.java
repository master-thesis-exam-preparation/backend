package cz.vutbr.fit.exampreparation.features.term.helpers;

import cz.vutbr.fit.exampreparation.features.appUser.AppUser;
import cz.vutbr.fit.exampreparation.features.assignment.Assignment;
import cz.vutbr.fit.exampreparation.features.cell.Cell;
import cz.vutbr.fit.exampreparation.features.course.Course;
import cz.vutbr.fit.exampreparation.features.role.Role;
import cz.vutbr.fit.exampreparation.features.room.Room;
import cz.vutbr.fit.exampreparation.features.roomPlan.RoomPlan;
import cz.vutbr.fit.exampreparation.features.roomPlan.SpacingBetweenStudents;
import cz.vutbr.fit.exampreparation.features.term.Term;
import cz.vutbr.fit.exampreparation.features.termRun.Distribution;
import cz.vutbr.fit.exampreparation.features.termRun.TermRun;
import cz.vutbr.fit.exampreparation.features.testSchedule.TestSchedule;
import cz.vutbr.fit.exampreparation.features.token.Token;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class FreeSeatsAtTheEndOfRoomPlanTest {
  @Test
  public void testCanEqual() {
    TermRun termRun = new TermRun();
    RoomPlan roomPlan = new RoomPlan();
    assertFalse((new FreeSeatsAtTheEndOfRoomPlan(termRun, roomPlan, new ArrayList<Cell>())).canEqual("Other"));
  }

  @Test
  public void testCanEqual2() {
    TermRun termRun = new TermRun();
    RoomPlan roomPlan = new RoomPlan();
    FreeSeatsAtTheEndOfRoomPlan freeSeatsAtTheEndOfRoomPlan = new FreeSeatsAtTheEndOfRoomPlan(termRun, roomPlan,
      new ArrayList<Cell>());
    TermRun termRun1 = new TermRun();
    RoomPlan roomPlan1 = new RoomPlan();
    assertTrue(freeSeatsAtTheEndOfRoomPlan
      .canEqual(new FreeSeatsAtTheEndOfRoomPlan(termRun1, roomPlan1, new ArrayList<Cell>())));
  }

  @Test
  public void testConstructor() {
    TermRun termRun = new TermRun();
    RoomPlan roomPlan = new RoomPlan();
    ArrayList<Cell> cellList = new ArrayList<Cell>();
    FreeSeatsAtTheEndOfRoomPlan actualFreeSeatsAtTheEndOfRoomPlan = new FreeSeatsAtTheEndOfRoomPlan(termRun, roomPlan,
      cellList);
    ArrayList<Cell> cellList1 = new ArrayList<Cell>();
    actualFreeSeatsAtTheEndOfRoomPlan.setFreeSeatsAtTheEndOfRoom(cellList1);
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    ArrayList<Token> tokenList = new ArrayList<Token>();
    appUser.setUserTokens(tokenList);
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    ArrayList<Room> roomList = new ArrayList<Room>();
    appUser.setRooms(roomList);
    appUser.setDegreesBehindName("Degrees Behind Name");
    ArrayList<Role> roleList = new ArrayList<Role>();
    appUser.setRoles(roleList);
    appUser.setLocked(true);
    ArrayList<Term> termList = new ArrayList<Term>();
    appUser.setTerms(termList);
    ArrayList<Assignment> assignmentList = new ArrayList<Assignment>();
    appUser.setAssignments(assignmentList);
    Room room = new Room();
    room.setAppUserReference(appUser);
    room.setAbbreviation("Abbreviation");
    ArrayList<RoomPlan> roomPlanList = new ArrayList<RoomPlan>();
    room.setRoomPlans(roomPlanList);
    room.setNumberOfColumns(10);
    room.setName("Name");
    room.setDescription("The characteristics of someone or something");
    room.setNumberOfRows(10);
    RoomPlan roomPlan1 = new RoomPlan();
    ArrayList<Cell> cellList2 = new ArrayList<Cell>();
    roomPlan1.setCells(cellList2);
    roomPlan1.setCapacity(3);
    ArrayList<TermRun> termRunList = new ArrayList<TermRun>();
    roomPlan1.setTermRuns(termRunList);
    roomPlan1.setSpacing(SpacingBetweenStudents.ZERO);
    roomPlan1.setRoomReference(room);
    actualFreeSeatsAtTheEndOfRoomPlan.setRoomPlan(roomPlan1);
    Course course = new Course();
    course.setAbbreviation("Abbreviation");
    course.setAcademicYear(1);
    course.setName("Name");
    ArrayList<Term> termList1 = new ArrayList<Term>();
    course.setTerms(termList1);
    Term term = new Term();
    term.setDistribution(Distribution.RANDOM);
    term.setCourseReference(course);
    ArrayList<TermRun> termRunList1 = new ArrayList<TermRun>();
    term.setTermRuns(termRunList1);
    term.setTermName("Term Name");
    ArrayList<AppUser> appUserList = new ArrayList<AppUser>();
    term.setTermManagers(appUserList);
    term.setDescription("The characteristics of someone or something");
    term.setSpacingBetweenStudents(SpacingBetweenStudents.ZERO);
    TermRun termRun1 = new TermRun();
    termRun1.setTermReference(term);
    ArrayList<RoomPlan> roomPlanList1 = new ArrayList<RoomPlan>();
    termRun1.setRoomPlans(roomPlanList1);
    termRun1.setStartDateTime(mock(Timestamp.class));
    termRun1.setEndDateTime(mock(Timestamp.class));
    ArrayList<TestSchedule> testScheduleList = new ArrayList<TestSchedule>();
    termRun1.setTestSchedules(testScheduleList);
    ArrayList<Assignment> assignmentList1 = new ArrayList<Assignment>();
    termRun1.setAssignments(assignmentList1);
    actualFreeSeatsAtTheEndOfRoomPlan.setTermRun(termRun1);
    List<Cell> freeSeatsAtTheEndOfRoom = actualFreeSeatsAtTheEndOfRoomPlan.getFreeSeatsAtTheEndOfRoom();
    assertEquals(roleList, freeSeatsAtTheEndOfRoom);
    assertEquals(termList1, freeSeatsAtTheEndOfRoom);
    assertEquals(roomPlanList1, freeSeatsAtTheEndOfRoom);
    assertEquals(tokenList, freeSeatsAtTheEndOfRoom);
    assertEquals(cellList2, freeSeatsAtTheEndOfRoom);
    assertEquals(testScheduleList, freeSeatsAtTheEndOfRoom);
    assertEquals(roomList, freeSeatsAtTheEndOfRoom);
    assertEquals(appUserList, freeSeatsAtTheEndOfRoom);
    assertEquals(cellList, freeSeatsAtTheEndOfRoom);
    assertEquals(assignmentList, freeSeatsAtTheEndOfRoom);
    assertEquals(termList, freeSeatsAtTheEndOfRoom);
    assertEquals(termRunList, freeSeatsAtTheEndOfRoom);
    assertSame(cellList1, freeSeatsAtTheEndOfRoom);
    assertEquals(termRunList1, freeSeatsAtTheEndOfRoom);
    assertEquals(assignmentList1, freeSeatsAtTheEndOfRoom);
    assertEquals(roomPlanList, freeSeatsAtTheEndOfRoom);
    assertSame(roomPlan1, actualFreeSeatsAtTheEndOfRoomPlan.getRoomPlan());
    assertSame(termRun1, actualFreeSeatsAtTheEndOfRoomPlan.getTermRun());
  }

  @Test
  public void testEquals() {
    TermRun termRun = new TermRun();
    RoomPlan roomPlan = new RoomPlan();
    assertNotEquals((new FreeSeatsAtTheEndOfRoomPlan(termRun, roomPlan, new ArrayList<Cell>())), null);
  }

  @Test
  public void testEquals2() {
    TermRun termRun = new TermRun();
    RoomPlan roomPlan = new RoomPlan();
    assertNotEquals((new FreeSeatsAtTheEndOfRoomPlan(termRun, roomPlan, new ArrayList<Cell>())), "Different type to FreeSeatsAtTheEndOfRoomPlan");
  }

  @Test
  public void testEquals3() {
    TermRun termRun = new TermRun();
    RoomPlan roomPlan = new RoomPlan();
    FreeSeatsAtTheEndOfRoomPlan freeSeatsAtTheEndOfRoomPlan = new FreeSeatsAtTheEndOfRoomPlan(termRun, roomPlan,
      new ArrayList<Cell>());
    assertEquals(freeSeatsAtTheEndOfRoomPlan, freeSeatsAtTheEndOfRoomPlan);
    int expectedHashCodeResult = freeSeatsAtTheEndOfRoomPlan.hashCode();
    assertEquals(expectedHashCodeResult, freeSeatsAtTheEndOfRoomPlan.hashCode());
  }

  @Test
  public void testEquals4() {
    TermRun termRun = new TermRun();
    RoomPlan roomPlan = new RoomPlan();
    FreeSeatsAtTheEndOfRoomPlan freeSeatsAtTheEndOfRoomPlan = new FreeSeatsAtTheEndOfRoomPlan(termRun, roomPlan,
      new ArrayList<Cell>());
    TermRun termRun1 = new TermRun();
    RoomPlan roomPlan1 = new RoomPlan();
    assertNotEquals(new FreeSeatsAtTheEndOfRoomPlan(termRun1, roomPlan1, new ArrayList<Cell>()), freeSeatsAtTheEndOfRoomPlan);
  }

  @Test
  public void testEquals5() {
    RoomPlan roomPlan = new RoomPlan();
    FreeSeatsAtTheEndOfRoomPlan freeSeatsAtTheEndOfRoomPlan = new FreeSeatsAtTheEndOfRoomPlan(null, roomPlan,
      new ArrayList<Cell>());
    TermRun termRun = new TermRun();
    RoomPlan roomPlan1 = new RoomPlan();
    assertNotEquals(new FreeSeatsAtTheEndOfRoomPlan(termRun, roomPlan1, new ArrayList<Cell>()), freeSeatsAtTheEndOfRoomPlan);
  }

  @Test
  public void testEquals6() {
    TermRun termRun = mock(TermRun.class);
    RoomPlan roomPlan = new RoomPlan();
    FreeSeatsAtTheEndOfRoomPlan freeSeatsAtTheEndOfRoomPlan = new FreeSeatsAtTheEndOfRoomPlan(termRun, roomPlan,
      new ArrayList<Cell>());
    TermRun termRun1 = new TermRun();
    RoomPlan roomPlan1 = new RoomPlan();
    assertNotEquals(new FreeSeatsAtTheEndOfRoomPlan(termRun1, roomPlan1, new ArrayList<Cell>()), freeSeatsAtTheEndOfRoomPlan);
  }
}

