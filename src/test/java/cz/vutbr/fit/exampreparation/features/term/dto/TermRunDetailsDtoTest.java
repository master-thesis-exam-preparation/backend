package cz.vutbr.fit.exampreparation.features.term.dto;

import cz.vutbr.fit.exampreparation.features.assignment.dto.BasicAssignmentInfoDto;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class TermRunDetailsDtoTest {
  @Test
  public void testConstructor() {
    ArrayList<SeatingPlanDto> seatingPlanDtoList = new ArrayList<SeatingPlanDto>();
    ArrayList<BasicAssignmentInfoDto> basicAssignmentInfoDtoList = new ArrayList<BasicAssignmentInfoDto>();
    TermRunDetailsDto actualTermRunDetailsDto = new TermRunDetailsDto(1, 1L, 1L, seatingPlanDtoList,
      basicAssignmentInfoDtoList);

    List<BasicAssignmentInfoDto> assignments = actualTermRunDetailsDto.getAssignments();
    assertSame(basicAssignmentInfoDtoList, assignments);
    assertEquals(seatingPlanDtoList, assignments);
    assertTrue(assignments.isEmpty());
    assertEquals("TermRunDetailsDto(highlight=false, seatingPlans=[], assignments=[])",
      actualTermRunDetailsDto.toString());
    assertFalse(actualTermRunDetailsDto.isHighlight());
    assertEquals(1L, actualTermRunDetailsDto.getStartTime().longValue());
    List<SeatingPlanDto> seatingPlans = actualTermRunDetailsDto.getSeatingPlans();
    assertSame(seatingPlanDtoList, seatingPlans);
    assertEquals(assignments, seatingPlans);
    assertTrue(seatingPlans.isEmpty());
    assertEquals(1, actualTermRunDetailsDto.getId().intValue());
    assertEquals(1L, actualTermRunDetailsDto.getEndTime().longValue());
    assertSame(seatingPlans, seatingPlanDtoList);
    assertSame(assignments, basicAssignmentInfoDtoList);
  }
}

