package cz.vutbr.fit.exampreparation.features.room.dto;

import cz.vutbr.fit.exampreparation.features.roomPlan.dto.RoomPlanDto;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class FreeRoomResponseDtoTest {
  @Test
  public void testCanEqual() {
    assertFalse((new FreeRoomResponseDto()).canEqual("Other"));
  }

  @Test
  public void testCanEqual2() {
    FreeRoomResponseDto freeRoomResponseDto = new FreeRoomResponseDto();
    assertTrue(freeRoomResponseDto.canEqual(new FreeRoomResponseDto()));
  }

  @Test
  public void testConstructor() {
    FreeRoomResponseDto actualFreeRoomResponseDto = new FreeRoomResponseDto();
    actualFreeRoomResponseDto.setRoomId(123);
    ArrayList<RoomPlanDto> roomPlanDtoList = new ArrayList<RoomPlanDto>();
    actualFreeRoomResponseDto.setRoomPlans(roomPlanDtoList);
    assertEquals(123, actualFreeRoomResponseDto.getRoomId().intValue());
    assertSame(roomPlanDtoList, actualFreeRoomResponseDto.getRoomPlans());
    assertEquals("FreeRoomResponseDto(roomId=123, roomPlans=[])", actualFreeRoomResponseDto.toString());
  }

  @Test
  public void testConstructor2() {
    ArrayList<RoomPlanDto> roomPlanDtoList = new ArrayList<RoomPlanDto>();
    FreeRoomResponseDto actualFreeRoomResponseDto = new FreeRoomResponseDto(123, roomPlanDtoList);
    actualFreeRoomResponseDto.setRoomId(123);
    ArrayList<RoomPlanDto> roomPlanDtoList1 = new ArrayList<RoomPlanDto>();
    actualFreeRoomResponseDto.setRoomPlans(roomPlanDtoList1);
    assertEquals(123, actualFreeRoomResponseDto.getRoomId().intValue());
    List<RoomPlanDto> roomPlans = actualFreeRoomResponseDto.getRoomPlans();
    assertSame(roomPlanDtoList1, roomPlans);
    assertEquals(roomPlanDtoList, roomPlans);
    assertEquals("FreeRoomResponseDto(roomId=123, roomPlans=[])", actualFreeRoomResponseDto.toString());
  }

  @Test
  public void testEquals() {
    assertNotEquals((new FreeRoomResponseDto()), null);
    assertNotEquals((new FreeRoomResponseDto()), "Different type to FreeRoomResponseDto");
  }

  @Test
  public void testEquals2() {
    FreeRoomResponseDto freeRoomResponseDto = new FreeRoomResponseDto();
    assertEquals(freeRoomResponseDto, freeRoomResponseDto);
    int expectedHashCodeResult = freeRoomResponseDto.hashCode();
    assertEquals(expectedHashCodeResult, freeRoomResponseDto.hashCode());
  }

  @Test
  public void testEquals3() {
    FreeRoomResponseDto freeRoomResponseDto = new FreeRoomResponseDto();
    FreeRoomResponseDto freeRoomResponseDto1 = new FreeRoomResponseDto();
    assertEquals(freeRoomResponseDto1, freeRoomResponseDto);
    int expectedHashCodeResult = freeRoomResponseDto.hashCode();
    assertEquals(expectedHashCodeResult, freeRoomResponseDto1.hashCode());
  }

  @Test
  public void testEquals4() {
    FreeRoomResponseDto freeRoomResponseDto = new FreeRoomResponseDto(123, new ArrayList<RoomPlanDto>());
    assertNotEquals(new FreeRoomResponseDto(), freeRoomResponseDto);
  }

  @Test
  public void testEquals5() {
    FreeRoomResponseDto freeRoomResponseDto = new FreeRoomResponseDto();
    assertNotEquals(new FreeRoomResponseDto(123, new ArrayList<RoomPlanDto>()), freeRoomResponseDto);
  }

  @Test
  public void testEquals6() {
    FreeRoomResponseDto freeRoomResponseDto = new FreeRoomResponseDto(null, new ArrayList<RoomPlanDto>());
    assertNotEquals(new FreeRoomResponseDto(), freeRoomResponseDto);
  }

  @Test
  public void testEquals7() {
    FreeRoomResponseDto freeRoomResponseDto = new FreeRoomResponseDto(123, new ArrayList<RoomPlanDto>());
    FreeRoomResponseDto freeRoomResponseDto1 = new FreeRoomResponseDto(123, new ArrayList<RoomPlanDto>());

    assertEquals(freeRoomResponseDto1, freeRoomResponseDto);
    int expectedHashCodeResult = freeRoomResponseDto.hashCode();
    assertEquals(expectedHashCodeResult, freeRoomResponseDto1.hashCode());
  }

  @Test
  public void testEquals8() {
    FreeRoomResponseDto freeRoomResponseDto = new FreeRoomResponseDto();
    assertNotEquals(new FreeRoomResponseDto(null, new ArrayList<RoomPlanDto>()), freeRoomResponseDto);
  }

  @Test
  public void testEquals9() {
    FreeRoomResponseDto freeRoomResponseDto = new FreeRoomResponseDto();
    freeRoomResponseDto.setAbbreviation("Abbreviation");
    assertNotEquals(new FreeRoomResponseDto(), freeRoomResponseDto);
  }
}

