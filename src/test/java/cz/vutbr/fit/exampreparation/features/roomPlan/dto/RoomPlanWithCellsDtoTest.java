package cz.vutbr.fit.exampreparation.features.roomPlan.dto;

import cz.vutbr.fit.exampreparation.features.cell.dto.CellDto;
import cz.vutbr.fit.exampreparation.features.roomPlan.SpacingBetweenStudents;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class RoomPlanWithCellsDtoTest {
  @Test
  public void testConstructor() {
    ArrayList<List<CellDto>> listList = new ArrayList<List<CellDto>>();
    RoomPlanWithCellsDto actualRoomPlanWithCellsDto = new RoomPlanWithCellsDto(123, 3, SpacingBetweenStudents.ZERO,
      listList);

    assertEquals(3, actualRoomPlanWithCellsDto.getCapacity().intValue());
    assertEquals("RoomPlanWithCellsDto(cells=[])", actualRoomPlanWithCellsDto.toString());
    assertEquals(SpacingBetweenStudents.ZERO, actualRoomPlanWithCellsDto.getSpacing());
    assertEquals(123, actualRoomPlanWithCellsDto.getRoomPlanId().intValue());
    List<List<CellDto>> cells = actualRoomPlanWithCellsDto.getCells();
    assertSame(listList, cells);
    assertTrue(cells.isEmpty());
    assertSame(cells, listList);
  }
}

