package cz.vutbr.fit.exampreparation.features.term.dto;

import cz.vutbr.fit.exampreparation.features.testSchedule.dto.TestScheduleDetailDto;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class SeatingPlanDtoTest {
  @Test
  public void testCanEqual() {
    assertFalse((new SeatingPlanDto()).canEqual("Other"));
  }

  @Test
  public void testCanEqual2() {
    SeatingPlanDto seatingPlanDto = new SeatingPlanDto();
    assertTrue(seatingPlanDto.canEqual(new SeatingPlanDto()));
  }

  @Test
  public void testConstructor() {
    SeatingPlanDto actualSeatingPlanDto = new SeatingPlanDto();
    actualSeatingPlanDto.setHighlight(true);
    actualSeatingPlanDto.setRoomAbbreviation("Room Abbreviation");
    ArrayList<List<SeatingPlanCell>> listList = new ArrayList<List<SeatingPlanCell>>();
    actualSeatingPlanDto.setSeatingPlan(listList);
    ArrayList<TestScheduleDetailDto> testScheduleDetailDtoList = new ArrayList<TestScheduleDetailDto>();
    actualSeatingPlanDto.setTestSchedules(testScheduleDetailDtoList);
    assertEquals("Room Abbreviation", actualSeatingPlanDto.getRoomAbbreviation());
    List<TestScheduleDetailDto> testSchedules = actualSeatingPlanDto.getTestSchedules();
    List<List<SeatingPlanCell>> seatingPlan = actualSeatingPlanDto.getSeatingPlan();
    assertEquals(testSchedules, seatingPlan);
    assertSame(listList, seatingPlan);
    assertSame(testScheduleDetailDtoList, testSchedules);
    assertEquals(listList, testSchedules);
    assertTrue(actualSeatingPlanDto.isHighlight());
    assertEquals("SeatingPlanDto(highlight=true, roomAbbreviation=Room Abbreviation, testSchedules=[], seatingPlan=[])",
      actualSeatingPlanDto.toString());
  }

  @Test
  public void testConstructor2() {
    ArrayList<TestScheduleDetailDto> testScheduleDetailDtoList = new ArrayList<TestScheduleDetailDto>();
    ArrayList<List<SeatingPlanCell>> listList = new ArrayList<List<SeatingPlanCell>>();
    SeatingPlanDto actualSeatingPlanDto = new SeatingPlanDto(true, "Room Abbreviation", testScheduleDetailDtoList,
      listList);
    actualSeatingPlanDto.setHighlight(true);
    actualSeatingPlanDto.setRoomAbbreviation("Room Abbreviation");
    ArrayList<List<SeatingPlanCell>> listList1 = new ArrayList<List<SeatingPlanCell>>();
    actualSeatingPlanDto.setSeatingPlan(listList1);
    ArrayList<TestScheduleDetailDto> testScheduleDetailDtoList1 = new ArrayList<TestScheduleDetailDto>();
    actualSeatingPlanDto.setTestSchedules(testScheduleDetailDtoList1);
    assertEquals("Room Abbreviation", actualSeatingPlanDto.getRoomAbbreviation());
    List<List<SeatingPlanCell>> seatingPlan = actualSeatingPlanDto.getSeatingPlan();
    assertEquals(listList, seatingPlan);
    assertSame(listList1, seatingPlan);
    List<TestScheduleDetailDto> testSchedules = actualSeatingPlanDto.getTestSchedules();
    assertEquals(testSchedules, seatingPlan);
    assertEquals(testScheduleDetailDtoList, seatingPlan);
    assertEquals(testScheduleDetailDtoList, testSchedules);
    assertEquals(listList, testSchedules);
    assertSame(testScheduleDetailDtoList1, testSchedules);
    assertEquals(listList1, testSchedules);
    assertTrue(actualSeatingPlanDto.isHighlight());
    assertEquals("SeatingPlanDto(highlight=true, roomAbbreviation=Room Abbreviation, testSchedules=[], seatingPlan=[])",
      actualSeatingPlanDto.toString());
  }

  @Test
  public void testEquals() {
    assertNotEquals((new SeatingPlanDto()), null);
    assertNotEquals((new SeatingPlanDto()), "Different type to SeatingPlanDto");
  }

  @Test
  public void testEquals10() {
    ArrayList<TestScheduleDetailDto> testSchedules = new ArrayList<TestScheduleDetailDto>();

    SeatingPlanDto seatingPlanDto = new SeatingPlanDto(true, "Room Abbreviation", testSchedules,
      new ArrayList<List<SeatingPlanCell>>());
    seatingPlanDto.setTestSchedules(null);
    ArrayList<TestScheduleDetailDto> testSchedules1 = new ArrayList<TestScheduleDetailDto>();
    assertNotEquals(new SeatingPlanDto(true, "Room Abbreviation", testSchedules1, new ArrayList<List<SeatingPlanCell>>()), seatingPlanDto);
  }

  @Test
  public void testEquals11() {
    ArrayList<TestScheduleDetailDto> testSchedules = new ArrayList<TestScheduleDetailDto>();

    SeatingPlanDto seatingPlanDto = new SeatingPlanDto(true, "Room Abbreviation", testSchedules,
      new ArrayList<List<SeatingPlanCell>>());
    seatingPlanDto.setSeatingPlan(null);
    ArrayList<TestScheduleDetailDto> testSchedules1 = new ArrayList<TestScheduleDetailDto>();
    assertNotEquals(new SeatingPlanDto(true, "Room Abbreviation", testSchedules1, new ArrayList<List<SeatingPlanCell>>()), seatingPlanDto);
  }

  @Test
  public void testEquals2() {
    SeatingPlanDto seatingPlanDto = new SeatingPlanDto();
    assertEquals(seatingPlanDto, seatingPlanDto);
    int expectedHashCodeResult = seatingPlanDto.hashCode();
    assertEquals(expectedHashCodeResult, seatingPlanDto.hashCode());
  }

  @Test
  public void testEquals3() {
    SeatingPlanDto seatingPlanDto = new SeatingPlanDto();
    SeatingPlanDto seatingPlanDto1 = new SeatingPlanDto();
    assertEquals(seatingPlanDto1, seatingPlanDto);
    int expectedHashCodeResult = seatingPlanDto.hashCode();
    assertEquals(expectedHashCodeResult, seatingPlanDto1.hashCode());
  }

  @Test
  public void testEquals4() {
    ArrayList<TestScheduleDetailDto> testSchedules = new ArrayList<TestScheduleDetailDto>();
    SeatingPlanDto seatingPlanDto = new SeatingPlanDto(true, "Room Abbreviation", testSchedules,
      new ArrayList<List<SeatingPlanCell>>());
    assertNotEquals(new SeatingPlanDto(), seatingPlanDto);
  }

  @Test
  public void testEquals5() {
    ArrayList<TestScheduleDetailDto> testSchedules = new ArrayList<TestScheduleDetailDto>();
    SeatingPlanDto seatingPlanDto = new SeatingPlanDto(false, "Room Abbreviation", testSchedules,
      new ArrayList<List<SeatingPlanCell>>());
    assertNotEquals(new SeatingPlanDto(), seatingPlanDto);
  }

  @Test
  public void testEquals6() {
    ArrayList<TestScheduleDetailDto> testSchedules = new ArrayList<TestScheduleDetailDto>();
    SeatingPlanDto seatingPlanDto = new SeatingPlanDto(true, "Room Abbreviation", testSchedules,
      new ArrayList<List<SeatingPlanCell>>());
    ArrayList<TestScheduleDetailDto> testSchedules1 = new ArrayList<TestScheduleDetailDto>();
    SeatingPlanDto seatingPlanDto1 = new SeatingPlanDto(true, "Room Abbreviation", testSchedules1,
      new ArrayList<List<SeatingPlanCell>>());

    assertEquals(seatingPlanDto1, seatingPlanDto);
    int expectedHashCodeResult = seatingPlanDto.hashCode();
    assertEquals(expectedHashCodeResult, seatingPlanDto1.hashCode());
  }

  @Test
  public void testEquals7() {
    ArrayList<TestScheduleDetailDto> testSchedules = new ArrayList<TestScheduleDetailDto>();
    SeatingPlanDto seatingPlanDto = new SeatingPlanDto(false, null, testSchedules,
      new ArrayList<List<SeatingPlanCell>>());
    assertNotEquals(new SeatingPlanDto(), seatingPlanDto);
  }

  @Test
  public void testEquals8() {
    ArrayList<TestScheduleDetailDto> testSchedules = new ArrayList<TestScheduleDetailDto>();
    SeatingPlanDto seatingPlanDto = new SeatingPlanDto(true, null, testSchedules,
      new ArrayList<List<SeatingPlanCell>>());
    ArrayList<TestScheduleDetailDto> testSchedules1 = new ArrayList<TestScheduleDetailDto>();
    assertFalse(seatingPlanDto
      .equals(new SeatingPlanDto(true, "Room Abbreviation", testSchedules1, new ArrayList<List<SeatingPlanCell>>())));
  }

  @Test
  public void testEquals9() {
    ArrayList<List<SeatingPlanCell>> listList = new ArrayList<List<SeatingPlanCell>>();
    listList.add(new ArrayList<SeatingPlanCell>());
    SeatingPlanDto seatingPlanDto = new SeatingPlanDto(true, "Room Abbreviation",
      new ArrayList<TestScheduleDetailDto>(), listList);
    ArrayList<TestScheduleDetailDto> testSchedules = new ArrayList<TestScheduleDetailDto>();
    assertNotEquals(new SeatingPlanDto(true, "Room Abbreviation", testSchedules, new ArrayList<List<SeatingPlanCell>>()), seatingPlanDto);
  }
}

