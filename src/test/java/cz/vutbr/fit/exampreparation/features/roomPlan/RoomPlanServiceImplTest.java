package cz.vutbr.fit.exampreparation.features.roomPlan;

import cz.vutbr.fit.exampreparation.features.appUser.AppUser;
import cz.vutbr.fit.exampreparation.features.assignment.Assignment;
import cz.vutbr.fit.exampreparation.features.cell.Cell;
import cz.vutbr.fit.exampreparation.features.role.Role;
import cz.vutbr.fit.exampreparation.features.room.Room;
import cz.vutbr.fit.exampreparation.features.term.Term;
import cz.vutbr.fit.exampreparation.features.termRun.TermRun;
import cz.vutbr.fit.exampreparation.features.token.Token;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {RoomPlanServiceImpl.class})
@ExtendWith(SpringExtension.class)
public class RoomPlanServiceImplTest {
  @MockBean
  private RoomPlanRepository roomPlanRepository;

  @Autowired
  private RoomPlanServiceImpl roomPlanServiceImpl;

  @Test
  public void testDeleteRoomPlans() {
    doNothing().when(this.roomPlanRepository).deleteInBatch((Iterable<RoomPlan>) any());
    this.roomPlanServiceImpl.deleteRoomPlans(new ArrayList<RoomPlan>());
    verify(this.roomPlanRepository).deleteInBatch((Iterable<RoomPlan>) any());
  }

  @Test
  public void testFindRoomPlanByRoomIdAndSpacing() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());

    Room room = new Room();
    room.setAppUserReference(appUser);
    room.setAbbreviation("Abbreviation");
    room.setRoomPlans(new ArrayList<RoomPlan>());
    room.setNumberOfColumns(10);
    room.setName("Name");
    room.setDescription("The characteristics of someone or something");
    room.setNumberOfRows(10);

    RoomPlan roomPlan = new RoomPlan();
    roomPlan.setCells(new ArrayList<Cell>());
    roomPlan.setCapacity(3);
    roomPlan.setTermRuns(new ArrayList<TermRun>());
    roomPlan.setSpacing(SpacingBetweenStudents.ZERO);
    roomPlan.setRoomReference(room);
    when(this.roomPlanRepository.findRoomPlanByRoomReference_IdRoomAndSpacing((Integer) any(),
      (SpacingBetweenStudents) any())).thenReturn(roomPlan);
    assertSame(roomPlan, this.roomPlanServiceImpl.findRoomPlanByRoomIdAndSpacing(123, SpacingBetweenStudents.ZERO));
    verify(this.roomPlanRepository).findRoomPlanByRoomReference_IdRoomAndSpacing((Integer) any(),
      (SpacingBetweenStudents) any());
  }

  @Test
  public void testFindRoomPlanById() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());

    Room room = new Room();
    room.setAppUserReference(appUser);
    room.setAbbreviation("Abbreviation");
    room.setRoomPlans(new ArrayList<RoomPlan>());
    room.setNumberOfColumns(10);
    room.setName("Name");
    room.setDescription("The characteristics of someone or something");
    room.setNumberOfRows(10);

    RoomPlan roomPlan = new RoomPlan();
    roomPlan.setCells(new ArrayList<Cell>());
    roomPlan.setCapacity(3);
    roomPlan.setTermRuns(new ArrayList<TermRun>());
    roomPlan.setSpacing(SpacingBetweenStudents.ZERO);
    roomPlan.setRoomReference(room);
    Optional<RoomPlan> ofResult = Optional.<RoomPlan>of(roomPlan);
    when(this.roomPlanRepository.findById((Integer) any())).thenReturn(ofResult);
    Optional<RoomPlan> actualFindRoomPlanByIdResult = this.roomPlanServiceImpl.findRoomPlanById(123);
    assertSame(ofResult, actualFindRoomPlanByIdResult);
    assertTrue(actualFindRoomPlanByIdResult.isPresent());
    verify(this.roomPlanRepository).findById((Integer) any());
  }
}

