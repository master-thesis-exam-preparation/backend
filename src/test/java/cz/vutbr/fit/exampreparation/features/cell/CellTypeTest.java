package cz.vutbr.fit.exampreparation.features.cell;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CellTypeTest {

  @Test
  public void testValueOf2() {
    assertEquals(CellType.AISLE, CellType.valueOf("AISLE"));
  }

  @Test
  public void testValues() {
    CellType[] actualValuesResult = CellType.values();
    assertEquals(5, actualValuesResult.length);
    assertEquals(CellType.SEAT, actualValuesResult[0]);
    assertEquals(CellType.AISLE, actualValuesResult[1]);
    assertEquals(CellType.DESK, actualValuesResult[2]);
    assertEquals(CellType.BLOCKED, actualValuesResult[3]);
    assertEquals(CellType.SELECTED, actualValuesResult[4]);
  }
}

