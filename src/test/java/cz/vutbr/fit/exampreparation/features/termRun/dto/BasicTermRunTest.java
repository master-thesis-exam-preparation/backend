package cz.vutbr.fit.exampreparation.features.termRun.dto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class BasicTermRunTest {
  @Test
  public void testCanEqual() {
    assertFalse((new BasicTermRun()).canEqual("Other"));
  }

  @Test
  public void testCanEqual2() {
    BasicTermRun basicTermRun = new BasicTermRun();
    assertTrue(basicTermRun.canEqual(new BasicTermRun()));
  }

  @Test
  public void testConstructor() {
    BasicTermRun actualBasicTermRun = new BasicTermRun();
    actualBasicTermRun.setEndTime(1L);
    actualBasicTermRun.setId(1);
    actualBasicTermRun.setStartTime(1L);
    assertEquals(1L, actualBasicTermRun.getEndTime().longValue());
    assertEquals(1, actualBasicTermRun.getId().intValue());
    assertEquals(1L, actualBasicTermRun.getStartTime().longValue());
    assertEquals("BasicTermRun(id=1, startTime=1, endTime=1)", actualBasicTermRun.toString());
  }

  @Test
  public void testConstructor2() {
    BasicTermRun actualBasicTermRun = new BasicTermRun(1, 1L, 1L);
    actualBasicTermRun.setEndTime(1L);
    actualBasicTermRun.setId(1);
    actualBasicTermRun.setStartTime(1L);
    assertEquals(1L, actualBasicTermRun.getEndTime().longValue());
    assertEquals(1, actualBasicTermRun.getId().intValue());
    assertEquals(1L, actualBasicTermRun.getStartTime().longValue());
    assertEquals("BasicTermRun(id=1, startTime=1, endTime=1)", actualBasicTermRun.toString());
  }

  @Test
  public void testEquals() {
    assertNotEquals((new BasicTermRun()), null);
    assertNotEquals((new BasicTermRun()), "Different type to BasicTermRun");
  }

  @Test
  public void testEquals2() {
    BasicTermRun basicTermRun = new BasicTermRun();
    assertEquals(basicTermRun, basicTermRun);
    int expectedHashCodeResult = basicTermRun.hashCode();
    assertEquals(expectedHashCodeResult, basicTermRun.hashCode());
  }

  @Test
  public void testEquals3() {
    BasicTermRun basicTermRun = new BasicTermRun();
    BasicTermRun basicTermRun1 = new BasicTermRun();
    assertEquals(basicTermRun1, basicTermRun);
    int expectedHashCodeResult = basicTermRun.hashCode();
    assertEquals(expectedHashCodeResult, basicTermRun1.hashCode());
  }

  @Test
  public void testEquals4() {
    BasicTermRun basicTermRun = new BasicTermRun(1, 1L, 1L);
    assertNotEquals(new BasicTermRun(), basicTermRun);
  }

  @Test
  public void testEquals5() {
    TermRunBasicRoomDto termRunBasicRoomDto = mock(TermRunBasicRoomDto.class);
    assertNotEquals(new BasicTermRun(), termRunBasicRoomDto);
  }

  @Test
  public void testEquals6() {
    BasicTermRun basicTermRun = new BasicTermRun();
    assertNotEquals(new BasicTermRun(1, 1L, 1L), basicTermRun);
  }

  @Test
  public void testEquals7() {
    BasicTermRun basicTermRun = new BasicTermRun();
    assertNotEquals(new TermRunBasicRoomDto(), basicTermRun);
  }

  @Test
  public void testEquals8() {
    BasicTermRun basicTermRun = new BasicTermRun();
    TermRunBasicRoomDto termRunBasicRoomDto = mock(TermRunBasicRoomDto.class);
    when(termRunBasicRoomDto.getId()).thenReturn(1);
    when(termRunBasicRoomDto.canEqual((Object) any())).thenReturn(true);
    assertNotEquals(termRunBasicRoomDto, basicTermRun);
  }

  @Test
  public void testEquals9() {
    BasicTermRun basicTermRun = new BasicTermRun(1, 0L, 1L);
    TermRunBasicRoomDto termRunBasicRoomDto = mock(TermRunBasicRoomDto.class);
    when(termRunBasicRoomDto.getEndTime()).thenReturn(1L);
    when(termRunBasicRoomDto.getStartTime()).thenReturn(1L);
    when(termRunBasicRoomDto.getId()).thenReturn(1);
    when(termRunBasicRoomDto.canEqual((Object) any())).thenReturn(true);
    assertNotEquals(termRunBasicRoomDto, basicTermRun);
  }

  @Test
  public void testEquals10() {
    BasicTermRun basicTermRun = new BasicTermRun(1, null, 1L);
    TermRunBasicRoomDto termRunBasicRoomDto = mock(TermRunBasicRoomDto.class);
    when(termRunBasicRoomDto.getEndTime()).thenReturn(1L);
    when(termRunBasicRoomDto.getStartTime()).thenReturn(1L);
    when(termRunBasicRoomDto.getId()).thenReturn(1);
    when(termRunBasicRoomDto.canEqual((Object) any())).thenReturn(true);
    assertNotEquals(termRunBasicRoomDto, basicTermRun);
  }

  @Test
  public void testEquals11() {
    BasicTermRun basicTermRun = new BasicTermRun(1, 1L, 0L);
    TermRunBasicRoomDto termRunBasicRoomDto = mock(TermRunBasicRoomDto.class);
    when(termRunBasicRoomDto.getEndTime()).thenReturn(1L);
    when(termRunBasicRoomDto.getStartTime()).thenReturn(1L);
    when(termRunBasicRoomDto.getId()).thenReturn(1);
    when(termRunBasicRoomDto.canEqual((Object) any())).thenReturn(true);
    assertNotEquals(termRunBasicRoomDto, basicTermRun);
  }

  @Test
  public void testEquals12() {
    BasicTermRun basicTermRun = new BasicTermRun(1, 1L, null);
    TermRunBasicRoomDto termRunBasicRoomDto = mock(TermRunBasicRoomDto.class);
    when(termRunBasicRoomDto.getEndTime()).thenReturn(1L);
    when(termRunBasicRoomDto.getStartTime()).thenReturn(1L);
    when(termRunBasicRoomDto.getId()).thenReturn(1);
    when(termRunBasicRoomDto.canEqual((Object) any())).thenReturn(true);
    assertNotEquals(termRunBasicRoomDto, basicTermRun);
  }
}

