package cz.vutbr.fit.exampreparation.features.appUser.dto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class EmailDtoTest {
  @Test
  public void testCanEqual() {
    assertFalse((new EmailDto("jane.doe@example.org")).canEqual("Other"));
  }

  @Test
  public void testCanEqual2() {
    EmailDto emailDto = new EmailDto("jane.doe@example.org");
    assertTrue(emailDto.canEqual(new EmailDto("jane.doe@example.org")));
  }

  @Test
  public void testConstructor() {
    EmailDto actualEmailDto = new EmailDto();
    actualEmailDto.setEmail("jane.doe@example.org");
    assertEquals("jane.doe@example.org", actualEmailDto.getEmail());
    assertEquals("EmailDto(email=jane.doe@example.org)", actualEmailDto.toString());
  }

  @Test
  public void testConstructor2() {
    EmailDto actualEmailDto = new EmailDto("jane.doe@example.org");
    actualEmailDto.setEmail("jane.doe@example.org");
    assertEquals("jane.doe@example.org", actualEmailDto.getEmail());
    assertEquals("EmailDto(email=jane.doe@example.org)", actualEmailDto.toString());
  }

  @Test
  public void testEquals() {
    assertNotEquals((new EmailDto("jane.doe@example.org")), null);
    assertNotEquals((new EmailDto("jane.doe@example.org")), "Different type to EmailDto");
  }

  @Test
  public void testEquals2() {
    EmailDto emailDto = new EmailDto("jane.doe@example.org");
    assertEquals(emailDto, emailDto);
    int expectedHashCodeResult = emailDto.hashCode();
    assertEquals(expectedHashCodeResult, emailDto.hashCode());
  }

  @Test
  public void testEquals3() {
    EmailDto emailDto = new EmailDto("jane.doe@example.org");
    EmailDto emailDto1 = new EmailDto("jane.doe@example.org");
    assertEquals(emailDto1, emailDto);
    int expectedHashCodeResult = emailDto.hashCode();
    assertEquals(expectedHashCodeResult, emailDto1.hashCode());
  }

  @Test
  public void testEquals4() {
    EmailDto emailDto = new EmailDto(null);
    assertNotEquals(new EmailDto("jane.doe@example.org"), emailDto);
  }

  @Test
  public void testEquals5() {
    EmailDto emailDto = new EmailDto("Email");
    assertNotEquals(new EmailDto("jane.doe@example.org"), emailDto);
  }

  @Test
  public void testEquals6() {
    EmailPasswordDto emailPasswordDto = mock(EmailPasswordDto.class);
    assertNotEquals(new EmailDto("jane.doe@example.org"), emailPasswordDto);
  }

  @Test
  public void testEquals7() {
    EmailDto emailDto = new EmailDto("jane.doe@example.org");
    assertNotEquals(new EmailPasswordDto("iloveyou"), emailDto);
  }

  @Test
  public void testEquals8() {
    EmailDto emailDto = new EmailDto("jane.doe@example.org");
    EmailPasswordDto emailPasswordDto = mock(EmailPasswordDto.class);
    when(emailPasswordDto.getEmail()).thenReturn("foo");
    when(emailPasswordDto.canEqual((Object) any())).thenReturn(true);
    assertNotEquals(emailPasswordDto, emailDto);
  }
}

