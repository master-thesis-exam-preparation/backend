package cz.vutbr.fit.exampreparation.features.room.dto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class FreeRoomRequestDtoTest {
  @Test
  public void testCanEqual() {
    assertFalse((new FreeRoomRequestDto()).canEqual("Other"));
  }

  @Test
  public void testCanEqual2() {
    FreeRoomRequestDto freeRoomRequestDto = new FreeRoomRequestDto();
    assertTrue(freeRoomRequestDto.canEqual(new FreeRoomRequestDto()));
  }

  @Test
  public void testConstructor() {
    FreeRoomRequestDto actualFreeRoomRequestDto = new FreeRoomRequestDto();
    actualFreeRoomRequestDto.setNumberOfTermRuns(10);
    actualFreeRoomRequestDto.setStartTime(1L);
    actualFreeRoomRequestDto.setTermId(123);
    actualFreeRoomRequestDto.setTermRunLength(3);
    assertEquals(10, actualFreeRoomRequestDto.getNumberOfTermRuns().intValue());
    assertEquals(1L, actualFreeRoomRequestDto.getStartTime().longValue());
    assertEquals(123, actualFreeRoomRequestDto.getTermId().intValue());
    assertEquals(3, actualFreeRoomRequestDto.getTermRunLength().intValue());
    assertEquals("FreeRoomRequestDto(startTime=1, numberOfTermRuns=10, termRunLength=3, termId=123)",
      actualFreeRoomRequestDto.toString());
  }

  @Test
  public void testConstructor2() {
    FreeRoomRequestDto actualFreeRoomRequestDto = new FreeRoomRequestDto(1L, 10, 3, 123);
    actualFreeRoomRequestDto.setNumberOfTermRuns(10);
    actualFreeRoomRequestDto.setStartTime(1L);
    actualFreeRoomRequestDto.setTermId(123);
    actualFreeRoomRequestDto.setTermRunLength(3);
    assertEquals(10, actualFreeRoomRequestDto.getNumberOfTermRuns().intValue());
    assertEquals(1L, actualFreeRoomRequestDto.getStartTime().longValue());
    assertEquals(123, actualFreeRoomRequestDto.getTermId().intValue());
    assertEquals(3, actualFreeRoomRequestDto.getTermRunLength().intValue());
    assertEquals("FreeRoomRequestDto(startTime=1, numberOfTermRuns=10, termRunLength=3, termId=123)",
      actualFreeRoomRequestDto.toString());
  }

  @Test
  public void testEquals() {
    assertNotEquals((new FreeRoomRequestDto()), null);
    assertNotEquals((new FreeRoomRequestDto()), "Different type to FreeRoomRequestDto");
  }

  @Test
  public void testEquals10() {
    FreeRoomRequestDto freeRoomRequestDto = new FreeRoomRequestDto();

    FreeRoomRequestDto freeRoomRequestDto1 = new FreeRoomRequestDto();
    freeRoomRequestDto1.setTermRunLength(3);
    assertNotEquals(freeRoomRequestDto1, freeRoomRequestDto);
  }

  @Test
  public void testEquals11() {
    FreeRoomRequestDto freeRoomRequestDto = new FreeRoomRequestDto();

    FreeRoomRequestDto freeRoomRequestDto1 = new FreeRoomRequestDto();
    freeRoomRequestDto1.setTermId(123);
    assertNotEquals(freeRoomRequestDto1, freeRoomRequestDto);
  }

  @Test
  public void testEquals12() {
    FreeRoomRequestDto freeRoomRequestDto = new FreeRoomRequestDto();

    FreeRoomRequestDto freeRoomRequestDto1 = new FreeRoomRequestDto();
    freeRoomRequestDto1.setNumberOfTermRuns(10);
    assertNotEquals(freeRoomRequestDto1, freeRoomRequestDto);
  }

  @Test
  public void testEquals2() {
    FreeRoomRequestDto freeRoomRequestDto = new FreeRoomRequestDto();
    assertEquals(freeRoomRequestDto, freeRoomRequestDto);
    int expectedHashCodeResult = freeRoomRequestDto.hashCode();
    assertEquals(expectedHashCodeResult, freeRoomRequestDto.hashCode());
  }

  @Test
  public void testEquals3() {
    FreeRoomRequestDto freeRoomRequestDto = new FreeRoomRequestDto();
    FreeRoomRequestDto freeRoomRequestDto1 = new FreeRoomRequestDto();
    assertEquals(freeRoomRequestDto1, freeRoomRequestDto);
    int expectedHashCodeResult = freeRoomRequestDto.hashCode();
    assertEquals(expectedHashCodeResult, freeRoomRequestDto1.hashCode());
  }

  @Test
  public void testEquals4() {
    FreeRoomRequestDto freeRoomRequestDto = new FreeRoomRequestDto(1L, 10, 3, 123);
    assertNotEquals(new FreeRoomRequestDto(), freeRoomRequestDto);
  }

  @Test
  public void testEquals5() {
    FreeRoomRequestDto freeRoomRequestDto = new FreeRoomRequestDto();
    assertNotEquals(new FreeRoomRequestDto(1L, 10, 3, 123), freeRoomRequestDto);
  }

  @Test
  public void testEquals6() {
    FreeRoomRequestDto freeRoomRequestDto = new FreeRoomRequestDto();
    freeRoomRequestDto.setTermRunLength(3);
    assertNotEquals(new FreeRoomRequestDto(), freeRoomRequestDto);
  }

  @Test
  public void testEquals7() {
    FreeRoomRequestDto freeRoomRequestDto = new FreeRoomRequestDto();
    freeRoomRequestDto.setTermId(123);
    assertNotEquals(new FreeRoomRequestDto(), freeRoomRequestDto);
  }

  @Test
  public void testEquals8() {
    FreeRoomRequestDto freeRoomRequestDto = new FreeRoomRequestDto();
    freeRoomRequestDto.setNumberOfTermRuns(10);
    assertNotEquals(new FreeRoomRequestDto(), freeRoomRequestDto);
  }

  @Test
  public void testEquals9() {
    FreeRoomRequestDto freeRoomRequestDto = new FreeRoomRequestDto(1L, 10, 3, 123);
    FreeRoomRequestDto freeRoomRequestDto1 = new FreeRoomRequestDto(1L, 10, 3, 123);

    assertEquals(freeRoomRequestDto1, freeRoomRequestDto);
    int expectedHashCodeResult = freeRoomRequestDto.hashCode();
    assertEquals(expectedHashCodeResult, freeRoomRequestDto1.hashCode());
  }
}

