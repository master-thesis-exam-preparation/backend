package cz.vutbr.fit.exampreparation.features.term.dto;

import cz.vutbr.fit.exampreparation.features.termRun.dto.TermRunBasicRoomDto;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class AllTermsInfoResponseDtoTest {
  @Test
  public void testConstructor() {
    ArrayList<Long> resultLongList = new ArrayList<Long>();
    ArrayList<String> stringList = new ArrayList<String>();
    ArrayList<TermRunBasicRoomDto> termRunBasicRoomDtoList = new ArrayList<TermRunBasicRoomDto>();
    AllTermsInfoResponseDto actualAllTermsInfoResponseDto = new AllTermsInfoResponseDto("Course Abbreviation",
      "Term Name", 1, resultLongList, 123, stringList, termRunBasicRoomDtoList);

    assertEquals(1, actualAllTermsInfoResponseDto.getAcademicYear().intValue());
    assertEquals("AllTermsInfoResponseDto(termId=123, managersEmails=[], termRuns=[])",
      actualAllTermsInfoResponseDto.toString());
    List<TermRunBasicRoomDto> termRuns = actualAllTermsInfoResponseDto.getTermRuns();
    assertSame(termRunBasicRoomDtoList, termRuns);
    assertEquals(stringList, termRuns);
    assertEquals(resultLongList, termRuns);
    assertTrue(termRuns.isEmpty());
    assertEquals("Term Name", actualAllTermsInfoResponseDto.getTermName());
    assertEquals(123, actualAllTermsInfoResponseDto.getTermId().intValue());
    List<Long> termDates = actualAllTermsInfoResponseDto.getTermDates();
    assertSame(resultLongList, termDates);
    assertEquals(stringList, termDates);
    assertEquals(termRuns, termDates);
    assertTrue(termDates.isEmpty());
    List<String> managersEmails = actualAllTermsInfoResponseDto.getManagersEmails();
    assertSame(stringList, managersEmails);
    assertEquals(termDates, managersEmails);
    assertEquals(termRuns, managersEmails);
    assertTrue(managersEmails.isEmpty());
    assertEquals("Course Abbreviation", actualAllTermsInfoResponseDto.getCourseAbbreviation());
    assertSame(termDates, resultLongList);
    assertSame(managersEmails, stringList);
    assertSame(termRuns, termRunBasicRoomDtoList);
  }
}

