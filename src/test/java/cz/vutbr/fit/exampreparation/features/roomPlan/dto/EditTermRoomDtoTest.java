package cz.vutbr.fit.exampreparation.features.roomPlan.dto;

import cz.vutbr.fit.exampreparation.features.term.dto.SeatingPlanCell;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class EditTermRoomDtoTest {
  @Test
  public void testCanEqual() {
    ArrayList<Integer> freeSeatIds = new ArrayList<Integer>();
    ArrayList<Integer> freeSeatsAtTheEndOfRoom = new ArrayList<Integer>();
    assertFalse(
      (new EditTermRoomDto(123, 3, freeSeatIds, freeSeatsAtTheEndOfRoom, new ArrayList<List<SeatingPlanCell>>()))
        .canEqual("Other"));
  }

  @Test
  public void testCanEqual2() {
    ArrayList<Integer> freeSeatIds = new ArrayList<Integer>();
    ArrayList<Integer> freeSeatsAtTheEndOfRoom = new ArrayList<Integer>();
    EditTermRoomDto editTermRoomDto = new EditTermRoomDto(123, 3, freeSeatIds, freeSeatsAtTheEndOfRoom,
      new ArrayList<List<SeatingPlanCell>>());
    ArrayList<Integer> freeSeatIds1 = new ArrayList<Integer>();
    ArrayList<Integer> freeSeatsAtTheEndOfRoom1 = new ArrayList<Integer>();
    assertTrue(editTermRoomDto.canEqual(
      new EditTermRoomDto(123, 3, freeSeatIds1, freeSeatsAtTheEndOfRoom1, new ArrayList<List<SeatingPlanCell>>())));
  }

  @Test
  public void testConstructor() {
    ArrayList<Integer> integerList = new ArrayList<Integer>();
    ArrayList<Integer> integerList1 = new ArrayList<Integer>();
    ArrayList<List<SeatingPlanCell>> listList = new ArrayList<List<SeatingPlanCell>>();
    EditTermRoomDto actualEditTermRoomDto = new EditTermRoomDto(123, 3, integerList, integerList1, listList);
    actualEditTermRoomDto.setCapacity(3);
    ArrayList<Integer> integerList2 = new ArrayList<Integer>();
    actualEditTermRoomDto.setFreeSeatIds(integerList2);
    ArrayList<Integer> integerList3 = new ArrayList<Integer>();
    actualEditTermRoomDto.setFreeSeatsAtTheEndOfRoom(integerList3);
    actualEditTermRoomDto.setRoomId(123);
    ArrayList<List<SeatingPlanCell>> listList1 = new ArrayList<List<SeatingPlanCell>>();
    actualEditTermRoomDto.setSeatingPlan(listList1);
    assertEquals(3, actualEditTermRoomDto.getCapacity());
    List<Integer> freeSeatsAtTheEndOfRoom = actualEditTermRoomDto.getFreeSeatsAtTheEndOfRoom();
    List<Integer> freeSeatIds = actualEditTermRoomDto.getFreeSeatIds();
    assertEquals(freeSeatsAtTheEndOfRoom, freeSeatIds);
    List<List<SeatingPlanCell>> seatingPlan = actualEditTermRoomDto.getSeatingPlan();
    assertEquals(seatingPlan, freeSeatIds);
    assertEquals(integerList1, freeSeatIds);
    assertEquals(listList, freeSeatIds);
    assertSame(integerList2, freeSeatIds);
    assertEquals(integerList, freeSeatIds);
    assertEquals(integerList2, freeSeatsAtTheEndOfRoom);
    assertEquals(integerList, freeSeatsAtTheEndOfRoom);
    assertEquals(listList, freeSeatsAtTheEndOfRoom);
    assertEquals(seatingPlan, freeSeatsAtTheEndOfRoom);
    assertSame(integerList3, freeSeatsAtTheEndOfRoom);
    assertEquals(integerList1, freeSeatsAtTheEndOfRoom);
    assertEquals(123, actualEditTermRoomDto.getRoomId());
    assertEquals(listList, seatingPlan);
    assertSame(listList1, seatingPlan);
    assertEquals(integerList3, seatingPlan);
    assertEquals(integerList2, seatingPlan);
    assertEquals(integerList, seatingPlan);
    assertEquals(integerList1, seatingPlan);
    assertEquals("EditTermRoomDto(roomId=123, capacity=3, freeSeatIds=[], freeSeatsAtTheEndOfRoom=[], seatingPlan=[])",
      actualEditTermRoomDto.toString());
  }

  @Test
  public void testEquals() {
    ArrayList<Integer> freeSeatIds = new ArrayList<Integer>();
    ArrayList<Integer> freeSeatsAtTheEndOfRoom = new ArrayList<Integer>();
    assertNotEquals((new EditTermRoomDto(123, 3, freeSeatIds, freeSeatsAtTheEndOfRoom, new ArrayList<List<SeatingPlanCell>>())), null);
  }

  @Test
  public void testEquals10() {
    ArrayList<Integer> freeSeatIds = new ArrayList<Integer>();
    ArrayList<Integer> freeSeatsAtTheEndOfRoom = new ArrayList<Integer>();

    EditTermRoomDto editTermRoomDto = new EditTermRoomDto(123, 3, freeSeatIds, freeSeatsAtTheEndOfRoom,
      new ArrayList<List<SeatingPlanCell>>());
    editTermRoomDto.setFreeSeatIds(null);
    ArrayList<Integer> freeSeatIds1 = new ArrayList<Integer>();
    ArrayList<Integer> freeSeatsAtTheEndOfRoom1 = new ArrayList<Integer>();
    assertNotEquals(new EditTermRoomDto(123, 3, freeSeatIds1, freeSeatsAtTheEndOfRoom1, new ArrayList<List<SeatingPlanCell>>()), editTermRoomDto);
  }

  @Test
  public void testEquals11() {
    ArrayList<Integer> freeSeatIds = new ArrayList<Integer>();
    ArrayList<Integer> freeSeatsAtTheEndOfRoom = new ArrayList<Integer>();

    EditTermRoomDto editTermRoomDto = new EditTermRoomDto(123, 3, freeSeatIds, freeSeatsAtTheEndOfRoom,
      new ArrayList<List<SeatingPlanCell>>());
    editTermRoomDto.setFreeSeatsAtTheEndOfRoom(null);
    ArrayList<Integer> freeSeatIds1 = new ArrayList<Integer>();
    ArrayList<Integer> freeSeatsAtTheEndOfRoom1 = new ArrayList<Integer>();
    assertNotEquals(new EditTermRoomDto(123, 3, freeSeatIds1, freeSeatsAtTheEndOfRoom1, new ArrayList<List<SeatingPlanCell>>()), editTermRoomDto);
  }

  @Test
  public void testEquals12() {
    ArrayList<Integer> freeSeatIds = new ArrayList<Integer>();
    ArrayList<Integer> freeSeatsAtTheEndOfRoom = new ArrayList<Integer>();

    EditTermRoomDto editTermRoomDto = new EditTermRoomDto(123, 3, freeSeatIds, freeSeatsAtTheEndOfRoom,
      new ArrayList<List<SeatingPlanCell>>());
    editTermRoomDto.setSeatingPlan(null);
    ArrayList<Integer> freeSeatIds1 = new ArrayList<Integer>();
    ArrayList<Integer> freeSeatsAtTheEndOfRoom1 = new ArrayList<Integer>();
    assertNotEquals(new EditTermRoomDto(123, 3, freeSeatIds1, freeSeatsAtTheEndOfRoom1, new ArrayList<List<SeatingPlanCell>>()), editTermRoomDto);
  }

  @Test
  public void testEquals13() {
    ArrayList<Integer> freeSeatIds = new ArrayList<Integer>();
    ArrayList<Integer> freeSeatsAtTheEndOfRoom = new ArrayList<Integer>();

    EditTermRoomDto editTermRoomDto = new EditTermRoomDto(123, 3, freeSeatIds, freeSeatsAtTheEndOfRoom,
      new ArrayList<List<SeatingPlanCell>>());
    editTermRoomDto.setFreeSeatIds(null);
    ArrayList<Integer> freeSeatIds1 = new ArrayList<Integer>();
    ArrayList<Integer> freeSeatsAtTheEndOfRoom1 = new ArrayList<Integer>();

    EditTermRoomDto editTermRoomDto1 = new EditTermRoomDto(123, 3, freeSeatIds1, freeSeatsAtTheEndOfRoom1,
      new ArrayList<List<SeatingPlanCell>>());
    editTermRoomDto1.setFreeSeatIds(null);
    assertEquals(editTermRoomDto1, editTermRoomDto);
    int expectedHashCodeResult = editTermRoomDto.hashCode();
    assertEquals(expectedHashCodeResult, editTermRoomDto1.hashCode());
  }

  @Test
  public void testEquals14() {
    ArrayList<Integer> freeSeatIds = new ArrayList<Integer>();
    ArrayList<Integer> freeSeatsAtTheEndOfRoom = new ArrayList<Integer>();

    EditTermRoomDto editTermRoomDto = new EditTermRoomDto(123, 3, freeSeatIds, freeSeatsAtTheEndOfRoom,
      new ArrayList<List<SeatingPlanCell>>());
    editTermRoomDto.setFreeSeatsAtTheEndOfRoom(null);
    ArrayList<Integer> freeSeatIds1 = new ArrayList<Integer>();
    ArrayList<Integer> freeSeatsAtTheEndOfRoom1 = new ArrayList<Integer>();

    EditTermRoomDto editTermRoomDto1 = new EditTermRoomDto(123, 3, freeSeatIds1, freeSeatsAtTheEndOfRoom1,
      new ArrayList<List<SeatingPlanCell>>());
    editTermRoomDto1.setFreeSeatsAtTheEndOfRoom(null);
    assertEquals(editTermRoomDto1, editTermRoomDto);
    int expectedHashCodeResult = editTermRoomDto.hashCode();
    assertEquals(expectedHashCodeResult, editTermRoomDto1.hashCode());
  }

  @Test
  public void testEquals15() {
    ArrayList<Integer> freeSeatIds = new ArrayList<Integer>();
    ArrayList<Integer> freeSeatsAtTheEndOfRoom = new ArrayList<Integer>();

    EditTermRoomDto editTermRoomDto = new EditTermRoomDto(123, 3, freeSeatIds, freeSeatsAtTheEndOfRoom,
      new ArrayList<List<SeatingPlanCell>>());
    editTermRoomDto.setSeatingPlan(null);
    ArrayList<Integer> freeSeatIds1 = new ArrayList<Integer>();
    ArrayList<Integer> freeSeatsAtTheEndOfRoom1 = new ArrayList<Integer>();

    EditTermRoomDto editTermRoomDto1 = new EditTermRoomDto(123, 3, freeSeatIds1, freeSeatsAtTheEndOfRoom1,
      new ArrayList<List<SeatingPlanCell>>());
    editTermRoomDto1.setSeatingPlan(null);
    assertEquals(editTermRoomDto1, editTermRoomDto);
    int expectedHashCodeResult = editTermRoomDto.hashCode();
    assertEquals(expectedHashCodeResult, editTermRoomDto1.hashCode());
  }

  @Test
  public void testEquals2() {
    ArrayList<Integer> freeSeatIds = new ArrayList<Integer>();
    ArrayList<Integer> freeSeatsAtTheEndOfRoom = new ArrayList<Integer>();
    assertNotEquals((new EditTermRoomDto(123, 3, freeSeatIds, freeSeatsAtTheEndOfRoom, new ArrayList<List<SeatingPlanCell>>())), "Different type to EditTermRoomDto");
  }

  @Test
  public void testEquals3() {
    ArrayList<Integer> freeSeatIds = new ArrayList<Integer>();
    ArrayList<Integer> freeSeatsAtTheEndOfRoom = new ArrayList<Integer>();
    EditTermRoomDto editTermRoomDto = new EditTermRoomDto(123, 3, freeSeatIds, freeSeatsAtTheEndOfRoom,
      new ArrayList<List<SeatingPlanCell>>());
    assertEquals(editTermRoomDto, editTermRoomDto);
    int expectedHashCodeResult = editTermRoomDto.hashCode();
    assertEquals(expectedHashCodeResult, editTermRoomDto.hashCode());
  }

  @Test
  public void testEquals4() {
    ArrayList<Integer> freeSeatIds = new ArrayList<Integer>();
    ArrayList<Integer> freeSeatsAtTheEndOfRoom = new ArrayList<Integer>();
    EditTermRoomDto editTermRoomDto = new EditTermRoomDto(123, 3, freeSeatIds, freeSeatsAtTheEndOfRoom,
      new ArrayList<List<SeatingPlanCell>>());
    ArrayList<Integer> freeSeatIds1 = new ArrayList<Integer>();
    ArrayList<Integer> freeSeatsAtTheEndOfRoom1 = new ArrayList<Integer>();
    EditTermRoomDto editTermRoomDto1 = new EditTermRoomDto(123, 3, freeSeatIds1, freeSeatsAtTheEndOfRoom1,
      new ArrayList<List<SeatingPlanCell>>());

    assertEquals(editTermRoomDto1, editTermRoomDto);
    int expectedHashCodeResult = editTermRoomDto.hashCode();
    assertEquals(expectedHashCodeResult, editTermRoomDto1.hashCode());
  }

  @Test
  public void testEquals5() {
    ArrayList<Integer> freeSeatIds = new ArrayList<Integer>();
    ArrayList<Integer> freeSeatsAtTheEndOfRoom = new ArrayList<Integer>();
    EditTermRoomDto editTermRoomDto = new EditTermRoomDto(0, 3, freeSeatIds, freeSeatsAtTheEndOfRoom,
      new ArrayList<List<SeatingPlanCell>>());
    ArrayList<Integer> freeSeatIds1 = new ArrayList<Integer>();
    ArrayList<Integer> freeSeatsAtTheEndOfRoom1 = new ArrayList<Integer>();
    assertNotEquals(new EditTermRoomDto(123, 3, freeSeatIds1, freeSeatsAtTheEndOfRoom1, new ArrayList<List<SeatingPlanCell>>()), editTermRoomDto);
  }

  @Test
  public void testEquals6() {
    ArrayList<Integer> freeSeatIds = new ArrayList<Integer>();
    ArrayList<Integer> freeSeatsAtTheEndOfRoom = new ArrayList<Integer>();
    EditTermRoomDto editTermRoomDto = new EditTermRoomDto(123, 0, freeSeatIds, freeSeatsAtTheEndOfRoom,
      new ArrayList<List<SeatingPlanCell>>());
    ArrayList<Integer> freeSeatIds1 = new ArrayList<Integer>();
    ArrayList<Integer> freeSeatsAtTheEndOfRoom1 = new ArrayList<Integer>();
    assertNotEquals(new EditTermRoomDto(123, 3, freeSeatIds1, freeSeatsAtTheEndOfRoom1, new ArrayList<List<SeatingPlanCell>>()), editTermRoomDto);
  }

  @Test
  public void testEquals7() {
    ArrayList<Integer> integerList = new ArrayList<Integer>();
    integerList.add(2);
    ArrayList<Integer> freeSeatsAtTheEndOfRoom = new ArrayList<Integer>();
    EditTermRoomDto editTermRoomDto = new EditTermRoomDto(123, 3, integerList, freeSeatsAtTheEndOfRoom,
      new ArrayList<List<SeatingPlanCell>>());
    ArrayList<Integer> freeSeatIds = new ArrayList<Integer>();
    ArrayList<Integer> freeSeatsAtTheEndOfRoom1 = new ArrayList<Integer>();
    assertNotEquals(new EditTermRoomDto(123, 3, freeSeatIds, freeSeatsAtTheEndOfRoom1, new ArrayList<List<SeatingPlanCell>>()), editTermRoomDto);
  }

  @Test
  public void testEquals8() {
    ArrayList<Integer> integerList = new ArrayList<Integer>();
    integerList.add(2);
    ArrayList<Integer> freeSeatIds = new ArrayList<Integer>();
    EditTermRoomDto editTermRoomDto = new EditTermRoomDto(123, 3, freeSeatIds, integerList,
      new ArrayList<List<SeatingPlanCell>>());
    ArrayList<Integer> freeSeatIds1 = new ArrayList<Integer>();
    ArrayList<Integer> freeSeatsAtTheEndOfRoom = new ArrayList<Integer>();
    assertNotEquals(new EditTermRoomDto(123, 3, freeSeatIds1, freeSeatsAtTheEndOfRoom, new ArrayList<List<SeatingPlanCell>>()), editTermRoomDto);
  }

  @Test
  public void testEquals9() {
    ArrayList<List<SeatingPlanCell>> listList = new ArrayList<List<SeatingPlanCell>>();
    listList.add(new ArrayList<SeatingPlanCell>());
    ArrayList<Integer> freeSeatIds = new ArrayList<Integer>();
    EditTermRoomDto editTermRoomDto = new EditTermRoomDto(123, 3, freeSeatIds, new ArrayList<Integer>(), listList);
    ArrayList<Integer> freeSeatIds1 = new ArrayList<Integer>();
    ArrayList<Integer> freeSeatsAtTheEndOfRoom = new ArrayList<Integer>();
    assertNotEquals(new EditTermRoomDto(123, 3, freeSeatIds1, freeSeatsAtTheEndOfRoom, new ArrayList<List<SeatingPlanCell>>()), editTermRoomDto);
  }
}

