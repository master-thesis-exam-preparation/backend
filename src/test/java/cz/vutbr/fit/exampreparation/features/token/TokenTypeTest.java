package cz.vutbr.fit.exampreparation.features.token;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TokenTypeTest {

  @Test
  public void testValueOf2() {
    assertEquals(TokenType.FORGET_PASSWORD, TokenType.valueOf("FORGET_PASSWORD"));
  }

  @Test
  public void testValues() {
    TokenType[] actualValuesResult = TokenType.values();
    assertEquals(3, actualValuesResult.length);
    assertEquals(TokenType.VERIFICATION, actualValuesResult[0]);
    assertEquals(TokenType.REFRESH, actualValuesResult[1]);
    assertEquals(TokenType.FORGET_PASSWORD, actualValuesResult[2]);
  }
}

