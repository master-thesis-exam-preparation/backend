package cz.vutbr.fit.exampreparation.features.term.helpers;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class EditActionWithIdsListTest {
  @Test
  public void testCanEqual() {
    assertFalse((new EditActionWithIdsList(EditAction.ADD, new ArrayList<Integer>())).canEqual("Other"));
  }

  @Test
  public void testCanEqual2() {
    EditActionWithIdsList editActionWithIdsList = new EditActionWithIdsList(EditAction.ADD, new ArrayList<Integer>());
    assertTrue(editActionWithIdsList.canEqual(new EditActionWithIdsList(EditAction.ADD, new ArrayList<Integer>())));
  }

  @Test
  public void testConstructor() {
    ArrayList<Integer> integerList = new ArrayList<Integer>();
    EditActionWithIdsList actualEditActionWithIdsList = new EditActionWithIdsList(EditAction.ADD, integerList);
    ArrayList<Integer> integerList1 = new ArrayList<Integer>();
    actualEditActionWithIdsList.setChangedObjectIdsList(integerList1);
    actualEditActionWithIdsList.setEditAction(EditAction.ADD);
    List<Integer> changedObjectIdsList = actualEditActionWithIdsList.getChangedObjectIdsList();
    assertSame(integerList1, changedObjectIdsList);
    assertEquals(integerList, changedObjectIdsList);
    assertEquals(EditAction.ADD, actualEditActionWithIdsList.getEditAction());
    assertEquals("EditActionWithIdsList(editAction=ADD, changedObjectIdsList=[])",
      actualEditActionWithIdsList.toString());
  }

  @Test
  public void testEquals() {
    assertNotEquals((new EditActionWithIdsList(EditAction.ADD, new ArrayList<Integer>())), null);
    assertNotEquals((new EditActionWithIdsList(EditAction.ADD, new ArrayList<Integer>())), "Different type to EditActionWithIdsList");
  }

  @Test
  public void testEquals2() {
    EditActionWithIdsList editActionWithIdsList = new EditActionWithIdsList(EditAction.ADD, new ArrayList<Integer>());
    assertEquals(editActionWithIdsList, editActionWithIdsList);
    int expectedHashCodeResult = editActionWithIdsList.hashCode();
    assertEquals(expectedHashCodeResult, editActionWithIdsList.hashCode());
  }

  @Test
  public void testEquals3() {
    EditActionWithIdsList editActionWithIdsList = new EditActionWithIdsList(EditAction.ADD, new ArrayList<Integer>());
    EditActionWithIdsList editActionWithIdsList1 = new EditActionWithIdsList(EditAction.ADD, new ArrayList<Integer>());

    assertEquals(editActionWithIdsList1, editActionWithIdsList);
    int expectedHashCodeResult = editActionWithIdsList.hashCode();
    assertEquals(expectedHashCodeResult, editActionWithIdsList1.hashCode());
  }

  @Test
  public void testEquals4() {
    EditActionWithIdsList editActionWithIdsList = new EditActionWithIdsList(null, new ArrayList<Integer>());
    assertNotEquals(new EditActionWithIdsList(EditAction.ADD, new ArrayList<Integer>()), editActionWithIdsList);
  }

  @Test
  public void testEquals5() {
    EditActionWithIdsList editActionWithIdsList = new EditActionWithIdsList(EditAction.REMOVE,
      new ArrayList<Integer>());
    assertNotEquals(new EditActionWithIdsList(EditAction.ADD, new ArrayList<Integer>()), editActionWithIdsList);
  }

  @Test
  public void testEquals6() {
    ArrayList<Integer> integerList = new ArrayList<Integer>();
    integerList.add(2);
    EditActionWithIdsList editActionWithIdsList = new EditActionWithIdsList(EditAction.ADD, integerList);
    assertNotEquals(new EditActionWithIdsList(EditAction.ADD, new ArrayList<Integer>()), editActionWithIdsList);
  }

  @Test
  public void testEquals7() {
    EditActionWithIdsList editActionWithIdsList = new EditActionWithIdsList(null, new ArrayList<Integer>());
    EditActionWithIdsList editActionWithIdsList1 = new EditActionWithIdsList(null, new ArrayList<Integer>());

    assertEquals(editActionWithIdsList1, editActionWithIdsList);
    int expectedHashCodeResult = editActionWithIdsList.hashCode();
    assertEquals(expectedHashCodeResult, editActionWithIdsList1.hashCode());
  }

  @Test
  public void testEquals8() {
    EditActionWithIdsList editActionWithIdsList = new EditActionWithIdsList(EditAction.ADD, new ArrayList<Integer>());
    editActionWithIdsList.setChangedObjectIdsList(null);
    assertNotEquals(new EditActionWithIdsList(EditAction.ADD, new ArrayList<Integer>()), editActionWithIdsList);
  }

  @Test
  public void testEquals9() {
    EditActionWithIdsList editActionWithIdsList = new EditActionWithIdsList(EditAction.ADD, new ArrayList<Integer>());
    editActionWithIdsList.setChangedObjectIdsList(null);

    EditActionWithIdsList editActionWithIdsList1 = new EditActionWithIdsList(EditAction.ADD, new ArrayList<Integer>());
    editActionWithIdsList1.setChangedObjectIdsList(null);
    assertEquals(editActionWithIdsList1, editActionWithIdsList);
    int expectedHashCodeResult = editActionWithIdsList.hashCode();
    assertEquals(expectedHashCodeResult, editActionWithIdsList1.hashCode());
  }
}

