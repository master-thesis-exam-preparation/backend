package cz.vutbr.fit.exampreparation.features.assignment.dto;

import cz.vutbr.fit.exampreparation.features.assignment.AssignmentStatus;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class BasicAssignmentInfoDtoTest {
  @Test
  public void testCanEqual() {
    assertFalse((new BasicAssignmentInfoDto()).canEqual("Other"));
  }

  @Test
  public void testCanEqual2() {
    BasicAssignmentInfoDto basicAssignmentInfoDto = new BasicAssignmentInfoDto();
    assertTrue(basicAssignmentInfoDto.canEqual(new AssignmentInfoDto()));
  }

  @Test
  public void testConstructor() {
    BasicAssignmentInfoDto actualBasicAssignmentInfoDto = new BasicAssignmentInfoDto();
    actualBasicAssignmentInfoDto.setAssignmentId(123);
    actualBasicAssignmentInfoDto.setName("Name");
    actualBasicAssignmentInfoDto.setStatus(AssignmentStatus.UPLOADED);
    assertEquals(123, actualBasicAssignmentInfoDto.getAssignmentId().intValue());
    assertEquals("Name", actualBasicAssignmentInfoDto.getName());
    assertEquals(AssignmentStatus.UPLOADED, actualBasicAssignmentInfoDto.getStatus());
    assertEquals("BasicAssignmentInfoDto(assignmentId=123, name=Name, status=UPLOADED)",
      actualBasicAssignmentInfoDto.toString());
  }

  @Test
  public void testConstructor2() {
    BasicAssignmentInfoDto actualBasicAssignmentInfoDto = new BasicAssignmentInfoDto(123, "Name",
      AssignmentStatus.UPLOADED);
    actualBasicAssignmentInfoDto.setAssignmentId(123);
    actualBasicAssignmentInfoDto.setName("Name");
    actualBasicAssignmentInfoDto.setStatus(AssignmentStatus.UPLOADED);
    assertEquals(123, actualBasicAssignmentInfoDto.getAssignmentId().intValue());
    assertEquals("Name", actualBasicAssignmentInfoDto.getName());
    assertEquals(AssignmentStatus.UPLOADED, actualBasicAssignmentInfoDto.getStatus());
    assertEquals("BasicAssignmentInfoDto(assignmentId=123, name=Name, status=UPLOADED)",
      actualBasicAssignmentInfoDto.toString());
  }

  @Test
  public void testEquals() {
    assertNotEquals((new BasicAssignmentInfoDto()), null);
    assertNotEquals((new BasicAssignmentInfoDto()), "Different type to BasicAssignmentInfoDto");
  }

  @Test
  public void testEquals10() {
    BasicAssignmentInfoDto basicAssignmentInfoDto = new BasicAssignmentInfoDto();
    AssignmentInfoDto assignmentInfoDto = mock(AssignmentInfoDto.class);
    when(assignmentInfoDto.getName()).thenReturn("foo");
    when(assignmentInfoDto.getAssignmentId()).thenReturn(null);
    when(assignmentInfoDto.canEqual((Object) any())).thenReturn(true);
    assertNotEquals(assignmentInfoDto, basicAssignmentInfoDto);
  }

  @Test
  public void testEquals11() {
    BasicAssignmentInfoDto basicAssignmentInfoDto = new BasicAssignmentInfoDto();
    AssignmentInfoDto assignmentInfoDto = mock(AssignmentInfoDto.class);
    when(assignmentInfoDto.getStatus()).thenReturn(AssignmentStatus.UPLOADED);
    when(assignmentInfoDto.getName()).thenReturn(null);
    when(assignmentInfoDto.getAssignmentId()).thenReturn(null);
    when(assignmentInfoDto.canEqual((Object) any())).thenReturn(true);
    assertNotEquals(assignmentInfoDto, basicAssignmentInfoDto);
  }

  @Test
  public void testEquals2() {
    BasicAssignmentInfoDto basicAssignmentInfoDto = new BasicAssignmentInfoDto();
    assertEquals(basicAssignmentInfoDto, basicAssignmentInfoDto);
    int expectedHashCodeResult = basicAssignmentInfoDto.hashCode();
    assertEquals(expectedHashCodeResult, basicAssignmentInfoDto.hashCode());
  }

  @Test
  public void testEquals3() {
    BasicAssignmentInfoDto basicAssignmentInfoDto = new BasicAssignmentInfoDto();
    BasicAssignmentInfoDto basicAssignmentInfoDto1 = new BasicAssignmentInfoDto();
    assertEquals(basicAssignmentInfoDto1, basicAssignmentInfoDto);
    int expectedHashCodeResult = basicAssignmentInfoDto.hashCode();
    assertEquals(expectedHashCodeResult, basicAssignmentInfoDto1.hashCode());
  }

  @Test
  public void testEquals4() {
    AssignmentInfoDto assignmentInfoDto = new AssignmentInfoDto();
    assertNotEquals(new BasicAssignmentInfoDto(), assignmentInfoDto);
  }

  @Test
  public void testEquals5() {
    BasicAssignmentInfoDto basicAssignmentInfoDto = new BasicAssignmentInfoDto(123, "Name", AssignmentStatus.UPLOADED);
    assertNotEquals(new BasicAssignmentInfoDto(), basicAssignmentInfoDto);
  }

  @Test
  public void testEquals6() {
    AssignmentInfoDto assignmentInfoDto = mock(AssignmentInfoDto.class);
    assertNotEquals(new BasicAssignmentInfoDto(), assignmentInfoDto);
  }

  @Test
  public void testEquals7() {
    BasicAssignmentInfoDto basicAssignmentInfoDto = new BasicAssignmentInfoDto();
    assertNotEquals(new AssignmentInfoDto(), basicAssignmentInfoDto);
  }

  @Test
  public void testEquals8() {
    BasicAssignmentInfoDto basicAssignmentInfoDto = new BasicAssignmentInfoDto();
    assertNotEquals(new BasicAssignmentInfoDto(123, "Name", AssignmentStatus.UPLOADED), basicAssignmentInfoDto);
  }

  @Test
  public void testEquals9() {
    BasicAssignmentInfoDto basicAssignmentInfoDto = new BasicAssignmentInfoDto();
    AssignmentInfoDto assignmentInfoDto = mock(AssignmentInfoDto.class);
    when(assignmentInfoDto.getAssignmentId()).thenReturn(1);
    when(assignmentInfoDto.canEqual((Object) any())).thenReturn(true);
    assertNotEquals(assignmentInfoDto, basicAssignmentInfoDto);
  }
}

