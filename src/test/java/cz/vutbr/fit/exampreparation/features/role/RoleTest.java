package cz.vutbr.fit.exampreparation.features.role;

import cz.vutbr.fit.exampreparation.features.appUser.AppUser;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

public class RoleTest {
  @Test
  public void testConstructor() {
    Role actualRole = new Role();
    ArrayList<AppUser> appUserList = new ArrayList<AppUser>();
    actualRole.setAppUsers(appUserList);
    actualRole.setRoleDescription("Role Description");
    actualRole.setRoleName(RoleName.STUDENT);
    assertSame(appUserList, actualRole.getAppUsers());
    assertEquals("Role Description", actualRole.getRoleDescription());
    assertEquals(RoleName.STUDENT, actualRole.getRoleName());
  }

  @Test
  public void testConstructor2() {
    ArrayList<AppUser> appUserList = new ArrayList<AppUser>();
    Role actualRole = new Role(RoleName.STUDENT, "Role Description", appUserList);
    ArrayList<AppUser> appUserList1 = new ArrayList<AppUser>();
    actualRole.setAppUsers(appUserList1);
    actualRole.setRoleDescription("Role Description");
    actualRole.setRoleName(RoleName.STUDENT);
    List<AppUser> appUsers = actualRole.getAppUsers();
    assertSame(appUserList1, appUsers);
    assertEquals(appUserList, appUsers);
    assertEquals("Role Description", actualRole.getRoleDescription());
    assertEquals(RoleName.STUDENT, actualRole.getRoleName());
  }
}

