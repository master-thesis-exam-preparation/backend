package cz.vutbr.fit.exampreparation.features.course.dto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CourseInfoDtoTest {
  @Test
  public void testConstructor() {
    CourseInfoDto actualCourseInfoDto = new CourseInfoDto("Abbreviation", "Name", 1, 123);

    assertEquals("Abbreviation", actualCourseInfoDto.getAbbreviation());
    assertEquals("CourseInfoDto(courseId=123)", actualCourseInfoDto.toString());
    assertEquals("Name", actualCourseInfoDto.getName());
    assertEquals(123, actualCourseInfoDto.getCourseId().intValue());
    assertEquals(1, actualCourseInfoDto.getAcademicYear().intValue());
  }
}

