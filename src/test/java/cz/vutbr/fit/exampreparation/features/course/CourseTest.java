package cz.vutbr.fit.exampreparation.features.course;

import cz.vutbr.fit.exampreparation.features.term.Term;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class CourseTest {
  @Test
  public void testConstructor() {
    ArrayList<Term> termList = new ArrayList<Term>();
    Course actualCourse = new Course("Abbreviation", 1, "Name", termList);

    assertEquals("Abbreviation", actualCourse.getAbbreviation());
    List<Term> terms = actualCourse.getTerms();
    assertSame(termList, terms);
    assertTrue(terms.isEmpty());
    assertEquals("Name", actualCourse.getName());
    assertEquals(0, actualCourse.getIdCourse());
    assertEquals(1, actualCourse.getAcademicYear().intValue());
    assertSame(terms, termList);
  }
}

