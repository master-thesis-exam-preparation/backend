package cz.vutbr.fit.exampreparation.features.term.dto;

import cz.vutbr.fit.exampreparation.features.appUser.dto.LoginNameDto;
import cz.vutbr.fit.exampreparation.features.cell.CellType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

public class SeatingPlanCellTest {
  @Test
  public void testConstructor() {
    LoginNameDto loginNameDto = new LoginNameDto("Login", "Name And Degrees");

    SeatingPlanCell actualSeatingPlanCell = new SeatingPlanCell(123, CellType.SEAT, 10, loginNameDto);

    assertEquals(123, actualSeatingPlanCell.getCellId().intValue());
    assertEquals("SeatingPlanCell(seatNumber=10, student=LoginNameDto(login=Login, nameAndDegrees=Name And Degrees))",
      actualSeatingPlanCell.toString());
    LoginNameDto student = actualSeatingPlanCell.getStudent();
    assertSame(loginNameDto, student);
    assertEquals(10, actualSeatingPlanCell.getSeatNumber().intValue());
    assertEquals(CellType.SEAT, actualSeatingPlanCell.getCellType());
    assertEquals("Name And Degrees", student.getNameAndDegrees());
    assertEquals("Login", student.getLogin());
    assertEquals("LoginNameDto(login=Login, nameAndDegrees=Name And Degrees)", student.toString());
    assertSame(student, loginNameDto);
  }
}

