package cz.vutbr.fit.exampreparation.features.term.dto;

import cz.vutbr.fit.exampreparation.features.termRun.dto.TermRunBasicRoomDto;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class BasicTermDtoTest {
  @Test
  public void testCanEqual() {
    assertFalse((new BasicTermDto()).canEqual("Other"));
  }

  @Test
  public void testCanEqual2() {
    BasicTermDto basicTermDto = new BasicTermDto();
    assertTrue(basicTermDto.canEqual(new AllTermsInfoResponseDto()));
  }

  @Test
  public void testConstructor() {
    BasicTermDto actualBasicTermDto = new BasicTermDto();
    actualBasicTermDto.setAcademicYear(1);
    actualBasicTermDto.setCourseAbbreviation("Course Abbreviation");
    ArrayList<Long> resultLongList = new ArrayList<Long>();
    actualBasicTermDto.setTermDates(resultLongList);
    actualBasicTermDto.setTermName("Term Name");
    assertEquals(1, actualBasicTermDto.getAcademicYear().intValue());
    assertEquals("Course Abbreviation", actualBasicTermDto.getCourseAbbreviation());
    assertSame(resultLongList, actualBasicTermDto.getTermDates());
    assertEquals("Term Name", actualBasicTermDto.getTermName());
    assertEquals(
      "BasicTermDto(courseAbbreviation=Course Abbreviation, termName=Term Name, academicYear=1," + " termDates=[])",
      actualBasicTermDto.toString());
  }

  @Test
  public void testConstructor2() {
    ArrayList<Long> resultLongList = new ArrayList<Long>();
    BasicTermDto actualBasicTermDto = new BasicTermDto("Course Abbreviation", "Term Name", 1, resultLongList);
    actualBasicTermDto.setAcademicYear(1);
    actualBasicTermDto.setCourseAbbreviation("Course Abbreviation");
    ArrayList<Long> resultLongList1 = new ArrayList<Long>();
    actualBasicTermDto.setTermDates(resultLongList1);
    actualBasicTermDto.setTermName("Term Name");
    assertEquals(1, actualBasicTermDto.getAcademicYear().intValue());
    assertEquals("Course Abbreviation", actualBasicTermDto.getCourseAbbreviation());
    List<Long> termDates = actualBasicTermDto.getTermDates();
    assertEquals(resultLongList, termDates);
    assertSame(resultLongList1, termDates);
    assertEquals("Term Name", actualBasicTermDto.getTermName());
    assertEquals(
      "BasicTermDto(courseAbbreviation=Course Abbreviation, termName=Term Name, academicYear=1," + " termDates=[])",
      actualBasicTermDto.toString());
  }

  @Test
  public void testEquals() {
    assertNotEquals((new BasicTermDto()), null);
    assertNotEquals((new BasicTermDto()), "Different type to BasicTermDto");
  }

  @Test
  public void testEquals10() {
    ArrayList<Long> termDates = new ArrayList<Long>();
    ArrayList<String> managersEmails = new ArrayList<String>();
    AllTermsInfoResponseDto allTermsInfoResponseDto = new AllTermsInfoResponseDto("Course Abbreviation", "Term Name", 1,
      termDates, 123, managersEmails, new ArrayList<TermRunBasicRoomDto>());
    AllTermsInfoResponseDto allTermsInfoResponseDto1 = mock(AllTermsInfoResponseDto.class);
    when(allTermsInfoResponseDto1.getCourseAbbreviation()).thenReturn("foo");
    when(allTermsInfoResponseDto1.getAcademicYear()).thenReturn(1);
    when(allTermsInfoResponseDto1.canEqual((Object) any())).thenReturn(true);
    assertNotEquals(allTermsInfoResponseDto1, allTermsInfoResponseDto);
  }

  @Test
  public void testEquals11() {
    ArrayList<Long> termDates = new ArrayList<Long>();
    ArrayList<String> managersEmails = new ArrayList<String>();
    AllTermsInfoResponseDto allTermsInfoResponseDto = new AllTermsInfoResponseDto(null, "Term Name", 1, termDates, 123,
      managersEmails, new ArrayList<TermRunBasicRoomDto>());
    AllTermsInfoResponseDto allTermsInfoResponseDto1 = mock(AllTermsInfoResponseDto.class);
    when(allTermsInfoResponseDto1.getCourseAbbreviation()).thenReturn("foo");
    when(allTermsInfoResponseDto1.getAcademicYear()).thenReturn(1);
    when(allTermsInfoResponseDto1.canEqual((Object) any())).thenReturn(true);
    assertNotEquals(allTermsInfoResponseDto1, allTermsInfoResponseDto);
  }

  @Test
  public void testEquals12() {
    ArrayList<Long> termDates = new ArrayList<Long>();
    ArrayList<String> managersEmails = new ArrayList<String>();
    AllTermsInfoResponseDto allTermsInfoResponseDto = new AllTermsInfoResponseDto("foo", "Term Name", 1, termDates, 123,
      managersEmails, new ArrayList<TermRunBasicRoomDto>());
    AllTermsInfoResponseDto allTermsInfoResponseDto1 = mock(AllTermsInfoResponseDto.class);
    when(allTermsInfoResponseDto1.getTermName()).thenReturn("foo");
    when(allTermsInfoResponseDto1.getCourseAbbreviation()).thenReturn("foo");
    when(allTermsInfoResponseDto1.getAcademicYear()).thenReturn(1);
    when(allTermsInfoResponseDto1.canEqual((Object) any())).thenReturn(true);
    assertNotEquals(allTermsInfoResponseDto1, allTermsInfoResponseDto);
  }

  @Test
  public void testEquals13() {
    ArrayList<Long> termDates = new ArrayList<Long>();
    ArrayList<String> managersEmails = new ArrayList<String>();
    AllTermsInfoResponseDto allTermsInfoResponseDto = new AllTermsInfoResponseDto("foo", "foo", 1, termDates, 123,
      managersEmails, new ArrayList<TermRunBasicRoomDto>());
    AllTermsInfoResponseDto allTermsInfoResponseDto1 = mock(AllTermsInfoResponseDto.class);
    when(allTermsInfoResponseDto1.getTermId()).thenReturn(1);
    when(allTermsInfoResponseDto1.getTermDates()).thenReturn(new ArrayList<Long>());
    when(allTermsInfoResponseDto1.getTermName()).thenReturn("foo");
    when(allTermsInfoResponseDto1.getCourseAbbreviation()).thenReturn("foo");
    when(allTermsInfoResponseDto1.getAcademicYear()).thenReturn(1);
    when(allTermsInfoResponseDto1.canEqual((Object) any())).thenReturn(true);
    assertNotEquals(allTermsInfoResponseDto1, allTermsInfoResponseDto);
  }

  @Test
  public void testEquals14() {
    ArrayList<Long> termDates = new ArrayList<Long>();
    ArrayList<String> managersEmails = new ArrayList<String>();
    AllTermsInfoResponseDto allTermsInfoResponseDto = new AllTermsInfoResponseDto("foo", null, 1, termDates, 123,
      managersEmails, new ArrayList<TermRunBasicRoomDto>());
    AllTermsInfoResponseDto allTermsInfoResponseDto1 = mock(AllTermsInfoResponseDto.class);
    when(allTermsInfoResponseDto1.getTermId()).thenReturn(1);
    when(allTermsInfoResponseDto1.getTermDates()).thenReturn(new ArrayList<Long>());
    when(allTermsInfoResponseDto1.getTermName()).thenReturn("foo");
    when(allTermsInfoResponseDto1.getCourseAbbreviation()).thenReturn("foo");
    when(allTermsInfoResponseDto1.getAcademicYear()).thenReturn(1);
    when(allTermsInfoResponseDto1.canEqual((Object) any())).thenReturn(true);
    assertNotEquals(allTermsInfoResponseDto1, allTermsInfoResponseDto);
  }

  @Test
  public void testEquals15() {
    ArrayList<Long> resultLongList = new ArrayList<Long>();
    resultLongList.add(0L);
    ArrayList<String> managersEmails = new ArrayList<String>();
    AllTermsInfoResponseDto allTermsInfoResponseDto = new AllTermsInfoResponseDto("foo", "foo", 1, resultLongList, 123,
      managersEmails, new ArrayList<TermRunBasicRoomDto>());
    AllTermsInfoResponseDto allTermsInfoResponseDto1 = mock(AllTermsInfoResponseDto.class);
    when(allTermsInfoResponseDto1.getTermId()).thenReturn(1);
    when(allTermsInfoResponseDto1.getTermDates()).thenReturn(new ArrayList<Long>());
    when(allTermsInfoResponseDto1.getTermName()).thenReturn("foo");
    when(allTermsInfoResponseDto1.getCourseAbbreviation()).thenReturn("foo");
    when(allTermsInfoResponseDto1.getAcademicYear()).thenReturn(1);
    when(allTermsInfoResponseDto1.canEqual((Object) any())).thenReturn(true);
    assertNotEquals(allTermsInfoResponseDto1, allTermsInfoResponseDto);
  }

  @Test
  public void testEquals16() {
    ArrayList<Long> termDates = new ArrayList<Long>();
    ArrayList<String> managersEmails = new ArrayList<String>();

    AllTermsInfoResponseDto allTermsInfoResponseDto = new AllTermsInfoResponseDto("foo", "foo", 1, termDates, 123,
      managersEmails, new ArrayList<TermRunBasicRoomDto>());
    allTermsInfoResponseDto.setTermDates(null);
    AllTermsInfoResponseDto allTermsInfoResponseDto1 = mock(AllTermsInfoResponseDto.class);
    when(allTermsInfoResponseDto1.getTermId()).thenReturn(1);
    when(allTermsInfoResponseDto1.getTermDates()).thenReturn(new ArrayList<Long>());
    when(allTermsInfoResponseDto1.getTermName()).thenReturn("foo");
    when(allTermsInfoResponseDto1.getCourseAbbreviation()).thenReturn("foo");
    when(allTermsInfoResponseDto1.getAcademicYear()).thenReturn(1);
    when(allTermsInfoResponseDto1.canEqual((Object) any())).thenReturn(true);
    assertNotEquals(allTermsInfoResponseDto1, allTermsInfoResponseDto);
  }

  @Test
  public void testEquals2() {
    BasicTermDto basicTermDto = new BasicTermDto();
    assertEquals(basicTermDto, basicTermDto);
    int expectedHashCodeResult = basicTermDto.hashCode();
    assertEquals(expectedHashCodeResult, basicTermDto.hashCode());
  }

  @Test
  public void testEquals3() {
    BasicTermDto basicTermDto = new BasicTermDto();
    BasicTermDto basicTermDto1 = new BasicTermDto();
    assertEquals(basicTermDto1, basicTermDto);
    int expectedHashCodeResult = basicTermDto.hashCode();
    assertEquals(expectedHashCodeResult, basicTermDto1.hashCode());
  }

  @Test
  public void testEquals4() {
    AllTermsInfoResponseDto allTermsInfoResponseDto = new AllTermsInfoResponseDto();
    assertNotEquals(new BasicTermDto(), allTermsInfoResponseDto);
  }

  @Test
  public void testEquals5() {
    BasicTermDto basicTermDto = new BasicTermDto("Course Abbreviation", "Term Name", 1, new ArrayList<Long>());
    assertNotEquals(new BasicTermDto(), basicTermDto);
  }

  @Test
  public void testEquals6() {
    AllTermsInfoResponseDto allTermsInfoResponseDto = mock(AllTermsInfoResponseDto.class);
    assertNotEquals(new BasicTermDto(), allTermsInfoResponseDto);
  }

  @Test
  public void testEquals7() {
    BasicTermDto basicTermDto = new BasicTermDto();
    assertNotEquals(new AllTermsInfoResponseDto(), basicTermDto);
  }

  @Test
  public void testEquals8() {
    BasicTermDto basicTermDto = new BasicTermDto();
    assertNotEquals(new BasicTermDto("Course Abbreviation", "Term Name", 1, new ArrayList<Long>()), basicTermDto);
  }

  @Test
  public void testEquals9() {
    BasicTermDto basicTermDto = new BasicTermDto();
    AllTermsInfoResponseDto allTermsInfoResponseDto = mock(AllTermsInfoResponseDto.class);
    when(allTermsInfoResponseDto.getAcademicYear()).thenReturn(1);
    when(allTermsInfoResponseDto.canEqual((Object) any())).thenReturn(true);
    assertNotEquals(allTermsInfoResponseDto, basicTermDto);
  }
}

