package cz.vutbr.fit.exampreparation.features.appUser.dto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class LoginUserDtoTest {
  @Test
  public void testCanEqual() {
    assertFalse((new LoginUserDto(true)).canEqual("Other"));
  }

  @Test
  public void testCanEqual2() {
    LoginUserDto loginUserDto = new LoginUserDto(true);
    assertTrue(loginUserDto.canEqual(new LoginUserDto(true)));
  }

  @Test
  public void testConstructor() {
    LoginUserDto actualLoginUserDto = new LoginUserDto();
    actualLoginUserDto.setRememberMe(true);
    assertTrue(actualLoginUserDto.isRememberMe());
    assertEquals("LoginUserDto(rememberMe=true)", actualLoginUserDto.toString());
  }

  @Test
  public void testConstructor2() {
    LoginUserDto actualLoginUserDto = new LoginUserDto(true);
    actualLoginUserDto.setRememberMe(true);
    assertTrue(actualLoginUserDto.isRememberMe());
    assertEquals("LoginUserDto(rememberMe=true)", actualLoginUserDto.toString());
  }

  @Test
  public void testEquals() {
    assertNotEquals((new LoginUserDto(true)), null);
    assertNotEquals((new LoginUserDto(true)), "Different type to LoginUserDto");
  }

  @Test
  public void testEquals2() {
    LoginUserDto loginUserDto = new LoginUserDto(true);
    assertEquals(loginUserDto, loginUserDto);
    int expectedHashCodeResult = loginUserDto.hashCode();
    assertEquals(expectedHashCodeResult, loginUserDto.hashCode());
  }

  @Test
  public void testEquals3() {
    LoginUserDto loginUserDto = new LoginUserDto(true);
    LoginUserDto loginUserDto1 = new LoginUserDto(true);
    assertEquals(loginUserDto1, loginUserDto);
    int expectedHashCodeResult = loginUserDto.hashCode();
    assertEquals(expectedHashCodeResult, loginUserDto1.hashCode());
  }

  @Test
  public void testEquals4() {
    LoginUserDto loginUserDto = new LoginUserDto(false);
    assertNotEquals(new LoginUserDto(true), loginUserDto);
  }

  @Test
  public void testEquals5() {
    LoginUserDto loginUserDto = new LoginUserDto(true);
    loginUserDto.setPassword("iloveyou");
    assertNotEquals(new LoginUserDto(true), loginUserDto);
  }
}

