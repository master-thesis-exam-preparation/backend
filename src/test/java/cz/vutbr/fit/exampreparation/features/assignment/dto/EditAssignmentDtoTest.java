package cz.vutbr.fit.exampreparation.features.assignment.dto;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class EditAssignmentDtoTest {
  @Test
  public void testCanEqual() {
    assertFalse((new EditAssignmentDto()).canEqual("Other"));
  }

  @Test
  public void testCanEqual2() {
    EditAssignmentDto editAssignmentDto = new EditAssignmentDto();
    assertTrue(editAssignmentDto.canEqual(new EditAssignmentDto()));
  }

  @Test
  public void testConstructor() {
    EditAssignmentDto actualEditAssignmentDto = new EditAssignmentDto();
    ArrayList<FileDataDto> fileDataDtoList = new ArrayList<FileDataDto>();
    actualEditAssignmentDto.setUploadedFiles(fileDataDtoList);
    assertSame(fileDataDtoList, actualEditAssignmentDto.getUploadedFiles());
    assertEquals("EditAssignmentDto(uploadedFiles=[])", actualEditAssignmentDto.toString());
  }

  @Test
  public void testConstructor2() {
    ArrayList<FileDataDto> fileDataDtoList = new ArrayList<FileDataDto>();
    EditAssignmentDto actualEditAssignmentDto = new EditAssignmentDto(fileDataDtoList);
    ArrayList<FileDataDto> fileDataDtoList1 = new ArrayList<FileDataDto>();
    actualEditAssignmentDto.setUploadedFiles(fileDataDtoList1);
    List<FileDataDto> uploadedFiles = actualEditAssignmentDto.getUploadedFiles();
    assertEquals(fileDataDtoList, uploadedFiles);
    assertSame(fileDataDtoList1, uploadedFiles);
    assertEquals("EditAssignmentDto(uploadedFiles=[])", actualEditAssignmentDto.toString());
  }

  @Test
  public void testEquals() {
    assertNotEquals((new EditAssignmentDto()), null);
    assertNotEquals((new EditAssignmentDto()), "Different type to EditAssignmentDto");
  }

  @Test
  public void testEquals2() {
    EditAssignmentDto editAssignmentDto = new EditAssignmentDto();
    assertEquals(editAssignmentDto, editAssignmentDto);
    int expectedHashCodeResult = editAssignmentDto.hashCode();
    assertEquals(expectedHashCodeResult, editAssignmentDto.hashCode());
  }

  @Test
  public void testEquals3() {
    EditAssignmentDto editAssignmentDto = new EditAssignmentDto();
    EditAssignmentDto editAssignmentDto1 = new EditAssignmentDto();
    assertEquals(editAssignmentDto1, editAssignmentDto);
    int expectedHashCodeResult = editAssignmentDto.hashCode();
    assertEquals(expectedHashCodeResult, editAssignmentDto1.hashCode());
  }

  @Test
  public void testEquals4() {
    EditAssignmentDto editAssignmentDto = new EditAssignmentDto(new ArrayList<FileDataDto>());
    assertNotEquals(new EditAssignmentDto(), editAssignmentDto);
  }

  @Test
  public void testEquals5() {
    EditAssignmentDto editAssignmentDto = new EditAssignmentDto();
    assertNotEquals(new EditAssignmentDto(new ArrayList<FileDataDto>()), editAssignmentDto);
  }

  @Test
  public void testEquals6() {
    EditAssignmentDto editAssignmentDto = new EditAssignmentDto(new ArrayList<FileDataDto>());
    EditAssignmentDto editAssignmentDto1 = new EditAssignmentDto(new ArrayList<FileDataDto>());
    assertEquals(editAssignmentDto1, editAssignmentDto);
    int expectedHashCodeResult = editAssignmentDto.hashCode();
    assertEquals(expectedHashCodeResult, editAssignmentDto1.hashCode());
  }

  @Test
  public void testEquals7() {
    EditAssignmentDto editAssignmentDto = new EditAssignmentDto();
    editAssignmentDto.setAdditionalAssignmentManagersEmails(new ArrayList<String>());
    assertNotEquals(new EditAssignmentDto(), editAssignmentDto);
  }
}

