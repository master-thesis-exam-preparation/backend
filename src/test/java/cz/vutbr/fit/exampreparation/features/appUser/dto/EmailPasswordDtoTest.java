package cz.vutbr.fit.exampreparation.features.appUser.dto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class EmailPasswordDtoTest {
  @Test
  public void testCanEqual() {
    assertFalse((new EmailPasswordDto("iloveyou")).canEqual("Other"));
  }

  @Test
  public void testCanEqual2() {
    EmailPasswordDto emailPasswordDto = new EmailPasswordDto("iloveyou");
    assertTrue(emailPasswordDto.canEqual(new EmailPasswordDto("iloveyou")));
  }

  @Test
  public void testConstructor() {
    EmailPasswordDto actualEmailPasswordDto = new EmailPasswordDto();
    actualEmailPasswordDto.setPassword("iloveyou");
    assertEquals("iloveyou", actualEmailPasswordDto.getPassword());
    assertEquals("EmailPasswordDto(password=iloveyou)", actualEmailPasswordDto.toString());
  }

  @Test
  public void testConstructor2() {
    EmailPasswordDto actualEmailPasswordDto = new EmailPasswordDto("iloveyou");
    actualEmailPasswordDto.setPassword("iloveyou");
    assertEquals("iloveyou", actualEmailPasswordDto.getPassword());
    assertEquals("EmailPasswordDto(password=iloveyou)", actualEmailPasswordDto.toString());
  }

  @Test
  public void testEquals() {
    assertNotEquals((new EmailPasswordDto("iloveyou")), null);
    assertNotEquals((new EmailPasswordDto("iloveyou")), "Different type to EmailPasswordDto");
  }

  @Test
  public void testEquals2() {
    EmailPasswordDto emailPasswordDto = new EmailPasswordDto("iloveyou");
    assertEquals(emailPasswordDto, emailPasswordDto);
    int expectedHashCodeResult = emailPasswordDto.hashCode();
    assertEquals(expectedHashCodeResult, emailPasswordDto.hashCode());
  }

  @Test
  public void testEquals3() {
    EmailPasswordDto emailPasswordDto = new EmailPasswordDto("iloveyou");
    EmailPasswordDto emailPasswordDto1 = new EmailPasswordDto("iloveyou");
    assertEquals(emailPasswordDto1, emailPasswordDto);
    int expectedHashCodeResult = emailPasswordDto.hashCode();
    assertEquals(expectedHashCodeResult, emailPasswordDto1.hashCode());
  }

  @Test
  public void testEquals4() {
    EmailPasswordDto emailPasswordDto = new EmailPasswordDto(null);
    assertNotEquals(new EmailPasswordDto("iloveyou"), emailPasswordDto);
  }

  @Test
  public void testEquals5() {
    EmailPasswordDto emailPasswordDto = new EmailPasswordDto("Password");
    assertNotEquals(new EmailPasswordDto("iloveyou"), emailPasswordDto);
  }

  @Test
  public void testEquals6() {
    LoginUserDto loginUserDto = mock(LoginUserDto.class);
    assertNotEquals(new EmailPasswordDto("iloveyou"), loginUserDto);
  }

  @Test
  public void testEquals7() {
    EmailPasswordDto emailPasswordDto = new EmailPasswordDto("iloveyou");
    assertNotEquals(new LoginUserDto(true), emailPasswordDto);
  }

  @Test
  public void testEquals8() {
    EmailPasswordDto emailPasswordDto = new EmailPasswordDto("iloveyou");
    LoginUserDto loginUserDto = mock(LoginUserDto.class);
    when(loginUserDto.getEmail()).thenReturn("foo");
    when(loginUserDto.canEqual((Object) any())).thenReturn(true);
    assertNotEquals(loginUserDto, emailPasswordDto);
  }
}

