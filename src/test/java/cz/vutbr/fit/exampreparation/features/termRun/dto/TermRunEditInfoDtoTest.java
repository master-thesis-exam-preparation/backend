package cz.vutbr.fit.exampreparation.features.termRun.dto;

import cz.vutbr.fit.exampreparation.features.roomPlan.dto.EditTermRoomDto;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class TermRunEditInfoDtoTest {
  @Test
  public void testConstructor() {
    ArrayList<EditTermRoomDto> editTermRoomDtoList = new ArrayList<EditTermRoomDto>();
    ArrayList<Integer> integerList = new ArrayList<Integer>();
    TermRunEditInfoDto actualTermRunEditInfoDto = new TermRunEditInfoDto(1, 1L, 1L, editTermRoomDtoList, integerList);

    List<Integer> assignments = actualTermRunEditInfoDto.getAssignments();
    assertSame(integerList, assignments);
    assertEquals(editTermRoomDtoList, assignments);
    assertTrue(assignments.isEmpty());
    assertEquals("TermRunEditInfoDto(rooms=[])", actualTermRunEditInfoDto.toString());
    assertEquals(1L, actualTermRunEditInfoDto.getStartTime().longValue());
    List<EditTermRoomDto> rooms = actualTermRunEditInfoDto.getRooms();
    assertSame(editTermRoomDtoList, rooms);
    assertEquals(assignments, rooms);
    assertTrue(rooms.isEmpty());
    assertEquals(1, actualTermRunEditInfoDto.getId().intValue());
    assertEquals(1L, actualTermRunEditInfoDto.getEndTime().longValue());
    assertSame(rooms, editTermRoomDtoList);
    assertSame(assignments, integerList);
  }
}

