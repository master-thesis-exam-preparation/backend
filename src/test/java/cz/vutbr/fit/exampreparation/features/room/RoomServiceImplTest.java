package cz.vutbr.fit.exampreparation.features.room;

import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.NotFoundException;
import cz.vutbr.fit.exampreparation.features.appUser.AppUser;
import cz.vutbr.fit.exampreparation.features.appUser.AppUserService;
import cz.vutbr.fit.exampreparation.features.appUser.dto.EmployeeDetailsDto;
import cz.vutbr.fit.exampreparation.features.assignment.Assignment;
import cz.vutbr.fit.exampreparation.features.cell.Cell;
import cz.vutbr.fit.exampreparation.features.role.Role;
import cz.vutbr.fit.exampreparation.features.room.dto.FreeRoomRequestDto;
import cz.vutbr.fit.exampreparation.features.room.dto.FreeRoomResponseDto;
import cz.vutbr.fit.exampreparation.features.room.dto.RoomDetailDto;
import cz.vutbr.fit.exampreparation.features.room.dto.RoomInfoDto;
import cz.vutbr.fit.exampreparation.features.roomPlan.RoomPlan;
import cz.vutbr.fit.exampreparation.features.roomPlan.RoomPlanService;
import cz.vutbr.fit.exampreparation.features.roomPlan.SpacingBetweenStudents;
import cz.vutbr.fit.exampreparation.features.roomPlan.dto.RoomPlanDto;
import cz.vutbr.fit.exampreparation.features.roomPlan.dto.RoomPlanWithCellsDto;
import cz.vutbr.fit.exampreparation.features.term.Term;
import cz.vutbr.fit.exampreparation.features.termRun.TermRun;
import cz.vutbr.fit.exampreparation.features.testSchedule.TestScheduleService;
import cz.vutbr.fit.exampreparation.features.token.Token;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {RoomServiceImpl.class})
@ExtendWith(SpringExtension.class)
public class RoomServiceImplTest {
  @MockBean
  private AppUserService appUserService;

  @MockBean
  private RoomPlanService roomPlanService;

  @MockBean
  private RoomRepository roomRepository;

  @Autowired
  private RoomServiceImpl roomServiceImpl;

  @MockBean
  private TestScheduleService testScheduleService;

  @Test
  public void testGetAllRoomsInfo() {
    when(this.roomRepository.findAllSorted()).thenReturn(new ArrayList<Room>());
    ResponseEntity<List<RoomInfoDto>> actualAllRoomsInfo = this.roomServiceImpl.getAllRoomsInfo();
    assertEquals("<200 OK OK,[],[]>", actualAllRoomsInfo.toString());
    assertTrue(actualAllRoomsInfo.hasBody());
    assertEquals(HttpStatus.OK, actualAllRoomsInfo.getStatusCode());
    assertTrue(actualAllRoomsInfo.getHeaders().isEmpty());
    verify(this.roomRepository).findAllSorted();
  }

  @Test
  public void testGetAllRoomsInfo2() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());

    Room room = new Room();
    room.setAppUserReference(appUser);
    room.setAbbreviation("Abbreviation");
    room.setRoomPlans(new ArrayList<RoomPlan>());
    room.setNumberOfColumns(10);
    room.setName("Name");
    room.setDescription("The characteristics of someone or something");
    room.setNumberOfRows(10);

    ArrayList<Room> roomList = new ArrayList<Room>();
    roomList.add(room);
    when(this.roomRepository.findAllSorted()).thenReturn(roomList);
    ResponseEntity<List<RoomInfoDto>> actualAllRoomsInfo = this.roomServiceImpl.getAllRoomsInfo();
    assertEquals(1, actualAllRoomsInfo.getBody().size());
    assertEquals("<200 OK OK,[RoomInfoDto(roomId=0, capacities=[], creatorNameWithDegrees=Degrees Before Name Jane Doe"
      + " Degrees Behind Name, creatorEmail=jane.doe@example.org)],[]>", actualAllRoomsInfo.toString());
    assertTrue(actualAllRoomsInfo.hasBody());
    assertEquals(HttpStatus.OK, actualAllRoomsInfo.getStatusCode());
    assertTrue(actualAllRoomsInfo.getHeaders().isEmpty());
    verify(this.roomRepository).findAllSorted();
  }

  @Test
  public void testGetRoomDetails() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());

    Room room = new Room();
    room.setAppUserReference(appUser);
    room.setAbbreviation("Abbreviation");
    room.setRoomPlans(new ArrayList<RoomPlan>());
    room.setNumberOfColumns(10);
    room.setName("Name");
    room.setDescription("The characteristics of someone or something");
    room.setNumberOfRows(10);
    Optional<Room> ofResult = Optional.<Room>of(room);
    when(this.roomRepository.findById((Integer) any())).thenReturn(ofResult);
    ResponseEntity<RoomDetailDto> actualRoomDetails = this.roomServiceImpl.getRoomDetails(123, true);
    assertEquals("<200 OK OK,RoomDetailDto(creator=null, description=The characteristics of someone or something,"
      + " roomPlans=[]),[]>", actualRoomDetails.toString());
    assertTrue(actualRoomDetails.getHeaders().isEmpty());
    assertTrue(actualRoomDetails.hasBody());
    assertEquals(HttpStatus.OK, actualRoomDetails.getStatusCode());
    RoomDetailDto body = actualRoomDetails.getBody();
    assertNull(body.getCreator());
    assertEquals("Abbreviation", body.getAbbreviation());
    assertTrue(body.getRoomPlans().isEmpty());
    assertEquals("The characteristics of someone or something", body.getDescription());
    assertEquals("Name", body.getName());
    verify(this.roomRepository).findById((Integer) any());
  }

  @Test
  public void testGetRoomDetails2() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());

    AppUser appUser1 = new AppUser();
    appUser1.setLastName("Doe");
    appUser1.setEmail("jane.doe@example.org");
    appUser1.setDegreesBeforeName("Degrees Before Name");
    appUser1.setPassword("iloveyou");
    appUser1.setUserTokens(new ArrayList<Token>());
    appUser1.setFirstName("Jane");
    appUser1.setVerified(true);
    appUser1.setRooms(new ArrayList<Room>());
    appUser1.setDegreesBehindName("Degrees Behind Name");
    appUser1.setRoles(new ArrayList<Role>());
    appUser1.setLocked(true);
    appUser1.setTerms(new ArrayList<Term>());
    appUser1.setAssignments(new ArrayList<Assignment>());

    Room room = new Room();
    room.setAppUserReference(appUser1);
    room.setAbbreviation("Abbreviation");
    room.setRoomPlans(new ArrayList<RoomPlan>());
    room.setNumberOfColumns(10);
    room.setName("Name");
    room.setDescription("The characteristics of someone or something");
    room.setNumberOfRows(10);

    RoomPlan roomPlan = new RoomPlan();
    roomPlan.setCells(new ArrayList<Cell>());
    roomPlan.setCapacity(3);
    roomPlan.setTermRuns(new ArrayList<TermRun>());
    roomPlan.setSpacing(SpacingBetweenStudents.ZERO);
    roomPlan.setRoomReference(room);

    ArrayList<RoomPlan> roomPlanList = new ArrayList<RoomPlan>();
    roomPlanList.add(roomPlan);

    Room room1 = new Room();
    room1.setAppUserReference(appUser);
    room1.setAbbreviation("Abbreviation");
    room1.setRoomPlans(roomPlanList);
    room1.setNumberOfColumns(10);
    room1.setName("Name");
    room1.setDescription("The characteristics of someone or something");
    room1.setNumberOfRows(10);
    Optional<Room> ofResult = Optional.<Room>of(room1);
    when(this.roomRepository.findById((Integer) any())).thenReturn(ofResult);
    ResponseEntity<RoomDetailDto> actualRoomDetails = this.roomServiceImpl.getRoomDetails(123, true);
    assertEquals(
      "<200 OK OK,RoomDetailDto(creator=null, description=The characteristics of someone or something,"
        + " roomPlans=[RoomPlanWithCellsDto(cells=[[], [], [], [], [], [], [], [], [], []])]),[]>",
      actualRoomDetails.toString());
    assertTrue(actualRoomDetails.getHeaders().isEmpty());
    assertTrue(actualRoomDetails.hasBody());
    assertEquals(HttpStatus.OK, actualRoomDetails.getStatusCode());
    RoomDetailDto body = actualRoomDetails.getBody();
    assertNull(body.getCreator());
    assertEquals("Abbreviation", body.getAbbreviation());
    List<RoomPlanWithCellsDto> roomPlans = body.getRoomPlans();
    assertEquals(1, roomPlans.size());
    assertEquals("Name", body.getName());
    assertEquals("The characteristics of someone or something", body.getDescription());
    RoomPlanWithCellsDto getResult = roomPlans.get(0);
    assertEquals(3, getResult.getCapacity().intValue());
    assertEquals("RoomPlanWithCellsDto(cells=[[], [], [], [], [], [], [], [], [], []])", getResult.toString());
    assertEquals(SpacingBetweenStudents.ZERO, getResult.getSpacing());
    assertEquals(0, getResult.getRoomPlanId().intValue());
    assertEquals(10, getResult.getCells().size());
    verify(this.roomRepository).findById((Integer) any());
  }

  @Test
  public void testGetRoomDetails3() {
    when(this.roomRepository.findById((Integer) any())).thenReturn(Optional.<Room>empty());
    assertThrows(NotFoundException.class, () -> this.roomServiceImpl.getRoomDetails(123, true));
    verify(this.roomRepository).findById((Integer) any());
  }

  @Test
  public void testGetRoomDetails4() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());

    Room room = new Room();
    room.setAppUserReference(appUser);
    room.setAbbreviation("Abbreviation");
    room.setRoomPlans(new ArrayList<RoomPlan>());
    room.setNumberOfColumns(10);
    room.setName("Name");
    room.setDescription("The characteristics of someone or something");
    room.setNumberOfRows(10);
    Optional<Room> ofResult = Optional.<Room>of(room);
    when(this.roomRepository.findById((Integer) any())).thenReturn(ofResult);
    ResponseEntity<RoomDetailDto> actualRoomDetails = this.roomServiceImpl.getRoomDetails(123, false);
    assertEquals(
      "<200 OK OK,RoomDetailDto(creator=EmployeeDetailsDto(nameWithDegrees=Degrees Before Name Jane Doe Degrees"
        + " Behind Name), description=The characteristics of someone or something, roomPlans=[]),[]>",
      actualRoomDetails.toString());
    assertTrue(actualRoomDetails.getHeaders().isEmpty());
    assertTrue(actualRoomDetails.hasBody());
    assertEquals(HttpStatus.OK, actualRoomDetails.getStatusCode());
    RoomDetailDto body = actualRoomDetails.getBody();
    assertEquals("Abbreviation", body.getAbbreviation());
    assertEquals("Name", body.getName());
    assertEquals("RoomDetailDto(creator=EmployeeDetailsDto(nameWithDegrees=Degrees Before Name Jane Doe Degrees Behind"
      + " Name), description=The characteristics of someone or something, roomPlans=[])", body.toString());
    assertTrue(body.getRoomPlans().isEmpty());
    assertEquals("The characteristics of someone or something", body.getDescription());
    EmployeeDetailsDto creator = body.getCreator();
    assertEquals("EmployeeDetailsDto(nameWithDegrees=Degrees Before Name Jane Doe Degrees Behind Name)",
      creator.toString());
    assertEquals("jane.doe@example.org", creator.getEmail());
    verify(this.roomRepository).findById((Integer) any());
  }

  @Test
  public void testGetEditRoomDetails() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());

    Room room = new Room();
    room.setAppUserReference(appUser);
    room.setAbbreviation("Abbreviation");
    room.setRoomPlans(new ArrayList<RoomPlan>());
    room.setNumberOfColumns(10);
    room.setName("Name");
    room.setDescription("The characteristics of someone or something");
    room.setNumberOfRows(10);
    Optional<Room> ofResult = Optional.<Room>of(room);
    when(this.roomRepository.findById((Integer) any())).thenReturn(ofResult);
    assertThrows(RuntimeException.class, () -> this.roomServiceImpl.getEditRoomDetails(123, true));
    verify(this.roomRepository).findById((Integer) any());
  }

  @Test
  public void testGetEditRoomDetails2() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());

    Room room = new Room();
    room.setAppUserReference(appUser);
    room.setAbbreviation("Abbreviation");
    room.setRoomPlans(new ArrayList<RoomPlan>());
    room.setNumberOfColumns(0);
    room.setName("Name");
    room.setDescription("The characteristics of someone or something");
    room.setNumberOfRows(10);
    Optional<Room> ofResult = Optional.<Room>of(room);
    when(this.roomRepository.findById((Integer) any())).thenReturn(ofResult);
    assertThrows(RuntimeException.class, () -> this.roomServiceImpl.getEditRoomDetails(123, true));
    verify(this.roomRepository).findById((Integer) any());
  }

  @Test
  public void testGetEditRoomDetails3() {
    when(this.roomRepository.findById((Integer) any())).thenReturn(Optional.<Room>empty());
    assertThrows(NotFoundException.class, () -> this.roomServiceImpl.getEditRoomDetails(123, true));
    verify(this.roomRepository).findById((Integer) any());
  }

  @Test
  public void testGetFreeRooms() {
    when(this.roomRepository.findFreeRooms((java.util.Date) any(), (java.util.Date) any()))
      .thenReturn(new ArrayList<Room>());

    FreeRoomRequestDto freeRoomRequestDto = new FreeRoomRequestDto();
    freeRoomRequestDto.setNumberOfTermRuns(10);
    freeRoomRequestDto.setTermRunLength(0);
    freeRoomRequestDto.setStartTime(1L);
    ResponseEntity<List<FreeRoomResponseDto>> actualFreeRooms = this.roomServiceImpl.getFreeRooms(freeRoomRequestDto);
    assertEquals("<200 OK OK,[],[]>", actualFreeRooms.toString());
    assertTrue(actualFreeRooms.hasBody());
    assertEquals(HttpStatus.OK, actualFreeRooms.getStatusCode());
    assertTrue(actualFreeRooms.getHeaders().isEmpty());
    verify(this.roomRepository).findFreeRooms((java.util.Date) any(), (java.util.Date) any());
    assertEquals(actualFreeRooms, this.roomServiceImpl.getAllRoomsInfo());
  }

  @Test
  public void testGetFreeRooms2() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName(null);
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName(null);
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());

    Room room = new Room();
    room.setAppUserReference(appUser);
    room.setAbbreviation(null);
    room.setRoomPlans(new ArrayList<RoomPlan>());
    room.setNumberOfColumns(10);
    room.setName(null);
    room.setDescription("The characteristics of someone or something");
    room.setNumberOfRows(10);

    ArrayList<Room> roomList = new ArrayList<Room>();
    roomList.add(room);
    when(this.roomRepository.findFreeRooms((java.util.Date) any(), (java.util.Date) any())).thenReturn(roomList);

    FreeRoomRequestDto freeRoomRequestDto = new FreeRoomRequestDto();
    freeRoomRequestDto.setNumberOfTermRuns(10);
    freeRoomRequestDto.setTermRunLength(0);
    freeRoomRequestDto.setStartTime(1L);
    ResponseEntity<List<FreeRoomResponseDto>> actualFreeRooms = this.roomServiceImpl.getFreeRooms(freeRoomRequestDto);
    List<FreeRoomResponseDto> body = actualFreeRooms.getBody();
    assertEquals(1, body.size());
    assertEquals("<200 OK OK,[FreeRoomResponseDto(roomId=0, roomPlans=[])],[]>", actualFreeRooms.toString());
    assertTrue(actualFreeRooms.getHeaders().isEmpty());
    assertTrue(actualFreeRooms.hasBody());
    assertEquals(HttpStatus.OK, actualFreeRooms.getStatusCode());
    FreeRoomResponseDto getResult = body.get(0);
    assertNull(getResult.getAbbreviation());
    assertTrue(getResult.getRoomPlans().isEmpty());
    assertEquals(0, getResult.getRoomId().intValue());
    assertNull(getResult.getName());
    verify(this.roomRepository).findFreeRooms((java.util.Date) any(), (java.util.Date) any());
  }

  @Test
  public void testGetFreeRooms3() {
    when(this.roomRepository.findFreeRooms((java.util.Date) any(), (java.util.Date) any(), anyInt()))
      .thenReturn(new ArrayList<Room>());
    when(this.roomRepository.findFreeRooms((java.util.Date) any(), (java.util.Date) any()))
      .thenReturn(new ArrayList<Room>());

    FreeRoomRequestDto freeRoomRequestDto = new FreeRoomRequestDto();
    freeRoomRequestDto.setTermId(123);
    freeRoomRequestDto.setNumberOfTermRuns(10);
    freeRoomRequestDto.setTermRunLength(0);
    freeRoomRequestDto.setStartTime(1L);
    ResponseEntity<List<FreeRoomResponseDto>> actualFreeRooms = this.roomServiceImpl.getFreeRooms(freeRoomRequestDto);
    assertEquals("<200 OK OK,[],[]>", actualFreeRooms.toString());
    assertTrue(actualFreeRooms.hasBody());
    assertEquals(HttpStatus.OK, actualFreeRooms.getStatusCode());
    assertTrue(actualFreeRooms.getHeaders().isEmpty());
    verify(this.roomRepository).findFreeRooms((java.util.Date) any(), (java.util.Date) any(), anyInt());
    assertEquals(actualFreeRooms, this.roomServiceImpl.getAllRoomsInfo());
  }

  @Test
  public void testGetFreeRooms4() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName(null);
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName(null);
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());

    AppUser appUser1 = new AppUser();
    appUser1.setLastName("Doe");
    appUser1.setEmail("jane.doe@example.org");
    appUser1.setDegreesBeforeName("Degrees Before Name");
    appUser1.setPassword("iloveyou");
    appUser1.setUserTokens(new ArrayList<Token>());
    appUser1.setFirstName("Jane");
    appUser1.setVerified(true);
    appUser1.setRooms(new ArrayList<Room>());
    appUser1.setDegreesBehindName("Degrees Behind Name");
    appUser1.setRoles(new ArrayList<Role>());
    appUser1.setLocked(true);
    appUser1.setTerms(new ArrayList<Term>());
    appUser1.setAssignments(new ArrayList<Assignment>());

    Room room = new Room();
    room.setAppUserReference(appUser1);
    room.setAbbreviation("Abbreviation");
    room.setRoomPlans(new ArrayList<RoomPlan>());
    room.setNumberOfColumns(10);
    room.setName("Name");
    room.setDescription("The characteristics of someone or something");
    room.setNumberOfRows(10);

    RoomPlan roomPlan = new RoomPlan();
    roomPlan.setCells(new ArrayList<Cell>());
    roomPlan.setCapacity(3);
    roomPlan.setTermRuns(new ArrayList<TermRun>());
    roomPlan.setSpacing(SpacingBetweenStudents.ZERO);
    roomPlan.setRoomReference(room);

    ArrayList<RoomPlan> roomPlanList = new ArrayList<RoomPlan>();
    roomPlanList.add(roomPlan);

    Room room1 = new Room();
    room1.setAppUserReference(appUser);
    room1.setAbbreviation(null);
    room1.setRoomPlans(roomPlanList);
    room1.setNumberOfColumns(10);
    room1.setName(null);
    room1.setDescription("The characteristics of someone or something");
    room1.setNumberOfRows(10);

    ArrayList<Room> roomList = new ArrayList<Room>();
    roomList.add(room1);
    when(this.roomRepository.findFreeRooms((java.util.Date) any(), (java.util.Date) any(), anyInt()))
      .thenReturn(roomList);
    when(this.roomRepository.findFreeRooms((java.util.Date) any(), (java.util.Date) any()))
      .thenReturn(new ArrayList<Room>());

    FreeRoomRequestDto freeRoomRequestDto = new FreeRoomRequestDto();
    freeRoomRequestDto.setTermId(123);
    freeRoomRequestDto.setNumberOfTermRuns(10);
    freeRoomRequestDto.setTermRunLength(0);
    freeRoomRequestDto.setStartTime(1L);
    ResponseEntity<List<FreeRoomResponseDto>> actualFreeRooms = this.roomServiceImpl.getFreeRooms(freeRoomRequestDto);
    List<FreeRoomResponseDto> body = actualFreeRooms.getBody();
    assertEquals(1, body.size());
    assertEquals("<200 OK OK,[FreeRoomResponseDto(roomId=0, roomPlans=[RoomPlanDto(roomPlanId=0, capacity=3, spacing"
      + "=ZERO)])],[]>", actualFreeRooms.toString());
    assertTrue(actualFreeRooms.getHeaders().isEmpty());
    assertTrue(actualFreeRooms.hasBody());
    assertEquals(HttpStatus.OK, actualFreeRooms.getStatusCode());
    FreeRoomResponseDto getResult = body.get(0);
    assertNull(getResult.getAbbreviation());
    List<RoomPlanDto> roomPlans = getResult.getRoomPlans();
    assertEquals(1, roomPlans.size());
    assertEquals(0, getResult.getRoomId().intValue());
    assertNull(getResult.getName());
    RoomPlanDto getResult1 = roomPlans.get(0);
    assertEquals(3, getResult1.getCapacity().intValue());
    assertEquals(SpacingBetweenStudents.ZERO, getResult1.getSpacing());
    assertEquals(0, getResult1.getRoomPlanId().intValue());
    verify(this.roomRepository).findFreeRooms((java.util.Date) any(), (java.util.Date) any(), anyInt());
  }

  @Test
  public void testFindRoomById() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());

    Room room = new Room();
    room.setAppUserReference(appUser);
    room.setAbbreviation("Abbreviation");
    room.setRoomPlans(new ArrayList<RoomPlan>());
    room.setNumberOfColumns(10);
    room.setName("Name");
    room.setDescription("The characteristics of someone or something");
    room.setNumberOfRows(10);
    Optional<Room> ofResult = Optional.<Room>of(room);
    when(this.roomRepository.findById((Integer) any())).thenReturn(ofResult);
    Optional<Room> actualFindRoomByIdResult = this.roomServiceImpl.findRoomById(1);
    assertSame(ofResult, actualFindRoomByIdResult);
    assertTrue(actualFindRoomByIdResult.isPresent());
    verify(this.roomRepository).findById((Integer) any());
  }

  @Test
  public void testFindFreeRooms() {
    ArrayList<Room> roomList = new ArrayList<Room>();
    when(this.roomRepository.findFreeRooms((java.util.Date) any(), (java.util.Date) any(), anyInt()))
      .thenReturn(roomList);
    List<Room> actualFindFreeRoomsResult = this.roomServiceImpl.findFreeRooms(1L, 1L, 123);
    assertSame(roomList, actualFindFreeRoomsResult);
    assertTrue(actualFindFreeRoomsResult.isEmpty());
    verify(this.roomRepository).findFreeRooms((java.util.Date) any(), (java.util.Date) any(), anyInt());
  }

  @Test
  public void testFindFreeRooms2() {
    ArrayList<Room> roomList = new ArrayList<Room>();
    when(this.roomRepository.findFreeRooms((java.util.Date) any(), (java.util.Date) any())).thenReturn(roomList);
    when(this.roomRepository.findFreeRooms((java.util.Date) any(), (java.util.Date) any(), anyInt()))
      .thenReturn(new ArrayList<Room>());
    List<Room> actualFindFreeRoomsResult = this.roomServiceImpl.findFreeRooms(1L, 1L, null);
    assertSame(roomList, actualFindFreeRoomsResult);
    assertTrue(actualFindFreeRoomsResult.isEmpty());
    verify(this.roomRepository).findFreeRooms((java.util.Date) any(), (java.util.Date) any());
  }
}

