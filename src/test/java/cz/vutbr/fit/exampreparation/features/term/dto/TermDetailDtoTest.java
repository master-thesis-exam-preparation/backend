package cz.vutbr.fit.exampreparation.features.term.dto;

import cz.vutbr.fit.exampreparation.features.appUser.dto.EmployeeDetailsDto;
import cz.vutbr.fit.exampreparation.features.assignment.dto.GeneratingFailDto;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class TermDetailDtoTest {
  @Test
  public void testConstructor() {
    ArrayList<Long> resultLongList = new ArrayList<Long>();
    ArrayList<EmployeeDetailsDto> employeeDetailsDtoList = new ArrayList<EmployeeDetailsDto>();
    ArrayList<GeneratingFailDto> generatingFailDtoList = new ArrayList<GeneratingFailDto>();
    ArrayList<TermRunDetailsDto> termRunDetailsDtoList = new ArrayList<TermRunDetailsDto>();
    TermDetailDto actualTermDetailDto = new TermDetailDto("Course Abbreviation", "Term Name", 1, resultLongList,
      "The characteristics of someone or something", employeeDetailsDtoList, 1, generatingFailDtoList,
      termRunDetailsDtoList);

    assertEquals(1, actualTermDetailDto.getAcademicYear().intValue());
    assertEquals(
      "TermDetailDto(description=The characteristics of someone or something, managers=[], remainingGenerating=1,"
        + " generatingFails=[], termRuns=[])",
      actualTermDetailDto.toString());
    List<TermRunDetailsDto> termRuns = actualTermDetailDto.getTermRuns();
    assertSame(termRunDetailsDtoList, termRuns);
    assertEquals(employeeDetailsDtoList, termRuns);
    assertEquals(generatingFailDtoList, termRuns);
    assertEquals(resultLongList, termRuns);
    assertTrue(termRuns.isEmpty());
    assertEquals("Term Name", actualTermDetailDto.getTermName());
    List<Long> termDates = actualTermDetailDto.getTermDates();
    assertSame(resultLongList, termDates);
    assertEquals(termRuns, termDates);
    assertEquals(employeeDetailsDtoList, termDates);
    assertEquals(generatingFailDtoList, termDates);
    assertTrue(termDates.isEmpty());
    assertEquals(1, actualTermDetailDto.getRemainingGenerating().intValue());
    List<EmployeeDetailsDto> managers = actualTermDetailDto.getManagers();
    assertSame(employeeDetailsDtoList, managers);
    assertEquals(termRuns, managers);
    assertEquals(generatingFailDtoList, managers);
    assertEquals(termDates, managers);
    assertTrue(managers.isEmpty());
    List<GeneratingFailDto> generatingFails = actualTermDetailDto.getGeneratingFails();
    assertSame(generatingFailDtoList, generatingFails);
    assertEquals(termRuns, generatingFails);
    assertEquals(managers, generatingFails);
    assertEquals(termDates, generatingFails);
    assertTrue(generatingFails.isEmpty());
    assertEquals("The characteristics of someone or something", actualTermDetailDto.getDescription());
    assertEquals("Course Abbreviation", actualTermDetailDto.getCourseAbbreviation());
    assertSame(termDates, resultLongList);
    assertSame(managers, employeeDetailsDtoList);
    assertSame(generatingFails, generatingFailDtoList);
    assertSame(termRuns, termRunDetailsDtoList);
  }
}

