package cz.vutbr.fit.exampreparation.features.testSchedule;

import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.BadRequestException;
import cz.vutbr.fit.exampreparation.features.appUser.AppUser;
import cz.vutbr.fit.exampreparation.features.assignment.Assignment;
import cz.vutbr.fit.exampreparation.features.assignment.AssignmentStatus;
import cz.vutbr.fit.exampreparation.features.assignment.helpers.RunningGeneratingService;
import cz.vutbr.fit.exampreparation.features.assignment.helpers.StorageManager;
import cz.vutbr.fit.exampreparation.features.cell.Cell;
import cz.vutbr.fit.exampreparation.features.cell.CellType;
import cz.vutbr.fit.exampreparation.features.course.Course;
import cz.vutbr.fit.exampreparation.features.role.Role;
import cz.vutbr.fit.exampreparation.features.room.Room;
import cz.vutbr.fit.exampreparation.features.roomPlan.RoomPlan;
import cz.vutbr.fit.exampreparation.features.roomPlan.SpacingBetweenStudents;
import cz.vutbr.fit.exampreparation.features.term.Term;
import cz.vutbr.fit.exampreparation.features.termRun.Distribution;
import cz.vutbr.fit.exampreparation.features.termRun.TermRun;
import cz.vutbr.fit.exampreparation.features.termRun.TermRunService;
import cz.vutbr.fit.exampreparation.features.termStudent.TermStudent;
import cz.vutbr.fit.exampreparation.features.token.Token;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {TestScheduleServiceImpl.class, RunningGeneratingService.class, StorageManager.class})
@ExtendWith(SpringExtension.class)
public class TestScheduleServiceImplTest {
  @MockBean
  private RunningGeneratingService runningGeneratingService;

  @MockBean
  private StorageManager storageManager;

  @MockBean
  private TermRunService termRunService;

  @MockBean
  private TestScheduleRepository testScheduleRepository;

  @Autowired
  private TestScheduleServiceImpl testScheduleServiceImpl;

  @Test
  public void testFindAllTestSchedulesInTermRunsRoomPlan() {
    ArrayList<TestSchedule> testScheduleList = new ArrayList<TestSchedule>();
    when(this.testScheduleRepository.findAllTestSchedulesInTermRunsRoomPlan(anyInt(), anyString()))
      .thenReturn(testScheduleList);
    List<TestSchedule> actualFindAllTestSchedulesInTermRunsRoomPlanResult = this.testScheduleServiceImpl
      .findAllTestSchedulesInTermRunsRoomPlan(123, "Room Plan Abbreviation");
    assertSame(testScheduleList, actualFindAllTestSchedulesInTermRunsRoomPlanResult);
    assertTrue(actualFindAllTestSchedulesInTermRunsRoomPlanResult.isEmpty());
    verify(this.testScheduleRepository).findAllTestSchedulesInTermRunsRoomPlan(anyInt(), anyString());
  }

  @Test
  public void testFindAllTestSchedulesInTermRunsRoomPlan2() {
    ArrayList<TestSchedule> testScheduleList = new ArrayList<TestSchedule>();
    when(this.testScheduleRepository.findAllTestSchedulesInTermRunsRoomPlan((TermRun) any(), (RoomPlan) any()))
      .thenReturn(testScheduleList);
    TermRun termRun = new TermRun();
    List<TestSchedule> actualFindAllTestSchedulesInTermRunsRoomPlanResult = this.testScheduleServiceImpl
      .findAllTestSchedulesInTermRunsRoomPlan(termRun, new RoomPlan());
    assertSame(testScheduleList, actualFindAllTestSchedulesInTermRunsRoomPlanResult);
    assertTrue(actualFindAllTestSchedulesInTermRunsRoomPlanResult.isEmpty());
    verify(this.testScheduleRepository).findAllTestSchedulesInTermRunsRoomPlan((TermRun) any(), (RoomPlan) any());
  }

  @Test
  public void testFindAllTestSchedulesInTermRun() {
    ArrayList<TestSchedule> testScheduleList = new ArrayList<TestSchedule>();
    when(this.testScheduleRepository.findAllByTermRun((TermRun) any())).thenReturn(testScheduleList);
    List<TestSchedule> actualFindAllTestSchedulesInTermRunResult = this.testScheduleServiceImpl
      .findAllTestSchedulesInTermRun(new TermRun());
    assertSame(testScheduleList, actualFindAllTestSchedulesInTermRunResult);
    assertTrue(actualFindAllTestSchedulesInTermRunResult.isEmpty());
    verify(this.testScheduleRepository).findAllByTermRun((TermRun) any());
  }

  @Test
  public void testFindAllTestSchedulesWithAssignment() {
    ArrayList<TestSchedule> testScheduleList = new ArrayList<TestSchedule>();
    when(this.testScheduleRepository.findAllByAssignment_IdAssignment(anyInt())).thenReturn(testScheduleList);
    List<TestSchedule> actualFindAllTestSchedulesWithAssignmentResult = this.testScheduleServiceImpl
      .findAllTestSchedulesWithAssignment(123);
    assertSame(testScheduleList, actualFindAllTestSchedulesWithAssignmentResult);
    assertTrue(actualFindAllTestSchedulesWithAssignmentResult.isEmpty());
    verify(this.testScheduleRepository).findAllByAssignment_IdAssignment(anyInt());
  }

  @Test
  public void testIsAnyStudentSeatedInRoom() {
    when(this.testScheduleRepository.findTestSchedulesInRoom((Room) any())).thenReturn(new ArrayList<TestSchedule>());
    assertFalse(this.testScheduleServiceImpl.isAnyStudentSeatedInRoom(new Room()));
    verify(this.testScheduleRepository).findTestSchedulesInRoom((Room) any());
  }

  @Test
  public void testIsAnyStudentSeatedInRoom2() {
    Assignment assignment = new Assignment();
    assignment.setAssignmentManagers(new ArrayList<AppUser>());
    assignment.setTermRuns(new ArrayList<TermRun>());
    assignment.setName("Name");
    assignment.setTestSchedules(new ArrayList<TestSchedule>());
    assignment.setStatus(AssignmentStatus.UPLOADED);

    TermStudent termStudent = new TermStudent();
    termStudent.setLogin("Login");
    termStudent.setNameWithDegrees("Name With Degrees");

    Course course = new Course();
    course.setAbbreviation("Abbreviation");
    course.setAcademicYear(0);
    course.setName("Name");
    course.setTerms(new ArrayList<Term>());

    Term term = new Term();
    term.setDistribution(Distribution.RANDOM);
    term.setCourseReference(course);
    term.setTermRuns(new ArrayList<TermRun>());
    term.setTermName("Term Name");
    term.setTermManagers(new ArrayList<AppUser>());
    term.setDescription("The characteristics of someone or something");
    term.setSpacingBetweenStudents(SpacingBetweenStudents.ZERO);

    TermRun termRun = new TermRun();
    termRun.setTermReference(term);
    termRun.setRoomPlans(new ArrayList<RoomPlan>());
    termRun.setStartDateTime(mock(Timestamp.class));
    termRun.setEndDateTime(mock(Timestamp.class));
    termRun.setTestSchedules(new ArrayList<TestSchedule>());
    termRun.setAssignments(new ArrayList<Assignment>());

    Room room = new Room();
    room.setAppUserReference(new AppUser());
    room.setAbbreviation("Abbreviation");
    room.setRoomPlans(new ArrayList<RoomPlan>());
    room.setNumberOfColumns(10);
    room.setName("Name");
    room.setDescription("The characteristics of someone or something");
    room.setNumberOfRows(10);

    RoomPlan roomPlan = new RoomPlan();
    roomPlan.setCells(new ArrayList<Cell>());
    roomPlan.setCapacity(3);
    roomPlan.setTermRuns(new ArrayList<TermRun>());
    roomPlan.setSpacing(SpacingBetweenStudents.ZERO);
    roomPlan.setRoomReference(room);

    Cell cell = new Cell();
    cell.setColumnNumber(10);
    cell.setRoomPlanReference(roomPlan);
    cell.setRowNumber(10);
    cell.setTestSchedules(new ArrayList<TestSchedule>());
    cell.setSeatRowNumber(10);
    cell.setCellType(CellType.SEAT);
    cell.setSeatColumnNumber(10);

    TestSchedule testSchedule = new TestSchedule();
    testSchedule.setAssignment(assignment);
    testSchedule.setStudent(termStudent);
    testSchedule.setSeatNumber(10);
    testSchedule.setOrderNumber(10);
    testSchedule.setPdfPath("Pdf Path");
    testSchedule.setTermRun(termRun);
    testSchedule.setCell(cell);

    ArrayList<TestSchedule> testScheduleList = new ArrayList<TestSchedule>();
    testScheduleList.add(testSchedule);
    when(this.testScheduleRepository.findTestSchedulesInRoom((Room) any())).thenReturn(testScheduleList);
    assertTrue(this.testScheduleServiceImpl.isAnyStudentSeatedInRoom(new Room()));
    verify(this.testScheduleRepository).findTestSchedulesInRoom((Room) any());
  }

  @Test
  public void testFindTestScheduleById() {
    Assignment assignment = new Assignment();
    assignment.setAssignmentManagers(new ArrayList<AppUser>());
    assignment.setTermRuns(new ArrayList<TermRun>());
    assignment.setName("Name");
    assignment.setTestSchedules(new ArrayList<TestSchedule>());
    assignment.setStatus(AssignmentStatus.UPLOADED);

    TermStudent termStudent = new TermStudent();
    termStudent.setLogin("Login");
    termStudent.setNameWithDegrees("Name With Degrees");

    Course course = new Course();
    course.setAbbreviation("Abbreviation");
    course.setAcademicYear(1);
    course.setName("Name");
    course.setTerms(new ArrayList<Term>());

    Term term = new Term();
    term.setDistribution(Distribution.RANDOM);
    term.setCourseReference(course);
    term.setTermRuns(new ArrayList<TermRun>());
    term.setTermName("Term Name");
    term.setTermManagers(new ArrayList<AppUser>());
    term.setDescription("The characteristics of someone or something");
    term.setSpacingBetweenStudents(SpacingBetweenStudents.ZERO);

    TermRun termRun = new TermRun();
    termRun.setTermReference(term);
    termRun.setRoomPlans(new ArrayList<RoomPlan>());
    termRun.setStartDateTime(mock(Timestamp.class));
    termRun.setEndDateTime(mock(Timestamp.class));
    termRun.setTestSchedules(new ArrayList<TestSchedule>());
    termRun.setAssignments(new ArrayList<Assignment>());

    Room room = new Room();
    room.setAppUserReference(new AppUser());
    room.setAbbreviation("Abbreviation");
    room.setRoomPlans(new ArrayList<RoomPlan>());
    room.setNumberOfColumns(10);
    room.setName("Name");
    room.setDescription("The characteristics of someone or something");
    room.setNumberOfRows(10);

    RoomPlan roomPlan = new RoomPlan();
    roomPlan.setCells(new ArrayList<Cell>());
    roomPlan.setCapacity(3);
    roomPlan.setTermRuns(new ArrayList<TermRun>());
    roomPlan.setSpacing(SpacingBetweenStudents.ZERO);
    roomPlan.setRoomReference(room);

    Cell cell = new Cell();
    cell.setColumnNumber(10);
    cell.setRoomPlanReference(roomPlan);
    cell.setRowNumber(10);
    cell.setTestSchedules(new ArrayList<TestSchedule>());
    cell.setSeatRowNumber(10);
    cell.setCellType(CellType.SEAT);
    cell.setSeatColumnNumber(10);

    TestSchedule testSchedule = new TestSchedule();
    testSchedule.setAssignment(assignment);
    testSchedule.setStudent(termStudent);
    testSchedule.setSeatNumber(10);
    testSchedule.setOrderNumber(10);
    testSchedule.setPdfPath("Pdf Path");
    testSchedule.setTermRun(termRun);
    testSchedule.setCell(cell);
    Optional<TestSchedule> ofResult = Optional.<TestSchedule>of(testSchedule);
    when(this.testScheduleRepository.findById((Integer) any())).thenReturn(ofResult);
    Optional<TestSchedule> actualFindTestScheduleByIdResult = this.testScheduleServiceImpl.findTestScheduleById(123);
    assertSame(ofResult, actualFindTestScheduleByIdResult);
    assertTrue(actualFindTestScheduleByIdResult.isPresent());
    verify(this.testScheduleRepository).findById((Integer) any());
  }

  @Test
  public void testUpdatePdfPath() {
    Assignment assignment = new Assignment();
    assignment.setAssignmentManagers(new ArrayList<AppUser>());
    assignment.setTermRuns(new ArrayList<TermRun>());
    assignment.setName("Name");
    assignment.setTestSchedules(new ArrayList<TestSchedule>());
    assignment.setStatus(AssignmentStatus.UPLOADED);

    TermStudent termStudent = new TermStudent();
    termStudent.setLogin("Login");
    termStudent.setNameWithDegrees("Name With Degrees");

    Course course = new Course();
    course.setAbbreviation("Abbreviation");
    course.setAcademicYear(1);
    course.setName("Name");
    course.setTerms(new ArrayList<Term>());

    Term term = new Term();
    term.setDistribution(Distribution.RANDOM);
    term.setCourseReference(course);
    term.setTermRuns(new ArrayList<TermRun>());
    term.setTermName("Term Name");
    term.setTermManagers(new ArrayList<AppUser>());
    term.setDescription("The characteristics of someone or something");
    term.setSpacingBetweenStudents(SpacingBetweenStudents.ZERO);

    TermRun termRun = new TermRun();
    termRun.setTermReference(term);
    termRun.setRoomPlans(new ArrayList<RoomPlan>());
    termRun.setStartDateTime(mock(Timestamp.class));
    termRun.setEndDateTime(mock(Timestamp.class));
    termRun.setTestSchedules(new ArrayList<TestSchedule>());
    termRun.setAssignments(new ArrayList<Assignment>());

    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());

    Room room = new Room();
    room.setAppUserReference(appUser);
    room.setAbbreviation("Abbreviation");
    room.setRoomPlans(new ArrayList<RoomPlan>());
    room.setNumberOfColumns(10);
    room.setName("Name");
    room.setDescription("The characteristics of someone or something");
    room.setNumberOfRows(10);

    RoomPlan roomPlan = new RoomPlan();
    roomPlan.setCells(new ArrayList<Cell>());
    roomPlan.setCapacity(3);
    roomPlan.setTermRuns(new ArrayList<TermRun>());
    roomPlan.setSpacing(SpacingBetweenStudents.ZERO);
    roomPlan.setRoomReference(room);

    Cell cell = new Cell();
    cell.setColumnNumber(10);
    cell.setRoomPlanReference(roomPlan);
    cell.setRowNumber(10);
    cell.setTestSchedules(new ArrayList<TestSchedule>());
    cell.setSeatRowNumber(10);
    cell.setCellType(CellType.SEAT);
    cell.setSeatColumnNumber(10);

    TestSchedule testSchedule = new TestSchedule();
    testSchedule.setAssignment(assignment);
    testSchedule.setStudent(termStudent);
    testSchedule.setSeatNumber(10);
    testSchedule.setOrderNumber(10);
    testSchedule.setPdfPath("Pdf Path");
    testSchedule.setTermRun(termRun);
    testSchedule.setCell(cell);
    when(this.testScheduleRepository.save((TestSchedule) any())).thenReturn(testSchedule);
    TestSchedule testSchedule1 = new TestSchedule();
    this.testScheduleServiceImpl.updatePdfPath(testSchedule1, "Pdf Path");
    verify(this.testScheduleRepository).save((TestSchedule) any());
    assertEquals("Pdf Path", testSchedule1.getPdfPath());
  }

  @Test
  public void testUpdatePdfPath2() {
    when(this.testScheduleRepository.save((TestSchedule) any()))
      .thenThrow(new BadRequestException("An error occurred"));
    TestSchedule testSchedule = mock(TestSchedule.class);
    doNothing().when(testSchedule).setPdfPath(anyString());
    assertThrows(BadRequestException.class, () -> this.testScheduleServiceImpl.updatePdfPath(testSchedule, "Pdf Path"));
    verify(this.testScheduleRepository).save((TestSchedule) any());
    verify(testSchedule).setPdfPath(anyString());
  }

  @Test
  public void testFindAllTestSchedulesInTerm() {
    ArrayList<TestSchedule> testScheduleList = new ArrayList<TestSchedule>();
    when(this.testScheduleRepository.findAllByTermRun_TermReference_IdTerm(anyInt())).thenReturn(testScheduleList);
    List<TestSchedule> actualFindAllTestSchedulesInTermResult = this.testScheduleServiceImpl
      .findAllTestSchedulesInTerm(123);
    assertSame(testScheduleList, actualFindAllTestSchedulesInTermResult);
    assertTrue(actualFindAllTestSchedulesInTermResult.isEmpty());
    verify(this.testScheduleRepository).findAllByTermRun_TermReference_IdTerm(anyInt());
  }

  @Test
  public void testGetRoomAbbreviationFromTestSchedule() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());

    Room room = new Room();
    room.setAppUserReference(appUser);
    room.setAbbreviation("Abbreviation");
    room.setRoomPlans(new ArrayList<RoomPlan>());
    room.setNumberOfColumns(10);
    room.setName("Name");
    room.setDescription("The characteristics of someone or something");
    room.setNumberOfRows(10);

    RoomPlan roomPlan = new RoomPlan();
    roomPlan.setCells(new ArrayList<Cell>());
    roomPlan.setCapacity(3);
    roomPlan.setTermRuns(new ArrayList<TermRun>());
    roomPlan.setSpacing(SpacingBetweenStudents.ZERO);
    roomPlan.setRoomReference(room);

    Cell cell = new Cell();
    cell.setColumnNumber(10);
    cell.setRoomPlanReference(roomPlan);
    cell.setRowNumber(10);
    cell.setTestSchedules(new ArrayList<TestSchedule>());
    cell.setSeatRowNumber(10);
    cell.setCellType(CellType.SEAT);
    cell.setSeatColumnNumber(10);

    TestSchedule testSchedule = new TestSchedule();
    testSchedule.setCell(cell);
    assertEquals("Abbreviation", this.testScheduleServiceImpl.getRoomAbbreviationFromTestSchedule(testSchedule));
  }

  @Test
  public void testGetRoomAbbreviationFromTestSchedule2() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());

    Room room = new Room();
    room.setAppUserReference(appUser);
    room.setAbbreviation("Abbreviation");
    room.setRoomPlans(new ArrayList<RoomPlan>());
    room.setNumberOfColumns(10);
    room.setName("Name");
    room.setDescription("The characteristics of someone or something");
    room.setNumberOfRows(10);

    RoomPlan roomPlan = new RoomPlan();
    roomPlan.setCells(new ArrayList<Cell>());
    roomPlan.setCapacity(3);
    roomPlan.setTermRuns(new ArrayList<TermRun>());
    roomPlan.setSpacing(SpacingBetweenStudents.ZERO);
    roomPlan.setRoomReference(room);

    Cell cell = new Cell();
    cell.setColumnNumber(10);
    cell.setRoomPlanReference(roomPlan);
    cell.setRowNumber(10);
    cell.setTestSchedules(new ArrayList<TestSchedule>());
    cell.setSeatRowNumber(10);
    cell.setCellType(CellType.SEAT);
    cell.setSeatColumnNumber(10);
    TestSchedule testSchedule = mock(TestSchedule.class);
    when(testSchedule.getCell()).thenReturn(cell);
    assertEquals("Abbreviation", this.testScheduleServiceImpl.getRoomAbbreviationFromTestSchedule(testSchedule));
    verify(testSchedule).getCell();
  }

  @Test
  public void testGetTermRunDir() {
    when(this.storageManager.getTermDirPath(anyInt())).thenReturn("foo");

    Course course = new Course();
    course.setAbbreviation("Abbreviation");
    course.setAcademicYear(0);
    course.setName("Name");
    course.setTerms(new ArrayList<Term>());

    Term term = new Term();
    term.setDistribution(Distribution.RANDOM);
    term.setCourseReference(course);
    term.setTermRuns(new ArrayList<TermRun>());
    term.setTermName("Term Name");
    term.setTermManagers(new ArrayList<AppUser>());
    term.setDescription("The characteristics of someone or something");
    term.setSpacingBetweenStudents(SpacingBetweenStudents.ZERO);

    TermRun termRun = new TermRun();
    termRun.setTermReference(term);
    termRun.setRoomPlans(new ArrayList<RoomPlan>());
    termRun.setStartDateTime(mock(Timestamp.class));
    termRun.setEndDateTime(mock(Timestamp.class));
    termRun.setTestSchedules(new ArrayList<TestSchedule>());
    termRun.setAssignments(new ArrayList<Assignment>());

    TestSchedule testSchedule = new TestSchedule();
    testSchedule.setTermRun(termRun);
    assertEquals("foo" + File.separator + "term-run-0", this.testScheduleServiceImpl.getTermRunDir(testSchedule));
    verify(this.storageManager).getTermDirPath(anyInt());
  }

  @Test
  public void testGetRoomPlanDir() {
    assertEquals("Term Run Dir" + File.separator + "Room Abbreviation",
      this.testScheduleServiceImpl.getRoomPlanDir("Term Run Dir", "Room Abbreviation"));
  }

  @Test
  public void testSaveAll() {
    when(this.testScheduleRepository.saveAll((Iterable<TestSchedule>) any())).thenReturn(new ArrayList<TestSchedule>());
    this.testScheduleServiceImpl.saveAll(new ArrayList<TestSchedule>());
    verify(this.testScheduleRepository).saveAll((Iterable<TestSchedule>) any());
  }

  @Test
  public void testGetLogFileContent() {
    when(this.testScheduleRepository.findById((Integer) any())).thenThrow(new BadRequestException("An error occurred"));
    assertThrows(BadRequestException.class, () -> this.testScheduleServiceImpl.getLogFileContent(123));
    verify(this.testScheduleRepository).findById((Integer) any());
  }

  @Test
  public void testDownloadStudentsCsv() {
    when(this.testScheduleRepository.findAllTestSchedulesInTermRunsRoomPlan(anyInt(), anyString()))
      .thenReturn(new ArrayList<TestSchedule>());

    Course course = new Course();
    course.setAbbreviation("Abbreviation");
    course.setAcademicYear(1);
    course.setName("Name");
    course.setTerms(new ArrayList<Term>());

    Term term = new Term();
    term.setDistribution(Distribution.RANDOM);
    term.setCourseReference(course);
    term.setTermRuns(new ArrayList<TermRun>());
    term.setTermName("Term Name");
    term.setTermManagers(new ArrayList<AppUser>());
    term.setDescription("The characteristics of someone or something");
    term.setSpacingBetweenStudents(SpacingBetweenStudents.ZERO);
    Timestamp timestamp = mock(Timestamp.class);
    when(timestamp.getTime()).thenReturn(1L);

    TermRun termRun = new TermRun();
    termRun.setTermReference(term);
    termRun.setRoomPlans(new ArrayList<RoomPlan>());
    termRun.setStartDateTime(timestamp);
    termRun.setEndDateTime(mock(Timestamp.class));
    termRun.setTestSchedules(new ArrayList<TestSchedule>());
    termRun.setAssignments(new ArrayList<Assignment>());
    Optional<TermRun> ofResult = Optional.<TermRun>of(termRun);
    when(this.termRunService.findTermRunById(anyInt())).thenReturn(ofResult);
    ResponseEntity<Resource> actualDownloadStudentsCsvResult = this.testScheduleServiceImpl.downloadStudentsCsv(123,
      123, "Room Abbreviation");
    assertEquals(
      "<200 OK OK,Byte array resource [resource loaded from byte array],[Content-Type:\"text/csv\", Content"
        + "-Disposition:\"attachment; filename=\"beh-01-01-1970-01-00_mistnost-Room Abbreviation.csv\"\"]>",
      actualDownloadStudentsCsvResult.toString());
    assertTrue(actualDownloadStudentsCsvResult.hasBody());
    assertEquals(2, actualDownloadStudentsCsvResult.getHeaders().size());
    assertEquals(HttpStatus.OK, actualDownloadStudentsCsvResult.getStatusCode());
    Resource body = actualDownloadStudentsCsvResult.getBody();
    assertEquals("Byte array resource [resource loaded from byte array]", body.toString());
    assertEquals(50, ((ByteArrayResource) body).getByteArray().length);
    verify(this.testScheduleRepository).findAllTestSchedulesInTermRunsRoomPlan(anyInt(), anyString());
    verify(this.termRunService).findTermRunById(anyInt());
    verify(timestamp).getTime();
  }

  @Test
  public void testDownloadStudentsCsv2() {
    Assignment assignment = new Assignment();
    assignment.setAssignmentManagers(new ArrayList<AppUser>());
    assignment.setTermRuns(new ArrayList<TermRun>());
    assignment.setName("dd-MM-yyyy-HH-mm");
    assignment.setTestSchedules(new ArrayList<TestSchedule>());
    assignment.setStatus(null);

    TermStudent termStudent = new TermStudent();
    termStudent.setLogin("dd-MM-yyyy-HH-mm");
    termStudent.setNameWithDegrees("dd-MM-yyyy-HH-mm");

    Course course = new Course();
    course.setAbbreviation("dd-MM-yyyy-HH-mm");
    course.setAcademicYear(0);
    course.setName("dd-MM-yyyy-HH-mm");
    course.setTerms(new ArrayList<Term>());

    Term term = new Term();
    term.setDistribution(null);
    term.setCourseReference(course);
    term.setTermRuns(new ArrayList<TermRun>());
    term.setTermName("dd-MM-yyyy-HH-mm");
    term.setTermManagers(new ArrayList<AppUser>());
    term.setDescription("The characteristics of someone or something");
    term.setSpacingBetweenStudents(null);

    TermRun termRun = new TermRun();
    termRun.setTermReference(term);
    termRun.setRoomPlans(new ArrayList<RoomPlan>());
    termRun.setStartDateTime(mock(Timestamp.class));
    termRun.setEndDateTime(mock(Timestamp.class));
    termRun.setTestSchedules(new ArrayList<TestSchedule>());
    termRun.setAssignments(new ArrayList<Assignment>());

    Room room = new Room();
    room.setAppUserReference(new AppUser());
    room.setAbbreviation("dd-MM-yyyy-HH-mm");
    room.setRoomPlans(new ArrayList<RoomPlan>());
    room.setNumberOfColumns(10);
    room.setName("dd-MM-yyyy-HH-mm");
    room.setDescription("The characteristics of someone or something");
    room.setNumberOfRows(10);

    RoomPlan roomPlan = new RoomPlan();
    roomPlan.setCells(new ArrayList<Cell>());
    roomPlan.setCapacity(3);
    roomPlan.setTermRuns(new ArrayList<TermRun>());
    roomPlan.setSpacing(null);
    roomPlan.setRoomReference(room);

    Cell cell = new Cell();
    cell.setColumnNumber(10);
    cell.setRoomPlanReference(roomPlan);
    cell.setRowNumber(10);
    cell.setTestSchedules(new ArrayList<TestSchedule>());
    cell.setSeatRowNumber(10);
    cell.setCellType(null);
    cell.setSeatColumnNumber(10);

    TestSchedule testSchedule = new TestSchedule();
    testSchedule.setAssignment(assignment);
    testSchedule.setStudent(termStudent);
    testSchedule.setSeatNumber(10);
    testSchedule.setOrderNumber(10);
    testSchedule.setPdfPath("dd-MM-yyyy-HH-mm");
    testSchedule.setTermRun(termRun);
    testSchedule.setCell(cell);

    ArrayList<TestSchedule> testScheduleList = new ArrayList<TestSchedule>();
    testScheduleList.add(testSchedule);
    when(this.testScheduleRepository.findAllTestSchedulesInTermRunsRoomPlan(anyInt(), anyString()))
      .thenReturn(testScheduleList);

    Course course1 = new Course();
    course1.setAbbreviation("Abbreviation");
    course1.setAcademicYear(1);
    course1.setName("Name");
    course1.setTerms(new ArrayList<Term>());

    Term term1 = new Term();
    term1.setDistribution(Distribution.RANDOM);
    term1.setCourseReference(course1);
    term1.setTermRuns(new ArrayList<TermRun>());
    term1.setTermName("Term Name");
    term1.setTermManagers(new ArrayList<AppUser>());
    term1.setDescription("The characteristics of someone or something");
    term1.setSpacingBetweenStudents(SpacingBetweenStudents.ZERO);
    Timestamp timestamp = mock(Timestamp.class);
    when(timestamp.getTime()).thenReturn(1L);

    TermRun termRun1 = new TermRun();
    termRun1.setTermReference(term1);
    termRun1.setRoomPlans(new ArrayList<RoomPlan>());
    termRun1.setStartDateTime(timestamp);
    termRun1.setEndDateTime(mock(Timestamp.class));
    termRun1.setTestSchedules(new ArrayList<TestSchedule>());
    termRun1.setAssignments(new ArrayList<Assignment>());
    Optional<TermRun> ofResult = Optional.<TermRun>of(termRun1);
    when(this.termRunService.findTermRunById(anyInt())).thenReturn(ofResult);
    ResponseEntity<Resource> actualDownloadStudentsCsvResult = this.testScheduleServiceImpl.downloadStudentsCsv(123,
      123, "Room Abbreviation");
    assertEquals(
      "<200 OK OK,Byte array resource [resource loaded from byte array],[Content-Type:\"text/csv\", Content"
        + "-Disposition:\"attachment; filename=\"beh-01-01-1970-01-00_mistnost-Room Abbreviation.csv\"\"]>",
      actualDownloadStudentsCsvResult.toString());
    assertTrue(actualDownloadStudentsCsvResult.hasBody());
    assertEquals(2, actualDownloadStudentsCsvResult.getHeaders().size());
    assertEquals(HttpStatus.OK, actualDownloadStudentsCsvResult.getStatusCode());
    Resource body = actualDownloadStudentsCsvResult.getBody();
    assertEquals("Byte array resource [resource loaded from byte array]", body.toString());
    assertEquals(90, ((ByteArrayResource) body).getByteArray().length);
    verify(this.testScheduleRepository).findAllTestSchedulesInTermRunsRoomPlan(anyInt(), anyString());
    verify(this.termRunService).findTermRunById(anyInt());
    verify(timestamp).getTime();
  }

  @Test
  public void testFindLastOrderNumber() {
    when(this.testScheduleRepository.findTermLastOrderNumber((Term) any())).thenReturn(1);
    assertEquals(1, this.testScheduleServiceImpl.findLastOrderNumber(new Term()));
    verify(this.testScheduleRepository).findTermLastOrderNumber((Term) any());
  }
}

