package cz.vutbr.fit.exampreparation.features.token;

import cz.vutbr.fit.exampreparation.features.appUser.AppUser;
import cz.vutbr.fit.exampreparation.features.assignment.Assignment;
import cz.vutbr.fit.exampreparation.features.role.Role;
import cz.vutbr.fit.exampreparation.features.room.Room;
import cz.vutbr.fit.exampreparation.features.term.Term;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class TokenTest {
  @Test
  public void testConstructor() {
    Token actualToken = new Token();
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());
    actualToken.setAppUserReference(appUser);
    actualToken.setExpireAt(null);
    actualToken.setRemember(true);
    actualToken.setToken("ABC123");
    actualToken.setTokenType(TokenType.VERIFICATION);
    assertSame(appUser, actualToken.getAppUserReference());
    assertNull(actualToken.getExpireAt());
    assertEquals(0, actualToken.getIdToken());
    assertEquals("ABC123", actualToken.getToken());
    assertEquals(TokenType.VERIFICATION, actualToken.getTokenType());
    assertTrue(actualToken.isRemember());
  }
}

