package cz.vutbr.fit.exampreparation.features.assignment.helpers;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class StudentInfoTest {
  @Test
  public void testCanEqual() {
    assertFalse((new StudentInfo("Login", "Student Name", "Term", "2020-03-01", 10, "Room Abbreviation", 10, 10, 10))
      .canEqual("Other"));
  }

  @Test
  public void testCanEqual2() {
    StudentInfo studentInfo = new StudentInfo("Login", "Student Name", "Term", "2020-03-01", 10, "Room Abbreviation",
      10, 10, 10);
    assertTrue(studentInfo
      .canEqual(new StudentInfo("Login", "Student Name", "Term", "2020-03-01", 10, "Room Abbreviation", 10, 10, 10)));
  }

  @Test
  public void testConstructor() {
    StudentInfo actualStudentInfo = new StudentInfo();
    actualStudentInfo.setColumnNumber(10);
    actualStudentInfo.setDate("2020-03-01");
    actualStudentInfo.setLogin("Login");
    actualStudentInfo.setRoomAbbreviation("Room Abbreviation");
    actualStudentInfo.setRowNumber(10);
    actualStudentInfo.setSeatNumber(10);
    actualStudentInfo.setStudentName("Student Name");
    actualStudentInfo.setStudentOrderNumber(10);
    actualStudentInfo.setTerm("Term");
    assertEquals(10, actualStudentInfo.getColumnNumber());
    assertEquals("2020-03-01", actualStudentInfo.getDate());
    assertEquals("Login", actualStudentInfo.getLogin());
    assertEquals("Room Abbreviation", actualStudentInfo.getRoomAbbreviation());
    assertEquals(10, actualStudentInfo.getRowNumber());
    assertEquals(10, actualStudentInfo.getSeatNumber());
    assertEquals("Student Name", actualStudentInfo.getStudentName());
    assertEquals(10, actualStudentInfo.getStudentOrderNumber());
    assertEquals("Term", actualStudentInfo.getTerm());
    assertEquals(
      "StudentInfo(login=Login, studentName=Student Name, term=Term, date=2020-03-01, studentOrderNumber=10,"
        + " roomAbbreviation=Room Abbreviation, seatNumber=10, rowNumber=10, columnNumber=10)",
      actualStudentInfo.toString());
  }

  @Test
  public void testConstructor2() {
    StudentInfo actualStudentInfo = new StudentInfo("Login", "Student Name", "Term", "2020-03-01", 10,
      "Room Abbreviation", 10, 10, 10);
    actualStudentInfo.setColumnNumber(10);
    actualStudentInfo.setDate("2020-03-01");
    actualStudentInfo.setLogin("Login");
    actualStudentInfo.setRoomAbbreviation("Room Abbreviation");
    actualStudentInfo.setRowNumber(10);
    actualStudentInfo.setSeatNumber(10);
    actualStudentInfo.setStudentName("Student Name");
    actualStudentInfo.setStudentOrderNumber(10);
    actualStudentInfo.setTerm("Term");
    assertEquals(10, actualStudentInfo.getColumnNumber());
    assertEquals("2020-03-01", actualStudentInfo.getDate());
    assertEquals("Login", actualStudentInfo.getLogin());
    assertEquals("Room Abbreviation", actualStudentInfo.getRoomAbbreviation());
    assertEquals(10, actualStudentInfo.getRowNumber());
    assertEquals(10, actualStudentInfo.getSeatNumber());
    assertEquals("Student Name", actualStudentInfo.getStudentName());
    assertEquals(10, actualStudentInfo.getStudentOrderNumber());
    assertEquals("Term", actualStudentInfo.getTerm());
    assertEquals(
      "StudentInfo(login=Login, studentName=Student Name, term=Term, date=2020-03-01, studentOrderNumber=10,"
        + " roomAbbreviation=Room Abbreviation, seatNumber=10, rowNumber=10, columnNumber=10)",
      actualStudentInfo.toString());
  }

  @Test
  public void testEquals() {
    assertNotEquals((new StudentInfo("Login", "Student Name", "Term", "2020-03-01", 10, "Room Abbreviation", 10, 10, 10)), null);
    assertNotEquals((new StudentInfo("Login", "Student Name", "Term", "2020-03-01", 10, "Room Abbreviation", 10, 10, 10)), "Different type to StudentInfo");
  }

  @Test
  public void testEquals10() {
    StudentInfo studentInfo = new StudentInfo("Login", "Student Name", "Term", "2020/03/01", 10, "Room Abbreviation",
      10, 10, 10);
    assertNotEquals(new StudentInfo("Login", "Student Name", "Term", "2020-03-01", 10, "Room Abbreviation", 10, 10, 10), studentInfo);
  }

  @Test
  public void testEquals11() {
    StudentInfo studentInfo = new StudentInfo("Login", "Student Name", "Term", null, 10, "Room Abbreviation", 10, 10,
      10);
    assertNotEquals(new StudentInfo("Login", "Student Name", "Term", "2020-03-01", 10, "Room Abbreviation", 10, 10, 10), studentInfo);
  }

  @Test
  public void testEquals12() {
    StudentInfo studentInfo = new StudentInfo("Login", "Student Name", "Term", "2020-03-01", 0, "Room Abbreviation", 10,
      10, 10);
    assertNotEquals(new StudentInfo("Login", "Student Name", "Term", "2020-03-01", 10, "Room Abbreviation", 10, 10, 10), studentInfo);
  }

  @Test
  public void testEquals13() {
    StudentInfo studentInfo = new StudentInfo("Login", "Student Name", "Term", "2020-03-01", 10, "Login", 10, 10, 10);
    assertNotEquals(new StudentInfo("Login", "Student Name", "Term", "2020-03-01", 10, "Room Abbreviation", 10, 10, 10), studentInfo);
  }

  @Test
  public void testEquals14() {
    StudentInfo studentInfo = new StudentInfo("Login", "Student Name", "Term", "2020-03-01", 10, null, 10, 10, 10);
    assertNotEquals(new StudentInfo("Login", "Student Name", "Term", "2020-03-01", 10, "Room Abbreviation", 10, 10, 10), studentInfo);
  }

  @Test
  public void testEquals15() {
    StudentInfo studentInfo = new StudentInfo("Login", "Student Name", "Term", "2020-03-01", 10, "Room Abbreviation", 0,
      10, 10);
    assertNotEquals(new StudentInfo("Login", "Student Name", "Term", "2020-03-01", 10, "Room Abbreviation", 10, 10, 10), studentInfo);
  }

  @Test
  public void testEquals16() {
    StudentInfo studentInfo = new StudentInfo("Login", "Student Name", "Term", "2020-03-01", 10, "Room Abbreviation",
      10, 0, 10);
    assertNotEquals(new StudentInfo("Login", "Student Name", "Term", "2020-03-01", 10, "Room Abbreviation", 10, 10, 10), studentInfo);
  }

  @Test
  public void testEquals17() {
    StudentInfo studentInfo = new StudentInfo("Login", "Student Name", "Term", "2020-03-01", 10, "Room Abbreviation",
      10, 10, 0);
    assertNotEquals(new StudentInfo("Login", "Student Name", "Term", "2020-03-01", 10, "Room Abbreviation", 10, 10, 10), studentInfo);
  }

  @Test
  public void testEquals18() {
    StudentInfo studentInfo = new StudentInfo(null, "Student Name", "Term", "2020-03-01", 10, "Room Abbreviation", 10,
      10, 10);
    StudentInfo studentInfo1 = new StudentInfo(null, "Student Name", "Term", "2020-03-01", 10, "Room Abbreviation", 10,
      10, 10);

    assertEquals(studentInfo1, studentInfo);
    int expectedHashCodeResult = studentInfo.hashCode();
    assertEquals(expectedHashCodeResult, studentInfo1.hashCode());
  }

  @Test
  public void testEquals19() {
    StudentInfo studentInfo = new StudentInfo("Login", null, "Term", "2020-03-01", 10, "Room Abbreviation", 10, 10, 10);
    StudentInfo studentInfo1 = new StudentInfo("Login", null, "Term", "2020-03-01", 10, "Room Abbreviation", 10, 10,
      10);

    assertEquals(studentInfo1, studentInfo);
    int expectedHashCodeResult = studentInfo.hashCode();
    assertEquals(expectedHashCodeResult, studentInfo1.hashCode());
  }

  @Test
  public void testEquals2() {
    StudentInfo studentInfo = new StudentInfo("Login", "Student Name", "Term", "2020-03-01", 10, "Room Abbreviation",
      10, 10, 10);
    assertEquals(studentInfo, studentInfo);
    int expectedHashCodeResult = studentInfo.hashCode();
    assertEquals(expectedHashCodeResult, studentInfo.hashCode());
  }

  @Test
  public void testEquals20() {
    StudentInfo studentInfo = new StudentInfo("Login", "Student Name", null, "2020-03-01", 10, "Room Abbreviation", 10,
      10, 10);
    StudentInfo studentInfo1 = new StudentInfo("Login", "Student Name", null, "2020-03-01", 10, "Room Abbreviation", 10,
      10, 10);

    assertEquals(studentInfo1, studentInfo);
    int expectedHashCodeResult = studentInfo.hashCode();
    assertEquals(expectedHashCodeResult, studentInfo1.hashCode());
  }

  @Test
  public void testEquals3() {
    StudentInfo studentInfo = new StudentInfo("Login", "Student Name", "Term", "2020-03-01", 10, "Room Abbreviation",
      10, 10, 10);
    StudentInfo studentInfo1 = new StudentInfo("Login", "Student Name", "Term", "2020-03-01", 10, "Room Abbreviation",
      10, 10, 10);

    assertEquals(studentInfo1, studentInfo);
    int expectedHashCodeResult = studentInfo.hashCode();
    assertEquals(expectedHashCodeResult, studentInfo1.hashCode());
  }

  @Test
  public void testEquals4() {
    StudentInfo studentInfo = new StudentInfo(null, "Student Name", "Term", "2020-03-01", 10, "Room Abbreviation", 10,
      10, 10);
    assertNotEquals(new StudentInfo("Login", "Student Name", "Term", "2020-03-01", 10, "Room Abbreviation", 10, 10, 10), studentInfo);
  }

  @Test
  public void testEquals5() {
    StudentInfo studentInfo = new StudentInfo("Student Name", "Student Name", "Term", "2020-03-01", 10,
      "Room Abbreviation", 10, 10, 10);
    assertNotEquals(new StudentInfo("Login", "Student Name", "Term", "2020-03-01", 10, "Room Abbreviation", 10, 10, 10), studentInfo);
  }

  @Test
  public void testEquals6() {
    StudentInfo studentInfo = new StudentInfo("Login", "Login", "Term", "2020-03-01", 10, "Room Abbreviation", 10, 10,
      10);
    assertNotEquals(new StudentInfo("Login", "Student Name", "Term", "2020-03-01", 10, "Room Abbreviation", 10, 10, 10), studentInfo);
  }

  @Test
  public void testEquals7() {
    StudentInfo studentInfo = new StudentInfo("Login", null, "Term", "2020-03-01", 10, "Room Abbreviation", 10, 10, 10);
    assertNotEquals(new StudentInfo("Login", "Student Name", "Term", "2020-03-01", 10, "Room Abbreviation", 10, 10, 10), studentInfo);
  }

  @Test
  public void testEquals8() {
    StudentInfo studentInfo = new StudentInfo("Login", "Student Name", "Login", "2020-03-01", 10, "Room Abbreviation",
      10, 10, 10);
    assertNotEquals(new StudentInfo("Login", "Student Name", "Term", "2020-03-01", 10, "Room Abbreviation", 10, 10, 10), studentInfo);
  }

  @Test
  public void testEquals9() {
    StudentInfo studentInfo = new StudentInfo("Login", "Student Name", null, "2020-03-01", 10, "Room Abbreviation", 10,
      10, 10);
    assertNotEquals(new StudentInfo("Login", "Student Name", "Term", "2020-03-01", 10, "Room Abbreviation", 10, 10, 10), studentInfo);
  }
}

