package cz.vutbr.fit.exampreparation.features.testSchedule;

import cz.vutbr.fit.exampreparation.features.appUser.AppUser;
import cz.vutbr.fit.exampreparation.features.assignment.Assignment;
import cz.vutbr.fit.exampreparation.features.cell.Cell;
import cz.vutbr.fit.exampreparation.features.termRun.TermRun;
import cz.vutbr.fit.exampreparation.features.termStudent.TermStudent;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class TestScheduleTest {
  @Test
  public void testConstructor() {
    TermStudent termStudent = new TermStudent("Login", "Name With Degrees");

    TermRun termRun = new TermRun();
    Cell cell = new Cell();
    TestSchedule actualTestSchedule = new TestSchedule(termStudent, termRun, cell, 10);

    assertNull(actualTestSchedule.getAssignment());
    TermRun termRun1 = actualTestSchedule.getTermRun();
    assertSame(termRun, termRun1);
    TermStudent student = actualTestSchedule.getStudent();
    assertSame(termStudent, student);
    Cell cell1 = actualTestSchedule.getCell();
    assertSame(cell, cell1);
    assertEquals(10, actualTestSchedule.getOrderNumber().intValue());
    assertNull(actualTestSchedule.getPdfPath());
    assertEquals(0, actualTestSchedule.getIdTestSchedule());
    assertNull(actualTestSchedule.getSeatNumber());
    assertNull(cell1.getRowNumber());
    assertNull(cell1.getRoomPlanReference());
    assertEquals(0, cell1.getIdCell());
    assertNull(cell1.getColumnNumber());
    assertNull(cell1.getCellType());
    assertEquals("Login", student.getLogin());
    assertEquals(0, student.getIdStudent());
    assertNull(termRun1.getTermReference());
    assertNull(termRun1.getStartDateTime());
    assertEquals(0, termRun1.getIdTermRun());
    assertNull(termRun1.getEndDateTime());
    assertTrue(termRun1.getAssignments().isEmpty());
    assertNull(cell1.getSeatRowNumber());
    assertEquals("Name With Degrees", student.getNameWithDegrees());
    assertNull(termRun1.getTestSchedules());
    assertNull(cell1.getSeatColumnNumber());
    assertNull(cell1.getTestSchedules());
    assertSame(student, termStudent);
    assertSame(termRun1, termRun);
    assertSame(cell1, cell);
  }

  @Test
  public void testConstructor2() {
    TermStudent termStudent = new TermStudent("Login", "Name With Degrees");

    TermRun termRun = new TermRun();
    Cell cell = new Cell();
    Assignment assignment = new Assignment();
    TestSchedule actualTestSchedule = new TestSchedule(termStudent, termRun, cell, 10, 10, assignment);

    Assignment assignment1 = actualTestSchedule.getAssignment();
    assertSame(assignment, assignment1);
    TermRun termRun1 = actualTestSchedule.getTermRun();
    assertSame(termRun, termRun1);
    Cell cell1 = actualTestSchedule.getCell();
    assertSame(cell, cell1);
    assertEquals(10, actualTestSchedule.getOrderNumber().intValue());
    assertNull(actualTestSchedule.getPdfPath());
    assertEquals(0, actualTestSchedule.getIdTestSchedule());
    assertEquals(10, actualTestSchedule.getSeatNumber().intValue());
    TermStudent student = actualTestSchedule.getStudent();
    assertSame(termStudent, student);
    assertNull(cell1.getRoomPlanReference());
    assertEquals("Name With Degrees", student.getNameWithDegrees());
    assertEquals("Login", student.getLogin());
    assertEquals(0, student.getIdStudent());
    assertEquals(0, cell1.getIdCell());
    assertNull(cell1.getColumnNumber());
    assertNull(cell1.getCellType());
    assertNull(termRun1.getTermReference());
    assertNull(termRun1.getStartDateTime());
    assertEquals(0, termRun1.getIdTermRun());
    assertNull(termRun1.getEndDateTime());
    List<Assignment> assignments = termRun1.getAssignments();
    assertTrue(assignments.isEmpty());
    List<TermRun> termRuns = assignment1.getTermRuns();
    assertEquals(assignments, termRuns);
    assertTrue(termRuns.isEmpty());
    assertNull(assignment1.getStatus());
    assertNull(assignment1.getName());
    assertEquals(0, assignment1.getIdAssignment());
    List<AppUser> assignmentManagers = assignment1.getAssignmentManagers();
    assertEquals(assignments, assignmentManagers);
    assertEquals(termRuns, assignmentManagers);
    assertTrue(assignmentManagers.isEmpty());
    assertNull(cell1.getSeatColumnNumber());
    assertNull(cell1.getSeatRowNumber());
    assertNull(termRun1.getTestSchedules());
    List<TestSchedule> testSchedules = assignment1.getTestSchedules();
    assertEquals(assignmentManagers, testSchedules);
    assertEquals(assignments, testSchedules);
    assertEquals(termRuns, testSchedules);
    assertTrue(testSchedules.isEmpty());
    assertNull(cell1.getRowNumber());
    assertNull(cell1.getTestSchedules());
    assertSame(student, termStudent);
    assertSame(termRun1, termRun);
    assertSame(cell1, cell);
    assertSame(assignment1, assignment);
  }
}

