package cz.vutbr.fit.exampreparation.features.testSchedule.dto;

import cz.vutbr.fit.exampreparation.features.assignment.Assignment;
import cz.vutbr.fit.exampreparation.features.assignment.dto.BasicAssignmentInfoDto;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestScheduleDetailDtoTest {
  @Test
  public void testConstructor() {
    TestScheduleDetailDto actualTestScheduleDetailDto = new TestScheduleDetailDto("Login", "Name And Degrees", 123, 10,
      10, new Assignment(), true);

    assertEquals(
      "TestScheduleDetailDto(testScheduleId=123, seatNumber=10, orderNumber=10, assignment=BasicAssignmentInfoDto"
        + "(assignmentId=0, name=null, status=null), isGenerated=true)",
      actualTestScheduleDetailDto.toString());
    assertEquals("Login", actualTestScheduleDetailDto.getLogin());
    assertEquals(10, actualTestScheduleDetailDto.getOrderNumber().intValue());
    assertEquals(10, actualTestScheduleDetailDto.getSeatNumber().intValue());
    assertEquals("Name And Degrees", actualTestScheduleDetailDto.getNameAndDegrees());
    assertTrue(actualTestScheduleDetailDto.isGenerated());
    assertEquals(123, actualTestScheduleDetailDto.getTestScheduleId().intValue());
    BasicAssignmentInfoDto assignment = actualTestScheduleDetailDto.getAssignment();
    assertNull(assignment.getStatus());
    assertEquals(0, assignment.getAssignmentId().intValue());
    assertNull(assignment.getName());
  }

  @Test
  public void testConstructor2() {
    TestScheduleDetailDto actualTestScheduleDetailDto = new TestScheduleDetailDto("Login", "Name And Degrees", 123, 10,
      10, null, true);

    assertNull(actualTestScheduleDetailDto.getAssignment());
    assertTrue(actualTestScheduleDetailDto.isGenerated());
    assertEquals(123, actualTestScheduleDetailDto.getTestScheduleId().intValue());
    assertEquals(10, actualTestScheduleDetailDto.getSeatNumber().intValue());
    assertEquals(10, actualTestScheduleDetailDto.getOrderNumber().intValue());
    assertEquals("Name And Degrees", actualTestScheduleDetailDto.getNameAndDegrees());
    assertEquals("Login", actualTestScheduleDetailDto.getLogin());
  }
}

