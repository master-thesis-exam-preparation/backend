package cz.vutbr.fit.exampreparation.features.termStudent.dto;

import cz.vutbr.fit.exampreparation.features.termStudent.TermStudent;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class TermStudentOrderDtoTest {
  @Test
  public void testCanEqual() {
    assertFalse((new TermStudentOrderDto()).canEqual("Other"));
  }

  @Test
  public void testCanEqual2() {
    TermStudentOrderDto termStudentOrderDto = new TermStudentOrderDto();
    assertTrue(termStudentOrderDto.canEqual(new TermStudentOrderDto()));
  }

  @Test
  public void testConstructor() {
    TermStudentOrderDto actualTermStudentOrderDto = new TermStudentOrderDto();
    actualTermStudentOrderDto.setOrderNumber(10);
    TermStudent termStudent = new TermStudent();
    termStudent.setLogin("Login");
    termStudent.setNameWithDegrees("Name With Degrees");
    actualTermStudentOrderDto.setStudent(termStudent);
    assertEquals(10, actualTermStudentOrderDto.getOrderNumber().intValue());
    assertSame(termStudent, actualTermStudentOrderDto.getStudent());
  }

  @Test
  public void testConstructor2() {
    TermStudentOrderDto actualTermStudentOrderDto = new TermStudentOrderDto(
      new TermStudent("Login", "Name With Degrees"), 10);
    actualTermStudentOrderDto.setOrderNumber(10);
    TermStudent termStudent = new TermStudent();
    termStudent.setLogin("Login");
    termStudent.setNameWithDegrees("Name With Degrees");
    actualTermStudentOrderDto.setStudent(termStudent);
    assertEquals(10, actualTermStudentOrderDto.getOrderNumber().intValue());
    assertSame(termStudent, actualTermStudentOrderDto.getStudent());
  }

  @Test
  public void testEquals() {
    assertNotEquals((new TermStudentOrderDto()), null);
    assertNotEquals((new TermStudentOrderDto()), "Different type to TermStudentOrderDto");
  }

  @Test
  public void testEquals2() {
    TermStudentOrderDto termStudentOrderDto = new TermStudentOrderDto();
    assertEquals(termStudentOrderDto, termStudentOrderDto);
    int expectedHashCodeResult = termStudentOrderDto.hashCode();
    assertEquals(expectedHashCodeResult, termStudentOrderDto.hashCode());
  }

  @Test
  public void testEquals3() {
    TermStudentOrderDto termStudentOrderDto = new TermStudentOrderDto();
    TermStudentOrderDto termStudentOrderDto1 = new TermStudentOrderDto();
    assertEquals(termStudentOrderDto1, termStudentOrderDto);
    int expectedHashCodeResult = termStudentOrderDto.hashCode();
    assertEquals(expectedHashCodeResult, termStudentOrderDto1.hashCode());
  }

  @Test
  public void testEquals4() {
    TermStudentOrderDto termStudentOrderDto = new TermStudentOrderDto(new TermStudent("Login", "Name With Degrees"),
      10);
    assertNotEquals(new TermStudentOrderDto(), termStudentOrderDto);
  }

  @Test
  public void testEquals5() {
    TermStudentOrderDto termStudentOrderDto = new TermStudentOrderDto();
    assertNotEquals(new TermStudentOrderDto(new TermStudent("Login", "Name With Degrees"), 10), termStudentOrderDto);
  }

  @Test
  public void testEquals6() {
    TermStudent termStudent = new TermStudent();
    termStudent.setLogin(null);
    termStudent.setNameWithDegrees(null);

    TermStudentOrderDto termStudentOrderDto = new TermStudentOrderDto();
    termStudentOrderDto.setStudent(termStudent);
    assertNotEquals(new TermStudentOrderDto(), termStudentOrderDto);
  }

  @Test
  public void testEquals7() {
    TermStudentOrderDto termStudentOrderDto = new TermStudentOrderDto(new TermStudent("Login", "Name With Degrees"),
      10);
    assertNotEquals(new TermStudentOrderDto(new TermStudent("Login", "Name With Degrees"), 10), termStudentOrderDto);
  }

  @Test
  public void testEquals8() {
    TermStudentOrderDto termStudentOrderDto = new TermStudentOrderDto();

    TermStudent termStudent = new TermStudent();
    termStudent.setLogin(null);
    termStudent.setNameWithDegrees(null);

    TermStudentOrderDto termStudentOrderDto1 = new TermStudentOrderDto();
    termStudentOrderDto1.setStudent(termStudent);
    assertNotEquals(termStudentOrderDto1, termStudentOrderDto);
  }

  @Test
  public void testEquals9() {
    TermStudentOrderDto termStudentOrderDto = new TermStudentOrderDto(mock(TermStudent.class), 10);
    assertNotEquals(new TermStudentOrderDto(new TermStudent("Login", "Name With Degrees"), 10), termStudentOrderDto);
  }
}

