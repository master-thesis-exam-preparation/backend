package cz.vutbr.fit.exampreparation.features.assignment.dto;

import cz.vutbr.fit.exampreparation.features.testSchedule.dto.TestScheduleDetailDto;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class GeneratingFailDtoTest {
  @Test
  public void testCanEqual() {
    assertFalse((new GeneratingFailDto(new TestScheduleDetailDto(), -1, "An error occurred", true, "/tmp/foo.txt"))
      .canEqual("Other"));
  }

  @Test
  public void testCanEqual2() {
    GeneratingFailDto generatingFailDto = new GeneratingFailDto(new TestScheduleDetailDto(), -1, "An error occurred",
      true, "/tmp/foo.txt");
    assertTrue(generatingFailDto
      .canEqual(new GeneratingFailDto(new TestScheduleDetailDto(), -1, "An error occurred", true, "/tmp/foo.txt")));
  }

  @Test
  public void testConstructor() {
    TestScheduleDetailDto testScheduleDetailDto = new TestScheduleDetailDto();
    GeneratingFailDto actualGeneratingFailDto = new GeneratingFailDto(testScheduleDetailDto, -1, "An error occurred",
      true, "/tmp/foo.txt");
    actualGeneratingFailDto.setErrorCode(-1);
    actualGeneratingFailDto.setErrorMessage("An error occurred");
    actualGeneratingFailDto.setHasLogFile(true);
    actualGeneratingFailDto.setLogFilePath("/tmp/foo.txt");
    TestScheduleDetailDto testScheduleDetailDto1 = new TestScheduleDetailDto();
    actualGeneratingFailDto.setTestSchedule(testScheduleDetailDto1);
    assertEquals(-1, actualGeneratingFailDto.getErrorCode().intValue());
    assertEquals("An error occurred", actualGeneratingFailDto.getErrorMessage());
    assertEquals("/tmp/foo.txt", actualGeneratingFailDto.getLogFilePath());
    TestScheduleDetailDto testSchedule = actualGeneratingFailDto.getTestSchedule();
    assertEquals(testScheduleDetailDto, testSchedule);
    assertSame(testScheduleDetailDto1, testSchedule);
    assertTrue(actualGeneratingFailDto.isHasLogFile());
    assertEquals(
      "GeneratingFailDto(testSchedule=TestScheduleDetailDto(testScheduleId=null, seatNumber=null, orderNumber=null,"
        + " assignment=null, isGenerated=false), errorCode=-1, errorMessage=An error occurred, hasLogFile=true,"
        + " logFilePath=/tmp/foo.txt)",
      actualGeneratingFailDto.toString());
  }

  @Test
  public void testEquals() {
    assertNotEquals((new GeneratingFailDto(new TestScheduleDetailDto(), -1, "An error occurred", true, "/tmp/foo.txt")), null);
    assertNotEquals((new GeneratingFailDto(new TestScheduleDetailDto(), -1, "An error occurred", true, "/tmp/foo.txt")), "Different type to GeneratingFailDto");
  }

  @Test
  public void testEquals10() {
    GeneratingFailDto generatingFailDto = new GeneratingFailDto(new TestScheduleDetailDto(), -1, "An error occurred",
      false, "/tmp/foo.txt");
    assertNotEquals(new GeneratingFailDto(new TestScheduleDetailDto(), -1, "An error occurred", true, "/tmp/foo.txt"), generatingFailDto);
  }

  @Test
  public void testEquals11() {
    GeneratingFailDto generatingFailDto = new GeneratingFailDto(new TestScheduleDetailDto(), -1, "An error occurred",
      true, "An error occurred");
    assertNotEquals(new GeneratingFailDto(new TestScheduleDetailDto(), -1, "An error occurred", true, "/tmp/foo.txt"), generatingFailDto);
  }

  @Test
  public void testEquals12() {
    GeneratingFailDto generatingFailDto = new GeneratingFailDto(new TestScheduleDetailDto(), -1, "An error occurred",
      true, null);
    assertNotEquals(new GeneratingFailDto(new TestScheduleDetailDto(), -1, "An error occurred", true, "/tmp/foo.txt"), generatingFailDto);
  }

  @Test
  public void testEquals13() {
    GeneratingFailDto generatingFailDto = new GeneratingFailDto(null, -1, "An error occurred", true, "/tmp/foo.txt");
    GeneratingFailDto generatingFailDto1 = new GeneratingFailDto(null, -1, "An error occurred", true, "/tmp/foo.txt");

    assertEquals(generatingFailDto1, generatingFailDto);
    int expectedHashCodeResult = generatingFailDto.hashCode();
    assertEquals(expectedHashCodeResult, generatingFailDto1.hashCode());
  }

  @Test
  public void testEquals14() {
    GeneratingFailDto generatingFailDto = new GeneratingFailDto(new TestScheduleDetailDto(), null, "An error occurred",
      true, "/tmp/foo.txt");
    GeneratingFailDto generatingFailDto1 = new GeneratingFailDto(new TestScheduleDetailDto(), null, "An error occurred",
      true, "/tmp/foo.txt");

    assertEquals(generatingFailDto1, generatingFailDto);
    int expectedHashCodeResult = generatingFailDto.hashCode();
    assertEquals(expectedHashCodeResult, generatingFailDto1.hashCode());
  }

  @Test
  public void testEquals15() {
    GeneratingFailDto generatingFailDto = new GeneratingFailDto(new TestScheduleDetailDto(), -1, null, true,
      "/tmp/foo.txt");
    GeneratingFailDto generatingFailDto1 = new GeneratingFailDto(new TestScheduleDetailDto(), -1, null, true,
      "/tmp/foo.txt");

    assertEquals(generatingFailDto1, generatingFailDto);
    int expectedHashCodeResult = generatingFailDto.hashCode();
    assertEquals(expectedHashCodeResult, generatingFailDto1.hashCode());
  }

  @Test
  public void testEquals16() {
    GeneratingFailDto generatingFailDto = new GeneratingFailDto(new TestScheduleDetailDto(), -1, "An error occurred",
      true, null);
    GeneratingFailDto generatingFailDto1 = new GeneratingFailDto(new TestScheduleDetailDto(), -1, "An error occurred",
      true, null);

    assertEquals(generatingFailDto1, generatingFailDto);
    int expectedHashCodeResult = generatingFailDto.hashCode();
    assertEquals(expectedHashCodeResult, generatingFailDto1.hashCode());
  }

  @Test
  public void testEquals2() {
    GeneratingFailDto generatingFailDto = new GeneratingFailDto(new TestScheduleDetailDto(), -1, "An error occurred",
      true, "/tmp/foo.txt");
    assertEquals(generatingFailDto, generatingFailDto);
    int expectedHashCodeResult = generatingFailDto.hashCode();
    assertEquals(expectedHashCodeResult, generatingFailDto.hashCode());
  }

  @Test
  public void testEquals3() {
    GeneratingFailDto generatingFailDto = new GeneratingFailDto(new TestScheduleDetailDto(), -1, "An error occurred",
      true, "/tmp/foo.txt");
    GeneratingFailDto generatingFailDto1 = new GeneratingFailDto(new TestScheduleDetailDto(), -1, "An error occurred",
      true, "/tmp/foo.txt");

    assertEquals(generatingFailDto1, generatingFailDto);
    int expectedHashCodeResult = generatingFailDto.hashCode();
    assertEquals(expectedHashCodeResult, generatingFailDto1.hashCode());
  }

  @Test
  public void testEquals4() {
    GeneratingFailDto generatingFailDto = new GeneratingFailDto(null, -1, "An error occurred", true, "/tmp/foo.txt");
    assertNotEquals(new GeneratingFailDto(new TestScheduleDetailDto(), -1, "An error occurred", true, "/tmp/foo.txt"), generatingFailDto);
  }

  @Test
  public void testEquals5() {
    GeneratingFailDto generatingFailDto = new GeneratingFailDto(
      new TestScheduleDetailDto(123, 10, 10, new BasicAssignmentInfoDto(), true), -1, "An error occurred", true,
      "/tmp/foo.txt");
    assertNotEquals(new GeneratingFailDto(new TestScheduleDetailDto(), -1, "An error occurred", true, "/tmp/foo.txt"), generatingFailDto);
  }

  @Test
  public void testEquals6() {
    GeneratingFailDto generatingFailDto = new GeneratingFailDto(new TestScheduleDetailDto(), 0, "An error occurred",
      true, "/tmp/foo.txt");
    assertNotEquals(new GeneratingFailDto(new TestScheduleDetailDto(), -1, "An error occurred", true, "/tmp/foo.txt"), generatingFailDto);
  }

  @Test
  public void testEquals7() {
    GeneratingFailDto generatingFailDto = new GeneratingFailDto(new TestScheduleDetailDto(), null, "An error occurred",
      true, "/tmp/foo.txt");
    assertNotEquals(new GeneratingFailDto(new TestScheduleDetailDto(), -1, "An error occurred", true, "/tmp/foo.txt"), generatingFailDto);
  }

  @Test
  public void testEquals8() {
    GeneratingFailDto generatingFailDto = new GeneratingFailDto(new TestScheduleDetailDto(), -1, null, true,
      "/tmp/foo.txt");
    assertNotEquals(new GeneratingFailDto(new TestScheduleDetailDto(), -1, "An error occurred", true, "/tmp/foo.txt"), generatingFailDto);
  }

  @Test
  public void testEquals9() {
    GeneratingFailDto generatingFailDto = new GeneratingFailDto(new TestScheduleDetailDto(), -1, "/tmp/foo.txt", true,
      "/tmp/foo.txt");
    assertNotEquals(new GeneratingFailDto(new TestScheduleDetailDto(), -1, "An error occurred", true, "/tmp/foo.txt"), generatingFailDto);
  }
}

