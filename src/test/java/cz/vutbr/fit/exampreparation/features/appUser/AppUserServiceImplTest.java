package cz.vutbr.fit.exampreparation.features.appUser;

import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.BadRequestException;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.ConflictException;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.NotFoundException;
import cz.vutbr.fit.exampreparation.features.appUser.dto.EmployeeDetailsDto;
import cz.vutbr.fit.exampreparation.features.assignment.Assignment;
import cz.vutbr.fit.exampreparation.features.role.Role;
import cz.vutbr.fit.exampreparation.features.role.RoleName;
import cz.vutbr.fit.exampreparation.features.role.RoleService;
import cz.vutbr.fit.exampreparation.features.room.Room;
import cz.vutbr.fit.exampreparation.features.term.Term;
import cz.vutbr.fit.exampreparation.features.token.Token;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {AppUserServiceImpl.class})
@ExtendWith(SpringExtension.class)
public class AppUserServiceImplTest {
  @MockBean
  private AppUserRepository appUserRepository;

  @Autowired
  private AppUserServiceImpl appUserServiceImpl;

  @MockBean
  private PasswordEncoder passwordEncoder;

  @MockBean
  private RoleService roleService;

  @Test
  public void testGetAllEmployeesList() {
    when(this.appUserRepository.findAllEmployeesAndAdmins()).thenReturn(new ArrayList<AppUser>());
    ResponseEntity<List<EmployeeDetailsDto>> actualAllEmployeesList = this.appUserServiceImpl.getAllEmployeesList();
    assertEquals("<200 OK OK,[],[]>", actualAllEmployeesList.toString());
    assertTrue(actualAllEmployeesList.hasBody());
    assertEquals(HttpStatus.OK, actualAllEmployeesList.getStatusCode());
    assertTrue(actualAllEmployeesList.getHeaders().isEmpty());
    verify(this.appUserRepository).findAllEmployeesAndAdmins();
  }

  @Test
  public void testGetAllEmployeesList2() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());

    ArrayList<AppUser> appUserList = new ArrayList<AppUser>();
    appUserList.add(appUser);
    when(this.appUserRepository.findAllEmployeesAndAdmins()).thenReturn(appUserList);
    ResponseEntity<List<EmployeeDetailsDto>> actualAllEmployeesList = this.appUserServiceImpl.getAllEmployeesList();
    assertEquals(
      "<200 OK OK,[EmployeeDetailsDto(nameWithDegrees=Degrees Before Name Jane Doe Degrees Behind" + " Name)],[]>",
      actualAllEmployeesList.toString());
    assertTrue(actualAllEmployeesList.getHeaders().isEmpty());
    assertTrue(actualAllEmployeesList.hasBody());
    assertEquals(HttpStatus.OK, actualAllEmployeesList.getStatusCode());
    EmployeeDetailsDto getResult = actualAllEmployeesList.getBody().get(0);
    assertEquals("jane.doe@example.org", getResult.getEmail());
    assertEquals("EmployeeDetailsDto(nameWithDegrees=Degrees Before Name Jane Doe Degrees Behind Name)",
      getResult.toString());
    verify(this.appUserRepository).findAllEmployeesAndAdmins();
  }

  @Test
  public void testGetAllEmployeesList3() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());

    AppUser appUser1 = new AppUser();
    appUser1.setLastName("Doe");
    appUser1.setEmail("jane.doe@example.org");
    appUser1.setDegreesBeforeName(" ");
    appUser1.setPassword("iloveyou");
    appUser1.setUserTokens(new ArrayList<Token>());
    appUser1.setFirstName("Jane");
    appUser1.setVerified(true);
    appUser1.setRooms(new ArrayList<Room>());
    appUser1.setDegreesBehindName(" ");
    appUser1.setRoles(new ArrayList<Role>());
    appUser1.setLocked(true);
    appUser1.setTerms(new ArrayList<Term>());
    appUser1.setAssignments(new ArrayList<Assignment>());

    ArrayList<AppUser> appUserList = new ArrayList<AppUser>();
    appUserList.add(appUser1);
    appUserList.add(appUser);
    when(this.appUserRepository.findAllEmployeesAndAdmins()).thenReturn(appUserList);
    ResponseEntity<List<EmployeeDetailsDto>> actualAllEmployeesList = this.appUserServiceImpl.getAllEmployeesList();
    assertEquals(
      "<200 OK OK,[EmployeeDetailsDto(nameWithDegrees=  Jane Doe  ), EmployeeDetailsDto(nameWithDegrees=Degrees"
        + " Before Name Jane Doe Degrees Behind Name)],[]>",
      actualAllEmployeesList.toString());
    assertTrue(actualAllEmployeesList.getHeaders().isEmpty());
    assertTrue(actualAllEmployeesList.hasBody());
    assertEquals(HttpStatus.OK, actualAllEmployeesList.getStatusCode());
    List<EmployeeDetailsDto> body = actualAllEmployeesList.getBody();
    EmployeeDetailsDto getResult = body.get(1);
    assertEquals("EmployeeDetailsDto(nameWithDegrees=Degrees Before Name Jane Doe Degrees Behind Name)",
      getResult.toString());
    EmployeeDetailsDto getResult1 = body.get(0);
    assertEquals("EmployeeDetailsDto(nameWithDegrees=  Jane Doe  )", getResult1.toString());
    assertEquals("jane.doe@example.org", getResult1.getEmail());
    assertEquals("jane.doe@example.org", getResult.getEmail());
    verify(this.appUserRepository).findAllEmployeesAndAdmins();
  }

  @Test
  public void testFindUserByLogin() {
    when(this.appUserRepository.findByLogin(anyString())).thenReturn(new ArrayList<AppUser>());
    assertNull(this.appUserServiceImpl.findUserByLogin("Login"));
    verify(this.appUserRepository).findByLogin(anyString());
  }

  @Test
  public void testFindUserByLogin2() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());

    ArrayList<AppUser> appUserList = new ArrayList<AppUser>();
    appUserList.add(appUser);
    when(this.appUserRepository.findByLogin(anyString())).thenReturn(appUserList);
    assertSame(appUser, this.appUserServiceImpl.findUserByLogin("Login"));
    verify(this.appUserRepository).findByLogin(anyString());
  }

  @Test
  public void testFindUserByLogin3() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());

    AppUser appUser1 = new AppUser();
    appUser1.setLastName("Doe");
    appUser1.setEmail("jane.doe@example.org");
    appUser1.setDegreesBeforeName("Degrees Before Name");
    appUser1.setPassword("iloveyou");
    appUser1.setUserTokens(new ArrayList<Token>());
    appUser1.setFirstName("Jane");
    appUser1.setVerified(true);
    appUser1.setRooms(new ArrayList<Room>());
    appUser1.setDegreesBehindName("Degrees Behind Name");
    appUser1.setRoles(new ArrayList<Role>());
    appUser1.setLocked(true);
    appUser1.setTerms(new ArrayList<Term>());
    appUser1.setAssignments(new ArrayList<Assignment>());

    ArrayList<AppUser> appUserList = new ArrayList<AppUser>();
    appUserList.add(appUser1);
    appUserList.add(appUser);
    when(this.appUserRepository.findByLogin(anyString())).thenReturn(appUserList);
    assertThrows(ConflictException.class, () -> this.appUserServiceImpl.findUserByLogin("Login"));
    verify(this.appUserRepository).findByLogin(anyString());
  }

  @Test
  public void testFindByEmail() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());
    Optional<AppUser> ofResult = Optional.<AppUser>of(appUser);
    when(this.appUserRepository.findByEmail(anyString())).thenReturn(ofResult);
    Optional<AppUser> actualFindByEmailResult = this.appUserServiceImpl.findByEmail("jane.doe@example.org");
    assertSame(ofResult, actualFindByEmailResult);
    assertTrue(actualFindByEmailResult.isPresent());
    verify(this.appUserRepository).findByEmail(anyString());
  }

  @Test
  public void testEncodePasswordSetRoleAndSave() {
    Role role = new Role();
    role.setRoleDescription("Role Description");
    role.setRoleName(RoleName.STUDENT);
    role.setAppUsers(new ArrayList<AppUser>());
    Optional<Role> ofResult = Optional.<Role>of(role);
    when(this.roleService.findRole((RoleName) any())).thenReturn(ofResult);
    when(this.passwordEncoder.encode((CharSequence) any())).thenReturn("foo");

    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());
    when(this.appUserRepository.save((AppUser) any())).thenReturn(appUser);

    AppUser appUser1 = new AppUser();
    appUser1.setEmail("jane.doe@example.org");
    assertSame(appUser, this.appUserServiceImpl.encodePasswordSetRoleAndSave(appUser1));
    verify(this.roleService).findRole((RoleName) any());
    verify(this.passwordEncoder).encode((CharSequence) any());
    verify(this.appUserRepository).save((AppUser) any());
    assertEquals(1, appUser1.getRoles().size());
    assertEquals("foo", appUser1.getPassword());
  }

  @Test
  public void testEncodePasswordSetRoleAndSave2() {
    when(this.roleService.findRole((cz.vutbr.fit.exampreparation.features.role.RoleName) any()))
      .thenReturn(Optional.<Role>empty());
    when(this.passwordEncoder.encode((CharSequence) any())).thenReturn("foo");

    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());
    when(this.appUserRepository.save((AppUser) any())).thenReturn(appUser);

    AppUser appUser1 = new AppUser();
    appUser1.setEmail("jane.doe@example.org");
    assertThrows(NotFoundException.class, () -> this.appUserServiceImpl.encodePasswordSetRoleAndSave(appUser1));
    verify(this.roleService).findRole((cz.vutbr.fit.exampreparation.features.role.RoleName) any());
  }

  @Test
  public void testEncodePasswordSetRoleAndSave3() {
    Role role = new Role();
    role.setRoleDescription("Role Description");
    role.setRoleName(RoleName.STUDENT);
    role.setAppUsers(new ArrayList<AppUser>());
    Optional<Role> ofResult = Optional.<Role>of(role);
    when(this.roleService.findRole((RoleName) any())).thenReturn(ofResult);
    when(this.passwordEncoder.encode((CharSequence) any())).thenReturn("foo");

    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());
    when(this.appUserRepository.save((AppUser) any())).thenReturn(appUser);

    AppUser appUser1 = new AppUser();
    appUser1.setEmail("U@stud.fit.vut.cz");
    assertSame(appUser, this.appUserServiceImpl.encodePasswordSetRoleAndSave(appUser1));
    verify(this.roleService).findRole((RoleName) any());
    verify(this.passwordEncoder).encode((CharSequence) any());
    verify(this.appUserRepository).save((AppUser) any());
    assertEquals(1, appUser1.getRoles().size());
    assertEquals("foo", appUser1.getPassword());
  }

  @Test
  public void testEncodePasswordSetRoleAndSave4() {
    Role role = new Role();
    role.setRoleDescription("Role Description");
    role.setRoleName(RoleName.STUDENT);
    role.setAppUsers(new ArrayList<AppUser>());
    Optional<Role> ofResult = Optional.<Role>of(role);
    when(this.roleService.findRole((RoleName) any())).thenReturn(ofResult);
    when(this.passwordEncoder.encode((CharSequence) any())).thenReturn("foo");

    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());
    when(this.appUserRepository.save((AppUser) any())).thenReturn(appUser);

    AppUser appUser1 = new AppUser();
    appUser1.setEmail("U@vut.cz");
    assertSame(appUser, this.appUserServiceImpl.encodePasswordSetRoleAndSave(appUser1));
    verify(this.roleService).findRole((RoleName) any());
    verify(this.passwordEncoder).encode((CharSequence) any());
    verify(this.appUserRepository).save((AppUser) any());
    assertEquals(1, appUser1.getRoles().size());
    assertEquals("foo", appUser1.getPassword());
  }

  @Test
  public void testSave() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());
    when(this.appUserRepository.save((AppUser) any())).thenReturn(appUser);
    assertSame(appUser, this.appUserServiceImpl.save(new AppUser()));
    verify(this.appUserRepository).save((AppUser) any());
  }

  @Test
  public void testDeleteUser() {
    doNothing().when(this.appUserRepository).delete((AppUser) any());
    this.appUserServiceImpl.deleteUser(new AppUser());
    verify(this.appUserRepository).delete((AppUser) any());
  }

  @Test
  public void testDeleteUserById() {
    doNothing().when(this.appUserRepository).deleteById((Integer) any());
    this.appUserServiceImpl.deleteUserById(123);
    verify(this.appUserRepository).deleteById((Integer) any());
  }

  @Test
  public void testCheckAdditionalManagers2() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());
    Optional<AppUser> ofResult = Optional.<AppUser>of(appUser);
    when(this.appUserRepository.findByEmail(anyString())).thenReturn(ofResult);

    ArrayList<String> stringList = new ArrayList<String>();
    stringList.add("foo");
    this.appUserServiceImpl.checkAdditionalManagers(stringList);
    verify(this.appUserRepository).findByEmail(anyString());
  }

  @Test
  public void testCheckAdditionalManagers3() {
    Role role = new Role();
    role.setRoleDescription(null);
    role.setRoleName(null);
    role.setAppUsers(new ArrayList<AppUser>());

    ArrayList<Role> roleList = new ArrayList<Role>();
    roleList.add(role);

    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(roleList);
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());
    Optional<AppUser> ofResult = Optional.<AppUser>of(appUser);
    when(this.appUserRepository.findByEmail(anyString())).thenReturn(ofResult);

    ArrayList<String> stringList = new ArrayList<String>();
    stringList.add("foo");
    assertThrows(BadRequestException.class, () -> this.appUserServiceImpl.checkAdditionalManagers(stringList));
    verify(this.appUserRepository).findByEmail(anyString());
  }

  @Test
  public void testFindById() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());
    Optional<AppUser> ofResult = Optional.<AppUser>of(appUser);
    when(this.appUserRepository.findById((Integer) any())).thenReturn(ofResult);
    Optional<AppUser> actualFindByIdResult = this.appUserServiceImpl.findById(123);
    assertSame(ofResult, actualFindByIdResult);
    assertTrue(actualFindByIdResult.isPresent());
    verify(this.appUserRepository).findById((Integer) any());
  }
}

