package cz.vutbr.fit.exampreparation.features.term.dto.term;

import cz.vutbr.fit.exampreparation.features.appUser.dto.EmployeeDetailsDto;
import cz.vutbr.fit.exampreparation.features.appUser.dto.LoginNameDto;
import cz.vutbr.fit.exampreparation.features.roomPlan.SpacingBetweenStudents;
import cz.vutbr.fit.exampreparation.features.termRun.Distribution;
import cz.vutbr.fit.exampreparation.features.termRun.dto.TermRunEditInfoDto;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class EditTermInfoDtoTest {
  @Test
  public void testConstructor() {
    ArrayList<LoginNameDto> loginNameDtoList = new ArrayList<LoginNameDto>();
    ArrayList<TermRunEditInfoDto> termRunEditInfoDtoList = new ArrayList<TermRunEditInfoDto>();
    ArrayList<EmployeeDetailsDto> employeeDetailsDtoList = new ArrayList<EmployeeDetailsDto>();
    EditTermInfoDto actualEditTermInfoDto = new EditTermInfoDto("Term Name", 123, loginNameDtoList,
      SpacingBetweenStudents.ZERO, termRunEditInfoDtoList, Distribution.RANDOM, employeeDetailsDtoList,
      "Information Text");

    assertEquals(123, actualEditTermInfoDto.getCourseId().intValue());
    assertEquals("EditTermInfoDto(studentsList=[], managers=[], termRuns=[])", actualEditTermInfoDto.toString());
    List<TermRunEditInfoDto> termRuns = actualEditTermInfoDto.getTermRuns();
    assertSame(termRunEditInfoDtoList, termRuns);
    assertEquals(employeeDetailsDtoList, termRuns);
    assertEquals(loginNameDtoList, termRuns);
    assertTrue(termRuns.isEmpty());
    assertEquals("Term Name", actualEditTermInfoDto.getTermName());
    List<LoginNameDto> studentsList = actualEditTermInfoDto.getStudentsList();
    assertSame(loginNameDtoList, studentsList);
    assertEquals(termRuns, studentsList);
    assertEquals(employeeDetailsDtoList, studentsList);
    assertTrue(studentsList.isEmpty());
    assertEquals(SpacingBetweenStudents.ZERO, actualEditTermInfoDto.getSpacingBetweenStudents());
    List<EmployeeDetailsDto> managers = actualEditTermInfoDto.getManagers();
    assertSame(employeeDetailsDtoList, managers);
    assertEquals(termRuns, managers);
    assertEquals(studentsList, managers);
    assertTrue(managers.isEmpty());
    assertEquals("Information Text", actualEditTermInfoDto.getInformationText());
    assertEquals(Distribution.RANDOM, actualEditTermInfoDto.getDistribution());
    assertSame(studentsList, loginNameDtoList);
    assertSame(termRuns, termRunEditInfoDtoList);
    assertSame(managers, employeeDetailsDtoList);
  }
}

