package cz.vutbr.fit.exampreparation.features.room.dto;

import cz.vutbr.fit.exampreparation.features.appUser.dto.EmployeeDetailsDto;
import cz.vutbr.fit.exampreparation.features.roomPlan.dto.RoomPlanWithCellsDto;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class RoomDetailDtoTest {
  @Test
  public void testConstructor() {
    EmployeeDetailsDto employeeDetailsDto = new EmployeeDetailsDto("Name With Degrees");
    ArrayList<RoomPlanWithCellsDto> roomPlanWithCellsDtoList = new ArrayList<RoomPlanWithCellsDto>();
    RoomDetailDto actualRoomDetailDto = new RoomDetailDto("Abbreviation", "Name", employeeDetailsDto,
      "The characteristics of someone or something", roomPlanWithCellsDtoList);

    assertEquals("Abbreviation", actualRoomDetailDto.getAbbreviation());
    assertEquals(
      "RoomDetailDto(creator=EmployeeDetailsDto(nameWithDegrees=Name With Degrees), description=The characteristics"
        + " of someone or something, roomPlans=[])",
      actualRoomDetailDto.toString());
    List<RoomPlanWithCellsDto> roomPlans = actualRoomDetailDto.getRoomPlans();
    assertSame(roomPlanWithCellsDtoList, roomPlans);
    assertTrue(roomPlans.isEmpty());
    assertEquals("Name", actualRoomDetailDto.getName());
    assertEquals("The characteristics of someone or something", actualRoomDetailDto.getDescription());
    EmployeeDetailsDto creator = actualRoomDetailDto.getCreator();
    assertSame(employeeDetailsDto, creator);
    assertNull(creator.getEmail());
    assertEquals("EmployeeDetailsDto(nameWithDegrees=Name With Degrees)", creator.toString());
    assertEquals("Name With Degrees", creator.getNameWithDegrees());
    assertSame(creator, employeeDetailsDto);
    assertSame(roomPlans, roomPlanWithCellsDtoList);
  }
}

