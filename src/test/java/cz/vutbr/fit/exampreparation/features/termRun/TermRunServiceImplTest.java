package cz.vutbr.fit.exampreparation.features.termRun;

import cz.vutbr.fit.exampreparation.features.appUser.AppUser;
import cz.vutbr.fit.exampreparation.features.assignment.Assignment;
import cz.vutbr.fit.exampreparation.features.course.Course;
import cz.vutbr.fit.exampreparation.features.roomPlan.RoomPlan;
import cz.vutbr.fit.exampreparation.features.roomPlan.SpacingBetweenStudents;
import cz.vutbr.fit.exampreparation.features.term.Term;
import cz.vutbr.fit.exampreparation.features.testSchedule.TestSchedule;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {TermRunServiceImpl.class})
@ExtendWith(SpringExtension.class)
public class TermRunServiceImplTest {
  @MockBean
  private TermRunRepository termRunRepository;

  @Autowired
  private TermRunServiceImpl termRunServiceImpl;

  @Test
  public void testGetTermTermRuns() {
    ArrayList<TermRun> termRunList = new ArrayList<TermRun>();
    when(this.termRunRepository.findByTermReference_IdTerm(anyInt())).thenReturn(termRunList);
    List<TermRun> actualTermTermRuns = this.termRunServiceImpl.getTermTermRuns(123);
    assertSame(termRunList, actualTermTermRuns);
    assertTrue(actualTermTermRuns.isEmpty());
    verify(this.termRunRepository).findByTermReference_IdTerm(anyInt());
  }

  @Test
  public void testFindTermRunById() {
    Course course = new Course();
    course.setAbbreviation("Abbreviation");
    course.setAcademicYear(1);
    course.setName("Name");
    course.setTerms(new ArrayList<Term>());

    Term term = new Term();
    term.setDistribution(Distribution.RANDOM);
    term.setCourseReference(course);
    term.setTermRuns(new ArrayList<TermRun>());
    term.setTermName("Term Name");
    term.setTermManagers(new ArrayList<AppUser>());
    term.setDescription("The characteristics of someone or something");
    term.setSpacingBetweenStudents(SpacingBetweenStudents.ZERO);

    TermRun termRun = new TermRun();
    termRun.setTermReference(term);
    termRun.setRoomPlans(new ArrayList<RoomPlan>());
    termRun.setStartDateTime(mock(Timestamp.class));
    termRun.setEndDateTime(mock(Timestamp.class));
    termRun.setTestSchedules(new ArrayList<TestSchedule>());
    termRun.setAssignments(new ArrayList<Assignment>());
    Optional<TermRun> ofResult = Optional.<TermRun>of(termRun);
    when(this.termRunRepository.findById((Integer) any())).thenReturn(ofResult);
    Optional<TermRun> actualFindTermRunByIdResult = this.termRunServiceImpl.findTermRunById(123);
    assertSame(ofResult, actualFindTermRunByIdResult);
    assertTrue(actualFindTermRunByIdResult.isPresent());
    verify(this.termRunRepository).findById((Integer) any());
  }
}

