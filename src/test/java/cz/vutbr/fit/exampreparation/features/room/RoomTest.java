package cz.vutbr.fit.exampreparation.features.room;

import cz.vutbr.fit.exampreparation.features.appUser.AppUser;
import cz.vutbr.fit.exampreparation.features.roomPlan.RoomPlan;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class RoomTest {
  @Test
  public void testConstructor() {
    AppUser appUser = new AppUser();
    ArrayList<RoomPlan> roomPlanList = new ArrayList<RoomPlan>();
    Room actualRoom = new Room("Abbreviation", "Name", "The characteristics of someone or something", 10, 10, appUser,
      roomPlanList);

    assertEquals("Abbreviation", actualRoom.getAbbreviation());
    List<RoomPlan> roomPlans = actualRoom.getRoomPlans();
    assertSame(roomPlanList, roomPlans);
    assertTrue(roomPlans.isEmpty());
    assertEquals(10, actualRoom.getNumberOfRows().intValue());
    assertEquals(10, actualRoom.getNumberOfColumns().intValue());
    assertEquals("Name", actualRoom.getName());
    assertEquals(0, actualRoom.getIdRoom());
    assertEquals("The characteristics of someone or something", actualRoom.getDescription());
    AppUser appUserReference = actualRoom.getAppUserReference();
    assertSame(appUser, appUserReference);
    assertNull(appUserReference.getAssignments());
    assertFalse(appUserReference.isVerified());
    assertFalse(appUserReference.isLocked());
    assertNull(appUserReference.getUserTokens());
    assertNull(appUserReference.getTerms());
    assertNull(appUserReference.getRooms());
    assertNull(appUserReference.getRoles());
    assertNull(appUserReference.getPassword());
    assertEquals("null null", appUserReference.getNameWithDegrees());
    assertNull(appUserReference.getLastName());
    assertEquals(0, appUserReference.getIdAppUser());
    assertNull(appUserReference.getFirstName());
    assertNull(appUserReference.getEmail());
    assertNull(appUserReference.getDegreesBehindName());
    assertNull(appUserReference.getDegreesBeforeName());
    assertSame(appUserReference, appUser);
    assertSame(roomPlans, roomPlanList);
  }
}

