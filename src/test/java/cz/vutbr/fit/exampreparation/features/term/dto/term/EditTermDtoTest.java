package cz.vutbr.fit.exampreparation.features.term.dto.term;

import cz.vutbr.fit.exampreparation.features.appUser.dto.LoginNameDto;
import cz.vutbr.fit.exampreparation.features.term.dto.StudentsPlacementDto;
import cz.vutbr.fit.exampreparation.features.termRun.dto.TermRunDto;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class EditTermDtoTest {
  @Test
  public void testCanEqual() {
    assertFalse((new EditTermDto()).canEqual("Other"));
  }

  @Test
  public void testCanEqual2() {
    EditTermDto editTermDto = new EditTermDto();
    assertTrue(editTermDto.canEqual(new EditTermDto()));
  }

  @Test
  public void testConstructor() {
    EditTermDto actualEditTermDto = new EditTermDto();
    actualEditTermDto.setAddStudentsAtTheEnd(true);
    ArrayList<String> stringList = new ArrayList<String>();
    actualEditTermDto.setAdditionalTermManagersEmails(stringList);
    actualEditTermDto.setEmailMessage("jane.doe@example.org");
    ArrayList<LoginNameDto> loginNameDtoList = new ArrayList<LoginNameDto>();
    actualEditTermDto.setOldStudentsList(loginNameDtoList);
    actualEditTermDto.setSendEmail(true);
    ArrayList<LoginNameDto> loginNameDtoList1 = new ArrayList<LoginNameDto>();
    actualEditTermDto.setStudentsList(loginNameDtoList1);
    ArrayList<StudentsPlacementDto> studentsPlacementDtoList = new ArrayList<StudentsPlacementDto>();
    actualEditTermDto.setStudentsPlacements(studentsPlacementDtoList);
    ArrayList<TermRunDto> termRunDtoList = new ArrayList<TermRunDto>();
    actualEditTermDto.setTermRuns(termRunDtoList);
    List<String> additionalTermManagersEmails = actualEditTermDto.getAdditionalTermManagersEmails();
    assertEquals(loginNameDtoList1, additionalTermManagersEmails);
    assertSame(stringList, additionalTermManagersEmails);
    assertEquals(termRunDtoList, additionalTermManagersEmails);
    assertEquals(loginNameDtoList, additionalTermManagersEmails);
    assertEquals(studentsPlacementDtoList, additionalTermManagersEmails);
    assertEquals("jane.doe@example.org", actualEditTermDto.getEmailMessage());
    List<StudentsPlacementDto> studentsPlacements = actualEditTermDto.getStudentsPlacements();
    List<LoginNameDto> oldStudentsList = actualEditTermDto.getOldStudentsList();
    assertEquals(studentsPlacements, oldStudentsList);
    List<TermRunDto> termRuns = actualEditTermDto.getTermRuns();
    assertEquals(termRuns, oldStudentsList);
    assertSame(loginNameDtoList, oldStudentsList);
    List<LoginNameDto> studentsList = actualEditTermDto.getStudentsList();
    assertEquals(studentsList, oldStudentsList);
    assertEquals(additionalTermManagersEmails, oldStudentsList);
    assertSame(loginNameDtoList1, studentsList);
    assertEquals(studentsPlacements, studentsList);
    assertEquals(additionalTermManagersEmails, studentsList);
    assertEquals(termRuns, studentsList);
    assertEquals(loginNameDtoList, studentsList);
    assertEquals(loginNameDtoList, studentsPlacements);
    assertEquals(loginNameDtoList1, studentsPlacements);
    assertEquals(additionalTermManagersEmails, studentsPlacements);
    assertSame(studentsPlacementDtoList, studentsPlacements);
    assertEquals(termRuns, studentsPlacements);
    assertEquals(loginNameDtoList, termRuns);
    assertSame(termRunDtoList, termRuns);
    assertEquals(additionalTermManagersEmails, termRuns);
    assertEquals(studentsPlacementDtoList, termRuns);
    assertEquals(loginNameDtoList1, termRuns);
    assertTrue(actualEditTermDto.isAddStudentsAtTheEnd());
    assertTrue(actualEditTermDto.isSendEmail());
    assertEquals(
      "EditTermDto(termRuns=[], studentsPlacements=[], studentsList=[], oldStudentsList=[], addStudentsAtTheEnd"
        + "=true, additionalTermManagersEmails=[], sendEmail=true, emailMessage=jane.doe@example.org)",
      actualEditTermDto.toString());
  }

  @Test
  public void testConstructor2() {
    ArrayList<TermRunDto> termRunDtoList = new ArrayList<TermRunDto>();
    ArrayList<StudentsPlacementDto> studentsPlacementDtoList = new ArrayList<StudentsPlacementDto>();
    ArrayList<LoginNameDto> loginNameDtoList = new ArrayList<LoginNameDto>();
    ArrayList<LoginNameDto> loginNameDtoList1 = new ArrayList<LoginNameDto>();
    ArrayList<String> stringList = new ArrayList<String>();
    EditTermDto actualEditTermDto = new EditTermDto(termRunDtoList, studentsPlacementDtoList, loginNameDtoList,
      loginNameDtoList1, true, stringList, true, "jane.doe@example.org");
    actualEditTermDto.setAddStudentsAtTheEnd(true);
    ArrayList<String> stringList1 = new ArrayList<String>();
    actualEditTermDto.setAdditionalTermManagersEmails(stringList1);
    actualEditTermDto.setEmailMessage("jane.doe@example.org");
    ArrayList<LoginNameDto> loginNameDtoList2 = new ArrayList<LoginNameDto>();
    actualEditTermDto.setOldStudentsList(loginNameDtoList2);
    actualEditTermDto.setSendEmail(true);
    ArrayList<LoginNameDto> loginNameDtoList3 = new ArrayList<LoginNameDto>();
    actualEditTermDto.setStudentsList(loginNameDtoList3);
    ArrayList<StudentsPlacementDto> studentsPlacementDtoList1 = new ArrayList<StudentsPlacementDto>();
    actualEditTermDto.setStudentsPlacements(studentsPlacementDtoList1);
    ArrayList<TermRunDto> termRunDtoList1 = new ArrayList<TermRunDto>();
    actualEditTermDto.setTermRuns(termRunDtoList1);
    List<String> additionalTermManagersEmails = actualEditTermDto.getAdditionalTermManagersEmails();
    assertSame(stringList1, additionalTermManagersEmails);
    assertEquals(termRunDtoList, additionalTermManagersEmails);
    assertEquals(studentsPlacementDtoList1, additionalTermManagersEmails);
    assertEquals(loginNameDtoList2, additionalTermManagersEmails);
    assertEquals(loginNameDtoList1, additionalTermManagersEmails);
    assertEquals(loginNameDtoList, additionalTermManagersEmails);
    assertEquals(termRunDtoList1, additionalTermManagersEmails);
    assertEquals(studentsPlacementDtoList, additionalTermManagersEmails);
    assertEquals(loginNameDtoList3, additionalTermManagersEmails);
    assertEquals(stringList, additionalTermManagersEmails);
    assertEquals("jane.doe@example.org", actualEditTermDto.getEmailMessage());
    List<LoginNameDto> oldStudentsList = actualEditTermDto.getOldStudentsList();
    assertEquals(termRunDtoList, oldStudentsList);
    assertEquals(additionalTermManagersEmails, oldStudentsList);
    assertSame(loginNameDtoList2, oldStudentsList);
    List<StudentsPlacementDto> studentsPlacements = actualEditTermDto.getStudentsPlacements();
    assertEquals(studentsPlacements, oldStudentsList);
    assertEquals(stringList, oldStudentsList);
    List<TermRunDto> termRuns = actualEditTermDto.getTermRuns();
    assertEquals(termRuns, oldStudentsList);
    assertEquals(loginNameDtoList, oldStudentsList);
    assertEquals(loginNameDtoList1, oldStudentsList);
    assertEquals(studentsPlacementDtoList, oldStudentsList);
    List<LoginNameDto> studentsList = actualEditTermDto.getStudentsList();
    assertEquals(studentsList, oldStudentsList);
    assertSame(loginNameDtoList3, studentsList);
    assertEquals(studentsPlacementDtoList, studentsList);
    assertEquals(studentsPlacements, studentsList);
    assertEquals(stringList, studentsList);
    assertEquals(loginNameDtoList, studentsList);
    assertEquals(loginNameDtoList2, studentsList);
    assertEquals(loginNameDtoList1, studentsList);
    assertEquals(termRunDtoList, studentsList);
    assertEquals(additionalTermManagersEmails, studentsList);
    assertEquals(termRuns, studentsList);
    assertEquals(studentsPlacementDtoList, studentsPlacements);
    assertEquals(additionalTermManagersEmails, studentsPlacements);
    assertEquals(loginNameDtoList1, studentsPlacements);
    assertEquals(loginNameDtoList2, studentsPlacements);
    assertSame(studentsPlacementDtoList1, studentsPlacements);
    assertEquals(termRunDtoList, studentsPlacements);
    assertEquals(loginNameDtoList, studentsPlacements);
    assertEquals(loginNameDtoList3, studentsPlacements);
    assertEquals(stringList, studentsPlacements);
    assertEquals(termRuns, studentsPlacements);
    assertEquals(additionalTermManagersEmails, termRuns);
    assertEquals(studentsPlacementDtoList, termRuns);
    assertEquals(loginNameDtoList, termRuns);
    assertEquals(studentsPlacementDtoList1, termRuns);
    assertEquals(loginNameDtoList2, termRuns);
    assertSame(termRunDtoList1, termRuns);
    assertEquals(termRunDtoList, termRuns);
    assertEquals(loginNameDtoList3, termRuns);
    assertEquals(stringList, termRuns);
    assertEquals(loginNameDtoList1, termRuns);
    assertTrue(actualEditTermDto.isAddStudentsAtTheEnd());
    assertTrue(actualEditTermDto.isSendEmail());
    assertEquals(
      "EditTermDto(termRuns=[], studentsPlacements=[], studentsList=[], oldStudentsList=[], addStudentsAtTheEnd"
        + "=true, additionalTermManagersEmails=[], sendEmail=true, emailMessage=jane.doe@example.org)",
      actualEditTermDto.toString());
  }

  @Test
  public void testEquals() {
    assertNotEquals((new EditTermDto()), null);
    assertNotEquals((new EditTermDto()), "Different type to EditTermDto");
  }

  @Test
  public void testEquals10() {
    ArrayList<TermRunDto> termRunDtoList = new ArrayList<TermRunDto>();
    termRunDtoList.add(new TermRunDto());
    ArrayList<StudentsPlacementDto> studentsPlacements = new ArrayList<StudentsPlacementDto>();
    ArrayList<LoginNameDto> studentsList = new ArrayList<LoginNameDto>();
    ArrayList<LoginNameDto> oldStudentsList = new ArrayList<LoginNameDto>();
    EditTermDto editTermDto = new EditTermDto(termRunDtoList, studentsPlacements, studentsList, oldStudentsList, true,
      new ArrayList<String>(), true, "jane.doe@example.org");
    ArrayList<TermRunDto> termRuns = new ArrayList<TermRunDto>();
    ArrayList<StudentsPlacementDto> studentsPlacements1 = new ArrayList<StudentsPlacementDto>();
    ArrayList<LoginNameDto> studentsList1 = new ArrayList<LoginNameDto>();
    ArrayList<LoginNameDto> oldStudentsList1 = new ArrayList<LoginNameDto>();
    assertNotEquals(new EditTermDto(termRuns, studentsPlacements1, studentsList1, oldStudentsList1, true,
      new ArrayList<String>(), true, "jane.doe@example.org"), editTermDto);
  }

  @Test
  public void testEquals11() {
    ArrayList<StudentsPlacementDto> studentsPlacementDtoList = new ArrayList<StudentsPlacementDto>();
    studentsPlacementDtoList.add(new StudentsPlacementDto());
    ArrayList<TermRunDto> termRuns = new ArrayList<TermRunDto>();
    ArrayList<LoginNameDto> studentsList = new ArrayList<LoginNameDto>();
    ArrayList<LoginNameDto> oldStudentsList = new ArrayList<LoginNameDto>();
    EditTermDto editTermDto = new EditTermDto(termRuns, studentsPlacementDtoList, studentsList, oldStudentsList, true,
      new ArrayList<String>(), true, "jane.doe@example.org");
    ArrayList<TermRunDto> termRuns1 = new ArrayList<TermRunDto>();
    ArrayList<StudentsPlacementDto> studentsPlacements = new ArrayList<StudentsPlacementDto>();
    ArrayList<LoginNameDto> studentsList1 = new ArrayList<LoginNameDto>();
    ArrayList<LoginNameDto> oldStudentsList1 = new ArrayList<LoginNameDto>();
    assertNotEquals(new EditTermDto(termRuns1, studentsPlacements, studentsList1, oldStudentsList1, true,
      new ArrayList<String>(), true, "jane.doe@example.org"), editTermDto);
  }

  @Test
  public void testEquals12() {
    ArrayList<LoginNameDto> loginNameDtoList = new ArrayList<LoginNameDto>();
    loginNameDtoList.add(new LoginNameDto("Login", "Name And Degrees"));
    ArrayList<TermRunDto> termRuns = new ArrayList<TermRunDto>();
    ArrayList<StudentsPlacementDto> studentsPlacements = new ArrayList<StudentsPlacementDto>();
    ArrayList<LoginNameDto> oldStudentsList = new ArrayList<LoginNameDto>();
    EditTermDto editTermDto = new EditTermDto(termRuns, studentsPlacements, loginNameDtoList, oldStudentsList, true,
      new ArrayList<String>(), true, "jane.doe@example.org");
    ArrayList<TermRunDto> termRuns1 = new ArrayList<TermRunDto>();
    ArrayList<StudentsPlacementDto> studentsPlacements1 = new ArrayList<StudentsPlacementDto>();
    ArrayList<LoginNameDto> studentsList = new ArrayList<LoginNameDto>();
    ArrayList<LoginNameDto> oldStudentsList1 = new ArrayList<LoginNameDto>();
    assertNotEquals(new EditTermDto(termRuns1, studentsPlacements1, studentsList, oldStudentsList1, true,
      new ArrayList<String>(), true, "jane.doe@example.org"), editTermDto);
  }

  @Test
  public void testEquals13() {
    ArrayList<LoginNameDto> loginNameDtoList = new ArrayList<LoginNameDto>();
    loginNameDtoList.add(new LoginNameDto("Login", "Name And Degrees"));
    ArrayList<TermRunDto> termRuns = new ArrayList<TermRunDto>();
    ArrayList<StudentsPlacementDto> studentsPlacements = new ArrayList<StudentsPlacementDto>();
    ArrayList<LoginNameDto> studentsList = new ArrayList<LoginNameDto>();
    EditTermDto editTermDto = new EditTermDto(termRuns, studentsPlacements, studentsList, loginNameDtoList, true,
      new ArrayList<String>(), true, "jane.doe@example.org");
    ArrayList<TermRunDto> termRuns1 = new ArrayList<TermRunDto>();
    ArrayList<StudentsPlacementDto> studentsPlacements1 = new ArrayList<StudentsPlacementDto>();
    ArrayList<LoginNameDto> studentsList1 = new ArrayList<LoginNameDto>();
    ArrayList<LoginNameDto> oldStudentsList = new ArrayList<LoginNameDto>();
    assertNotEquals(new EditTermDto(termRuns1, studentsPlacements1, studentsList1, oldStudentsList, true,
      new ArrayList<String>(), true, "jane.doe@example.org"), editTermDto);
  }

  @Test
  public void testEquals14() {
    ArrayList<String> stringList = new ArrayList<String>();
    stringList.add("jane.doe@example.org");
    ArrayList<TermRunDto> termRuns = new ArrayList<TermRunDto>();
    ArrayList<StudentsPlacementDto> studentsPlacements = new ArrayList<StudentsPlacementDto>();
    ArrayList<LoginNameDto> studentsList = new ArrayList<LoginNameDto>();
    EditTermDto editTermDto = new EditTermDto(termRuns, studentsPlacements, studentsList, new ArrayList<LoginNameDto>(),
      true, stringList, true, "jane.doe@example.org");
    ArrayList<TermRunDto> termRuns1 = new ArrayList<TermRunDto>();
    ArrayList<StudentsPlacementDto> studentsPlacements1 = new ArrayList<StudentsPlacementDto>();
    ArrayList<LoginNameDto> studentsList1 = new ArrayList<LoginNameDto>();
    ArrayList<LoginNameDto> oldStudentsList = new ArrayList<LoginNameDto>();
    assertNotEquals(new EditTermDto(termRuns1, studentsPlacements1, studentsList1, oldStudentsList, true,
      new ArrayList<String>(), true, "jane.doe@example.org"), editTermDto);
  }

  @Test
  public void testEquals2() {
    EditTermDto editTermDto = new EditTermDto();
    assertEquals(editTermDto, editTermDto);
    int expectedHashCodeResult = editTermDto.hashCode();
    assertEquals(expectedHashCodeResult, editTermDto.hashCode());
  }

  @Test
  public void testEquals3() {
    EditTermDto editTermDto = new EditTermDto();
    EditTermDto editTermDto1 = new EditTermDto();
    assertEquals(editTermDto1, editTermDto);
    int expectedHashCodeResult = editTermDto.hashCode();
    assertEquals(expectedHashCodeResult, editTermDto1.hashCode());
  }

  @Test
  public void testEquals4() {
    ArrayList<TermRunDto> termRuns = new ArrayList<TermRunDto>();
    ArrayList<StudentsPlacementDto> studentsPlacements = new ArrayList<StudentsPlacementDto>();
    ArrayList<LoginNameDto> studentsList = new ArrayList<LoginNameDto>();
    ArrayList<LoginNameDto> oldStudentsList = new ArrayList<LoginNameDto>();
    EditTermDto editTermDto = new EditTermDto(termRuns, studentsPlacements, studentsList, oldStudentsList, true,
      new ArrayList<String>(), true, "jane.doe@example.org");
    assertNotEquals(new EditTermDto(), editTermDto);
  }

  @Test
  public void testEquals5() {
    EditTermDto editTermDto = new EditTermDto();
    editTermDto.setEmailMessage("jane.doe@example.org");
    assertNotEquals(new EditTermDto(), editTermDto);
  }

  @Test
  public void testEquals6() {
    EditTermDto editTermDto = new EditTermDto();
    editTermDto.setCourseId(123);
    assertNotEquals(new EditTermDto(), editTermDto);
  }

  @Test
  public void testEquals7() {
    EditTermDto editTermDto = new EditTermDto();
    editTermDto.setSendEmail(true);
    assertNotEquals(new EditTermDto(), editTermDto);
  }

  @Test
  public void testEquals8() {
    ArrayList<TermRunDto> termRuns = new ArrayList<TermRunDto>();
    ArrayList<StudentsPlacementDto> studentsPlacements = new ArrayList<StudentsPlacementDto>();
    ArrayList<LoginNameDto> studentsList = new ArrayList<LoginNameDto>();
    ArrayList<LoginNameDto> oldStudentsList = new ArrayList<LoginNameDto>();
    EditTermDto editTermDto = new EditTermDto(termRuns, studentsPlacements, studentsList, oldStudentsList, true,
      new ArrayList<String>(), true, "jane.doe@example.org");
    ArrayList<TermRunDto> termRuns1 = new ArrayList<TermRunDto>();
    ArrayList<StudentsPlacementDto> studentsPlacements1 = new ArrayList<StudentsPlacementDto>();
    ArrayList<LoginNameDto> studentsList1 = new ArrayList<LoginNameDto>();
    ArrayList<LoginNameDto> oldStudentsList1 = new ArrayList<LoginNameDto>();
    EditTermDto editTermDto1 = new EditTermDto(termRuns1, studentsPlacements1, studentsList1, oldStudentsList1, true,
      new ArrayList<String>(), true, "jane.doe@example.org");

    assertEquals(editTermDto1, editTermDto);
    int expectedHashCodeResult = editTermDto.hashCode();
    assertEquals(expectedHashCodeResult, editTermDto1.hashCode());
  }

  @Test
  public void testEquals9() {
    EditTermDto editTermDto = new EditTermDto();

    EditTermDto editTermDto1 = new EditTermDto();
    editTermDto1.setEmailMessage("jane.doe@example.org");
    assertNotEquals(editTermDto1, editTermDto);
  }
}

