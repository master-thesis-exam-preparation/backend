package cz.vutbr.fit.exampreparation.features.assignment.dto;

import cz.vutbr.fit.exampreparation.features.appUser.dto.EmployeeDetailsDto;
import cz.vutbr.fit.exampreparation.features.assignment.AssignmentStatus;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class AssignmentInfoDtoTest {
  @Test
  public void testConstructor() {
    ArrayList<FileDataDto> fileDataDtoList = new ArrayList<FileDataDto>();
    ArrayList<EmployeeDetailsDto> employeeDetailsDtoList = new ArrayList<EmployeeDetailsDto>();
    AssignmentInfoDto actualAssignmentInfoDto = new AssignmentInfoDto(123, "Name", AssignmentStatus.UPLOADED,
      fileDataDtoList, employeeDetailsDtoList);

    assertEquals(123, actualAssignmentInfoDto.getAssignmentId().intValue());
    assertEquals("AssignmentInfoDto(files=[], managers=[])", actualAssignmentInfoDto.toString());
    assertEquals(AssignmentStatus.UPLOADED, actualAssignmentInfoDto.getStatus());
    assertEquals("Name", actualAssignmentInfoDto.getName());
    List<EmployeeDetailsDto> managers = actualAssignmentInfoDto.getManagers();
    assertSame(employeeDetailsDtoList, managers);
    assertEquals(fileDataDtoList, managers);
    assertTrue(managers.isEmpty());
    List<FileDataDto> files = actualAssignmentInfoDto.getFiles();
    assertSame(fileDataDtoList, files);
    assertEquals(managers, files);
    assertTrue(files.isEmpty());
    assertSame(files, fileDataDtoList);
    assertSame(managers, employeeDetailsDtoList);
  }
}

