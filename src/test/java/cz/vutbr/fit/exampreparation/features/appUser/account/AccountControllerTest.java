package cz.vutbr.fit.exampreparation.features.appUser.account;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.vutbr.fit.exampreparation.common.MessageResponseDto;
import cz.vutbr.fit.exampreparation.features.appUser.dto.AccountInfoDto;
import cz.vutbr.fit.exampreparation.features.appUser.dto.ChangePasswordDto;
import cz.vutbr.fit.exampreparation.features.appUser.dto.NewRoleDto;
import cz.vutbr.fit.exampreparation.features.appUser.dto.UserDataDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {AccountController.class})
@ExtendWith(SpringExtension.class)
public class AccountControllerTest {
  @Autowired
  private AccountController accountController;

  @MockBean
  private AccountService accountService;

  @Test
  public void testChangePassword() {
    AccountService accountService = mock(AccountService.class);
    ResponseEntity<MessageResponseDto> responseEntity = new ResponseEntity<MessageResponseDto>(HttpStatus.CONTINUE);
    when(accountService.changePassword((ChangePasswordDto) any())).thenReturn(responseEntity);
    AccountController accountController = new AccountController(accountService);
    assertSame(responseEntity, accountController.changePassword(new ChangePasswordDto("iloveyou", "iloveyou")));
    verify(accountService).changePassword((ChangePasswordDto) any());
    assertNull(accountController.getAllUsersData());
  }

  @Test
  public void testGetUserData() throws Exception {
    when(this.accountService.getUserData()).thenReturn(new ResponseEntity<UserDataDto>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/internal/accounts/change-user-details");
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.accountController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testChangeUserDetails() {
    AccountService accountService = mock(AccountService.class);
    ResponseEntity<MessageResponseDto> responseEntity = new ResponseEntity<MessageResponseDto>(HttpStatus.CONTINUE);
    when(accountService.changeUserDetails((UserDataDto) any())).thenReturn(responseEntity);
    AccountController accountController = new AccountController(accountService);
    assertSame(responseEntity, accountController
      .changeUserDetails(new UserDataDto("Degrees Before Name", "Degrees Behind Name", "Jane", "Doe")));
    verify(accountService).changeUserDetails((UserDataDto) any());
    assertNull(accountController.getAllUsersData());
  }

  @Test
  public void testGetAllUsersData() throws Exception {
    when(this.accountService.getAllUsersData())
      .thenReturn(new ResponseEntity<List<AccountInfoDto>>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/internal/accounts");
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.accountController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testChangeAccountRole() throws Exception {
    when(this.accountService.changeAccountRole((NewRoleDto) any(), anyInt()))
      .thenReturn(new ResponseEntity<MessageResponseDto>(HttpStatus.CONTINUE));

    NewRoleDto newRoleDto = new NewRoleDto();
    newRoleDto.setRole("Role");
    String content = (new ObjectMapper()).writeValueAsString(newRoleDto);
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
      .post("/internal/accounts/change-role/{userId}", 123)
      .contentType(MediaType.APPLICATION_JSON)
      .content(content);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.accountController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testDeleteAccount() throws Exception {
    when(this.accountService.deleteAccount(anyInt()))
      .thenReturn(new ResponseEntity<MessageResponseDto>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/internal/accounts/delete/{userId}",
      123);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.accountController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testLockAccount() throws Exception {
    when(this.accountService.lockAccount(anyInt()))
      .thenReturn(new ResponseEntity<MessageResponseDto>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/internal/accounts/lock/{userId}", 123);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.accountController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testUnlockAccount() throws Exception {
    when(this.accountService.unlockAccount(anyInt()))
      .thenReturn(new ResponseEntity<MessageResponseDto>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/internal/accounts/unlock/{userId}",
      123);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.accountController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }
}

