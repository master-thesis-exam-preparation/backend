package cz.vutbr.fit.exampreparation.features.appUser.dto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class NewRoleDtoTest {
  @Test
  public void testCanEqual() {
    assertFalse((new NewRoleDto("Role")).canEqual("Other"));
  }

  @Test
  public void testCanEqual2() {
    NewRoleDto newRoleDto = new NewRoleDto("Role");
    assertTrue(newRoleDto.canEqual(new NewRoleDto("Role")));
  }

  @Test
  public void testConstructor() {
    NewRoleDto actualNewRoleDto = new NewRoleDto();
    actualNewRoleDto.setRole("Role");
    assertEquals("Role", actualNewRoleDto.getRole());
    assertEquals("NewRoleDto(role=Role)", actualNewRoleDto.toString());
  }

  @Test
  public void testConstructor2() {
    NewRoleDto actualNewRoleDto = new NewRoleDto("Role");
    actualNewRoleDto.setRole("Role");
    assertEquals("Role", actualNewRoleDto.getRole());
    assertEquals("NewRoleDto(role=Role)", actualNewRoleDto.toString());
  }

  @Test
  public void testEquals() {
    assertNotEquals((new NewRoleDto("Role")), null);
    assertNotEquals((new NewRoleDto("Role")), "Different type to NewRoleDto");
  }

  @Test
  public void testEquals2() {
    NewRoleDto newRoleDto = new NewRoleDto("Role");
    assertEquals(newRoleDto, newRoleDto);
    int expectedHashCodeResult = newRoleDto.hashCode();
    assertEquals(expectedHashCodeResult, newRoleDto.hashCode());
  }

  @Test
  public void testEquals3() {
    NewRoleDto newRoleDto = new NewRoleDto("Role");
    NewRoleDto newRoleDto1 = new NewRoleDto("Role");
    assertEquals(newRoleDto1, newRoleDto);
    int expectedHashCodeResult = newRoleDto.hashCode();
    assertEquals(expectedHashCodeResult, newRoleDto1.hashCode());
  }

  @Test
  public void testEquals4() {
    NewRoleDto newRoleDto = new NewRoleDto(null);
    assertNotEquals(new NewRoleDto("Role"), newRoleDto);
  }

  @Test
  public void testEquals5() {
    NewRoleDto newRoleDto = new NewRoleDto("cz.vutbr.fit.exampreparation.features.appUser.dto.NewRoleDto");
    assertNotEquals(new NewRoleDto("Role"), newRoleDto);
  }

  @Test
  public void testEquals6() {
    NewRoleDto newRoleDto = new NewRoleDto(null);
    NewRoleDto newRoleDto1 = new NewRoleDto(null);
    assertEquals(newRoleDto1, newRoleDto);
    int expectedHashCodeResult = newRoleDto.hashCode();
    assertEquals(expectedHashCodeResult, newRoleDto1.hashCode());
  }
}

