package cz.vutbr.fit.exampreparation.features.roomPlan.dto;

import cz.vutbr.fit.exampreparation.features.roomPlan.SpacingBetweenStudents;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class RoomPlanDtoTest {
  @Test
  public void testCanEqual() {
    assertFalse((new RoomPlanDto()).canEqual("Other"));
  }

  @Test
  public void testCanEqual2() {
    RoomPlanDto roomPlanDto = new RoomPlanDto();
    assertTrue(roomPlanDto.canEqual(new RoomPlanDto()));
  }

  @Test
  public void testConstructor() {
    RoomPlanDto actualRoomPlanDto = new RoomPlanDto();
    actualRoomPlanDto.setCapacity(3);
    actualRoomPlanDto.setRoomPlanId(123);
    actualRoomPlanDto.setSpacing(SpacingBetweenStudents.ZERO);
    assertEquals(3, actualRoomPlanDto.getCapacity().intValue());
    assertEquals(123, actualRoomPlanDto.getRoomPlanId().intValue());
    assertEquals(SpacingBetweenStudents.ZERO, actualRoomPlanDto.getSpacing());
    assertEquals("RoomPlanDto(roomPlanId=123, capacity=3, spacing=ZERO)", actualRoomPlanDto.toString());
  }

  @Test
  public void testConstructor2() {
    RoomPlanDto actualRoomPlanDto = new RoomPlanDto(123, 3, SpacingBetweenStudents.ZERO);
    actualRoomPlanDto.setCapacity(3);
    actualRoomPlanDto.setRoomPlanId(123);
    actualRoomPlanDto.setSpacing(SpacingBetweenStudents.ZERO);
    assertEquals(3, actualRoomPlanDto.getCapacity().intValue());
    assertEquals(123, actualRoomPlanDto.getRoomPlanId().intValue());
    assertEquals(SpacingBetweenStudents.ZERO, actualRoomPlanDto.getSpacing());
    assertEquals("RoomPlanDto(roomPlanId=123, capacity=3, spacing=ZERO)", actualRoomPlanDto.toString());
  }

  @Test
  public void testEquals() {
    assertNotEquals((new RoomPlanDto()), null);
    assertNotEquals((new RoomPlanDto()), "Different type to RoomPlanDto");
  }

  @Test
  public void testEquals10() {
    RoomPlanDto roomPlanDto = new RoomPlanDto();
    RoomPlanWithCellsDto roomPlanWithCellsDto = mock(RoomPlanWithCellsDto.class);
    when(roomPlanWithCellsDto.getSpacing()).thenReturn(SpacingBetweenStudents.ZERO);
    when(roomPlanWithCellsDto.getCapacity()).thenReturn(null);
    when(roomPlanWithCellsDto.getRoomPlanId()).thenReturn(null);
    when(roomPlanWithCellsDto.canEqual((Object) any())).thenReturn(true);
    assertNotEquals(roomPlanWithCellsDto, roomPlanDto);
  }

  @Test
  public void testEquals11() {
    RoomPlanDto roomPlanDto = new RoomPlanDto();
    roomPlanDto.setCapacity(3);
    RoomPlanWithCellsDto roomPlanWithCellsDto = mock(RoomPlanWithCellsDto.class);
    when(roomPlanWithCellsDto.getSpacing()).thenReturn(SpacingBetweenStudents.ZERO);
    when(roomPlanWithCellsDto.getCapacity()).thenReturn(null);
    when(roomPlanWithCellsDto.getRoomPlanId()).thenReturn(null);
    when(roomPlanWithCellsDto.canEqual((Object) any())).thenReturn(true);
    assertNotEquals(roomPlanWithCellsDto, roomPlanDto);
  }

  @Test
  public void testEquals2() {
    RoomPlanDto roomPlanDto = new RoomPlanDto();
    assertEquals(roomPlanDto, roomPlanDto);
    int expectedHashCodeResult = roomPlanDto.hashCode();
    assertEquals(expectedHashCodeResult, roomPlanDto.hashCode());
  }

  @Test
  public void testEquals3() {
    RoomPlanDto roomPlanDto = new RoomPlanDto();
    RoomPlanDto roomPlanDto1 = new RoomPlanDto();
    assertEquals(roomPlanDto1, roomPlanDto);
    int expectedHashCodeResult = roomPlanDto.hashCode();
    assertEquals(expectedHashCodeResult, roomPlanDto1.hashCode());
  }

  @Test
  public void testEquals4() {
    RoomPlanDto roomPlanDto = new RoomPlanDto(123, 3, SpacingBetweenStudents.ZERO);
    assertNotEquals(new RoomPlanDto(), roomPlanDto);
  }

  @Test
  public void testEquals5() {
    RoomPlanWithCellsDto roomPlanWithCellsDto = mock(RoomPlanWithCellsDto.class);
    assertNotEquals(new RoomPlanDto(), roomPlanWithCellsDto);
  }

  @Test
  public void testEquals6() {
    RoomPlanDto roomPlanDto = new RoomPlanDto();
    assertNotEquals(new RoomPlanDto(123, 3, SpacingBetweenStudents.ZERO), roomPlanDto);
  }

  @Test
  public void testEquals7() {
    RoomPlanDto roomPlanDto = new RoomPlanDto();
    assertNotEquals(new RoomPlanWithCellsDto(), roomPlanDto);
  }

  @Test
  public void testEquals8() {
    RoomPlanDto roomPlanDto = new RoomPlanDto();
    RoomPlanWithCellsDto roomPlanWithCellsDto = mock(RoomPlanWithCellsDto.class);
    when(roomPlanWithCellsDto.getRoomPlanId()).thenReturn(1);
    when(roomPlanWithCellsDto.canEqual((Object) any())).thenReturn(true);
    assertNotEquals(roomPlanWithCellsDto, roomPlanDto);
  }

  @Test
  public void testEquals9() {
    RoomPlanDto roomPlanDto = new RoomPlanDto();
    RoomPlanWithCellsDto roomPlanWithCellsDto = mock(RoomPlanWithCellsDto.class);
    when(roomPlanWithCellsDto.getCapacity()).thenReturn(1);
    when(roomPlanWithCellsDto.getRoomPlanId()).thenReturn(null);
    when(roomPlanWithCellsDto.canEqual((Object) any())).thenReturn(true);
    assertNotEquals(roomPlanWithCellsDto, roomPlanDto);
  }
}

