package cz.vutbr.fit.exampreparation.features.term.dto.term;

import cz.vutbr.fit.exampreparation.features.appUser.dto.LoginNameDto;
import cz.vutbr.fit.exampreparation.features.roomPlan.SpacingBetweenStudents;
import cz.vutbr.fit.exampreparation.features.term.dto.StudentsPlacementDto;
import cz.vutbr.fit.exampreparation.features.termRun.Distribution;
import cz.vutbr.fit.exampreparation.features.termRun.dto.TermRunDto;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class NewTermDtoTest {
  @Test
  public void testConstructor() {
    ArrayList<TermRunDto> termRunDtoList = new ArrayList<TermRunDto>();
    ArrayList<StudentsPlacementDto> studentsPlacementDtoList = new ArrayList<StudentsPlacementDto>();
    ArrayList<LoginNameDto> loginNameDtoList = new ArrayList<LoginNameDto>();
    ArrayList<String> stringList = new ArrayList<String>();
    NewTermDto actualNewTermDto = new NewTermDto("Term Name", 123, SpacingBetweenStudents.ZERO, termRunDtoList,
      studentsPlacementDtoList, Distribution.RANDOM, "Information Text", loginNameDtoList, stringList, true,
      "jane.doe@example.org");

    List<String> additionalTermManagersEmails = actualNewTermDto.getAdditionalTermManagersEmails();
    assertSame(stringList, additionalTermManagersEmails);
    assertEquals(termRunDtoList, additionalTermManagersEmails);
    assertEquals(studentsPlacementDtoList, additionalTermManagersEmails);
    assertEquals(loginNameDtoList, additionalTermManagersEmails);
    assertTrue(additionalTermManagersEmails.isEmpty());
    assertEquals("NewTermDto(studentsList=[], additionalTermManagersEmails=[], termRuns=[], studentsPlacements=[],"
      + " sendEmail=true, emailMessage=jane.doe@example.org)", actualNewTermDto.toString());
    assertTrue(actualNewTermDto.isSendEmail());
    List<TermRunDto> termRuns = actualNewTermDto.getTermRuns();
    assertSame(termRunDtoList, termRuns);
    assertEquals(studentsPlacementDtoList, termRuns);
    assertEquals(loginNameDtoList, termRuns);
    assertEquals(additionalTermManagersEmails, termRuns);
    assertTrue(termRuns.isEmpty());
    assertEquals("Term Name", actualNewTermDto.getTermName());
    List<StudentsPlacementDto> studentsPlacements = actualNewTermDto.getStudentsPlacements();
    assertSame(studentsPlacementDtoList, studentsPlacements);
    assertEquals(termRuns, studentsPlacements);
    assertEquals(loginNameDtoList, studentsPlacements);
    assertEquals(additionalTermManagersEmails, studentsPlacements);
    assertTrue(studentsPlacements.isEmpty());
    List<LoginNameDto> studentsList = actualNewTermDto.getStudentsList();
    assertSame(loginNameDtoList, studentsList);
    assertEquals(termRuns, studentsList);
    assertEquals(studentsPlacements, studentsList);
    assertEquals(additionalTermManagersEmails, studentsList);
    assertTrue(studentsList.isEmpty());
    assertEquals(SpacingBetweenStudents.ZERO, actualNewTermDto.getSpacingBetweenStudents());
    assertEquals("Information Text", actualNewTermDto.getInformationText());
    assertEquals("jane.doe@example.org", actualNewTermDto.getEmailMessage());
    assertEquals(Distribution.RANDOM, actualNewTermDto.getDistribution());
    assertEquals(123, actualNewTermDto.getCourseId().intValue());
    assertSame(termRuns, termRunDtoList);
    assertSame(studentsPlacements, studentsPlacementDtoList);
    assertSame(studentsList, loginNameDtoList);
    assertSame(additionalTermManagersEmails, stringList);
  }
}

