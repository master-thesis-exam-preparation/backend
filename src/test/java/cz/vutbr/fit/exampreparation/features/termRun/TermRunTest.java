package cz.vutbr.fit.exampreparation.features.termRun;

import cz.vutbr.fit.exampreparation.features.appUser.AppUser;
import cz.vutbr.fit.exampreparation.features.assignment.Assignment;
import cz.vutbr.fit.exampreparation.features.cell.Cell;
import cz.vutbr.fit.exampreparation.features.room.Room;
import cz.vutbr.fit.exampreparation.features.roomPlan.RoomPlan;
import cz.vutbr.fit.exampreparation.features.roomPlan.SpacingBetweenStudents;
import cz.vutbr.fit.exampreparation.features.term.Term;
import cz.vutbr.fit.exampreparation.features.testSchedule.TestSchedule;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class TermRunTest {
  @Test
  public void testConstructor() {
    Timestamp startDateTime = mock(Timestamp.class);
    Timestamp endDateTime = mock(Timestamp.class);
    Term term = new Term();
    ArrayList<Assignment> assignmentList = new ArrayList<Assignment>();
    ArrayList<RoomPlan> roomPlanList = new ArrayList<RoomPlan>();
    ArrayList<TestSchedule> testScheduleList = new ArrayList<TestSchedule>();
    TermRun actualTermRun = new TermRun(startDateTime, endDateTime, term, assignmentList, roomPlanList,
      testScheduleList);

    List<Assignment> assignments = actualTermRun.getAssignments();
    assertSame(assignmentList, assignments);
    assertEquals(roomPlanList, assignments);
    assertEquals(testScheduleList, assignments);
    assertTrue(assignments.isEmpty());
    List<TestSchedule> testSchedules = actualTermRun.getTestSchedules();
    assertSame(testScheduleList, testSchedules);
    assertEquals(assignments, testSchedules);
    assertEquals(roomPlanList, testSchedules);
    assertTrue(testSchedules.isEmpty());
    Term termReference = actualTermRun.getTermReference();
    assertSame(term, termReference);
    List<RoomPlan> roomPlans = actualTermRun.getRoomPlans();
    assertEquals(assignments, roomPlans);
    assertEquals(roomPlanList, roomPlans);
    assertEquals(testSchedules, roomPlans);
    assertTrue(roomPlans.isEmpty());
    assertEquals(0, actualTermRun.getIdTermRun());
    assertEquals(0, termReference.getIdTerm());
    assertNull(termReference.getDistribution());
    assertNull(termReference.getDescription());
    assertNull(termReference.getCourseReference());
    assertNull(termReference.getTermName());
    assertNull(termReference.getSpacingBetweenStudents());
    List<AppUser> termManagers = termReference.getTermManagers();
    assertEquals(assignments, termManagers);
    assertEquals(roomPlans, termManagers);
    assertEquals(roomPlanList, termManagers);
    assertEquals(testSchedules, termManagers);
    assertTrue(termManagers.isEmpty());
    assertSame(termReference, term);
    assertSame(assignments, assignmentList);
    assertEquals(assignments, roomPlanList);
    assertEquals(roomPlans, roomPlanList);
    assertEquals(termManagers, roomPlanList);
    assertEquals(testSchedules, roomPlanList);
    assertTrue(roomPlanList.isEmpty());
    assertSame(testSchedules, testScheduleList);
  }

  @Test
  public void testAddAssignment() {
    TermRun termRun = new TermRun();
    Assignment assignment = new Assignment();
    termRun.addAssignment(assignment);
    assertEquals(1, assignment.getTermRuns().size());
    assertEquals(1, termRun.getAssignments().size());
  }

  @Test
  public void testRemoveAssignment() {
    TermRun termRun = new TermRun();
    Assignment assignment = new Assignment();
    termRun.removeAssignment(assignment);
    assertTrue(assignment.getTermRuns().isEmpty());
    assertTrue(termRun.getAssignments().isEmpty());
  }

  @Test
  public void testAddRoomPlan() {
    Timestamp startDateTime = mock(Timestamp.class);
    Timestamp endDateTime = mock(Timestamp.class);
    Term termReference = new Term();
    ArrayList<Assignment> assignments = new ArrayList<Assignment>();
    ArrayList<RoomPlan> roomPlans = new ArrayList<RoomPlan>();
    TermRun termRun = new TermRun(startDateTime, endDateTime, termReference, assignments, roomPlans,
      new ArrayList<TestSchedule>());
    ArrayList<TermRun> termRuns = new ArrayList<TermRun>();
    ArrayList<Cell> cells = new ArrayList<Cell>();
    RoomPlan roomPlan = new RoomPlan(3, SpacingBetweenStudents.ZERO, termRuns, cells, new Room());

    termRun.addRoomPlan(roomPlan);
    assertEquals(1, roomPlan.getTermRuns().size());
    assertEquals(1, termRun.getRoomPlans().size());
  }

  @Test
  public void testRemoveRoomPlan() {
    Timestamp startDateTime = mock(Timestamp.class);
    Timestamp endDateTime = mock(Timestamp.class);
    Term termReference = new Term();
    ArrayList<Assignment> assignments = new ArrayList<Assignment>();
    ArrayList<RoomPlan> roomPlans = new ArrayList<RoomPlan>();
    TermRun termRun = new TermRun(startDateTime, endDateTime, termReference, assignments, roomPlans,
      new ArrayList<TestSchedule>());
    ArrayList<TermRun> termRuns = new ArrayList<TermRun>();
    ArrayList<Cell> cells = new ArrayList<Cell>();
    RoomPlan roomPlan = new RoomPlan(3, SpacingBetweenStudents.ZERO, termRuns, cells, new Room());

    termRun.removeRoomPlan(roomPlan);
    assertTrue(roomPlan.getTermRuns().isEmpty());
    assertTrue(termRun.getRoomPlans().isEmpty());
  }

  @Test
  public void testGetRoomPlans() {
    TermRun termRun = new TermRun();
    termRun.setRoomPlans(new ArrayList<RoomPlan>());
    assertTrue(termRun.getRoomPlans().isEmpty());
  }

  @Test
  public void testAddTestSchedule() {
    Timestamp startDateTime = mock(Timestamp.class);
    Timestamp endDateTime = mock(Timestamp.class);
    Term termReference = new Term();
    ArrayList<Assignment> assignments = new ArrayList<Assignment>();
    ArrayList<RoomPlan> roomPlans = new ArrayList<RoomPlan>();
    TermRun termRun = new TermRun(startDateTime, endDateTime, termReference, assignments, roomPlans,
      new ArrayList<TestSchedule>());
    TestSchedule testSchedule = new TestSchedule();
    termRun.addTestSchedule(testSchedule);
    TermRun termRun1 = testSchedule.getTermRun();
    assertSame(termRun, termRun1);
    assertEquals(1, termRun1.getTestSchedules().size());
  }

  @Test
  public void testRemoveTestSchedule() {
    Timestamp startDateTime = mock(Timestamp.class);
    Timestamp endDateTime = mock(Timestamp.class);
    Term termReference = new Term();
    ArrayList<Assignment> assignments = new ArrayList<Assignment>();
    ArrayList<RoomPlan> roomPlans = new ArrayList<RoomPlan>();
    TermRun termRun = new TermRun(startDateTime, endDateTime, termReference, assignments, roomPlans,
      new ArrayList<TestSchedule>());
    TestSchedule testSchedule = new TestSchedule();
    termRun.removeTestSchedule(testSchedule);
    assertNull(testSchedule.getTermRun());
    assertTrue(termRun.getTestSchedules().isEmpty());
  }
}

