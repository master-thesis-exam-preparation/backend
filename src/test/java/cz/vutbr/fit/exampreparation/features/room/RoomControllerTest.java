package cz.vutbr.fit.exampreparation.features.room;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.vutbr.fit.exampreparation.common.MessageResponseDto;
import cz.vutbr.fit.exampreparation.features.cell.CellType;
import cz.vutbr.fit.exampreparation.features.room.dto.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {RoomController.class})
@ExtendWith(SpringExtension.class)
public class RoomControllerTest {
  @Autowired
  private RoomController roomController;

  @MockBean
  private RoomService roomService;

  @Test
  public void testCreateNewRoom3() throws Exception {
    NewRoomDto newRoomDto = new NewRoomDto();
    newRoomDto.setCells(new ArrayList<List<CellType>>());
    newRoomDto.setAbbreviation("Abbreviation");
    newRoomDto.setName("Name");
    newRoomDto.setDescription("The characteristics of someone or something");
    String content = (new ObjectMapper()).writeValueAsString(newRoomDto);
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/internal/rooms")
      .contentType(MediaType.APPLICATION_JSON)
      .content(content);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.roomController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(400));
  }

  @Test
  public void testEditRoom3() throws Exception {
    NewRoomDto newRoomDto = new NewRoomDto();
    newRoomDto.setCells(new ArrayList<List<CellType>>());
    newRoomDto.setAbbreviation("Abbreviation");
    newRoomDto.setName("Name");
    newRoomDto.setDescription("The characteristics of someone or something");
    String content = (new ObjectMapper()).writeValueAsString(newRoomDto);
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.put("/internal/rooms/{roomId:[\\d]+}", 9)
      .contentType(MediaType.APPLICATION_JSON)
      .content(content);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.roomController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(400));
  }

  @Test
  public void testGetAllRoomsInfo() throws Exception {
    when(this.roomService.getAllRoomsInfo()).thenReturn(new ResponseEntity<List<RoomInfoDto>>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/public/rooms");
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.roomController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testGetAllRoomsInfo2() throws Exception {
    when(this.roomService.getAllRoomsInfo()).thenReturn(new ResponseEntity<List<RoomInfoDto>>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/public/rooms");
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.roomController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testCreateNewRoom() throws Exception {
    NewRoomDto newRoomDto = new NewRoomDto();
    newRoomDto.setCells(new ArrayList<List<CellType>>());
    newRoomDto.setAbbreviation("Abbreviation");
    newRoomDto.setName("Name");
    newRoomDto.setDescription("The characteristics of someone or something");
    String content = (new ObjectMapper()).writeValueAsString(newRoomDto);
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/internal/rooms")
      .contentType(MediaType.APPLICATION_JSON)
      .content(content);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.roomController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(400));
  }

  @Test
  public void testCreateNewRoom2() throws Exception {
    when(this.roomService.createNewRoom((NewRoomDto) any()))
      .thenReturn(new ResponseEntity<RoomInfoDto>(HttpStatus.CONTINUE));

    ArrayList<List<CellType>> listList = new ArrayList<List<CellType>>();
    listList.add(new ArrayList<CellType>());

    NewRoomDto newRoomDto = new NewRoomDto();
    newRoomDto.setCells(listList);
    newRoomDto.setAbbreviation("Abbreviation");
    newRoomDto.setName("Name");
    newRoomDto.setDescription("The characteristics of someone or something");
    String content = (new ObjectMapper()).writeValueAsString(newRoomDto);
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/internal/rooms")
      .contentType(MediaType.APPLICATION_JSON)
      .content(content);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.roomController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testCreateNewRoom4() throws Exception {
    when(this.roomService.createNewRoom((NewRoomDto) any()))
      .thenReturn(new ResponseEntity<RoomInfoDto>(HttpStatus.CONTINUE));

    ArrayList<List<CellType>> listList = new ArrayList<List<CellType>>();
    listList.add(new ArrayList<CellType>());

    NewRoomDto newRoomDto = new NewRoomDto();
    newRoomDto.setCells(listList);
    newRoomDto.setAbbreviation("Abbreviation");
    newRoomDto.setName("Name");
    newRoomDto.setDescription("The characteristics of someone or something");
    String content = (new ObjectMapper()).writeValueAsString(newRoomDto);
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/internal/rooms")
      .contentType(MediaType.APPLICATION_JSON)
      .content(content);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.roomController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testGetEditRoomDetails() throws Exception {
    when(this.roomService.getEditRoomDetails(anyInt(), anyBoolean()))
      .thenReturn(new ResponseEntity<EditRoomDto>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/internal/rooms/{roomId:[\\d]+}/edit",
      9);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.roomController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testGetEditRoomDetails2() throws Exception {
    when(this.roomService.getEditRoomDetails(anyInt(), anyBoolean()))
      .thenReturn(new ResponseEntity<EditRoomDto>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/internal/rooms/{roomId:[\\d]+}/edit",
      9);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.roomController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testGetEditRoomDetailsForCreation() throws Exception {
    when(this.roomService.getEditRoomDetails(anyInt(), anyBoolean()))
      .thenReturn(new ResponseEntity<EditRoomDto>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
      .get("/internal/rooms/{roomId:[\\d]+}/form-info", 9);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.roomController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testGetEditRoomDetailsForCreation2() throws Exception {
    when(this.roomService.getEditRoomDetails(anyInt(), anyBoolean()))
      .thenReturn(new ResponseEntity<EditRoomDto>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
      .get("/internal/rooms/{roomId:[\\d]+}/form-info", 9);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.roomController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testEditRoom2() throws Exception {
    when(this.roomService.editRoom(anyInt(), (NewRoomDto) any()))
      .thenReturn(new ResponseEntity<MessageResponseDto>(HttpStatus.CONTINUE));

    ArrayList<List<CellType>> listList = new ArrayList<List<CellType>>();
    listList.add(new ArrayList<CellType>());

    NewRoomDto newRoomDto = new NewRoomDto();
    newRoomDto.setCells(listList);
    newRoomDto.setAbbreviation("Abbreviation");
    newRoomDto.setName("Name");
    newRoomDto.setDescription("The characteristics of someone or something");
    String content = (new ObjectMapper()).writeValueAsString(newRoomDto);
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.put("/internal/rooms/{roomId:[\\d]+}", 9)
      .contentType(MediaType.APPLICATION_JSON)
      .content(content);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.roomController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testEditRoom4() throws Exception {
    when(this.roomService.editRoom(anyInt(), (NewRoomDto) any()))
      .thenReturn(new ResponseEntity<MessageResponseDto>(HttpStatus.CONTINUE));

    ArrayList<List<CellType>> listList = new ArrayList<List<CellType>>();
    listList.add(new ArrayList<CellType>());

    NewRoomDto newRoomDto = new NewRoomDto();
    newRoomDto.setCells(listList);
    newRoomDto.setAbbreviation("Abbreviation");
    newRoomDto.setName("Name");
    newRoomDto.setDescription("The characteristics of someone or something");
    String content = (new ObjectMapper()).writeValueAsString(newRoomDto);
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.put("/internal/rooms/{roomId:[\\d]+}", 9)
      .contentType(MediaType.APPLICATION_JSON)
      .content(content);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.roomController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testDeleteRoom() throws Exception {
    when(this.roomService.deleteRoom(anyInt())).thenReturn(new ResponseEntity<MessageResponseDto>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/internal/rooms/{roomId:[\\d]+}", 9);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.roomController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testDeleteRoom2() throws Exception {
    when(this.roomService.deleteRoom(anyInt())).thenReturn(new ResponseEntity<MessageResponseDto>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/internal/rooms/{roomId:[\\d]+}", 9);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.roomController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testGetFreeRooms() throws Exception {
    when(this.roomService.getFreeRooms((FreeRoomRequestDto) any()))
      .thenReturn(new ResponseEntity<List<FreeRoomResponseDto>>(HttpStatus.CONTINUE));

    FreeRoomRequestDto freeRoomRequestDto = new FreeRoomRequestDto();
    freeRoomRequestDto.setStartTime(1L);
    freeRoomRequestDto.setTermRunLength(3);
    freeRoomRequestDto.setTermId(123);
    freeRoomRequestDto.setNumberOfTermRuns(10);
    String content = (new ObjectMapper()).writeValueAsString(freeRoomRequestDto);
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/internal/rooms/free")
      .contentType(MediaType.APPLICATION_JSON)
      .content(content);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.roomController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testGetFreeRooms2() throws Exception {
    when(this.roomService.getFreeRooms((FreeRoomRequestDto) any()))
      .thenReturn(new ResponseEntity<List<FreeRoomResponseDto>>(HttpStatus.CONTINUE));

    FreeRoomRequestDto freeRoomRequestDto = new FreeRoomRequestDto();
    freeRoomRequestDto.setStartTime(1L);
    freeRoomRequestDto.setTermRunLength(3);
    freeRoomRequestDto.setTermId(123);
    freeRoomRequestDto.setNumberOfTermRuns(10);
    String content = (new ObjectMapper()).writeValueAsString(freeRoomRequestDto);
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/internal/rooms/free")
      .contentType(MediaType.APPLICATION_JSON)
      .content(content);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.roomController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testEditRoom() throws Exception {
    NewRoomDto newRoomDto = new NewRoomDto();
    newRoomDto.setCells(new ArrayList<List<CellType>>());
    newRoomDto.setAbbreviation("Abbreviation");
    newRoomDto.setName("Name");
    newRoomDto.setDescription("The characteristics of someone or something");
    String content = (new ObjectMapper()).writeValueAsString(newRoomDto);
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.put("/internal/rooms/{roomId:[\\d]+}", 9)
      .contentType(MediaType.APPLICATION_JSON)
      .content(content);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.roomController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(400));
  }

  @Test
  public void testGetRoomDetailsInternal() throws Exception {
    when(this.roomService.getRoomDetails(anyInt(), anyBoolean()))
      .thenReturn(new ResponseEntity<RoomDetailDto>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/internal/rooms/{roomId:[\\d]+}", 9);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.roomController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testGetRoomDetailsInternal2() throws Exception {
    when(this.roomService.getRoomDetails(anyInt(), anyBoolean()))
      .thenReturn(new ResponseEntity<RoomDetailDto>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/internal/rooms/{roomId:[\\d]+}", 9);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.roomController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testGetRoomDetailsPublic() throws Exception {
    when(this.roomService.getRoomDetails(anyInt(), anyBoolean()))
      .thenReturn(new ResponseEntity<RoomDetailDto>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/public/rooms/{roomId:[\\d]+}", 9);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.roomController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testGetRoomDetailsPublic2() throws Exception {
    when(this.roomService.getRoomDetails(anyInt(), anyBoolean()))
      .thenReturn(new ResponseEntity<RoomDetailDto>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/public/rooms/{roomId:[\\d]+}", 9);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.roomController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }
}

