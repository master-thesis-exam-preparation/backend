package cz.vutbr.fit.exampreparation.features.appUser.dto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EmployeeDetailsDtoTest {
  @Test
  public void testConstructor() {
    EmployeeDetailsDto actualEmployeeDetailsDto = new EmployeeDetailsDto("jane.doe@example.org", "Name With Degrees");

    assertEquals("jane.doe@example.org", actualEmployeeDetailsDto.getEmail());
    assertEquals("EmployeeDetailsDto(nameWithDegrees=Name With Degrees)", actualEmployeeDetailsDto.toString());
    assertEquals("Name With Degrees", actualEmployeeDetailsDto.getNameWithDegrees());
  }
}

