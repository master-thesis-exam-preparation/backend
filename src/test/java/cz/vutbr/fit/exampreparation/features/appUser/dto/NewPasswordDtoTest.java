package cz.vutbr.fit.exampreparation.features.appUser.dto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class NewPasswordDtoTest {
  @Test
  public void testCanEqual() {
    assertFalse((new NewPasswordDto("ABC123", "iloveyou")).canEqual("Other"));
  }

  @Test
  public void testCanEqual2() {
    NewPasswordDto newPasswordDto = new NewPasswordDto("ABC123", "iloveyou");
    assertTrue(newPasswordDto.canEqual(new NewPasswordDto("ABC123", "iloveyou")));
  }

  @Test
  public void testConstructor() {
    NewPasswordDto actualNewPasswordDto = new NewPasswordDto();
    actualNewPasswordDto.setPassword("iloveyou");
    actualNewPasswordDto.setToken("ABC123");
    assertEquals("iloveyou", actualNewPasswordDto.getPassword());
    assertEquals("ABC123", actualNewPasswordDto.getToken());
    assertEquals("NewPasswordDto(token=ABC123, password=iloveyou)", actualNewPasswordDto.toString());
  }

  @Test
  public void testConstructor2() {
    NewPasswordDto actualNewPasswordDto = new NewPasswordDto("ABC123", "iloveyou");
    actualNewPasswordDto.setPassword("iloveyou");
    actualNewPasswordDto.setToken("ABC123");
    assertEquals("iloveyou", actualNewPasswordDto.getPassword());
    assertEquals("ABC123", actualNewPasswordDto.getToken());
    assertEquals("NewPasswordDto(token=ABC123, password=iloveyou)", actualNewPasswordDto.toString());
  }

  @Test
  public void testEquals() {
    assertNotEquals((new NewPasswordDto("ABC123", "iloveyou")), null);
    assertNotEquals((new NewPasswordDto("ABC123", "iloveyou")), "Different type to NewPasswordDto");
  }

  @Test
  public void testEquals2() {
    NewPasswordDto newPasswordDto = new NewPasswordDto("ABC123", "iloveyou");
    assertEquals(newPasswordDto, newPasswordDto);
    int expectedHashCodeResult = newPasswordDto.hashCode();
    assertEquals(expectedHashCodeResult, newPasswordDto.hashCode());
  }

  @Test
  public void testEquals3() {
    NewPasswordDto newPasswordDto = new NewPasswordDto("ABC123", "iloveyou");
    NewPasswordDto newPasswordDto1 = new NewPasswordDto("ABC123", "iloveyou");

    assertEquals(newPasswordDto1, newPasswordDto);
    int expectedHashCodeResult = newPasswordDto.hashCode();
    assertEquals(expectedHashCodeResult, newPasswordDto1.hashCode());
  }

  @Test
  public void testEquals4() {
    NewPasswordDto newPasswordDto = new NewPasswordDto(null, "iloveyou");
    assertNotEquals(new NewPasswordDto("ABC123", "iloveyou"), newPasswordDto);
  }

  @Test
  public void testEquals5() {
    NewPasswordDto newPasswordDto = new NewPasswordDto("iloveyou", "iloveyou");
    assertNotEquals(new NewPasswordDto("ABC123", "iloveyou"), newPasswordDto);
  }

  @Test
  public void testEquals6() {
    NewPasswordDto newPasswordDto = new NewPasswordDto("ABC123", "ABC123");
    assertNotEquals(new NewPasswordDto("ABC123", "iloveyou"), newPasswordDto);
  }

  @Test
  public void testEquals7() {
    NewPasswordDto newPasswordDto = new NewPasswordDto("ABC123", null);
    assertNotEquals(new NewPasswordDto("ABC123", "iloveyou"), newPasswordDto);
  }

  @Test
  public void testEquals8() {
    NewPasswordDto newPasswordDto = new NewPasswordDto(null, "iloveyou");
    NewPasswordDto newPasswordDto1 = new NewPasswordDto(null, "iloveyou");

    assertEquals(newPasswordDto1, newPasswordDto);
    int expectedHashCodeResult = newPasswordDto.hashCode();
    assertEquals(expectedHashCodeResult, newPasswordDto1.hashCode());
  }

  @Test
  public void testEquals9() {
    NewPasswordDto newPasswordDto = new NewPasswordDto("ABC123", null);
    NewPasswordDto newPasswordDto1 = new NewPasswordDto("ABC123", null);

    assertEquals(newPasswordDto1, newPasswordDto);
    int expectedHashCodeResult = newPasswordDto.hashCode();
    assertEquals(expectedHashCodeResult, newPasswordDto1.hashCode());
  }
}

