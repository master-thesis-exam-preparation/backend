package cz.vutbr.fit.exampreparation.features.token;

import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.NotFoundException;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.UnauthorizedException;
import cz.vutbr.fit.exampreparation.features.appUser.AppUser;
import cz.vutbr.fit.exampreparation.features.appUser.AppUserService;
import cz.vutbr.fit.exampreparation.features.assignment.Assignment;
import cz.vutbr.fit.exampreparation.features.role.Role;
import cz.vutbr.fit.exampreparation.features.room.Room;
import cz.vutbr.fit.exampreparation.features.term.Term;
import cz.vutbr.fit.exampreparation.security.jwt.JwtTokenUtil;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseCookie;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {JwtTokenUtil.class, TokenServiceImpl.class})
@ExtendWith(SpringExtension.class)
public class TokenServiceImplTest {
  @MockBean
  private AppUserService appUserService;

  @MockBean
  private JwtTokenUtil jwtTokenUtil;

  @MockBean
  private PasswordEncoder passwordEncoder;

  @MockBean
  private TokenRepository tokenRepository;

  @Autowired
  private TokenServiceImpl tokenServiceImpl;

  @Test
  public void testGenerateToken() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());

    Token token = new Token();
    token.setAppUserReference(appUser);
    token.setRemember(true);
    token.setExpireAt(null);
    token.setToken("ABC123");
    token.setTokenType(TokenType.VERIFICATION);
    when(this.tokenRepository.save((Token) any())).thenReturn(token);
    when(this.passwordEncoder.encode((CharSequence) any())).thenReturn("foo");
    this.tokenServiceImpl.generateToken(new AppUser(), TokenType.VERIFICATION, 1, true);
    verify(this.tokenRepository).save((Token) any());
    verify(this.passwordEncoder).encode((CharSequence) any());
  }

  @Test
  public void testDeleteAllUserTokens() {
    doNothing().when(this.tokenRepository)
      .deleteAllByAppUserReference_EmailAndTokenType(anyString(), (TokenType) any());
    this.tokenServiceImpl.deleteAllUserTokens("jane.doe@example.org", TokenType.VERIFICATION);
    verify(this.tokenRepository).deleteAllByAppUserReference_EmailAndTokenType(anyString(), (TokenType) any());
  }

  @Test
  public void testFindByTokenId() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());

    Token token = new Token();
    token.setAppUserReference(appUser);
    token.setRemember(true);
    token.setExpireAt(null);
    token.setToken("ABC123");
    token.setTokenType(TokenType.VERIFICATION);
    Optional<Token> ofResult = Optional.<Token>of(token);
    when(this.tokenRepository.findById((Integer) any())).thenReturn(ofResult);
    Optional<Token> actualFindByTokenIdResult = this.tokenServiceImpl.findByTokenId(1);
    assertSame(ofResult, actualFindByTokenIdResult);
    assertTrue(actualFindByTokenIdResult.isPresent());
    verify(this.tokenRepository).findById((Integer) any());
  }

  @Test
  public void testDeleteByTokenId() {
    doNothing().when(this.tokenRepository).deleteByIdToken(anyInt());
    this.tokenServiceImpl.deleteByTokenId(123);
    verify(this.tokenRepository).deleteByIdToken(anyInt());
  }

  @Test
  public void testGenerateNewKeys() {
    assertThrows(UnauthorizedException.class, () -> this.tokenServiceImpl.generateNewKeys("ABC123"));
    assertThrows(UnauthorizedException.class, () -> this.tokenServiceImpl.generateNewKeys("&"));
    assertThrows(UnauthorizedException.class, () -> this.tokenServiceImpl.generateNewKeys(null));
  }

  @Test
  public void testGetTokenIfIsValid() {
    assertThrows(UnauthorizedException.class, () -> this.tokenServiceImpl.getTokenIfIsValid("ABC123"));
    assertThrows(UnauthorizedException.class, () -> this.tokenServiceImpl.getTokenIfIsValid("&"));
  }

  @Test
  public void testCreateRefreshTokenCookie() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());

    Token token = new Token();
    token.setAppUserReference(appUser);
    token.setRemember(true);
    token.setExpireAt(null);
    token.setToken("ABC123");
    token.setTokenType(TokenType.VERIFICATION);
    when(this.tokenRepository.save((Token) any())).thenReturn(token);
    when(this.passwordEncoder.encode((CharSequence) any())).thenReturn("foo");
    ResponseCookie actualCreateRefreshTokenCookieResult = this.tokenServiceImpl.createRefreshTokenCookie(new AppUser(),
      true);
    assertNull(actualCreateRefreshTokenCookieResult.getDomain());
    assertFalse(actualCreateRefreshTokenCookieResult.isSecure());
    assertTrue(actualCreateRefreshTokenCookieResult.isHttpOnly());
    assertEquals("Strict", actualCreateRefreshTokenCookieResult.getSameSite());
    assertEquals("/auth", actualCreateRefreshTokenCookieResult.getPath());
    assertEquals("refreshToken", actualCreateRefreshTokenCookieResult.getName());
    verify(this.tokenRepository).save((Token) any());
    verify(this.passwordEncoder).encode((CharSequence) any());
  }

  @Test
  public void testCreateRefreshTokenCookie2() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());

    Token token = new Token();
    token.setAppUserReference(appUser);
    token.setRemember(true);
    token.setExpireAt(null);
    token.setToken("ABC123");
    token.setTokenType(TokenType.VERIFICATION);
    when(this.tokenRepository.save((Token) any())).thenReturn(token);
    when(this.passwordEncoder.encode((CharSequence) any())).thenReturn("foo");
    ResponseCookie actualCreateRefreshTokenCookieResult = this.tokenServiceImpl.createRefreshTokenCookie(new AppUser(),
      false);
    assertNull(actualCreateRefreshTokenCookieResult.getDomain());
    assertFalse(actualCreateRefreshTokenCookieResult.isSecure());
    assertTrue(actualCreateRefreshTokenCookieResult.isHttpOnly());
    assertEquals("Strict", actualCreateRefreshTokenCookieResult.getSameSite());
    assertEquals("/auth", actualCreateRefreshTokenCookieResult.getPath());
    assertEquals("refreshToken", actualCreateRefreshTokenCookieResult.getName());
    verify(this.tokenRepository).save((Token) any());
    verify(this.passwordEncoder).encode((CharSequence) any());
  }

  @Test
  public void testCreateRefreshTokenCookie3() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());

    Token token = new Token();
    token.setAppUserReference(appUser);
    token.setRemember(true);
    token.setExpireAt(null);
    token.setToken("ABC123");
    token.setTokenType(TokenType.VERIFICATION);
    when(this.tokenRepository.save((Token) any())).thenReturn(token);
    when(this.passwordEncoder.encode((CharSequence) any())).thenReturn("foo");

    AppUser appUser1 = new AppUser();
    appUser1.setLastName("Doe");
    appUser1.setEmail("jane.doe@example.org");
    appUser1.setDegreesBeforeName("Degrees Before Name");
    appUser1.setPassword("iloveyou");
    appUser1.setUserTokens(new ArrayList<Token>());
    appUser1.setFirstName("Jane");
    appUser1.setVerified(true);
    appUser1.setRooms(new ArrayList<Room>());
    appUser1.setDegreesBehindName("Degrees Behind Name");
    appUser1.setRoles(new ArrayList<Role>());
    appUser1.setLocked(true);
    appUser1.setTerms(new ArrayList<Term>());
    appUser1.setAssignments(new ArrayList<Assignment>());
    Optional<AppUser> ofResult = Optional.<AppUser>of(appUser1);
    when(this.appUserService.findByEmail(anyString())).thenReturn(ofResult);
    ResponseCookie actualCreateRefreshTokenCookieResult = this.tokenServiceImpl
      .createRefreshTokenCookie("jane.doe@example.org", true);
    assertNull(actualCreateRefreshTokenCookieResult.getDomain());
    assertFalse(actualCreateRefreshTokenCookieResult.isSecure());
    assertTrue(actualCreateRefreshTokenCookieResult.isHttpOnly());
    assertEquals("Strict", actualCreateRefreshTokenCookieResult.getSameSite());
    assertEquals("/auth", actualCreateRefreshTokenCookieResult.getPath());
    assertEquals("refreshToken", actualCreateRefreshTokenCookieResult.getName());
    verify(this.tokenRepository).save((Token) any());
    verify(this.passwordEncoder).encode((CharSequence) any());
    verify(this.appUserService).findByEmail(anyString());
  }

  @Test
  public void testCreateRefreshTokenCookie4() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());

    Token token = new Token();
    token.setAppUserReference(appUser);
    token.setRemember(true);
    token.setExpireAt(null);
    token.setToken("ABC123");
    token.setTokenType(TokenType.VERIFICATION);
    when(this.tokenRepository.save((Token) any())).thenReturn(token);
    when(this.passwordEncoder.encode((CharSequence) any())).thenReturn("foo");
    when(this.appUserService.findByEmail(anyString())).thenReturn(Optional.<AppUser>empty());
    assertThrows(NotFoundException.class,
      () -> this.tokenServiceImpl.createRefreshTokenCookie("jane.doe@example.org", true));
    verify(this.appUserService).findByEmail(anyString());
  }

  @Test
  public void testCreateRefreshTokenCookie5() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());

    Token token = new Token();
    token.setAppUserReference(appUser);
    token.setRemember(true);
    token.setExpireAt(null);
    token.setToken("ABC123");
    token.setTokenType(TokenType.VERIFICATION);
    when(this.tokenRepository.save((Token) any())).thenReturn(token);
    when(this.passwordEncoder.encode((CharSequence) any())).thenReturn("foo");

    AppUser appUser1 = new AppUser();
    appUser1.setLastName("Doe");
    appUser1.setEmail("jane.doe@example.org");
    appUser1.setDegreesBeforeName("Degrees Before Name");
    appUser1.setPassword("iloveyou");
    appUser1.setUserTokens(new ArrayList<Token>());
    appUser1.setFirstName("Jane");
    appUser1.setVerified(true);
    appUser1.setRooms(new ArrayList<Room>());
    appUser1.setDegreesBehindName("Degrees Behind Name");
    appUser1.setRoles(new ArrayList<Role>());
    appUser1.setLocked(true);
    appUser1.setTerms(new ArrayList<Term>());
    appUser1.setAssignments(new ArrayList<Assignment>());
    Optional<AppUser> ofResult = Optional.<AppUser>of(appUser1);
    when(this.appUserService.findByEmail(anyString())).thenReturn(ofResult);
    ResponseCookie actualCreateRefreshTokenCookieResult = this.tokenServiceImpl
      .createRefreshTokenCookie("jane.doe@example.org", false);
    assertNull(actualCreateRefreshTokenCookieResult.getDomain());
    assertFalse(actualCreateRefreshTokenCookieResult.isSecure());
    assertTrue(actualCreateRefreshTokenCookieResult.isHttpOnly());
    assertEquals("Strict", actualCreateRefreshTokenCookieResult.getSameSite());
    assertEquals("/auth", actualCreateRefreshTokenCookieResult.getPath());
    assertEquals("refreshToken", actualCreateRefreshTokenCookieResult.getName());
    verify(this.tokenRepository).save((Token) any());
    verify(this.passwordEncoder).encode((CharSequence) any());
    verify(this.appUserService).findByEmail(anyString());
  }

  @Test
  public void testDeleteRefreshTokenCookie() {
    ResponseCookie actualDeleteRefreshTokenCookieResult = this.tokenServiceImpl.deleteRefreshTokenCookie();
    assertNull(actualDeleteRefreshTokenCookieResult.getDomain());
    assertEquals(
      "refreshToken=; Path=/auth; Max-Age=0; Expires=Thu, 01 Jan 1970 00:00:00 GMT; HttpOnly;" + " SameSite=Strict",
      actualDeleteRefreshTokenCookieResult.toString());
    assertFalse(actualDeleteRefreshTokenCookieResult.isSecure());
    assertTrue(actualDeleteRefreshTokenCookieResult.isHttpOnly());
    assertEquals("", actualDeleteRefreshTokenCookieResult.getValue());
    assertEquals("Strict", actualDeleteRefreshTokenCookieResult.getSameSite());
    assertEquals("/auth", actualDeleteRefreshTokenCookieResult.getPath());
    assertEquals("refreshToken", actualDeleteRefreshTokenCookieResult.getName());
  }

  @Test
  public void testIsTokenValid() {
    when(this.passwordEncoder.matches((CharSequence) any(), anyString())).thenReturn(true);
    assertTrue(this.tokenServiceImpl.isTokenValid("ABC123", "ABC123"));
    verify(this.passwordEncoder).matches((CharSequence) any(), anyString());
  }
}

