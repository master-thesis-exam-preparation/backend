package cz.vutbr.fit.exampreparation.features.term.helpers;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EditActionTest {

  @Test
  public void testValueOf3() {
    assertEquals(EditAction.ADD, EditAction.valueOf("ADD"));
  }

  @Test
  public void testValues() {
    EditAction[] actualValuesResult = EditAction.values();
    assertEquals(4, actualValuesResult.length);
    assertEquals(EditAction.ADD, actualValuesResult[0]);
    assertEquals(EditAction.REMOVE, actualValuesResult[1]);
    assertEquals(EditAction.NEW, actualValuesResult[2]);
    assertEquals(EditAction.NOTHING, actualValuesResult[3]);
  }
}

