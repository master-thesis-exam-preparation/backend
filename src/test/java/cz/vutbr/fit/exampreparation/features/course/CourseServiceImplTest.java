package cz.vutbr.fit.exampreparation.features.course;

import cz.vutbr.fit.exampreparation.common.MessageResponseDto;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.BadRequestException;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.ForbiddenException;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.NotFoundException;
import cz.vutbr.fit.exampreparation.features.appUser.AppUser;
import cz.vutbr.fit.exampreparation.features.course.dto.BasicCourseInfo;
import cz.vutbr.fit.exampreparation.features.course.dto.CourseInfoDto;
import cz.vutbr.fit.exampreparation.features.roomPlan.SpacingBetweenStudents;
import cz.vutbr.fit.exampreparation.features.term.Term;
import cz.vutbr.fit.exampreparation.features.termRun.Distribution;
import cz.vutbr.fit.exampreparation.features.termRun.TermRun;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {CourseServiceImpl.class})
@ExtendWith(SpringExtension.class)
public class CourseServiceImplTest {
  @MockBean
  private CourseRepository courseRepository;

  @Autowired
  private CourseServiceImpl courseServiceImpl;

  @Test
  public void testGetAllCoursesInfo() {
    when(this.courseRepository.findAllCurrentAndSorted(anyInt(), anyInt())).thenReturn(new ArrayList<Course>());
    ResponseEntity<List<CourseInfoDto>> actualAllCoursesInfo = this.courseServiceImpl.getAllCoursesInfo(true);
    assertEquals("<200 OK OK,[],[]>", actualAllCoursesInfo.toString());
    assertTrue(actualAllCoursesInfo.hasBody());
    assertEquals(HttpStatus.OK, actualAllCoursesInfo.getStatusCode());
    assertTrue(actualAllCoursesInfo.getHeaders().isEmpty());
    verify(this.courseRepository).findAllCurrentAndSorted(anyInt(), anyInt());
  }

  @Test
  public void testGetAllCoursesInfo2() {
    Course course = new Course();
    course.setAbbreviation("Abbreviation");
    course.setAcademicYear(0);
    course.setName("Name");
    course.setTerms(new ArrayList<Term>());

    ArrayList<Course> courseList = new ArrayList<Course>();
    courseList.add(course);
    when(this.courseRepository.findAllCurrentAndSorted(anyInt(), anyInt())).thenReturn(courseList);
    ResponseEntity<List<CourseInfoDto>> actualAllCoursesInfo = this.courseServiceImpl.getAllCoursesInfo(true);
    assertEquals("<200 OK OK,[CourseInfoDto(courseId=0)],[]>", actualAllCoursesInfo.toString());
    assertTrue(actualAllCoursesInfo.getHeaders().isEmpty());
    assertTrue(actualAllCoursesInfo.hasBody());
    assertEquals(HttpStatus.OK, actualAllCoursesInfo.getStatusCode());
    CourseInfoDto getResult = actualAllCoursesInfo.getBody().get(0);
    assertEquals("Abbreviation", getResult.getAbbreviation());
    assertEquals("CourseInfoDto(courseId=0)", getResult.toString());
    assertEquals("Name", getResult.getName());
    assertEquals(0, getResult.getAcademicYear().intValue());
    verify(this.courseRepository).findAllCurrentAndSorted(anyInt(), anyInt());
  }

  @Test
  public void testGetAllCoursesInfo3() {
    Course course = new Course();
    course.setAbbreviation("Abbreviation");
    course.setAcademicYear(0);
    course.setName("Name");
    course.setTerms(new ArrayList<Term>());

    Course course1 = new Course();
    course1.setAbbreviation("Abbreviation");
    course1.setAcademicYear(0);
    course1.setName("Name");
    course1.setTerms(new ArrayList<Term>());

    ArrayList<Course> courseList = new ArrayList<Course>();
    courseList.add(course1);
    courseList.add(course);
    when(this.courseRepository.findAllCurrentAndSorted(anyInt(), anyInt())).thenReturn(courseList);
    ResponseEntity<List<CourseInfoDto>> actualAllCoursesInfo = this.courseServiceImpl.getAllCoursesInfo(true);
    assertEquals("<200 OK OK,[CourseInfoDto(courseId=0), CourseInfoDto(courseId=0)],[]>",
      actualAllCoursesInfo.toString());
    assertTrue(actualAllCoursesInfo.getHeaders().isEmpty());
    assertTrue(actualAllCoursesInfo.hasBody());
    assertEquals(HttpStatus.OK, actualAllCoursesInfo.getStatusCode());
    List<CourseInfoDto> body = actualAllCoursesInfo.getBody();
    CourseInfoDto getResult = body.get(1);
    assertEquals("CourseInfoDto(courseId=0)", getResult.toString());
    CourseInfoDto getResult1 = body.get(0);
    assertEquals("CourseInfoDto(courseId=0)", getResult1.toString());
    assertEquals("Name", getResult1.getName());
    assertEquals(0, getResult1.getAcademicYear().intValue());
    assertEquals("Abbreviation", getResult1.getAbbreviation());
    assertEquals("Name", getResult.getName());
    assertEquals(0, getResult.getAcademicYear().intValue());
    assertEquals("Abbreviation", getResult.getAbbreviation());
    verify(this.courseRepository).findAllCurrentAndSorted(anyInt(), anyInt());
  }

  @Test
  public void testGetAllCoursesInfo4() {
    when(this.courseRepository.findAllSorted()).thenReturn(new ArrayList<Course>());
    when(this.courseRepository.findAllCurrentAndSorted(anyInt(), anyInt())).thenReturn(new ArrayList<Course>());
    ResponseEntity<List<CourseInfoDto>> actualAllCoursesInfo = this.courseServiceImpl.getAllCoursesInfo(false);
    assertEquals("<200 OK OK,[],[]>", actualAllCoursesInfo.toString());
    assertTrue(actualAllCoursesInfo.hasBody());
    assertEquals(HttpStatus.OK, actualAllCoursesInfo.getStatusCode());
    assertTrue(actualAllCoursesInfo.getHeaders().isEmpty());
    verify(this.courseRepository).findAllSorted();
  }

  @Test
  public void testEditCourse() {
    when(this.courseRepository.findById((Integer) any())).thenThrow(new BadRequestException("An error occurred"));
    assertThrows(BadRequestException.class, () -> this.courseServiceImpl.editCourse(123, new BasicCourseInfo()));
    verify(this.courseRepository).findById((Integer) any());
  }

  @Test
  public void testEditCourse2() {
    when(this.courseRepository.findById((Integer) any())).thenReturn(Optional.<Course>empty());
    assertThrows(NotFoundException.class, () -> this.courseServiceImpl.editCourse(123, new BasicCourseInfo()));
    verify(this.courseRepository).findById((Integer) any());
  }

  @Test
  public void testDeleteCourse() {
    Course course = new Course();
    course.setAbbreviation("Abbreviation");
    course.setAcademicYear(1);
    course.setName("Name");
    course.setTerms(new ArrayList<Term>());
    Optional<Course> ofResult = Optional.<Course>of(course);
    when(this.courseRepository.findById((Integer) any())).thenReturn(ofResult);
    ResponseEntity<MessageResponseDto> actualDeleteCourseResult = this.courseServiceImpl.deleteCourse(123);
    assertTrue(actualDeleteCourseResult.getHeaders().isEmpty());
    assertTrue(actualDeleteCourseResult.hasBody());
    assertEquals(HttpStatus.OK, actualDeleteCourseResult.getStatusCode());
    assertEquals("Předmět úspěšně odstraněn", actualDeleteCourseResult.getBody().getMessage());
    verify(this.courseRepository).findById((Integer) any());
  }

  @Test
  public void testDeleteCourse2() {
    Course course = new Course();
    course.setAbbreviation("Nepodařilo se smazat předmět. Ujistěte se, že tento předmět existuje.");
    course.setAcademicYear(0);
    course.setName("Nepodařilo se smazat předmět. Ujistěte se, že tento předmět existuje.");
    course.setTerms(new ArrayList<Term>());

    Term term = new Term();
    term.setDistribution(Distribution.RANDOM);
    term.setCourseReference(course);
    term.setTermRuns(new ArrayList<TermRun>());
    term.setTermName("Nepodařilo se smazat předmět. Ujistěte se, že tento předmět existuje.");
    term.setTermManagers(new ArrayList<AppUser>());
    term.setDescription("The characteristics of someone or something");
    term.setSpacingBetweenStudents(SpacingBetweenStudents.ZERO);

    ArrayList<Term> termList = new ArrayList<Term>();
    termList.add(term);

    Course course1 = new Course();
    course1.setAbbreviation("Abbreviation");
    course1.setAcademicYear(1);
    course1.setName("Name");
    course1.setTerms(termList);
    Optional<Course> ofResult = Optional.<Course>of(course1);
    when(this.courseRepository.findById((Integer) any())).thenReturn(ofResult);
    assertThrows(ForbiddenException.class, () -> this.courseServiceImpl.deleteCourse(123));
    verify(this.courseRepository).findById((Integer) any());
  }

  @Test
  public void testDeleteCourse3() {
    when(this.courseRepository.findById((Integer) any())).thenReturn(Optional.<Course>empty());
    assertThrows(NotFoundException.class, () -> this.courseServiceImpl.deleteCourse(123));
    verify(this.courseRepository).findById((Integer) any());
  }

  @Test
  public void testImportCourses() {
    ResponseEntity<MessageResponseDto> actualImportCoursesResult = this.courseServiceImpl
      .importCourses(new ArrayList<BasicCourseInfo>());
    assertTrue(actualImportCoursesResult.getHeaders().isEmpty());
    assertTrue(actualImportCoursesResult.hasBody());
    assertEquals(HttpStatus.OK, actualImportCoursesResult.getStatusCode());
    assertEquals("Předměty byly úspěšně importovány", actualImportCoursesResult.getBody().getMessage());
  }

  @Test
  public void testImportCourses2() {
    when(this.courseRepository.findAllByAbbreviationAndNameAndAcademicYear(anyString(), anyString(), anyInt()))
      .thenReturn(new ArrayList<Course>());

    ArrayList<BasicCourseInfo> basicCourseInfoList = new ArrayList<BasicCourseInfo>();
    basicCourseInfoList.add(new CourseInfoDto("Abbreviation", "Name", 1, 123));
    ResponseEntity<MessageResponseDto> actualImportCoursesResult = this.courseServiceImpl
      .importCourses(basicCourseInfoList);
    assertTrue(actualImportCoursesResult.getHeaders().isEmpty());
    assertTrue(actualImportCoursesResult.hasBody());
    assertEquals(HttpStatus.OK, actualImportCoursesResult.getStatusCode());
    assertEquals("Předměty byly úspěšně importovány", actualImportCoursesResult.getBody().getMessage());
    verify(this.courseRepository).findAllByAbbreviationAndNameAndAcademicYear(anyString(), anyString(), anyInt());
  }

  @Test
  public void testFindCourseById() {
    Course course = new Course();
    course.setAbbreviation("Abbreviation");
    course.setAcademicYear(1);
    course.setName("Name");
    course.setTerms(new ArrayList<Term>());
    Optional<Course> ofResult = Optional.<Course>of(course);
    when(this.courseRepository.findById((Integer) any())).thenReturn(ofResult);
    Optional<Course> actualFindCourseByIdResult = this.courseServiceImpl.findCourseById(123);
    assertSame(ofResult, actualFindCourseByIdResult);
    assertTrue(actualFindCourseByIdResult.isPresent());
    verify(this.courseRepository).findById((Integer) any());
  }
}

