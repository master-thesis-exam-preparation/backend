package cz.vutbr.fit.exampreparation.features.appUser.dto;

import cz.vutbr.fit.exampreparation.features.testSchedule.dto.TestScheduleDetailDto;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class LoginNameDtoTest {
  @Test
  public void testCanEqual() {
    assertFalse((new LoginNameDto("Login", "Name And Degrees")).canEqual("Other"));
  }

  @Test
  public void testCanEqual2() {
    LoginNameDto loginNameDto = new LoginNameDto("Login", "Name And Degrees");
    assertTrue(loginNameDto.canEqual(new LoginNameDto("Login", "Name And Degrees")));
  }

  @Test
  public void testConstructor() {
    LoginNameDto actualLoginNameDto = new LoginNameDto();
    actualLoginNameDto.setLogin("Login");
    actualLoginNameDto.setNameAndDegrees("Name And Degrees");
    assertEquals("Login", actualLoginNameDto.getLogin());
    assertEquals("Name And Degrees", actualLoginNameDto.getNameAndDegrees());
    assertEquals("LoginNameDto(login=Login, nameAndDegrees=Name And Degrees)", actualLoginNameDto.toString());
  }

  @Test
  public void testConstructor2() {
    LoginNameDto actualLoginNameDto = new LoginNameDto("Login", "Name And Degrees");
    actualLoginNameDto.setLogin("Login");
    actualLoginNameDto.setNameAndDegrees("Name And Degrees");
    assertEquals("Login", actualLoginNameDto.getLogin());
    assertEquals("Name And Degrees", actualLoginNameDto.getNameAndDegrees());
    assertEquals("LoginNameDto(login=Login, nameAndDegrees=Name And Degrees)", actualLoginNameDto.toString());
  }

  @Test
  public void testEquals() {
    assertNotEquals((new LoginNameDto("Login", "Name And Degrees")), null);
    assertNotEquals((new LoginNameDto("Login", "Name And Degrees")), "Different type to LoginNameDto");
  }

  @Test
  public void testEquals10() {
    LoginNameDto loginNameDto = new LoginNameDto(null, "Name And Degrees");
    LoginNameDto loginNameDto1 = new LoginNameDto(null, "Name And Degrees");

    assertEquals(loginNameDto1, loginNameDto);
    int expectedHashCodeResult = loginNameDto.hashCode();
    assertEquals(expectedHashCodeResult, loginNameDto1.hashCode());
  }

  @Test
  public void testEquals11() {
    LoginNameDto loginNameDto = new LoginNameDto("Login", null);
    LoginNameDto loginNameDto1 = new LoginNameDto("Login", null);

    assertEquals(loginNameDto1, loginNameDto);
    int expectedHashCodeResult = loginNameDto.hashCode();
    assertEquals(expectedHashCodeResult, loginNameDto1.hashCode());
  }

  @Test
  public void testEquals2() {
    LoginNameDto loginNameDto = new LoginNameDto("Login", "Name And Degrees");
    assertEquals(loginNameDto, loginNameDto);
    int expectedHashCodeResult = loginNameDto.hashCode();
    assertEquals(expectedHashCodeResult, loginNameDto.hashCode());
  }

  @Test
  public void testEquals3() {
    LoginNameDto loginNameDto = new LoginNameDto("Login", "Name And Degrees");
    LoginNameDto loginNameDto1 = new LoginNameDto("Login", "Name And Degrees");

    assertEquals(loginNameDto1, loginNameDto);
    int expectedHashCodeResult = loginNameDto.hashCode();
    assertEquals(expectedHashCodeResult, loginNameDto1.hashCode());
  }

  @Test
  public void testEquals4() {
    LoginNameDto loginNameDto = new LoginNameDto(null, "Name And Degrees");
    assertNotEquals(new LoginNameDto("Login", "Name And Degrees"), loginNameDto);
  }

  @Test
  public void testEquals5() {
    LoginNameDto loginNameDto = new LoginNameDto("Name And Degrees", "Name And Degrees");
    assertNotEquals(new LoginNameDto("Login", "Name And Degrees"), loginNameDto);
  }

  @Test
  public void testEquals6() {
    LoginNameDto loginNameDto = new LoginNameDto("Login", "Login");
    assertNotEquals(new LoginNameDto("Login", "Name And Degrees"), loginNameDto);
  }

  @Test
  public void testEquals7() {
    LoginNameDto loginNameDto = new LoginNameDto("Login", null);
    assertNotEquals(new LoginNameDto("Login", "Name And Degrees"), loginNameDto);
  }

  @Test
  public void testEquals8() {
    TestScheduleDetailDto testScheduleDetailDto = mock(TestScheduleDetailDto.class);
    assertNotEquals(new LoginNameDto("Login", "Name And Degrees"), testScheduleDetailDto);
  }

  @Test
  public void testEquals9() {
    LoginNameDto loginNameDto = new LoginNameDto("Login", "Name And Degrees");
    assertNotEquals(new TestScheduleDetailDto(), loginNameDto);
  }
}

