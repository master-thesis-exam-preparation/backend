package cz.vutbr.fit.exampreparation.features.role;

import cz.vutbr.fit.exampreparation.features.appUser.AppUser;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {RoleServiceImpl.class})
@ExtendWith(SpringExtension.class)
public class RoleServiceImplTest {
  @MockBean
  private RoleRepository roleRepository;

  @Autowired
  private RoleServiceImpl roleServiceImpl;

  @Test
  public void testFindRole() {
    Role role = new Role();
    role.setRoleDescription("Role Description");
    role.setRoleName(RoleName.STUDENT);
    role.setAppUsers(new ArrayList<AppUser>());
    Optional<Role> ofResult = Optional.<Role>of(role);
    when(this.roleRepository.findByRoleName((RoleName) any())).thenReturn(ofResult);
    Optional<Role> actualFindRoleResult = this.roleServiceImpl.findRole(RoleName.STUDENT);
    assertSame(ofResult, actualFindRoleResult);
    assertTrue(actualFindRoleResult.isPresent());
    verify(this.roleRepository).findByRoleName((RoleName) any());
  }
}

