package cz.vutbr.fit.exampreparation.features.room.dto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class BasicRoomDtoTest {
  @Test
  public void testCanEqual() {
    assertFalse((new BasicRoomDto("Abbreviation", "Name")).canEqual("Other"));
  }

  @Test
  public void testCanEqual2() {
    BasicRoomDto basicRoomDto = new BasicRoomDto("Abbreviation", "Name");
    assertTrue(basicRoomDto.canEqual(new BasicRoomDto("Abbreviation", "Name")));
  }

  @Test
  public void testConstructor() {
    BasicRoomDto actualBasicRoomDto = new BasicRoomDto();
    actualBasicRoomDto.setAbbreviation("Abbreviation");
    actualBasicRoomDto.setName("Name");
    assertEquals("Abbreviation", actualBasicRoomDto.getAbbreviation());
    assertEquals("Name", actualBasicRoomDto.getName());
    assertEquals("BasicRoomDto(abbreviation=Abbreviation, name=Name)", actualBasicRoomDto.toString());
  }

  @Test
  public void testConstructor2() {
    BasicRoomDto actualBasicRoomDto = new BasicRoomDto("Abbreviation", "Name");
    actualBasicRoomDto.setAbbreviation("Abbreviation");
    actualBasicRoomDto.setName("Name");
    assertEquals("Abbreviation", actualBasicRoomDto.getAbbreviation());
    assertEquals("Name", actualBasicRoomDto.getName());
    assertEquals("BasicRoomDto(abbreviation=Abbreviation, name=Name)", actualBasicRoomDto.toString());
  }

  @Test
  public void testEquals() {
    assertNotEquals((new BasicRoomDto("Abbreviation", "Name")), null);
    assertNotEquals((new BasicRoomDto("Abbreviation", "Name")), "Different type to BasicRoomDto");
  }

  @Test
  public void testEquals10() {
    BasicRoomDto basicRoomDto = new BasicRoomDto("Abbreviation", "Name");
    EditRoomDto editRoomDto = mock(EditRoomDto.class);
    when(editRoomDto.getAbbreviation()).thenReturn("foo");
    when(editRoomDto.canEqual((Object) any())).thenReturn(true);
    assertNotEquals(editRoomDto, basicRoomDto);
  }

  @Test
  public void testEquals11() {
    BasicRoomDto basicRoomDto = new BasicRoomDto("foo", "Name");
    EditRoomDto editRoomDto = mock(EditRoomDto.class);
    when(editRoomDto.getName()).thenReturn("foo");
    when(editRoomDto.getAbbreviation()).thenReturn("foo");
    when(editRoomDto.canEqual((Object) any())).thenReturn(true);
    assertNotEquals(editRoomDto, basicRoomDto);
  }

  @Test
  public void testEquals2() {
    BasicRoomDto basicRoomDto = new BasicRoomDto("Abbreviation", "Name");
    assertEquals(basicRoomDto, basicRoomDto);
    int expectedHashCodeResult = basicRoomDto.hashCode();
    assertEquals(expectedHashCodeResult, basicRoomDto.hashCode());
  }

  @Test
  public void testEquals3() {
    BasicRoomDto basicRoomDto = new BasicRoomDto("Abbreviation", "Name");
    BasicRoomDto basicRoomDto1 = new BasicRoomDto("Abbreviation", "Name");

    assertEquals(basicRoomDto1, basicRoomDto);
    int expectedHashCodeResult = basicRoomDto.hashCode();
    assertEquals(expectedHashCodeResult, basicRoomDto1.hashCode());
  }

  @Test
  public void testEquals4() {
    BasicRoomDto basicRoomDto = new BasicRoomDto(null, "Name");
    assertNotEquals(new BasicRoomDto("Abbreviation", "Name"), basicRoomDto);
  }

  @Test
  public void testEquals5() {
    BasicRoomDto basicRoomDto = new BasicRoomDto("Name", "Name");
    assertNotEquals(new BasicRoomDto("Abbreviation", "Name"), basicRoomDto);
  }

  @Test
  public void testEquals6() {
    BasicRoomDto basicRoomDto = new BasicRoomDto("Abbreviation", "Abbreviation");
    assertNotEquals(new BasicRoomDto("Abbreviation", "Name"), basicRoomDto);
  }

  @Test
  public void testEquals7() {
    BasicRoomDto basicRoomDto = new BasicRoomDto("Abbreviation", null);
    assertNotEquals(new BasicRoomDto("Abbreviation", "Name"), basicRoomDto);
  }

  @Test
  public void testEquals8() {
    EditRoomDto editRoomDto = mock(EditRoomDto.class);
    assertNotEquals(new BasicRoomDto("Abbreviation", "Name"), editRoomDto);
  }

  @Test
  public void testEquals9() {
    BasicRoomDto basicRoomDto = new BasicRoomDto("Abbreviation", "Name");
    assertNotEquals(new EditRoomDto(), basicRoomDto);
  }
}

