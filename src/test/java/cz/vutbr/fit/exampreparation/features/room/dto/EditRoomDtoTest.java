package cz.vutbr.fit.exampreparation.features.room.dto;

import cz.vutbr.fit.exampreparation.features.cell.CellType;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class EditRoomDtoTest {
  @Test
  public void testConstructor() {
    ArrayList<List<CellType>> listList = new ArrayList<List<CellType>>();
    EditRoomDto actualEditRoomDto = new EditRoomDto("Abbreviation", "Name",
      "The characteristics of someone or something", 10, 10, listList);

    assertEquals("Abbreviation", actualEditRoomDto.getAbbreviation());
    assertEquals(
      "EditRoomDto(description=The characteristics of someone or something, numberOfRows=10, numberOfColumns=10,"
        + " cells=[])",
      actualEditRoomDto.toString());
    assertEquals(10, actualEditRoomDto.getNumberOfRows().intValue());
    assertEquals(10, actualEditRoomDto.getNumberOfColumns().intValue());
    assertEquals("Name", actualEditRoomDto.getName());
    assertEquals("The characteristics of someone or something", actualEditRoomDto.getDescription());
    List<List<CellType>> cells = actualEditRoomDto.getCells();
    assertSame(listList, cells);
    assertTrue(cells.isEmpty());
    assertSame(cells, listList);
  }
}

