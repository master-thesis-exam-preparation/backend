package cz.vutbr.fit.exampreparation.features.room.dto;

import cz.vutbr.fit.exampreparation.features.cell.CellType;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class NewRoomDtoTest {
  @Test
  public void testCanEqual() {
    assertFalse((new NewRoomDto()).canEqual("Other"));
  }

  @Test
  public void testCanEqual2() {
    NewRoomDto newRoomDto = new NewRoomDto();
    assertTrue(newRoomDto.canEqual(new NewRoomDto()));
  }

  @Test
  public void testConstructor() {
    NewRoomDto actualNewRoomDto = new NewRoomDto();
    ArrayList<List<CellType>> listList = new ArrayList<List<CellType>>();
    actualNewRoomDto.setCells(listList);
    actualNewRoomDto.setDescription("The characteristics of someone or something");
    assertSame(listList, actualNewRoomDto.getCells());
    assertEquals("The characteristics of someone or something", actualNewRoomDto.getDescription());
    assertEquals("NewRoomDto(description=The characteristics of someone or something, cells=[])",
      actualNewRoomDto.toString());
  }

  @Test
  public void testConstructor2() {
    ArrayList<List<CellType>> listList = new ArrayList<List<CellType>>();
    NewRoomDto actualNewRoomDto = new NewRoomDto("The characteristics of someone or something", listList);
    ArrayList<List<CellType>> listList1 = new ArrayList<List<CellType>>();
    actualNewRoomDto.setCells(listList1);
    actualNewRoomDto.setDescription("The characteristics of someone or something");
    List<List<CellType>> cells = actualNewRoomDto.getCells();
    assertSame(listList1, cells);
    assertEquals(listList, cells);
    assertEquals("The characteristics of someone or something", actualNewRoomDto.getDescription());
    assertEquals("NewRoomDto(description=The characteristics of someone or something, cells=[])",
      actualNewRoomDto.toString());
  }

  @Test
  public void testEquals() {
    assertNotEquals((new NewRoomDto()), null);
    assertNotEquals((new NewRoomDto()), "Different type to NewRoomDto");
  }

  @Test
  public void testEquals2() {
    NewRoomDto newRoomDto = new NewRoomDto();
    assertEquals(newRoomDto, newRoomDto);
    int expectedHashCodeResult = newRoomDto.hashCode();
    assertEquals(expectedHashCodeResult, newRoomDto.hashCode());
  }

  @Test
  public void testEquals3() {
    NewRoomDto newRoomDto = new NewRoomDto();
    NewRoomDto newRoomDto1 = new NewRoomDto();
    assertEquals(newRoomDto1, newRoomDto);
    int expectedHashCodeResult = newRoomDto.hashCode();
    assertEquals(expectedHashCodeResult, newRoomDto1.hashCode());
  }

  @Test
  public void testEquals4() {
    NewRoomDto newRoomDto = new NewRoomDto("The characteristics of someone or something",
      new ArrayList<List<CellType>>());
    assertNotEquals(new NewRoomDto(), newRoomDto);
  }

  @Test
  public void testEquals5() {
    NewRoomDto newRoomDto = new NewRoomDto();
    assertNotEquals(new NewRoomDto("The characteristics of someone or something", new ArrayList<List<CellType>>()), newRoomDto);
  }

  @Test
  public void testEquals6() {
    NewRoomDto newRoomDto = new NewRoomDto(null, new ArrayList<List<CellType>>());
    assertNotEquals(new NewRoomDto(), newRoomDto);
  }

  @Test
  public void testEquals7() {
    NewRoomDto newRoomDto = new NewRoomDto("The characteristics of someone or something",
      new ArrayList<List<CellType>>());
    NewRoomDto newRoomDto1 = new NewRoomDto("The characteristics of someone or something",
      new ArrayList<List<CellType>>());

    assertEquals(newRoomDto1, newRoomDto);
    int expectedHashCodeResult = newRoomDto.hashCode();
    assertEquals(expectedHashCodeResult, newRoomDto1.hashCode());
  }

  @Test
  public void testEquals8() {
    NewRoomDto newRoomDto = new NewRoomDto();
    assertNotEquals(new NewRoomDto(null, new ArrayList<List<CellType>>()), newRoomDto);
  }

  @Test
  public void testEquals9() {
    NewRoomDto newRoomDto = new NewRoomDto("The characteristics of someone or something",
      new ArrayList<List<CellType>>());
    newRoomDto.setAbbreviation("The characteristics of someone or something");
    assertNotEquals(new NewRoomDto("The characteristics of someone or something", new ArrayList<List<CellType>>()), newRoomDto);
  }
}

