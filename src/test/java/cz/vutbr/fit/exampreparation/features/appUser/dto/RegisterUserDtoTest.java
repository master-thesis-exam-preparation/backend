package cz.vutbr.fit.exampreparation.features.appUser.dto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class RegisterUserDtoTest {
  @Test
  public void testCanEqual() {
    assertFalse((new RegisterUserDto("Jane", "Doe", "Degrees Before Name", "Degrees Behind Name")).canEqual("Other"));
  }

  @Test
  public void testCanEqual2() {
    RegisterUserDto registerUserDto = new RegisterUserDto("Jane", "Doe", "Degrees Before Name", "Degrees Behind Name");
    assertTrue(
      registerUserDto.canEqual(new RegisterUserDto("Jane", "Doe", "Degrees Before Name", "Degrees Behind Name")));
  }

  @Test
  public void testConstructor() {
    RegisterUserDto actualRegisterUserDto = new RegisterUserDto();
    actualRegisterUserDto.setDegreesBeforeName("Degrees Before Name");
    actualRegisterUserDto.setDegreesBehindName("Degrees Behind Name");
    actualRegisterUserDto.setFirstName("Jane");
    actualRegisterUserDto.setLastName("Doe");
    assertEquals("Degrees Before Name", actualRegisterUserDto.getDegreesBeforeName());
    assertEquals("Degrees Behind Name", actualRegisterUserDto.getDegreesBehindName());
    assertEquals("Jane", actualRegisterUserDto.getFirstName());
    assertEquals("Doe", actualRegisterUserDto.getLastName());
    assertEquals(
      "RegisterUserDto(firstName=Jane, lastName=Doe, degreesBeforeName=Degrees Before Name, degreesBehindName"
        + "=Degrees Behind Name)",
      actualRegisterUserDto.toString());
  }

  @Test
  public void testConstructor2() {
    RegisterUserDto actualRegisterUserDto = new RegisterUserDto("Jane", "Doe", "Degrees Before Name",
      "Degrees Behind Name");
    actualRegisterUserDto.setDegreesBeforeName("Degrees Before Name");
    actualRegisterUserDto.setDegreesBehindName("Degrees Behind Name");
    actualRegisterUserDto.setFirstName("Jane");
    actualRegisterUserDto.setLastName("Doe");
    assertEquals("Degrees Before Name", actualRegisterUserDto.getDegreesBeforeName());
    assertEquals("Degrees Behind Name", actualRegisterUserDto.getDegreesBehindName());
    assertEquals("Jane", actualRegisterUserDto.getFirstName());
    assertEquals("Doe", actualRegisterUserDto.getLastName());
    assertEquals(
      "RegisterUserDto(firstName=Jane, lastName=Doe, degreesBeforeName=Degrees Before Name, degreesBehindName"
        + "=Degrees Behind Name)",
      actualRegisterUserDto.toString());
  }

  @Test
  public void testEquals() {
    assertNotEquals((new RegisterUserDto("Jane", "Doe", "Degrees Before Name", "Degrees Behind Name")), null);
    assertNotEquals((new RegisterUserDto("Jane", "Doe", "Degrees Before Name", "Degrees Behind Name")), "Different type to RegisterUserDto");
  }

  @Test
  public void testEquals10() {
    RegisterUserDto registerUserDto = new RegisterUserDto("Jane", "Doe", "Degrees Before Name", "Jane");
    assertNotEquals(new RegisterUserDto("Jane", "Doe", "Degrees Before Name", "Degrees Behind Name"), registerUserDto);
  }

  @Test
  public void testEquals11() {
    RegisterUserDto registerUserDto = new RegisterUserDto("Jane", "Doe", "Degrees Before Name", null);
    assertNotEquals(new RegisterUserDto("Jane", "Doe", "Degrees Before Name", "Degrees Behind Name"), registerUserDto);
  }

  @Test
  public void testEquals12() {
    RegisterUserDto registerUserDto = new RegisterUserDto(null, "Doe", "Degrees Before Name", "Degrees Behind Name");
    RegisterUserDto registerUserDto1 = new RegisterUserDto(null, "Doe", "Degrees Before Name", "Degrees Behind Name");

    assertEquals(registerUserDto1, registerUserDto);
    int expectedHashCodeResult = registerUserDto.hashCode();
    assertEquals(expectedHashCodeResult, registerUserDto1.hashCode());
  }

  @Test
  public void testEquals13() {
    RegisterUserDto registerUserDto = new RegisterUserDto("Jane", null, "Degrees Before Name", "Degrees Behind Name");
    RegisterUserDto registerUserDto1 = new RegisterUserDto("Jane", null, "Degrees Before Name", "Degrees Behind Name");

    assertEquals(registerUserDto1, registerUserDto);
    int expectedHashCodeResult = registerUserDto.hashCode();
    assertEquals(expectedHashCodeResult, registerUserDto1.hashCode());
  }

  @Test
  public void testEquals14() {
    RegisterUserDto registerUserDto = new RegisterUserDto("Jane", "Doe", null, "Degrees Behind Name");
    RegisterUserDto registerUserDto1 = new RegisterUserDto("Jane", "Doe", null, "Degrees Behind Name");

    assertEquals(registerUserDto1, registerUserDto);
    int expectedHashCodeResult = registerUserDto.hashCode();
    assertEquals(expectedHashCodeResult, registerUserDto1.hashCode());
  }

  @Test
  public void testEquals15() {
    RegisterUserDto registerUserDto = new RegisterUserDto("Jane", "Doe", "Degrees Before Name", null);
    RegisterUserDto registerUserDto1 = new RegisterUserDto("Jane", "Doe", "Degrees Before Name", null);

    assertEquals(registerUserDto1, registerUserDto);
    int expectedHashCodeResult = registerUserDto.hashCode();
    assertEquals(expectedHashCodeResult, registerUserDto1.hashCode());
  }

  @Test
  public void testEquals16() {
    RegisterUserDto registerUserDto = new RegisterUserDto("Jane", "Doe", "Degrees Before Name", "Degrees Behind Name");
    registerUserDto.setPassword("iloveyou");
    assertNotEquals(new RegisterUserDto("Jane", "Doe", "Degrees Before Name", "Degrees Behind Name"), registerUserDto);
  }

  @Test
  public void testEquals2() {
    RegisterUserDto registerUserDto = new RegisterUserDto("Jane", "Doe", "Degrees Before Name", "Degrees Behind Name");
    assertEquals(registerUserDto, registerUserDto);
    int expectedHashCodeResult = registerUserDto.hashCode();
    assertEquals(expectedHashCodeResult, registerUserDto.hashCode());
  }

  @Test
  public void testEquals3() {
    RegisterUserDto registerUserDto = new RegisterUserDto("Jane", "Doe", "Degrees Before Name", "Degrees Behind Name");
    RegisterUserDto registerUserDto1 = new RegisterUserDto("Jane", "Doe", "Degrees Before Name", "Degrees Behind Name");

    assertEquals(registerUserDto1, registerUserDto);
    int expectedHashCodeResult = registerUserDto.hashCode();
    assertEquals(expectedHashCodeResult, registerUserDto1.hashCode());
  }

  @Test
  public void testEquals4() {
    RegisterUserDto registerUserDto = new RegisterUserDto(null, "Doe", "Degrees Before Name", "Degrees Behind Name");
    assertNotEquals(new RegisterUserDto("Jane", "Doe", "Degrees Before Name", "Degrees Behind Name"), registerUserDto);
  }

  @Test
  public void testEquals5() {
    RegisterUserDto registerUserDto = new RegisterUserDto("Doe", "Doe", "Degrees Before Name", "Degrees Behind Name");
    assertNotEquals(new RegisterUserDto("Jane", "Doe", "Degrees Before Name", "Degrees Behind Name"), registerUserDto);
  }

  @Test
  public void testEquals6() {
    RegisterUserDto registerUserDto = new RegisterUserDto("Jane", "Jane", "Degrees Before Name", "Degrees Behind Name");
    assertNotEquals(new RegisterUserDto("Jane", "Doe", "Degrees Before Name", "Degrees Behind Name"), registerUserDto);
  }

  @Test
  public void testEquals7() {
    RegisterUserDto registerUserDto = new RegisterUserDto("Jane", null, "Degrees Before Name", "Degrees Behind Name");
    assertNotEquals(new RegisterUserDto("Jane", "Doe", "Degrees Before Name", "Degrees Behind Name"), registerUserDto);
  }

  @Test
  public void testEquals8() {
    RegisterUserDto registerUserDto = new RegisterUserDto("Jane", "Doe", "Jane", "Degrees Behind Name");
    assertNotEquals(new RegisterUserDto("Jane", "Doe", "Degrees Before Name", "Degrees Behind Name"), registerUserDto);
  }

  @Test
  public void testEquals9() {
    RegisterUserDto registerUserDto = new RegisterUserDto("Jane", "Doe", null, "Degrees Behind Name");
    assertNotEquals(new RegisterUserDto("Jane", "Doe", "Degrees Before Name", "Degrees Behind Name"), registerUserDto);
  }
}

