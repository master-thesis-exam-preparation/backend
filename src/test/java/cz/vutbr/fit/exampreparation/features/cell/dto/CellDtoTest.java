package cz.vutbr.fit.exampreparation.features.cell.dto;

import cz.vutbr.fit.exampreparation.features.cell.CellType;
import cz.vutbr.fit.exampreparation.features.term.dto.SeatingPlanCell;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class CellDtoTest {
  @Test
  public void testCanEqual() {
    assertFalse((new CellDto()).canEqual("Other"));
  }

  @Test
  public void testCanEqual2() {
    CellDto cellDto = new CellDto();
    assertTrue(cellDto.canEqual(new CellDto()));
  }

  @Test
  public void testConstructor() {
    CellDto actualCellDto = new CellDto();
    actualCellDto.setCellId(123);
    actualCellDto.setCellType(CellType.SEAT);
    assertEquals(123, actualCellDto.getCellId().intValue());
    assertEquals(CellType.SEAT, actualCellDto.getCellType());
    assertEquals("CellDto(cellId=123, cellType=SEAT)", actualCellDto.toString());
  }

  @Test
  public void testConstructor2() {
    CellDto actualCellDto = new CellDto(123, CellType.SEAT);
    actualCellDto.setCellId(123);
    actualCellDto.setCellType(CellType.SEAT);
    assertEquals(123, actualCellDto.getCellId().intValue());
    assertEquals(CellType.SEAT, actualCellDto.getCellType());
    assertEquals("CellDto(cellId=123, cellType=SEAT)", actualCellDto.toString());
  }

  @Test
  public void testEquals() {
    assertNotEquals((new CellDto()), null);
    assertNotEquals((new CellDto()), "Different type to CellDto");
  }

  @Test
  public void testEquals10() {
    CellDto cellDto = new CellDto();
    assertNotEquals(new CellDto(null, CellType.SEAT), cellDto);
  }

  @Test
  public void testEquals2() {
    CellDto cellDto = new CellDto();
    assertEquals(cellDto, cellDto);
    int expectedHashCodeResult = cellDto.hashCode();
    assertEquals(expectedHashCodeResult, cellDto.hashCode());
  }

  @Test
  public void testEquals3() {
    CellDto cellDto = new CellDto();
    CellDto cellDto1 = new CellDto();
    assertEquals(cellDto1, cellDto);
    int expectedHashCodeResult = cellDto.hashCode();
    assertEquals(expectedHashCodeResult, cellDto1.hashCode());
  }

  @Test
  public void testEquals4() {
    CellDto cellDto = new CellDto(123, CellType.SEAT);
    assertNotEquals(new CellDto(), cellDto);
  }

  @Test
  public void testEquals5() {
    SeatingPlanCell seatingPlanCell = mock(SeatingPlanCell.class);
    assertNotEquals(new CellDto(), seatingPlanCell);
  }

  @Test
  public void testEquals6() {
    CellDto cellDto = new CellDto();
    assertNotEquals(new CellDto(123, CellType.SEAT), cellDto);
  }

  @Test
  public void testEquals7() {
    CellDto cellDto = new CellDto();
    assertNotEquals(new SeatingPlanCell(), cellDto);
  }

  @Test
  public void testEquals8() {
    CellDto cellDto = new CellDto(null, CellType.SEAT);
    assertNotEquals(new CellDto(), cellDto);
  }

  @Test
  public void testEquals9() {
    CellDto cellDto = new CellDto(123, CellType.SEAT);
    CellDto cellDto1 = new CellDto(123, CellType.SEAT);

    assertEquals(cellDto1, cellDto);
    int expectedHashCodeResult = cellDto.hashCode();
    assertEquals(expectedHashCodeResult, cellDto1.hashCode());
  }
}

