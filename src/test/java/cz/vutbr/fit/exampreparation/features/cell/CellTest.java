package cz.vutbr.fit.exampreparation.features.cell;

import cz.vutbr.fit.exampreparation.features.roomPlan.RoomPlan;
import cz.vutbr.fit.exampreparation.features.testSchedule.TestSchedule;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class CellTest {
  @Test
  public void testConstructor() {
    RoomPlan roomPlan = new RoomPlan();
    ArrayList<TestSchedule> testScheduleList = new ArrayList<TestSchedule>();
    Cell actualCell = new Cell(CellType.SEAT, 10, 10, roomPlan, testScheduleList, 10, 10);

    assertEquals(CellType.SEAT, actualCell.getCellType());
    List<TestSchedule> testSchedules = actualCell.getTestSchedules();
    assertSame(testScheduleList, testSchedules);
    assertTrue(testSchedules.isEmpty());
    assertEquals(10, actualCell.getSeatRowNumber().intValue());
    assertEquals(10, actualCell.getSeatColumnNumber().intValue());
    assertEquals(10, actualCell.getRowNumber().intValue());
    RoomPlan roomPlanReference = actualCell.getRoomPlanReference();
    assertSame(roomPlan, roomPlanReference);
    assertEquals(0, actualCell.getIdCell());
    assertEquals(10, actualCell.getColumnNumber().intValue());
    assertNull(roomPlanReference.getSpacing());
    assertNull(roomPlanReference.getRoomReference());
    assertEquals(0, roomPlanReference.getIdRoomPlan());
    assertNull(roomPlanReference.getCells());
    assertEquals(0, roomPlanReference.getCapacity());
    assertNull(roomPlanReference.getTermRuns());
    assertSame(roomPlanReference, roomPlan);
    assertSame(testSchedules, testScheduleList);
  }
}

