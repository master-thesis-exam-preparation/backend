package cz.vutbr.fit.exampreparation.features.appUser;

import cz.vutbr.fit.exampreparation.features.appUser.dto.EmployeeDetailsDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static org.mockito.Mockito.when;

@ContextConfiguration(classes = {AppUserController.class})
@ExtendWith(SpringExtension.class)
public class AppUserControllerTest {
  @Autowired
  private AppUserController appUserController;

  @MockBean
  private AppUserService appUserService;

  @Test
  public void testGetAllEmployeesList() throws Exception {
    when(this.appUserService.getAllEmployeesList())
      .thenReturn(new ResponseEntity<List<EmployeeDetailsDto>>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/internal/employees");
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.appUserController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testGetAllEmployeesList2() throws Exception {
    when(this.appUserService.getAllEmployeesList())
      .thenReturn(new ResponseEntity<List<EmployeeDetailsDto>>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/internal/employees");
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.appUserController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }
}

