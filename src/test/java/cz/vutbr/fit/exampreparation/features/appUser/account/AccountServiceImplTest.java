package cz.vutbr.fit.exampreparation.features.appUser.account;

import cz.vutbr.fit.exampreparation.common.MessageResponseDto;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.BadRequestException;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.NotFoundException;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.UnauthorizedException;
import cz.vutbr.fit.exampreparation.features.appUser.AppUser;
import cz.vutbr.fit.exampreparation.features.appUser.AppUserRepository;
import cz.vutbr.fit.exampreparation.features.appUser.AppUserService;
import cz.vutbr.fit.exampreparation.features.appUser.AppUserServiceImpl;
import cz.vutbr.fit.exampreparation.features.appUser.dto.*;
import cz.vutbr.fit.exampreparation.features.assignment.Assignment;
import cz.vutbr.fit.exampreparation.features.role.Role;
import cz.vutbr.fit.exampreparation.features.role.RoleService;
import cz.vutbr.fit.exampreparation.features.room.Room;
import cz.vutbr.fit.exampreparation.features.term.Term;
import cz.vutbr.fit.exampreparation.features.token.*;
import cz.vutbr.fit.exampreparation.mail.MailBuilder;
import cz.vutbr.fit.exampreparation.mail.MailNotificationSender;
import cz.vutbr.fit.exampreparation.security.jwt.JwtConfig;
import cz.vutbr.fit.exampreparation.security.jwt.JwtTokenUtil;
import cz.vutbr.fit.exampreparation.security.jwt.dto.AccessTokenDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.event.SubstituteLoggingEvent;
import org.slf4j.helpers.SubstituteLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.security.access.intercept.RunAsImplAuthenticationProvider;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.thymeleaf.TemplateEngine;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {JwtTokenUtil.class, MailBuilder.class, AccountServiceImpl.class,
  MailNotificationSender.class})
@ExtendWith(SpringExtension.class)
public class AccountServiceImplTest {
  @Autowired
  private AccountServiceImpl accountServiceImpl;

  @MockBean
  private AppUserService appUserService;

  @MockBean
  private AuthenticationManager authenticationManager;

  @MockBean
  private JwtTokenUtil jwtTokenUtil;

  @MockBean
  private MailBuilder mailBuilder;

  @MockBean
  private MailNotificationSender mailNotificationSender;

  @MockBean
  private PasswordEncoder passwordEncoder;

  @MockBean
  private RoleService roleService;

  @MockBean
  private TokenService tokenService;

  @Test
  public void testCreateNewUserAccount() {
    RegisterUserDto registerUserDto = new RegisterUserDto("Jane", "Doe", "Degrees Before Name", "Degrees Behind Name");
    registerUserDto.setPassword("iloveyou");
    ResponseEntity<MessageResponseDto> actualCreateNewUserAccountResult = this.accountServiceImpl
      .createNewUserAccount(registerUserDto);
    assertTrue(actualCreateNewUserAccountResult.getHeaders().isEmpty());
    assertTrue(actualCreateNewUserAccountResult.hasBody());
    assertEquals(HttpStatus.BAD_REQUEST, actualCreateNewUserAccountResult.getStatusCode());
    assertEquals("Heslo musí obsahovat číslici, malé a velké písmeno a speciální znak a musí být delší než 8 znaků",
      actualCreateNewUserAccountResult.getBody().getMessage());
  }

  @Test
  public void testCreateNewUserAccount2() {
    RegisterUserDto registerUserDto = mock(RegisterUserDto.class);
    when(registerUserDto.getPassword()).thenReturn("foo");
    ResponseEntity<MessageResponseDto> actualCreateNewUserAccountResult = this.accountServiceImpl
      .createNewUserAccount(registerUserDto);
    assertTrue(actualCreateNewUserAccountResult.getHeaders().isEmpty());
    assertTrue(actualCreateNewUserAccountResult.hasBody());
    assertEquals(HttpStatus.BAD_REQUEST, actualCreateNewUserAccountResult.getStatusCode());
    assertEquals("Heslo musí obsahovat číslici, malé a velké písmeno a speciální znak a musí být delší než 8 znaků",
      actualCreateNewUserAccountResult.getBody().getMessage());
    verify(registerUserDto).getPassword();
  }

  @Test
  public void testCreateNewUserAccount3() {
    RegisterUserDto registerUserDto = mock(RegisterUserDto.class);
    when(registerUserDto.getPassword()).thenReturn("9");
    ResponseEntity<MessageResponseDto> actualCreateNewUserAccountResult = this.accountServiceImpl
      .createNewUserAccount(registerUserDto);
    assertTrue(actualCreateNewUserAccountResult.getHeaders().isEmpty());
    assertTrue(actualCreateNewUserAccountResult.hasBody());
    assertEquals(HttpStatus.BAD_REQUEST, actualCreateNewUserAccountResult.getStatusCode());
    assertEquals("Heslo musí obsahovat číslici, malé a velké písmeno a speciální znak a musí být delší než 8 znaků",
      actualCreateNewUserAccountResult.getBody().getMessage());
    verify(registerUserDto).getPassword();
  }

  @Test
  public void testCreateNewUserAccount4() {
    RegisterUserDto registerUserDto = mock(RegisterUserDto.class);
    when(registerUserDto.getEmail()).thenReturn("foo");
    when(registerUserDto.getPassword())
      .thenReturn("Heslo musí obsahovat číslici, malé a velké písmeno a speciální znak a musí být delší než 8 znaků");
    ResponseEntity<MessageResponseDto> actualCreateNewUserAccountResult = this.accountServiceImpl
      .createNewUserAccount(registerUserDto);
    assertTrue(actualCreateNewUserAccountResult.getHeaders().isEmpty());
    assertTrue(actualCreateNewUserAccountResult.hasBody());
    assertEquals(HttpStatus.BAD_REQUEST, actualCreateNewUserAccountResult.getStatusCode());
    assertEquals("E-mail neodpovídá tvaru VUT", actualCreateNewUserAccountResult.getBody().getMessage());
    verify(registerUserDto).getEmail();
    verify(registerUserDto).getPassword();
  }

  @Test
  public void testCreateNewUserAccount5() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());
    Optional<AppUser> ofResult = Optional.<AppUser>of(appUser);
    when(this.appUserService.findByEmail(anyString())).thenReturn(ofResult);
    RegisterUserDto registerUserDto = mock(RegisterUserDto.class);
    when(registerUserDto.getEmail()).thenReturn("U@stud.fit.vut.cz");
    when(registerUserDto.getPassword())
      .thenReturn("Heslo musí obsahovat číslici, malé a velké písmeno a speciální znak a musí být delší než 8 znaků");
    ResponseEntity<MessageResponseDto> actualCreateNewUserAccountResult = this.accountServiceImpl
      .createNewUserAccount(registerUserDto);
    assertTrue(actualCreateNewUserAccountResult.getHeaders().isEmpty());
    assertTrue(actualCreateNewUserAccountResult.hasBody());
    assertEquals(HttpStatus.CONFLICT, actualCreateNewUserAccountResult.getStatusCode());
    assertEquals("Tento e-mail je již zaregistrován.", actualCreateNewUserAccountResult.getBody().getMessage());
    verify(this.appUserService).findByEmail(anyString());
    verify(registerUserDto, times(2)).getEmail();
    verify(registerUserDto).getPassword();
  }

  @Test
  public void testCreateNewUserAccount6() {
    when(this.appUserService.findByEmail(anyString())).thenReturn(Optional.<AppUser>empty());
    RegisterUserDto registerUserDto = mock(RegisterUserDto.class);
    when(registerUserDto.getEmail()).thenReturn("U@stud.fit.vut.cz");
    when(registerUserDto.getPassword())
      .thenReturn("Heslo musí obsahovat číslici, malé a velké písmeno a speciální znak a musí být delší než 8 znaků");
    assertThrows(RuntimeException.class, () -> this.accountServiceImpl.createNewUserAccount(registerUserDto));
    verify(this.appUserService).findByEmail(anyString());
    verify(registerUserDto, times(3)).getEmail();
    verify(registerUserDto, times(2)).getPassword();
  }

  @Test
  public void testVerifyAccount() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());

    Token token = new Token();
    token.setAppUserReference(appUser);
    token.setRemember(true);
    token.setExpireAt(null);
    token.setToken("ABC123");
    token.setTokenType(TokenType.VERIFICATION);
    when(this.tokenService.getTokenIfIsValid(anyString())).thenReturn(token);

    AppUser appUser1 = new AppUser();
    appUser1.setLastName("Doe");
    appUser1.setEmail("jane.doe@example.org");
    appUser1.setDegreesBeforeName("Degrees Before Name");
    appUser1.setPassword("iloveyou");
    appUser1.setUserTokens(new ArrayList<Token>());
    appUser1.setFirstName("Jane");
    appUser1.setVerified(true);
    appUser1.setRooms(new ArrayList<Room>());
    appUser1.setDegreesBehindName("Degrees Behind Name");
    appUser1.setRoles(new ArrayList<Role>());
    appUser1.setLocked(true);
    appUser1.setTerms(new ArrayList<Term>());
    appUser1.setAssignments(new ArrayList<Assignment>());
    when(this.appUserService.save((AppUser) any())).thenReturn(appUser1);
    ResponseEntity<MessageResponseDto> actualVerifyAccountResult = this.accountServiceImpl.verifyAccount("ABC123");
    assertEquals(1, actualVerifyAccountResult.getHeaders().size());
    assertTrue(actualVerifyAccountResult.hasBody());
    assertEquals(HttpStatus.FOUND, actualVerifyAccountResult.getStatusCode());
    assertEquals("Vaše registrace byla potvrzena a účet aktivován.\nMůžete se přihlásit.",
      actualVerifyAccountResult.getBody().getMessage());
    verify(this.tokenService).getTokenIfIsValid(anyString());
    verify(this.appUserService).save((AppUser) any());
  }

  @Test
  public void testVerifyAccount2() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());

    Token token = new Token();
    token.setAppUserReference(appUser);
    token.setRemember(true);
    token.setExpireAt(null);
    token.setToken("ABC123");
    token.setTokenType(TokenType.VERIFICATION);
    when(this.tokenService.getTokenIfIsValid(anyString())).thenReturn(token);
    when(this.appUserService.save((AppUser) any())).thenThrow(new BadRequestException("An error occurred"));
    assertThrows(UnauthorizedException.class, () -> this.accountServiceImpl.verifyAccount("ABC123"));
    verify(this.tokenService).getTokenIfIsValid(anyString());
    verify(this.appUserService).save((AppUser) any());
  }

  @Test
  public void testVerifyAccount3() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());

    Token token = new Token();
    token.setAppUserReference(appUser);
    token.setRemember(true);
    token.setExpireAt(null);
    token.setToken("ABC123");
    token.setTokenType(TokenType.REFRESH);
    when(this.tokenService.getTokenIfIsValid(anyString())).thenReturn(token);

    AppUser appUser1 = new AppUser();
    appUser1.setLastName("Doe");
    appUser1.setEmail("jane.doe@example.org");
    appUser1.setDegreesBeforeName("Degrees Before Name");
    appUser1.setPassword("iloveyou");
    appUser1.setUserTokens(new ArrayList<Token>());
    appUser1.setFirstName("Jane");
    appUser1.setVerified(true);
    appUser1.setRooms(new ArrayList<Room>());
    appUser1.setDegreesBehindName("Degrees Behind Name");
    appUser1.setRoles(new ArrayList<Role>());
    appUser1.setLocked(true);
    appUser1.setTerms(new ArrayList<Term>());
    appUser1.setAssignments(new ArrayList<Assignment>());
    when(this.appUserService.save((AppUser) any())).thenReturn(appUser1);
    assertThrows(UnauthorizedException.class, () -> this.accountServiceImpl.verifyAccount("ABC123"));
    verify(this.tokenService).getTokenIfIsValid(anyString());
  }

  @Test
  public void testGenerateNewKeys() {
    ResponseEntity<AccessTokenDto> responseEntity = new ResponseEntity<AccessTokenDto>(HttpStatus.CONTINUE);
    when(this.tokenService.generateNewKeys(anyString())).thenReturn(responseEntity);
    assertSame(responseEntity, this.accountServiceImpl.generateNewKeys("ABC123"));
    verify(this.tokenService).generateNewKeys(anyString());
  }

  @Test
  public void testLoginUser() {
    assertThrows(NullPointerException.class, () -> this.accountServiceImpl.loginUser(new LoginUserDto(true)));
    assertThrows(UnauthorizedException.class, () -> this.accountServiceImpl.loginUser(null));
  }

  @Test
  public void testLogoutUser() {
    ArrayList<AuthenticationProvider> authenticationProviderList = new ArrayList<AuthenticationProvider>();
    authenticationProviderList.add(new RunAsImplAuthenticationProvider());
    ProviderManager authenticationManager = new ProviderManager(authenticationProviderList);
    TokenRepository tokenRepository = mock(TokenRepository.class);
    AppUserRepository appUserRepository = mock(AppUserRepository.class);
    AppUserServiceImpl appUserService = new AppUserServiceImpl(appUserRepository, new Argon2PasswordEncoder(),
      mock(RoleService.class));

    JwtConfig jwtConfig = new JwtConfig();
    AppUserServiceImpl appUserService1 = new AppUserServiceImpl(mock(AppUserRepository.class), null,
      mock(RoleService.class));

    JwtTokenUtil jwtTokenUtil = new JwtTokenUtil(jwtConfig, appUserService1, new SubstituteLogger("Name", null, true));

    TokenServiceImpl tokenService = new TokenServiceImpl(tokenRepository, appUserService, jwtTokenUtil,
      new Argon2PasswordEncoder());

    MailNotificationSender mailNotificationSender = new MailNotificationSender(new JavaMailSenderImpl());
    MailBuilder mailBuilder = new MailBuilder(new TemplateEngine());
    AppUserRepository appUserRepository1 = mock(AppUserRepository.class);
    AppUserServiceImpl appUserService2 = new AppUserServiceImpl(appUserRepository1, new Argon2PasswordEncoder(),
      mock(RoleService.class));

    JwtConfig jwtConfig1 = new JwtConfig();
    AppUserRepository appUserRepository2 = mock(AppUserRepository.class);
    AppUserServiceImpl appUserService3 = new AppUserServiceImpl(appUserRepository2, new Argon2PasswordEncoder(),
      mock(RoleService.class));

    JwtTokenUtil jwtTokenUtil1 = new JwtTokenUtil(jwtConfig1, appUserService3,
      new SubstituteLogger("Name", new LinkedList<SubstituteLoggingEvent>(), true));

    assertThrows(RuntimeException.class,
      () -> (new AccountServiceImpl(tokenService, mailNotificationSender, mailBuilder, appUserService2,
        authenticationManager, jwtTokenUtil1, new Argon2PasswordEncoder(), mock(RoleService.class)))
        .logoutUser("ABC123"));
  }

  @Test
  public void testLogMeIn() {
    ResponseEntity<AccessTokenDto> responseEntity = new ResponseEntity<AccessTokenDto>(HttpStatus.CONTINUE);
    when(this.tokenService.generateNewKeys(anyString())).thenReturn(responseEntity);
    assertSame(responseEntity, this.accountServiceImpl.logMeIn("ABC123"));
    verify(this.tokenService).generateNewKeys(anyString());
  }

  @Test
  public void testLogMeIn2() {
    when(this.tokenService.generateNewKeys(anyString())).thenThrow(new BadRequestException("An error occurred"));
    ResponseEntity<AccessTokenDto> actualLogMeInResult = this.accountServiceImpl.logMeIn("ABC123");
    assertNull(actualLogMeInResult.getBody());
    assertEquals("<200 OK OK,[]>", actualLogMeInResult.toString());
    assertEquals(HttpStatus.OK, actualLogMeInResult.getStatusCode());
    assertTrue(actualLogMeInResult.getHeaders().isEmpty());
    verify(this.tokenService).generateNewKeys(anyString());
  }

  @Test
  public void testForgetPassword() {
    ResponseEntity<MessageResponseDto> actualForgetPasswordResult = this.accountServiceImpl
      .forgetPassword(new EmailDto("jane.doe@example.org"));
    assertTrue(actualForgetPasswordResult.getHeaders().isEmpty());
    assertTrue(actualForgetPasswordResult.hasBody());
    assertEquals(HttpStatus.BAD_REQUEST, actualForgetPasswordResult.getStatusCode());
    assertEquals("E-mail neodpovídá tvaru VUT", actualForgetPasswordResult.getBody().getMessage());
  }

  @Test
  public void testForgetPassword2() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());
    Optional<AppUser> ofResult = Optional.<AppUser>of(appUser);
    when(this.appUserService.findByEmail(anyString())).thenReturn(ofResult);
    ResponseEntity<MessageResponseDto> actualForgetPasswordResult = this.accountServiceImpl
      .forgetPassword(new EmailDto("U@stud.fit.vut.cz"));
    assertTrue(actualForgetPasswordResult.getHeaders().isEmpty());
    assertTrue(actualForgetPasswordResult.hasBody());
    assertEquals(HttpStatus.OK, actualForgetPasswordResult.getStatusCode());
    assertEquals("Na vaši e-mailovou adresu U@stud.fit.vut.cz byl zaslán odkaz pro obnovení hesla.",
      actualForgetPasswordResult.getBody().getMessage());
    verify(this.appUserService).findByEmail(anyString());
  }

  @Test
  public void testForgetPassword3() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("U@stud.fit.vut.cz");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("U@stud.fit.vut.cz");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());

    Token token = new Token();
    token.setAppUserReference(appUser);
    token.setRemember(true);
    token.setExpireAt(null);
    token.setToken("ABC123");
    token.setTokenType(TokenType.VERIFICATION);

    ArrayList<Token> tokenList = new ArrayList<Token>();
    tokenList.add(token);

    AppUser appUser1 = new AppUser();
    appUser1.setLastName("Doe");
    appUser1.setEmail("jane.doe@example.org");
    appUser1.setDegreesBeforeName("Degrees Before Name");
    appUser1.setPassword("iloveyou");
    appUser1.setUserTokens(tokenList);
    appUser1.setFirstName("Jane");
    appUser1.setVerified(true);
    appUser1.setRooms(new ArrayList<Room>());
    appUser1.setDegreesBehindName("Degrees Behind Name");
    appUser1.setRoles(new ArrayList<Role>());
    appUser1.setLocked(true);
    appUser1.setTerms(new ArrayList<Term>());
    appUser1.setAssignments(new ArrayList<Assignment>());
    Optional<AppUser> ofResult = Optional.<AppUser>of(appUser1);
    when(this.appUserService.findByEmail(anyString())).thenReturn(ofResult);
    ResponseEntity<MessageResponseDto> actualForgetPasswordResult = this.accountServiceImpl
      .forgetPassword(new EmailDto("U@stud.fit.vut.cz"));
    assertTrue(actualForgetPasswordResult.getHeaders().isEmpty());
    assertTrue(actualForgetPasswordResult.hasBody());
    assertEquals(HttpStatus.OK, actualForgetPasswordResult.getStatusCode());
    assertEquals("Na vaši e-mailovou adresu U@stud.fit.vut.cz byl zaslán odkaz pro obnovení hesla.",
      actualForgetPasswordResult.getBody().getMessage());
    verify(this.appUserService).findByEmail(anyString());
  }

  @Test
  public void testForgetPassword4() {
    when(this.appUserService.findByEmail(anyString())).thenReturn(Optional.<AppUser>empty());
    assertThrows(BadRequestException.class,
      () -> this.accountServiceImpl.forgetPassword(new EmailDto("U@stud.fit.vut.cz")));
    verify(this.appUserService).findByEmail(anyString());
  }

  @Test
  public void testForgetPassword5() {
    ArrayList<Role> roles = new ArrayList<Role>();
    ArrayList<Term> terms = new ArrayList<Term>();
    ArrayList<Room> rooms = new ArrayList<Room>();

    AppUser appUser = new AppUser("jane.doe@example.org", "Jane", "Doe", "Degrees Before Name", "Degrees Behind Name",
      "iloveyou", true, true, roles, terms, rooms, new ArrayList<Token>());
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());
    Optional<AppUser> ofResult = Optional.<AppUser>of(appUser);
    when(this.appUserService.findByEmail(anyString())).thenReturn(ofResult);
    EmailDto emailDto = mock(EmailDto.class);
    when(emailDto.getEmail()).thenReturn("foo");
    ResponseEntity<MessageResponseDto> actualForgetPasswordResult = this.accountServiceImpl.forgetPassword(emailDto);
    assertTrue(actualForgetPasswordResult.getHeaders().isEmpty());
    assertTrue(actualForgetPasswordResult.hasBody());
    assertEquals(HttpStatus.BAD_REQUEST, actualForgetPasswordResult.getStatusCode());
    assertEquals("E-mail neodpovídá tvaru VUT", actualForgetPasswordResult.getBody().getMessage());
    verify(emailDto).getEmail();
  }

  @Test
  public void testResetPassword() {
    ResponseEntity<MessageResponseDto> actualResetPasswordResult = this.accountServiceImpl
      .resetPassword(new NewPasswordDto("ABC123", "iloveyou"));
    assertTrue(actualResetPasswordResult.getHeaders().isEmpty());
    assertTrue(actualResetPasswordResult.hasBody());
    assertEquals(HttpStatus.BAD_REQUEST, actualResetPasswordResult.getStatusCode());
    assertEquals("Heslo musí obsahovat číslici, malé a velké písmeno a speciální znak a musí být delší než 8 znaků",
      actualResetPasswordResult.getBody().getMessage());
  }

  @Test
  public void testResetPassword2() {
    ResponseEntity<MessageResponseDto> actualResetPasswordResult = this.accountServiceImpl
      .resetPassword(new NewPasswordDto("ABC123", "9"));
    assertTrue(actualResetPasswordResult.getHeaders().isEmpty());
    assertTrue(actualResetPasswordResult.hasBody());
    assertEquals(HttpStatus.BAD_REQUEST, actualResetPasswordResult.getStatusCode());
    assertEquals("Heslo musí obsahovat číslici, malé a velké písmeno a speciální znak a musí být delší než 8 znaků",
      actualResetPasswordResult.getBody().getMessage());
  }

  @Test
  public void testResetPassword3() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());

    Token token = new Token();
    token.setAppUserReference(appUser);
    token.setRemember(true);
    token.setExpireAt(null);
    token.setToken("ABC123");
    token.setTokenType(TokenType.VERIFICATION);
    when(this.tokenService.getTokenIfIsValid(anyString())).thenReturn(token);
    assertThrows(UnauthorizedException.class, () -> this.accountServiceImpl.resetPassword(new NewPasswordDto("ABC123",
      "Heslo musí obsahovat číslici, malé a velké písmeno a speciální znak a musí být delší než 8 znaků")));
    verify(this.tokenService).getTokenIfIsValid(anyString());
  }

  @Test
  public void testResetPassword4() {
    when(this.tokenService.getTokenIfIsValid(anyString())).thenThrow(new BadRequestException("An error occurred"));
    assertThrows(UnauthorizedException.class, () -> this.accountServiceImpl.resetPassword(new NewPasswordDto("ABC123",
      "Heslo musí obsahovat číslici, malé a velké písmeno a speciální znak a musí být delší než 8 znaků")));
    verify(this.tokenService).getTokenIfIsValid(anyString());
  }

  @Test
  public void testResetPassword5() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());

    Token token = new Token();
    token.setAppUserReference(appUser);
    token.setRemember(true);
    token.setExpireAt(null);
    token.setToken("ABC123");
    token.setTokenType(TokenType.FORGET_PASSWORD);
    doNothing().when(this.tokenService).deleteByTokenId(anyInt());
    when(this.tokenService.getTokenIfIsValid(anyString())).thenReturn(token);
    when(this.passwordEncoder.encode((CharSequence) any())).thenReturn("foo");

    AppUser appUser1 = new AppUser();
    appUser1.setLastName("Doe");
    appUser1.setEmail("jane.doe@example.org");
    appUser1.setDegreesBeforeName("Degrees Before Name");
    appUser1.setPassword("iloveyou");
    appUser1.setUserTokens(new ArrayList<Token>());
    appUser1.setFirstName("Jane");
    appUser1.setVerified(true);
    appUser1.setRooms(new ArrayList<Room>());
    appUser1.setDegreesBehindName("Degrees Behind Name");
    appUser1.setRoles(new ArrayList<Role>());
    appUser1.setLocked(true);
    appUser1.setTerms(new ArrayList<Term>());
    appUser1.setAssignments(new ArrayList<Assignment>());
    when(this.appUserService.save((AppUser) any())).thenReturn(appUser1);
    ResponseEntity<MessageResponseDto> actualResetPasswordResult = this.accountServiceImpl
      .resetPassword(new NewPasswordDto("ABC123",
        "Heslo musí obsahovat číslici, malé a velké písmeno a speciální znak a musí být delší než 8 znaků"));
    assertTrue(actualResetPasswordResult.getHeaders().isEmpty());
    assertTrue(actualResetPasswordResult.hasBody());
    assertEquals(HttpStatus.OK, actualResetPasswordResult.getStatusCode());
    assertEquals("Vaše heslo bylo změněno, můžete se přihlásit", actualResetPasswordResult.getBody().getMessage());
    verify(this.tokenService).deleteByTokenId(anyInt());
    verify(this.tokenService).getTokenIfIsValid(anyString());
    verify(this.passwordEncoder).encode((CharSequence) any());
    verify(this.appUserService).save((AppUser) any());
  }

  @Test
  public void testResetPassword6() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());

    Token token = new Token();
    token.setAppUserReference(appUser);
    token.setRemember(true);
    token.setExpireAt(null);
    token.setToken("ABC123");
    token.setTokenType(TokenType.FORGET_PASSWORD);
    doThrow(new BadRequestException("An error occurred")).when(this.tokenService).deleteByTokenId(anyInt());
    when(this.tokenService.getTokenIfIsValid(anyString())).thenReturn(token);
    when(this.passwordEncoder.encode((CharSequence) any())).thenReturn("foo");

    AppUser appUser1 = new AppUser();
    appUser1.setLastName("Doe");
    appUser1.setEmail("jane.doe@example.org");
    appUser1.setDegreesBeforeName("Degrees Before Name");
    appUser1.setPassword("iloveyou");
    appUser1.setUserTokens(new ArrayList<Token>());
    appUser1.setFirstName("Jane");
    appUser1.setVerified(true);
    appUser1.setRooms(new ArrayList<Room>());
    appUser1.setDegreesBehindName("Degrees Behind Name");
    appUser1.setRoles(new ArrayList<Role>());
    appUser1.setLocked(true);
    appUser1.setTerms(new ArrayList<Term>());
    appUser1.setAssignments(new ArrayList<Assignment>());
    when(this.appUserService.save((AppUser) any())).thenReturn(appUser1);
    assertThrows(UnauthorizedException.class, () -> this.accountServiceImpl.resetPassword(new NewPasswordDto("ABC123",
      "Heslo musí obsahovat číslici, malé a velké písmeno a speciální znak a musí být delší než 8 znaků")));
    verify(this.tokenService).deleteByTokenId(anyInt());
    verify(this.tokenService).getTokenIfIsValid(anyString());
    verify(this.passwordEncoder).encode((CharSequence) any());
    verify(this.appUserService).save((AppUser) any());
  }

  @Test
  public void testGetAllUsersData() {
    when(this.appUserService.getAllAccountsInfo()).thenReturn(new ArrayList<AccountInfoDto>());
    ResponseEntity<List<AccountInfoDto>> actualAllUsersData = this.accountServiceImpl.getAllUsersData();
    assertEquals("<200 OK OK,[],[]>", actualAllUsersData.toString());
    assertTrue(actualAllUsersData.hasBody());
    assertEquals(HttpStatus.OK, actualAllUsersData.getStatusCode());
    assertTrue(actualAllUsersData.getHeaders().isEmpty());
    verify(this.appUserService).getAllAccountsInfo();
  }

  @Test
  public void testChangeAccountRole2() {
    when(this.appUserService.findById(anyInt())).thenReturn(Optional.<AppUser>empty());
    assertThrows(NotFoundException.class, () -> this.accountServiceImpl.changeAccountRole(new NewRoleDto("Role"), 123));
    verify(this.appUserService).findById(anyInt());
  }

  @Test
  public void testDeleteAccount() {
    doNothing().when(this.appUserService).deleteUserById(anyInt());
    ResponseEntity<MessageResponseDto> actualDeleteAccountResult = this.accountServiceImpl.deleteAccount(123);
    assertTrue(actualDeleteAccountResult.getHeaders().isEmpty());
    assertTrue(actualDeleteAccountResult.hasBody());
    assertEquals(HttpStatus.OK, actualDeleteAccountResult.getStatusCode());
    assertEquals("Uživatelský účet byl úspěšně odstraněn.", actualDeleteAccountResult.getBody().getMessage());
    verify(this.appUserService).deleteUserById(anyInt());
  }

  @Test
  public void testLockAccount() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());
    Optional<AppUser> ofResult = Optional.<AppUser>of(appUser);

    AppUser appUser1 = new AppUser();
    appUser1.setLastName("Doe");
    appUser1.setEmail("jane.doe@example.org");
    appUser1.setDegreesBeforeName("Degrees Before Name");
    appUser1.setPassword("iloveyou");
    appUser1.setUserTokens(new ArrayList<Token>());
    appUser1.setFirstName("Jane");
    appUser1.setVerified(true);
    appUser1.setRooms(new ArrayList<Room>());
    appUser1.setDegreesBehindName("Degrees Behind Name");
    appUser1.setRoles(new ArrayList<Role>());
    appUser1.setLocked(true);
    appUser1.setTerms(new ArrayList<Term>());
    appUser1.setAssignments(new ArrayList<Assignment>());
    when(this.appUserService.save((AppUser) any())).thenReturn(appUser1);
    when(this.appUserService.findById(anyInt())).thenReturn(ofResult);
    ResponseEntity<MessageResponseDto> actualLockAccountResult = this.accountServiceImpl.lockAccount(123);
    assertTrue(actualLockAccountResult.getHeaders().isEmpty());
    assertTrue(actualLockAccountResult.hasBody());
    assertEquals(HttpStatus.OK, actualLockAccountResult.getStatusCode());
    assertEquals("Uživatelský účet byl úspěšně zablokován.", actualLockAccountResult.getBody().getMessage());
    verify(this.appUserService).findById(anyInt());
    verify(this.appUserService).save((AppUser) any());
  }

  @Test
  public void testLockAccount2() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());
    when(this.appUserService.save((AppUser) any())).thenReturn(appUser);
    when(this.appUserService.findById(anyInt())).thenReturn(Optional.<AppUser>empty());
    assertThrows(NotFoundException.class, () -> this.accountServiceImpl.lockAccount(123));
    verify(this.appUserService).findById(anyInt());
  }

  @Test
  public void testUnlockAccount() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());
    Optional<AppUser> ofResult = Optional.<AppUser>of(appUser);

    AppUser appUser1 = new AppUser();
    appUser1.setLastName("Doe");
    appUser1.setEmail("jane.doe@example.org");
    appUser1.setDegreesBeforeName("Degrees Before Name");
    appUser1.setPassword("iloveyou");
    appUser1.setUserTokens(new ArrayList<Token>());
    appUser1.setFirstName("Jane");
    appUser1.setVerified(true);
    appUser1.setRooms(new ArrayList<Room>());
    appUser1.setDegreesBehindName("Degrees Behind Name");
    appUser1.setRoles(new ArrayList<Role>());
    appUser1.setLocked(true);
    appUser1.setTerms(new ArrayList<Term>());
    appUser1.setAssignments(new ArrayList<Assignment>());
    when(this.appUserService.save((AppUser) any())).thenReturn(appUser1);
    when(this.appUserService.findById(anyInt())).thenReturn(ofResult);
    ResponseEntity<MessageResponseDto> actualUnlockAccountResult = this.accountServiceImpl.unlockAccount(123);
    assertTrue(actualUnlockAccountResult.getHeaders().isEmpty());
    assertTrue(actualUnlockAccountResult.hasBody());
    assertEquals(HttpStatus.OK, actualUnlockAccountResult.getStatusCode());
    assertEquals("Uživatelský účet byl úspěšně odblokován.", actualUnlockAccountResult.getBody().getMessage());
    verify(this.appUserService).findById(anyInt());
    verify(this.appUserService).save((AppUser) any());
  }

  @Test
  public void testUnlockAccount2() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());
    when(this.appUserService.save((AppUser) any())).thenReturn(appUser);
    when(this.appUserService.findById(anyInt())).thenReturn(Optional.<AppUser>empty());
    assertThrows(NotFoundException.class, () -> this.accountServiceImpl.unlockAccount(123));
    verify(this.appUserService).findById(anyInt());
  }
}

