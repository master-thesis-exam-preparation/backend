package cz.vutbr.fit.exampreparation.features.appUser.dto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class UserDataDtoTest {
  @Test
  public void testCanEqual() {
    assertFalse((new UserDataDto("Degrees Before Name", "Degrees Behind Name", "Jane", "Doe")).canEqual("Other"));
  }

  @Test
  public void testCanEqual2() {
    UserDataDto userDataDto = new UserDataDto("Degrees Before Name", "Degrees Behind Name", "Jane", "Doe");
    assertTrue(userDataDto.canEqual(new UserDataDto("Degrees Before Name", "Degrees Behind Name", "Jane", "Doe")));
  }

  @Test
  public void testConstructor() {
    UserDataDto actualUserDataDto = new UserDataDto("Degrees Before Name", "Degrees Behind Name", "Jane", "Doe");
    actualUserDataDto.setDegreesBeforeName("Degrees Before Name");
    actualUserDataDto.setDegreesBehindName("Degrees Behind Name");
    actualUserDataDto.setFirstName("Jane");
    actualUserDataDto.setLastName("Doe");
    assertEquals("Degrees Before Name", actualUserDataDto.getDegreesBeforeName());
    assertEquals("Degrees Behind Name", actualUserDataDto.getDegreesBehindName());
    assertEquals("Jane", actualUserDataDto.getFirstName());
    assertEquals("Doe", actualUserDataDto.getLastName());
    assertEquals(
      "UserDataDto(degreesBeforeName=Degrees Before Name, degreesBehindName=Degrees Behind Name, firstName=Jane,"
        + " lastName=Doe)",
      actualUserDataDto.toString());
  }

  @Test
  public void testEquals() {
    assertNotEquals((new UserDataDto("Degrees Before Name", "Degrees Behind Name", "Jane", "Doe")), null);
    assertNotEquals((new UserDataDto("Degrees Before Name", "Degrees Behind Name", "Jane", "Doe")), "Different type to UserDataDto");
  }

  @Test
  public void testEquals10() {
    UserDataDto userDataDto = new UserDataDto("Degrees Before Name", "Degrees Behind Name", "Jane",
      "Degrees Before Name");
    assertNotEquals(new UserDataDto("Degrees Before Name", "Degrees Behind Name", "Jane", "Doe"), userDataDto);
  }

  @Test
  public void testEquals11() {
    UserDataDto userDataDto = new UserDataDto("Degrees Before Name", "Degrees Behind Name", "Jane", null);
    assertNotEquals(new UserDataDto("Degrees Before Name", "Degrees Behind Name", "Jane", "Doe"), userDataDto);
  }

  @Test
  public void testEquals12() {
    UserDataDto userDataDto = new UserDataDto(null, "Degrees Behind Name", "Jane", "Doe");
    UserDataDto userDataDto1 = new UserDataDto(null, "Degrees Behind Name", "Jane", "Doe");

    assertEquals(userDataDto1, userDataDto);
    int expectedHashCodeResult = userDataDto.hashCode();
    assertEquals(expectedHashCodeResult, userDataDto1.hashCode());
  }

  @Test
  public void testEquals13() {
    UserDataDto userDataDto = new UserDataDto("Degrees Before Name", null, "Jane", "Doe");
    UserDataDto userDataDto1 = new UserDataDto("Degrees Before Name", null, "Jane", "Doe");

    assertEquals(userDataDto1, userDataDto);
    int expectedHashCodeResult = userDataDto.hashCode();
    assertEquals(expectedHashCodeResult, userDataDto1.hashCode());
  }

  @Test
  public void testEquals14() {
    UserDataDto userDataDto = new UserDataDto("Degrees Before Name", "Degrees Behind Name", null, "Doe");
    UserDataDto userDataDto1 = new UserDataDto("Degrees Before Name", "Degrees Behind Name", null, "Doe");

    assertEquals(userDataDto1, userDataDto);
    int expectedHashCodeResult = userDataDto.hashCode();
    assertEquals(expectedHashCodeResult, userDataDto1.hashCode());
  }

  @Test
  public void testEquals15() {
    UserDataDto userDataDto = new UserDataDto("Degrees Before Name", "Degrees Behind Name", "Jane", null);
    UserDataDto userDataDto1 = new UserDataDto("Degrees Before Name", "Degrees Behind Name", "Jane", null);

    assertEquals(userDataDto1, userDataDto);
    int expectedHashCodeResult = userDataDto.hashCode();
    assertEquals(expectedHashCodeResult, userDataDto1.hashCode());
  }

  @Test
  public void testEquals2() {
    UserDataDto userDataDto = new UserDataDto("Degrees Before Name", "Degrees Behind Name", "Jane", "Doe");
    assertEquals(userDataDto, userDataDto);
    int expectedHashCodeResult = userDataDto.hashCode();
    assertEquals(expectedHashCodeResult, userDataDto.hashCode());
  }

  @Test
  public void testEquals3() {
    UserDataDto userDataDto = new UserDataDto("Degrees Before Name", "Degrees Behind Name", "Jane", "Doe");
    UserDataDto userDataDto1 = new UserDataDto("Degrees Before Name", "Degrees Behind Name", "Jane", "Doe");

    assertEquals(userDataDto1, userDataDto);
    int expectedHashCodeResult = userDataDto.hashCode();
    assertEquals(expectedHashCodeResult, userDataDto1.hashCode());
  }

  @Test
  public void testEquals4() {
    UserDataDto userDataDto = new UserDataDto(null, "Degrees Behind Name", "Jane", "Doe");
    assertNotEquals(new UserDataDto("Degrees Before Name", "Degrees Behind Name", "Jane", "Doe"), userDataDto);
  }

  @Test
  public void testEquals5() {
    UserDataDto userDataDto = new UserDataDto("Degrees Behind Name", "Degrees Behind Name", "Jane", "Doe");
    assertNotEquals(new UserDataDto("Degrees Before Name", "Degrees Behind Name", "Jane", "Doe"), userDataDto);
  }

  @Test
  public void testEquals6() {
    UserDataDto userDataDto = new UserDataDto("Degrees Before Name", "Degrees Before Name", "Jane", "Doe");
    assertNotEquals(new UserDataDto("Degrees Before Name", "Degrees Behind Name", "Jane", "Doe"), userDataDto);
  }

  @Test
  public void testEquals7() {
    UserDataDto userDataDto = new UserDataDto("Degrees Before Name", null, "Jane", "Doe");
    assertNotEquals(new UserDataDto("Degrees Before Name", "Degrees Behind Name", "Jane", "Doe"), userDataDto);
  }

  @Test
  public void testEquals8() {
    UserDataDto userDataDto = new UserDataDto("Degrees Before Name", "Degrees Behind Name", "Degrees Before Name",
      "Doe");
    assertNotEquals(new UserDataDto("Degrees Before Name", "Degrees Behind Name", "Jane", "Doe"), userDataDto);
  }

  @Test
  public void testEquals9() {
    UserDataDto userDataDto = new UserDataDto("Degrees Before Name", "Degrees Behind Name", null, "Doe");
    assertFalse(userDataDto.equals(new UserDataDto("Degrees Before Name", "Degrees Behind Name", "Jane", "Doe")));
  }
}

