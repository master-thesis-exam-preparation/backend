package cz.vutbr.fit.exampreparation.features.termStudent;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {TermStudentServiceImpl.class})
@ExtendWith(SpringExtension.class)
public class TermStudentServiceImplTest {
  @MockBean
  private TermStudentRepository termStudentRepository;

  @Autowired
  private TermStudentServiceImpl termStudentServiceImpl;

  @Test
  public void testFindUserByLogin() {
    TermStudent termStudent = new TermStudent();
    termStudent.setLogin("Login");
    termStudent.setNameWithDegrees("Name With Degrees");
    when(this.termStudentRepository.findByLogin(anyString())).thenReturn(termStudent);
    assertSame(termStudent, this.termStudentServiceImpl.findUserByLogin("Login"));
    verify(this.termStudentRepository).findByLogin(anyString());
  }
}

