package cz.vutbr.fit.exampreparation.features.termStudent;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TermStudentTest {
  @Test
  public void testConstructor() {
    TermStudent actualTermStudent = new TermStudent("Login", "Name With Degrees");

    assertEquals(0, actualTermStudent.getIdStudent());
    assertEquals("Name With Degrees", actualTermStudent.getNameWithDegrees());
    assertEquals("Login", actualTermStudent.getLogin());
  }
}

