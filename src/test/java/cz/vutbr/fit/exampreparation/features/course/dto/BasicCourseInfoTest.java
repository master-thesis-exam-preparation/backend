package cz.vutbr.fit.exampreparation.features.course.dto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class BasicCourseInfoTest {
  @Test
  public void testCanEqual() {
    assertFalse((new BasicCourseInfo()).canEqual("Other"));
  }

  @Test
  public void testCanEqual2() {
    BasicCourseInfo basicCourseInfo = new BasicCourseInfo();
    assertTrue(basicCourseInfo.canEqual(new BasicCourseInfo()));
  }

  @Test
  public void testConstructor() {
    BasicCourseInfo actualBasicCourseInfo = new BasicCourseInfo();
    actualBasicCourseInfo.setAbbreviation("Abbreviation");
    actualBasicCourseInfo.setAcademicYear(1);
    actualBasicCourseInfo.setName("Name");
    assertEquals("Abbreviation", actualBasicCourseInfo.getAbbreviation());
    assertEquals(1, actualBasicCourseInfo.getAcademicYear().intValue());
    assertEquals("Name", actualBasicCourseInfo.getName());
    assertEquals("BasicCourseInfo(abbreviation=Abbreviation, name=Name, academicYear=1)",
      actualBasicCourseInfo.toString());
  }

  @Test
  public void testConstructor2() {
    BasicCourseInfo actualBasicCourseInfo = new BasicCourseInfo("Abbreviation", "Name", 1);
    actualBasicCourseInfo.setAbbreviation("Abbreviation");
    actualBasicCourseInfo.setAcademicYear(1);
    actualBasicCourseInfo.setName("Name");
    assertEquals("Abbreviation", actualBasicCourseInfo.getAbbreviation());
    assertEquals(1, actualBasicCourseInfo.getAcademicYear().intValue());
    assertEquals("Name", actualBasicCourseInfo.getName());
    assertEquals("BasicCourseInfo(abbreviation=Abbreviation, name=Name, academicYear=1)",
      actualBasicCourseInfo.toString());
  }

  @Test
  public void testEquals() {
    assertNotEquals((new BasicCourseInfo()), null);
    assertNotEquals((new BasicCourseInfo()), "Different type to BasicCourseInfo");
  }

  @Test
  public void testEquals10() {
    BasicCourseInfo basicCourseInfo = new BasicCourseInfo(null, "Name", 1);
    CourseInfoDto courseInfoDto = mock(CourseInfoDto.class);
    when(courseInfoDto.getAbbreviation()).thenReturn("foo");
    when(courseInfoDto.getAcademicYear()).thenReturn(1);
    when(courseInfoDto.canEqual((Object) any())).thenReturn(true);
    assertNotEquals(courseInfoDto, basicCourseInfo);
  }

  @Test
  public void testEquals11() {
    BasicCourseInfo basicCourseInfo = new BasicCourseInfo("foo", "Name", 1);
    CourseInfoDto courseInfoDto = mock(CourseInfoDto.class);
    when(courseInfoDto.getName()).thenReturn("foo");
    when(courseInfoDto.getAbbreviation()).thenReturn("foo");
    when(courseInfoDto.getAcademicYear()).thenReturn(1);
    when(courseInfoDto.canEqual((Object) any())).thenReturn(true);
    assertNotEquals(courseInfoDto, basicCourseInfo);
  }

  @Test
  public void testEquals12() {
    BasicCourseInfo basicCourseInfo = new BasicCourseInfo("foo", null, 1);
    CourseInfoDto courseInfoDto = mock(CourseInfoDto.class);
    when(courseInfoDto.getName()).thenReturn("foo");
    when(courseInfoDto.getAbbreviation()).thenReturn("foo");
    when(courseInfoDto.getAcademicYear()).thenReturn(1);
    when(courseInfoDto.canEqual((Object) any())).thenReturn(true);
    assertNotEquals(courseInfoDto, basicCourseInfo);
  }

  @Test
  public void testEquals2() {
    BasicCourseInfo basicCourseInfo = new BasicCourseInfo();
    assertEquals(basicCourseInfo, basicCourseInfo);
    int expectedHashCodeResult = basicCourseInfo.hashCode();
    assertEquals(expectedHashCodeResult, basicCourseInfo.hashCode());
  }

  @Test
  public void testEquals3() {
    BasicCourseInfo basicCourseInfo = new BasicCourseInfo();
    BasicCourseInfo basicCourseInfo1 = new BasicCourseInfo();
    assertEquals(basicCourseInfo1, basicCourseInfo);
    int expectedHashCodeResult = basicCourseInfo.hashCode();
    assertEquals(expectedHashCodeResult, basicCourseInfo1.hashCode());
  }

  @Test
  public void testEquals4() {
    BasicCourseInfo basicCourseInfo = new BasicCourseInfo("Abbreviation", "Name", 1);
    assertNotEquals(new BasicCourseInfo(), basicCourseInfo);
  }

  @Test
  public void testEquals5() {
    CourseInfoDto courseInfoDto = mock(CourseInfoDto.class);
    assertNotEquals(new BasicCourseInfo(), courseInfoDto);
  }

  @Test
  public void testEquals6() {
    BasicCourseInfo basicCourseInfo = new BasicCourseInfo();
    assertNotEquals(new BasicCourseInfo("Abbreviation", "Name", 1), basicCourseInfo);
  }

  @Test
  public void testEquals7() {
    BasicCourseInfo basicCourseInfo = new BasicCourseInfo();
    assertNotEquals(new CourseInfoDto(), basicCourseInfo);
  }

  @Test
  public void testEquals8() {
    BasicCourseInfo basicCourseInfo = new BasicCourseInfo();
    CourseInfoDto courseInfoDto = mock(CourseInfoDto.class);
    when(courseInfoDto.getAcademicYear()).thenReturn(1);
    when(courseInfoDto.canEqual((Object) any())).thenReturn(true);
    assertNotEquals(courseInfoDto, basicCourseInfo);
  }

  @Test
  public void testEquals9() {
    BasicCourseInfo basicCourseInfo = new BasicCourseInfo("Abbreviation", "Name", 1);
    CourseInfoDto courseInfoDto = mock(CourseInfoDto.class);
    when(courseInfoDto.getAbbreviation()).thenReturn("foo");
    when(courseInfoDto.getAcademicYear()).thenReturn(1);
    when(courseInfoDto.canEqual((Object) any())).thenReturn(true);
    assertNotEquals(courseInfoDto, basicCourseInfo);
  }
}

