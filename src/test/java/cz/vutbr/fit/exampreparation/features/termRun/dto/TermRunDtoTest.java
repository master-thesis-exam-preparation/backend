package cz.vutbr.fit.exampreparation.features.termRun.dto;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class TermRunDtoTest {
  @Test
  public void testCanEqual() {
    assertFalse((new TermRunDto()).canEqual("Other"));
  }

  @Test
  public void testCanEqual2() {
    TermRunDto termRunDto = new TermRunDto();
    assertTrue(termRunDto.canEqual(new TermRunDto()));
  }

  @Test
  public void testConstructor() {
    TermRunDto actualTermRunDto = new TermRunDto();
    ArrayList<Integer> integerList = new ArrayList<Integer>();
    actualTermRunDto.setRooms(integerList);
    assertSame(integerList, actualTermRunDto.getRooms());
    assertEquals("TermRunDto(rooms=[])", actualTermRunDto.toString());
  }

  @Test
  public void testEquals() {
    assertNotEquals((new TermRunDto()), null);
    assertNotEquals((new TermRunDto()), "Different type to TermRunDto");
    assertNotEquals((new TermRunDto()), null);
  }

  @Test
  public void testEquals2() {
    TermRunDto termRunDto = new TermRunDto();
    assertEquals(termRunDto, termRunDto);
    int expectedHashCodeResult = termRunDto.hashCode();
    assertEquals(expectedHashCodeResult, termRunDto.hashCode());
  }

  @Test
  public void testEquals3() {
    TermRunDto termRunDto = new TermRunDto();
    TermRunDto termRunDto1 = new TermRunDto();
    assertEquals(termRunDto1, termRunDto);
    int expectedHashCodeResult = termRunDto.hashCode();
    assertEquals(expectedHashCodeResult, termRunDto1.hashCode());
  }

  @Test
  public void testEquals4() {
    TermRunDto termRunDto = new TermRunDto();
    termRunDto.setId(1);
    assertNotEquals(new TermRunDto(), termRunDto);
  }

  @Test
  public void testEquals5() {
    TermRunDto termRunDto = new TermRunDto();
    termRunDto.setRooms(new ArrayList<Integer>());
    assertNotEquals(new TermRunDto(), termRunDto);
  }

  @Test
  public void testEquals6() {
    TermRunDto termRunDto = new TermRunDto();

    TermRunDto termRunDto1 = new TermRunDto();
    termRunDto1.setRooms(new ArrayList<Integer>());
    assertNotEquals(termRunDto1, termRunDto);
  }
}

