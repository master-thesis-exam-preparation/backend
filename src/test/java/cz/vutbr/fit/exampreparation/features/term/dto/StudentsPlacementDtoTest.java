package cz.vutbr.fit.exampreparation.features.term.dto;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class StudentsPlacementDtoTest {
  @Test
  public void testCanEqual() {
    assertFalse((new StudentsPlacementDto()).canEqual("Other"));
  }

  @Test
  public void testCanEqual2() {
    StudentsPlacementDto studentsPlacementDto = new StudentsPlacementDto();
    assertTrue(studentsPlacementDto.canEqual(new StudentsPlacementDto()));
  }

  @Test
  public void testConstructor() {
    StudentsPlacementDto actualStudentsPlacementDto = new StudentsPlacementDto();
    actualStudentsPlacementDto.setRoomId(123);
    ArrayList<Integer> integerList = new ArrayList<Integer>();
    actualStudentsPlacementDto.setSeatIds(integerList);
    ArrayList<String> stringList = new ArrayList<String>();
    actualStudentsPlacementDto.setStudentLogins(stringList);
    actualStudentsPlacementDto.setTermRunId(123);
    assertEquals(123, actualStudentsPlacementDto.getRoomId().intValue());
    List<String> studentLogins = actualStudentsPlacementDto.getStudentLogins();
    List<Integer> seatIds = actualStudentsPlacementDto.getSeatIds();
    assertEquals(studentLogins, seatIds);
    assertSame(integerList, seatIds);
    assertSame(stringList, studentLogins);
    assertEquals(integerList, studentLogins);
    assertEquals(123, actualStudentsPlacementDto.getTermRunId().intValue());
    assertEquals("StudentsPlacementDto(studentLogins=[], termRunId=123, roomId=123, seatIds=[])",
      actualStudentsPlacementDto.toString());
  }

  @Test
  public void testConstructor2() {
    ArrayList<String> stringList = new ArrayList<String>();
    ArrayList<Integer> integerList = new ArrayList<Integer>();
    StudentsPlacementDto actualStudentsPlacementDto = new StudentsPlacementDto(stringList, 123, 123, integerList);
    actualStudentsPlacementDto.setRoomId(123);
    ArrayList<Integer> integerList1 = new ArrayList<Integer>();
    actualStudentsPlacementDto.setSeatIds(integerList1);
    ArrayList<String> stringList1 = new ArrayList<String>();
    actualStudentsPlacementDto.setStudentLogins(stringList1);
    actualStudentsPlacementDto.setTermRunId(123);
    assertEquals(123, actualStudentsPlacementDto.getRoomId().intValue());
    List<Integer> seatIds = actualStudentsPlacementDto.getSeatIds();
    assertSame(integerList1, seatIds);
    assertEquals(stringList, seatIds);
    assertEquals(integerList, seatIds);
    List<String> studentLogins = actualStudentsPlacementDto.getStudentLogins();
    assertEquals(studentLogins, seatIds);
    assertSame(stringList1, studentLogins);
    assertEquals(integerList1, studentLogins);
    assertEquals(stringList, studentLogins);
    assertEquals(integerList, studentLogins);
    assertEquals(123, actualStudentsPlacementDto.getTermRunId().intValue());
    assertEquals("StudentsPlacementDto(studentLogins=[], termRunId=123, roomId=123, seatIds=[])",
      actualStudentsPlacementDto.toString());
  }

  @Test
  public void testEquals() {
    assertNotEquals((new StudentsPlacementDto()), null);
    assertNotEquals((new StudentsPlacementDto()), "Different type to StudentsPlacementDto");
  }

  @Test
  public void testEquals10() {
    ArrayList<Integer> integerList = new ArrayList<Integer>();
    integerList.add(2);
    StudentsPlacementDto studentsPlacementDto = new StudentsPlacementDto(new ArrayList<String>(), 123, 123,
      integerList);
    ArrayList<String> studentLogins = new ArrayList<String>();
    assertNotEquals(new StudentsPlacementDto(studentLogins, 123, 123, new ArrayList<Integer>()), studentsPlacementDto);
  }

  @Test
  public void testEquals11() {
    ArrayList<String> studentLogins = new ArrayList<String>();

    StudentsPlacementDto studentsPlacementDto = new StudentsPlacementDto(studentLogins, 123, 123,
      new ArrayList<Integer>());
    studentsPlacementDto.setSeatIds(null);
    ArrayList<String> studentLogins1 = new ArrayList<String>();
    assertNotEquals(new StudentsPlacementDto(studentLogins1, 123, 123, new ArrayList<Integer>()), studentsPlacementDto);
  }

  @Test
  public void testEquals12() {
    ArrayList<String> studentLogins = new ArrayList<String>();

    StudentsPlacementDto studentsPlacementDto = new StudentsPlacementDto(studentLogins, 123, 123,
      new ArrayList<Integer>());
    studentsPlacementDto.setStudentLogins(null);
    ArrayList<String> studentLogins1 = new ArrayList<String>();
    assertNotEquals(new StudentsPlacementDto(studentLogins1, 123, 123, new ArrayList<Integer>()), studentsPlacementDto);
  }

  @Test
  public void testEquals2() {
    StudentsPlacementDto studentsPlacementDto = new StudentsPlacementDto();
    assertEquals(studentsPlacementDto, studentsPlacementDto);
    int expectedHashCodeResult = studentsPlacementDto.hashCode();
    assertEquals(expectedHashCodeResult, studentsPlacementDto.hashCode());
  }

  @Test
  public void testEquals3() {
    StudentsPlacementDto studentsPlacementDto = new StudentsPlacementDto();
    StudentsPlacementDto studentsPlacementDto1 = new StudentsPlacementDto();
    assertEquals(studentsPlacementDto1, studentsPlacementDto);
    int expectedHashCodeResult = studentsPlacementDto.hashCode();
    assertEquals(expectedHashCodeResult, studentsPlacementDto1.hashCode());
  }

  @Test
  public void testEquals4() {
    ArrayList<String> studentLogins = new ArrayList<String>();
    StudentsPlacementDto studentsPlacementDto = new StudentsPlacementDto(studentLogins, 123, 123,
      new ArrayList<Integer>());
    assertNotEquals(new StudentsPlacementDto(), studentsPlacementDto);
  }

  @Test
  public void testEquals5() {
    StudentsPlacementDto studentsPlacementDto = new StudentsPlacementDto();
    ArrayList<String> studentLogins = new ArrayList<String>();
    assertNotEquals(new StudentsPlacementDto(studentLogins, 123, 123, new ArrayList<Integer>()), studentsPlacementDto);
  }

  @Test
  public void testEquals6() {
    StudentsPlacementDto studentsPlacementDto = new StudentsPlacementDto();
    studentsPlacementDto.setRoomId(123);
    assertNotEquals(new StudentsPlacementDto(), studentsPlacementDto);
  }

  @Test
  public void testEquals7() {
    ArrayList<String> studentLogins = new ArrayList<String>();
    StudentsPlacementDto studentsPlacementDto = new StudentsPlacementDto(studentLogins, 123, 123,
      new ArrayList<Integer>());
    ArrayList<String> studentLogins1 = new ArrayList<String>();
    StudentsPlacementDto studentsPlacementDto1 = new StudentsPlacementDto(studentLogins1, 123, 123,
      new ArrayList<Integer>());

    assertEquals(studentsPlacementDto1, studentsPlacementDto);
    int expectedHashCodeResult = studentsPlacementDto.hashCode();
    assertEquals(expectedHashCodeResult, studentsPlacementDto1.hashCode());
  }

  @Test
  public void testEquals8() {
    StudentsPlacementDto studentsPlacementDto = new StudentsPlacementDto();

    StudentsPlacementDto studentsPlacementDto1 = new StudentsPlacementDto();
    studentsPlacementDto1.setRoomId(123);
    assertNotEquals(studentsPlacementDto1, studentsPlacementDto);
  }

  @Test
  public void testEquals9() {
    ArrayList<String> stringList = new ArrayList<String>();
    stringList.add("foo");
    StudentsPlacementDto studentsPlacementDto = new StudentsPlacementDto(stringList, 123, 123,
      new ArrayList<Integer>());
    ArrayList<String> studentLogins = new ArrayList<String>();
    assertNotEquals(new StudentsPlacementDto(studentLogins, 123, 123, new ArrayList<Integer>()), studentsPlacementDto);
  }
}

