package cz.vutbr.fit.exampreparation.features.room.dto;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class RoomInfoDtoTest {
  @Test
  public void testConstructor() {
    ArrayList<Integer> integerList = new ArrayList<Integer>();
    RoomInfoDto actualRoomInfoDto = new RoomInfoDto(123, "Abbreviation", "Name", integerList,
      "Creator Name With Degrees", "jane.doe@example.org");

    assertEquals("Abbreviation", actualRoomInfoDto.getAbbreviation());
    assertEquals("RoomInfoDto(roomId=123, capacities=[], creatorNameWithDegrees=Creator Name With Degrees, creatorEmail"
      + "=jane.doe@example.org)", actualRoomInfoDto.toString());
    assertEquals(123, actualRoomInfoDto.getRoomId().intValue());
    assertEquals("Name", actualRoomInfoDto.getName());
    assertEquals("Creator Name With Degrees", actualRoomInfoDto.getCreatorNameWithDegrees());
    assertEquals("jane.doe@example.org", actualRoomInfoDto.getCreatorEmail());
    List<Integer> capacities = actualRoomInfoDto.getCapacities();
    assertSame(integerList, capacities);
    assertTrue(capacities.isEmpty());
    assertSame(capacities, integerList);
  }
}

