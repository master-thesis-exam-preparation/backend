package cz.vutbr.fit.exampreparation.features.testSchedule;

import cz.vutbr.fit.exampreparation.features.assignment.dto.FileContentDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {TestScheduleController.class})
@ExtendWith(SpringExtension.class)
public class TestScheduleControllerTest {
  @Autowired
  private TestScheduleController testScheduleController;

  @MockBean
  private TestScheduleService testScheduleService;

  @Test
  public void testDownloadPdf() throws Exception {
    when(this.testScheduleService.downloadPdfs((Integer) any(), (Integer) any(), anyString(), (Integer) any(),
      anyBoolean(), anyBoolean())).thenReturn(new ResponseEntity<Resource>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder getResult = MockMvcRequestBuilders.get("/internal/test-schedules/download");
    MockHttpServletRequestBuilder paramResult = getResult.param("doubleSided", String.valueOf(true));
    MockHttpServletRequestBuilder requestBuilder = paramResult.param("pack", String.valueOf(true));
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.testScheduleController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(200));
  }

  @Test
  public void testDownloadPdf2() throws Exception {
    when(this.testScheduleService.downloadPdfs((Integer) any(), (Integer) any(), anyString(), (Integer) any(),
      anyBoolean(), anyBoolean())).thenReturn(new ResponseEntity<Resource>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder getResult = MockMvcRequestBuilders.get("/internal/test-schedules/download");
    MockHttpServletRequestBuilder paramResult = getResult.param("doubleSided", String.valueOf(true));
    MockHttpServletRequestBuilder requestBuilder = paramResult.param("pack", String.valueOf(true));
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.testScheduleController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(200));
  }

  @Test
  public void testDownloadStudentsCsv() throws Exception {
    when(this.testScheduleService.downloadStudentsCsv((Integer) any(), (Integer) any(), anyString()))
      .thenReturn(new ResponseEntity<Resource>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
      .get("/internal/test-schedules/download-students-csv");
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.testScheduleController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(200));
  }

  @Test
  public void testDownloadStudentsCsv2() throws Exception {
    when(this.testScheduleService.downloadStudentsCsv((Integer) any(), (Integer) any(), anyString()))
      .thenReturn(new ResponseEntity<Resource>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
      .get("/internal/test-schedules/download-students-csv");
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.testScheduleController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(200));
  }

  @Test
  public void testGetLogFileContent$() throws Exception {
    when(this.testScheduleService.getLogFileContent(anyInt()))
      .thenReturn(new ResponseEntity<FileContentDto>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
      .get("/internal/test-schedules/{testScheduleId:[\\d]+}/log-file-content", 123);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.testScheduleController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testGetLogFileContent$2() throws Exception {
    when(this.testScheduleService.getLogFileContent(anyInt()))
      .thenReturn(new ResponseEntity<FileContentDto>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
      .get("/internal/test-schedules/{testScheduleId:[\\d]+}/log-file-content", 9);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.testScheduleController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }
}

