package cz.vutbr.fit.exampreparation.features.assignment.dto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class FileContentDtoTest {
  @Test
  public void testCanEqual() {
    assertFalse((new FileContentDto("Not all who wander are lost")).canEqual("Other"));
  }

  @Test
  public void testCanEqual2() {
    FileContentDto fileContentDto = new FileContentDto("Not all who wander are lost");
    assertTrue(fileContentDto.canEqual(new FileContentDto("Not all who wander are lost")));
  }

  @Test
  public void testConstructor() {
    FileContentDto actualFileContentDto = new FileContentDto();
    actualFileContentDto.setFileContent("Not all who wander are lost");
    assertEquals("Not all who wander are lost", actualFileContentDto.getFileContent());
    assertEquals("FileContentDto(fileContent=Not all who wander are lost)", actualFileContentDto.toString());
  }

  @Test
  public void testConstructor2() {
    FileContentDto actualFileContentDto = new FileContentDto("Not all who wander are lost");
    actualFileContentDto.setFileContent("Not all who wander are lost");
    assertEquals("Not all who wander are lost", actualFileContentDto.getFileContent());
    assertEquals("FileContentDto(fileContent=Not all who wander are lost)", actualFileContentDto.toString());
  }

  @Test
  public void testEquals() {
    assertNotEquals((new FileContentDto("Not all who wander are lost")), null);
    assertNotEquals((new FileContentDto("Not all who wander are lost")), "Different type to FileContentDto");
  }

  @Test
  public void testEquals2() {
    FileContentDto fileContentDto = new FileContentDto("Not all who wander are lost");
    assertEquals(fileContentDto, fileContentDto);
    int expectedHashCodeResult = fileContentDto.hashCode();
    assertEquals(expectedHashCodeResult, fileContentDto.hashCode());
  }

  @Test
  public void testEquals3() {
    FileContentDto fileContentDto = new FileContentDto("Not all who wander are lost");
    FileContentDto fileContentDto1 = new FileContentDto("Not all who wander are lost");
    assertEquals(fileContentDto1, fileContentDto);
    int expectedHashCodeResult = fileContentDto.hashCode();
    assertEquals(expectedHashCodeResult, fileContentDto1.hashCode());
  }

  @Test
  public void testEquals4() {
    FileContentDto fileContentDto = new FileContentDto(null);
    assertNotEquals(new FileContentDto("Not all who wander are lost"), fileContentDto);
  }

  @Test
  public void testEquals5() {
    FileContentDto fileContentDto = new FileContentDto("File Content");
    assertNotEquals(new FileContentDto("Not all who wander are lost"), fileContentDto);
  }

  @Test
  public void testEquals6() {
    FileContentDto fileContentDto = new FileContentDto(null);
    FileContentDto fileContentDto1 = new FileContentDto(null);
    assertEquals(fileContentDto1, fileContentDto);
    int expectedHashCodeResult = fileContentDto.hashCode();
    assertEquals(expectedHashCodeResult, fileContentDto1.hashCode());
  }
}

