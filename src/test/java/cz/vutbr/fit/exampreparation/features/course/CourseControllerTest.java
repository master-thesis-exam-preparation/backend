package cz.vutbr.fit.exampreparation.features.course;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.vutbr.fit.exampreparation.common.MessageResponseDto;
import cz.vutbr.fit.exampreparation.features.course.dto.BasicCourseInfo;
import cz.vutbr.fit.exampreparation.features.course.dto.CourseInfoDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {CourseController.class})
@ExtendWith(SpringExtension.class)
public class CourseControllerTest {
  @Autowired
  private CourseController courseController;

  @MockBean
  private CourseService courseService;

  @Test
  public void testCreateNewCourse2() throws Exception {
    BasicCourseInfo basicCourseInfo = new BasicCourseInfo();
    basicCourseInfo.setAbbreviation("Abbreviation");
    basicCourseInfo.setAcademicYear(1);
    basicCourseInfo.setName("Name");
    String content = (new ObjectMapper()).writeValueAsString(basicCourseInfo);
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/internal/courses")
      .contentType(MediaType.APPLICATION_JSON)
      .content(content);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.courseController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(400));
  }

  @Test
  public void testEditCourse2() throws Exception {
    BasicCourseInfo basicCourseInfo = new BasicCourseInfo();
    basicCourseInfo.setAbbreviation("Abbreviation");
    basicCourseInfo.setAcademicYear(1);
    basicCourseInfo.setName("Name");
    String content = (new ObjectMapper()).writeValueAsString(basicCourseInfo);
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.put("/internal/courses/{courseId:[\\d]+}", 9)
      .contentType(MediaType.APPLICATION_JSON)
      .content(content);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.courseController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(400));
  }

  @Test
  public void testGetAllCoursesInfo() throws Exception {
    when(this.courseService.getAllCoursesInfo(anyBoolean()))
      .thenReturn(new ResponseEntity<List<CourseInfoDto>>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/internal/courses");
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.courseController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testGetAllCoursesInfo2() throws Exception {
    when(this.courseService.getAllCoursesInfo(anyBoolean()))
      .thenReturn(new ResponseEntity<List<CourseInfoDto>>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/internal/courses");
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.courseController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testCreateNewCourse() throws Exception {
    BasicCourseInfo basicCourseInfo = new BasicCourseInfo();
    basicCourseInfo.setAbbreviation("Abbreviation");
    basicCourseInfo.setAcademicYear(1);
    basicCourseInfo.setName("Name");
    String content = (new ObjectMapper()).writeValueAsString(basicCourseInfo);
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/internal/courses")
      .contentType(MediaType.APPLICATION_JSON)
      .content(content);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.courseController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(400));
  }

  @Test
  public void testDeleteCourse() throws Exception {
    when(this.courseService.deleteCourse(anyInt()))
      .thenReturn(new ResponseEntity<MessageResponseDto>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/internal/courses/{courseId:[\\d]+}",
      9);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.courseController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testDeleteCourse2() throws Exception {
    when(this.courseService.deleteCourse(anyInt()))
      .thenReturn(new ResponseEntity<MessageResponseDto>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/internal/courses/{courseId:[\\d]+}",
      9);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.courseController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testEditCourse() throws Exception {
    BasicCourseInfo basicCourseInfo = new BasicCourseInfo();
    basicCourseInfo.setAbbreviation("Abbreviation");
    basicCourseInfo.setAcademicYear(1);
    basicCourseInfo.setName("Name");
    String content = (new ObjectMapper()).writeValueAsString(basicCourseInfo);
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.put("/internal/courses/{courseId:[\\d]+}", 9)
      .contentType(MediaType.APPLICATION_JSON)
      .content(content);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.courseController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(400));
  }

  @Test
  public void testGetAllCurrentCoursesInfo() throws Exception {
    when(this.courseService.getAllCoursesInfo(anyBoolean()))
      .thenReturn(new ResponseEntity<List<CourseInfoDto>>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/internal/current-courses");
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.courseController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testGetAllCurrentCoursesInfo2() throws Exception {
    when(this.courseService.getAllCoursesInfo(anyBoolean()))
      .thenReturn(new ResponseEntity<List<CourseInfoDto>>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/internal/current-courses");
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.courseController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testImportNewCourses() throws Exception {
    when(this.courseService.importCourses((java.util.List<BasicCourseInfo>) any()))
      .thenReturn(new ResponseEntity<MessageResponseDto>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder contentTypeResult = MockMvcRequestBuilders.post("/internal/courses/import")
      .contentType(MediaType.APPLICATION_JSON);

    ObjectMapper objectMapper = new ObjectMapper();
    MockHttpServletRequestBuilder requestBuilder = contentTypeResult
      .content(objectMapper.writeValueAsString(new ArrayList<BasicCourseInfo>()));
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.courseController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testImportNewCourses2() throws Exception {
    when(this.courseService.importCourses((java.util.List<BasicCourseInfo>) any()))
      .thenReturn(new ResponseEntity<MessageResponseDto>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder contentTypeResult = MockMvcRequestBuilders.post("/internal/courses/import")
      .contentType(MediaType.APPLICATION_JSON);

    ObjectMapper objectMapper = new ObjectMapper();
    MockHttpServletRequestBuilder requestBuilder = contentTypeResult
      .content(objectMapper.writeValueAsString(new ArrayList<BasicCourseInfo>()));
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.courseController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }
}

