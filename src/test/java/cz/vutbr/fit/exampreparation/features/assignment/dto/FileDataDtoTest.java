package cz.vutbr.fit.exampreparation.features.assignment.dto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class FileDataDtoTest {
  @Test
  public void testCanEqual() {
    assertFalse((new FileDataDto()).canEqual("Other"));
  }

  @Test
  public void testCanEqual2() {
    FileDataDto fileDataDto = new FileDataDto();
    assertTrue(fileDataDto.canEqual(new FileDataDto()));
  }

  @Test
  public void testConstructor() {
    FileDataDto actualFileDataDto = new FileDataDto();
    actualFileDataDto.setName("Name");
    actualFileDataDto.setSize(3L);
    actualFileDataDto.setType("Type");
    assertEquals("Name", actualFileDataDto.getName());
    assertEquals(3L, actualFileDataDto.getSize().longValue());
    assertEquals("Type", actualFileDataDto.getType());
    assertEquals("FileDataDto(type=Type, name=Name, size=3)", actualFileDataDto.toString());
  }

  @Test
  public void testConstructor2() {
    FileDataDto actualFileDataDto = new FileDataDto("Type", "Name", 3L);
    actualFileDataDto.setName("Name");
    actualFileDataDto.setSize(3L);
    actualFileDataDto.setType("Type");
    assertEquals("Name", actualFileDataDto.getName());
    assertEquals(3L, actualFileDataDto.getSize().longValue());
    assertEquals("Type", actualFileDataDto.getType());
    assertEquals("FileDataDto(type=Type, name=Name, size=3)", actualFileDataDto.toString());
  }

  @Test
  public void testEquals() {
    assertNotEquals((new FileDataDto()), null);
    assertNotEquals((new FileDataDto()), "Different type to FileDataDto");
  }

  @Test
  public void testEquals10() {
    FileDataDto fileDataDto = new FileDataDto("Type", null, 3L);
    assertNotEquals(new FileDataDto("Type", "Name", 3L), fileDataDto);
  }

  @Test
  public void testEquals2() {
    FileDataDto fileDataDto = new FileDataDto();
    assertEquals(fileDataDto, fileDataDto);
    int expectedHashCodeResult = fileDataDto.hashCode();
    assertEquals(expectedHashCodeResult, fileDataDto.hashCode());
  }

  @Test
  public void testEquals3() {
    FileDataDto fileDataDto = new FileDataDto();
    FileDataDto fileDataDto1 = new FileDataDto();
    assertEquals(fileDataDto1, fileDataDto);
    int expectedHashCodeResult = fileDataDto.hashCode();
    assertEquals(expectedHashCodeResult, fileDataDto1.hashCode());
  }

  @Test
  public void testEquals4() {
    FileDataDto fileDataDto = new FileDataDto("Type", "Name", 3L);
    assertNotEquals(new FileDataDto(), fileDataDto);
  }

  @Test
  public void testEquals5() {
    FileDataDto fileDataDto = new FileDataDto();
    assertNotEquals(new FileDataDto("Type", "Name", 3L), fileDataDto);
  }

  @Test
  public void testEquals6() {
    FileDataDto fileDataDto = new FileDataDto("Type", "Name", null);
    assertNotEquals(new FileDataDto(), fileDataDto);
  }

  @Test
  public void testEquals7() {
    FileDataDto fileDataDto = new FileDataDto("Type", "Name", 3L);
    FileDataDto fileDataDto1 = new FileDataDto("Type", "Name", 3L);

    assertEquals(fileDataDto1, fileDataDto);
    int expectedHashCodeResult = fileDataDto.hashCode();
    assertEquals(expectedHashCodeResult, fileDataDto1.hashCode());
  }

  @Test
  public void testEquals8() {
    FileDataDto fileDataDto = new FileDataDto();
    assertNotEquals(new FileDataDto("Type", "Name", null), fileDataDto);
  }

  @Test
  public void testEquals9() {
    FileDataDto fileDataDto = new FileDataDto(null, "Name", null);
    assertNotEquals(new FileDataDto(), fileDataDto);
  }
}

