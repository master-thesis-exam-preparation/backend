package cz.vutbr.fit.exampreparation.features.termRun.dto;

import cz.vutbr.fit.exampreparation.features.assignment.dto.BasicAssignmentInfoDto;
import cz.vutbr.fit.exampreparation.features.room.dto.BasicRoomDto;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class TermRunBasicRoomDtoTest {
  @Test
  public void testConstructor() {
    ArrayList<BasicRoomDto> basicRoomDtoList = new ArrayList<BasicRoomDto>();
    ArrayList<BasicAssignmentInfoDto> basicAssignmentInfoDtoList = new ArrayList<BasicAssignmentInfoDto>();
    TermRunBasicRoomDto actualTermRunBasicRoomDto = new TermRunBasicRoomDto(1, 1L, 1L, basicRoomDtoList,
      basicAssignmentInfoDtoList);

    List<BasicAssignmentInfoDto> assignments = actualTermRunBasicRoomDto.getAssignments();
    assertSame(basicAssignmentInfoDtoList, assignments);
    assertEquals(basicRoomDtoList, assignments);
    assertTrue(assignments.isEmpty());
    assertEquals("TermRunBasicRoomDto(rooms=[], assignments=[])", actualTermRunBasicRoomDto.toString());
    assertEquals(1L, actualTermRunBasicRoomDto.getStartTime().longValue());
    List<BasicRoomDto> rooms = actualTermRunBasicRoomDto.getRooms();
    assertSame(basicRoomDtoList, rooms);
    assertEquals(assignments, rooms);
    assertTrue(rooms.isEmpty());
    assertEquals(1, actualTermRunBasicRoomDto.getId().intValue());
    assertEquals(1L, actualTermRunBasicRoomDto.getEndTime().longValue());
    assertSame(rooms, basicRoomDtoList);
    assertSame(assignments, basicAssignmentInfoDtoList);
  }
}

