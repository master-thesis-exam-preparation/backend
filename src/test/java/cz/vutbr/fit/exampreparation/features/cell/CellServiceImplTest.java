package cz.vutbr.fit.exampreparation.features.cell;

import cz.vutbr.fit.exampreparation.features.appUser.AppUser;
import cz.vutbr.fit.exampreparation.features.assignment.Assignment;
import cz.vutbr.fit.exampreparation.features.role.Role;
import cz.vutbr.fit.exampreparation.features.room.Room;
import cz.vutbr.fit.exampreparation.features.roomPlan.RoomPlan;
import cz.vutbr.fit.exampreparation.features.roomPlan.SpacingBetweenStudents;
import cz.vutbr.fit.exampreparation.features.term.Term;
import cz.vutbr.fit.exampreparation.features.termRun.TermRun;
import cz.vutbr.fit.exampreparation.features.testSchedule.TestSchedule;
import cz.vutbr.fit.exampreparation.features.token.Token;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {CellServiceImpl.class})
@ExtendWith(SpringExtension.class)
public class CellServiceImplTest {
  @MockBean
  private CellRepository cellRepository;

  @Autowired
  private CellServiceImpl cellServiceImpl;

  @Test
  public void testGetRoomPlanSeatIds() {
    when(this.cellRepository.findCellsByRoomPlanAndCellType((RoomPlan) any(), (CellType) any()))
      .thenReturn(new ArrayList<Cell>());
    assertTrue(this.cellServiceImpl.getRoomPlanSeatIds(new RoomPlan()).isEmpty());
    verify(this.cellRepository).findCellsByRoomPlanAndCellType((RoomPlan) any(), (CellType) any());
  }

  @Test
  public void testGetRoomPlanSeatsInSnakePattern() {
    when(this.cellRepository.findCellsByRoomPlanAndCellType((RoomPlan) any(), (CellType) any()))
      .thenReturn(new ArrayList<Cell>());
    assertTrue(this.cellServiceImpl.getRoomPlanSeatsInSnakePattern(new RoomPlan()).isEmpty());
    verify(this.cellRepository).findCellsByRoomPlanAndCellType((RoomPlan) any(), (CellType) any());
  }

  @Test
  public void testGetRoomPlanSeatsInSnakePattern2() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());

    Room room = new Room();
    room.setAppUserReference(appUser);
    room.setAbbreviation("Abbreviation");
    room.setRoomPlans(new ArrayList<RoomPlan>());
    room.setNumberOfColumns(10);
    room.setName("Name");
    room.setDescription("The characteristics of someone or something");
    room.setNumberOfRows(10);

    RoomPlan roomPlan = new RoomPlan();
    roomPlan.setCells(new ArrayList<Cell>());
    roomPlan.setCapacity(3);
    roomPlan.setTermRuns(new ArrayList<TermRun>());
    roomPlan.setSpacing(SpacingBetweenStudents.ZERO);
    roomPlan.setRoomReference(room);

    Cell cell = new Cell();
    cell.setColumnNumber(10);
    cell.setRoomPlanReference(roomPlan);
    cell.setRowNumber(10);
    cell.setTestSchedules(new ArrayList<TestSchedule>());
    cell.setSeatRowNumber(10);
    cell.setCellType(CellType.SEAT);
    cell.setSeatColumnNumber(10);

    ArrayList<Cell> cellList = new ArrayList<Cell>();
    cellList.add(cell);
    when(this.cellRepository.findCellsByRoomPlanAndCellType((RoomPlan) any(), (CellType) any())).thenReturn(cellList);
    assertEquals(1, this.cellServiceImpl.getRoomPlanSeatsInSnakePattern(new RoomPlan()).size());
    verify(this.cellRepository).findCellsByRoomPlanAndCellType((RoomPlan) any(), (CellType) any());
  }

  @Test
  public void testGetCellById() {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());

    Room room = new Room();
    room.setAppUserReference(appUser);
    room.setAbbreviation("Abbreviation");
    room.setRoomPlans(new ArrayList<RoomPlan>());
    room.setNumberOfColumns(10);
    room.setName("Name");
    room.setDescription("The characteristics of someone or something");
    room.setNumberOfRows(10);

    RoomPlan roomPlan = new RoomPlan();
    roomPlan.setCells(new ArrayList<Cell>());
    roomPlan.setCapacity(3);
    roomPlan.setTermRuns(new ArrayList<TermRun>());
    roomPlan.setSpacing(SpacingBetweenStudents.ZERO);
    roomPlan.setRoomReference(room);

    Cell cell = new Cell();
    cell.setColumnNumber(10);
    cell.setRoomPlanReference(roomPlan);
    cell.setRowNumber(10);
    cell.setTestSchedules(new ArrayList<TestSchedule>());
    cell.setSeatRowNumber(10);
    cell.setCellType(CellType.SEAT);
    cell.setSeatColumnNumber(10);
    Optional<Cell> ofResult = Optional.<Cell>of(cell);
    when(this.cellRepository.findById((Integer) any())).thenReturn(ofResult);
    Optional<Cell> actualCellById = this.cellServiceImpl.getCellById(123);
    assertSame(ofResult, actualCellById);
    assertTrue(actualCellById.isPresent());
    verify(this.cellRepository).findById((Integer) any());
  }

  @Test
  public void testFindFirstRowNumberWithSeatInRoomPlan() {
    when(this.cellRepository.findFirstRowNumberWithSeatInRoomPlan((RoomPlan) any())).thenReturn(1);
    assertEquals(1, this.cellServiceImpl.findFirstRowNumberWithSeatInRoomPlan(new RoomPlan()));
    verify(this.cellRepository).findFirstRowNumberWithSeatInRoomPlan((RoomPlan) any());
  }

  @Test
  public void testFindFreeSeatsInRoomPlan() {
    ArrayList<Cell> cellList = new ArrayList<Cell>();
    when(this.cellRepository.findFreeSeatsInTermRunsRoomPlan((TermRun) any(), (RoomPlan) any())).thenReturn(cellList);
    TermRun termRun = new TermRun();
    List<Cell> actualFindFreeSeatsInRoomPlanResult = this.cellServiceImpl.findFreeSeatsInRoomPlan(termRun,
      new RoomPlan());
    assertSame(cellList, actualFindFreeSeatsInRoomPlanResult);
    assertTrue(actualFindFreeSeatsInRoomPlanResult.isEmpty());
    verify(this.cellRepository).findFreeSeatsInTermRunsRoomPlan((TermRun) any(), (RoomPlan) any());
  }
}

