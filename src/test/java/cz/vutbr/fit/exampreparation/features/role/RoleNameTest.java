package cz.vutbr.fit.exampreparation.features.role;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RoleNameTest {

  @Test
  public void testValueOf2() {
    assertEquals(RoleName.ADMIN, RoleName.valueOf("ADMIN"));
  }

  @Test
  public void testValues() {
    RoleName[] actualValuesResult = RoleName.values();
    assertEquals(3, actualValuesResult.length);
    assertEquals(RoleName.STUDENT, actualValuesResult[0]);
    assertEquals(RoleName.EMPLOYEE, actualValuesResult[1]);
    assertEquals(RoleName.ADMIN, actualValuesResult[2]);
  }
}

