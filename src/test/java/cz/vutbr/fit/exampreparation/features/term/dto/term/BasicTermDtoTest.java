package cz.vutbr.fit.exampreparation.features.term.dto.term;

import cz.vutbr.fit.exampreparation.features.roomPlan.SpacingBetweenStudents;
import cz.vutbr.fit.exampreparation.features.termRun.Distribution;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class BasicTermDtoTest {
  @Test
  public void testCanEqual() {
    assertFalse((new BasicTermDto()).canEqual("Other"));
  }

  @Test
  public void testCanEqual2() {
    BasicTermDto basicTermDto = new BasicTermDto();
    assertTrue(basicTermDto.canEqual(new BasicTermDto()));
  }

  @Test
  public void testConstructor() {
    BasicTermDto actualBasicTermDto = new BasicTermDto();
    actualBasicTermDto.setCourseId(123);
    actualBasicTermDto.setDistribution(Distribution.RANDOM);
    actualBasicTermDto.setInformationText("Information Text");
    actualBasicTermDto.setSpacingBetweenStudents(SpacingBetweenStudents.ZERO);
    actualBasicTermDto.setTermName("Term Name");
    assertEquals(123, actualBasicTermDto.getCourseId().intValue());
    assertEquals(Distribution.RANDOM, actualBasicTermDto.getDistribution());
    assertEquals("Information Text", actualBasicTermDto.getInformationText());
    assertEquals(SpacingBetweenStudents.ZERO, actualBasicTermDto.getSpacingBetweenStudents());
    assertEquals("Term Name", actualBasicTermDto.getTermName());
    assertEquals("BasicTermDto(termName=Term Name, courseId=123, spacingBetweenStudents=ZERO, distribution=RANDOM,"
      + " informationText=Information Text)", actualBasicTermDto.toString());
  }

  @Test
  public void testConstructor2() {
    BasicTermDto actualBasicTermDto = new BasicTermDto("Term Name", 123, SpacingBetweenStudents.ZERO,
      Distribution.RANDOM, "Information Text");
    actualBasicTermDto.setCourseId(123);
    actualBasicTermDto.setDistribution(Distribution.RANDOM);
    actualBasicTermDto.setInformationText("Information Text");
    actualBasicTermDto.setSpacingBetweenStudents(SpacingBetweenStudents.ZERO);
    actualBasicTermDto.setTermName("Term Name");
    assertEquals(123, actualBasicTermDto.getCourseId().intValue());
    assertEquals(Distribution.RANDOM, actualBasicTermDto.getDistribution());
    assertEquals("Information Text", actualBasicTermDto.getInformationText());
    assertEquals(SpacingBetweenStudents.ZERO, actualBasicTermDto.getSpacingBetweenStudents());
    assertEquals("Term Name", actualBasicTermDto.getTermName());
    assertEquals("BasicTermDto(termName=Term Name, courseId=123, spacingBetweenStudents=ZERO, distribution=RANDOM,"
      + " informationText=Information Text)", actualBasicTermDto.toString());
  }

  @Test
  public void testEquals() {
    assertNotEquals((new BasicTermDto()), null);
    assertNotEquals((new BasicTermDto()), "Different type to BasicTermDto");
  }

  @Test
  public void testEquals2() {
    BasicTermDto basicTermDto = new BasicTermDto();
    assertEquals(basicTermDto, basicTermDto);
    int expectedHashCodeResult = basicTermDto.hashCode();
    assertEquals(expectedHashCodeResult, basicTermDto.hashCode());
  }

  @Test
  public void testEquals3() {
    BasicTermDto basicTermDto = new BasicTermDto();
    BasicTermDto basicTermDto1 = new BasicTermDto();
    assertTrue(basicTermDto.equals(basicTermDto1));
    int expectedHashCodeResult = basicTermDto.hashCode();
    assertEquals(expectedHashCodeResult, basicTermDto1.hashCode());
  }

  @Test
  public void testEquals4() {
    BasicTermDto basicTermDto = new BasicTermDto("Term Name", 123, SpacingBetweenStudents.ZERO, Distribution.RANDOM,
      "Information Text");
    assertNotEquals(new BasicTermDto(), basicTermDto);
  }

  @Test
  public void testEquals5() {
    EditTermDto editTermDto = mock(EditTermDto.class);
    assertNotEquals(new BasicTermDto(), editTermDto);
  }
}

