package cz.vutbr.fit.exampreparation.features.term;

import cz.vutbr.fit.exampreparation.common.MessageResponseDto;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.BadRequestException;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.NotFoundException;
import cz.vutbr.fit.exampreparation.features.appUser.AppUser;
import cz.vutbr.fit.exampreparation.features.appUser.AppUserService;
import cz.vutbr.fit.exampreparation.features.appUser.dto.LoginNameDto;
import cz.vutbr.fit.exampreparation.features.assignment.Assignment;
import cz.vutbr.fit.exampreparation.features.assignment.AssignmentService;
import cz.vutbr.fit.exampreparation.features.assignment.dto.GeneratingFailDto;
import cz.vutbr.fit.exampreparation.features.assignment.helpers.RunningGeneratingService;
import cz.vutbr.fit.exampreparation.features.cell.CellService;
import cz.vutbr.fit.exampreparation.features.course.Course;
import cz.vutbr.fit.exampreparation.features.course.CourseService;
import cz.vutbr.fit.exampreparation.features.room.RoomService;
import cz.vutbr.fit.exampreparation.features.roomPlan.RoomPlan;
import cz.vutbr.fit.exampreparation.features.roomPlan.RoomPlanService;
import cz.vutbr.fit.exampreparation.features.roomPlan.SpacingBetweenStudents;
import cz.vutbr.fit.exampreparation.features.term.dto.AllTermsInfoResponseDto;
import cz.vutbr.fit.exampreparation.features.term.dto.StudentsPlacementDto;
import cz.vutbr.fit.exampreparation.features.term.dto.term.NewTermDto;
import cz.vutbr.fit.exampreparation.features.termRun.Distribution;
import cz.vutbr.fit.exampreparation.features.termRun.TermRun;
import cz.vutbr.fit.exampreparation.features.termRun.dto.TermRunDto;
import cz.vutbr.fit.exampreparation.features.termStudent.TermStudentService;
import cz.vutbr.fit.exampreparation.features.testSchedule.TestSchedule;
import cz.vutbr.fit.exampreparation.features.testSchedule.TestScheduleService;
import cz.vutbr.fit.exampreparation.mail.MailBuilder;
import cz.vutbr.fit.exampreparation.mail.MailNotificationSender;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {TermServiceImpl.class, RunningGeneratingService.class, MailBuilder.class,
  MailNotificationSender.class})
@ExtendWith(SpringExtension.class)
public class TermServiceImplTest {
  @MockBean
  private AppUserService appUserService;

  @MockBean
  private AssignmentService assignmentService;

  @MockBean
  private CellService cellService;

  @MockBean
  private CourseService courseService;

  @MockBean
  private MailBuilder mailBuilder;

  @MockBean
  private MailNotificationSender mailNotificationSender;

  @MockBean
  private RoomPlanService roomPlanService;

  @MockBean
  private RoomService roomService;

  @MockBean
  private RunningGeneratingService runningGeneratingService;

  @MockBean
  private TermRepository termRepository;

  @Autowired
  private TermServiceImpl termServiceImpl;

  @MockBean
  private TermStudentService termStudentService;

  @MockBean
  private TestScheduleService testScheduleService;

  @Test
  public void testGetAllTermsInfo() {
    when(this.termRepository.findAll()).thenReturn(new ArrayList<Term>());
    ResponseEntity<List<AllTermsInfoResponseDto>> actualAllTermsInfo = this.termServiceImpl.getAllTermsInfo(true);
    assertEquals("<200 OK OK,[],[]>", actualAllTermsInfo.toString());
    assertTrue(actualAllTermsInfo.hasBody());
    assertEquals(HttpStatus.OK, actualAllTermsInfo.getStatusCode());
    assertTrue(actualAllTermsInfo.getHeaders().isEmpty());
    verify(this.termRepository).findAll();
  }

  @Test
  public void testGetAllTermsInfo2() {
    Course course = new Course();
    course.setAbbreviation("Abbreviation");
    course.setAcademicYear(0);
    course.setName("Name");
    course.setTerms(new ArrayList<Term>());

    Term term = new Term();
    term.setDistribution(Distribution.RANDOM);
    term.setCourseReference(course);
    term.setTermRuns(new ArrayList<TermRun>());
    term.setTermName("Term Name");
    term.setTermManagers(new ArrayList<AppUser>());
    term.setDescription("The characteristics of someone or something");
    term.setSpacingBetweenStudents(SpacingBetweenStudents.ZERO);

    ArrayList<Term> termList = new ArrayList<Term>();
    termList.add(term);
    when(this.termRepository.findAll()).thenReturn(termList);
    ResponseEntity<List<AllTermsInfoResponseDto>> actualAllTermsInfo = this.termServiceImpl.getAllTermsInfo(true);
    assertEquals(1, actualAllTermsInfo.getBody().size());
    assertEquals("<200 OK OK,[AllTermsInfoResponseDto(termId=0, managersEmails=[], termRuns=[])],[]>",
      actualAllTermsInfo.toString());
    assertTrue(actualAllTermsInfo.hasBody());
    assertEquals(HttpStatus.OK, actualAllTermsInfo.getStatusCode());
    assertTrue(actualAllTermsInfo.getHeaders().isEmpty());
    verify(this.termRepository).findAll();
  }

  @Test
  public void testGetAllTermsInfo3() {
    Course course = new Course();
    course.setAbbreviation("Abbreviation");
    course.setAcademicYear(0);
    course.setName("Name");
    course.setTerms(new ArrayList<Term>());

    Course course1 = new Course();
    course1.setAbbreviation("Abbreviation");
    course1.setAcademicYear(0);
    course1.setName("Name");
    course1.setTerms(new ArrayList<Term>());

    Term term = new Term();
    term.setDistribution(Distribution.RANDOM);
    term.setCourseReference(course1);
    term.setTermRuns(new ArrayList<TermRun>());
    term.setTermName("Term Name");
    term.setTermManagers(new ArrayList<AppUser>());
    term.setDescription("The characteristics of someone or something");
    term.setSpacingBetweenStudents(SpacingBetweenStudents.ZERO);
    Timestamp timestamp = mock(Timestamp.class);
    when(timestamp.getTime()).thenReturn(1L);
    Timestamp timestamp1 = mock(Timestamp.class);
    when(timestamp1.getTime()).thenReturn(1L);

    TermRun termRun = new TermRun();
    termRun.setTermReference(term);
    termRun.setRoomPlans(new ArrayList<RoomPlan>());
    termRun.setStartDateTime(timestamp);
    termRun.setEndDateTime(timestamp1);
    termRun.setTestSchedules(new ArrayList<TestSchedule>());
    termRun.setAssignments(new ArrayList<Assignment>());

    ArrayList<TermRun> termRunList = new ArrayList<TermRun>();
    termRunList.add(termRun);

    Term term1 = new Term();
    term1.setDistribution(Distribution.RANDOM);
    term1.setCourseReference(course);
    term1.setTermRuns(termRunList);
    term1.setTermName("Term Name");
    term1.setTermManagers(new ArrayList<AppUser>());
    term1.setDescription("The characteristics of someone or something");
    term1.setSpacingBetweenStudents(SpacingBetweenStudents.ZERO);

    ArrayList<Term> termList = new ArrayList<Term>();
    termList.add(term1);
    when(this.termRepository.findAll()).thenReturn(termList);
    ResponseEntity<List<AllTermsInfoResponseDto>> actualAllTermsInfo = this.termServiceImpl.getAllTermsInfo(true);
    assertEquals(1, actualAllTermsInfo.getBody().size());
    assertEquals(
      "<200 OK OK,[AllTermsInfoResponseDto(termId=0, managersEmails=[], termRuns=[TermRunBasicRoomDto(rooms=[],"
        + " assignments=[])])],[]>",
      actualAllTermsInfo.toString());
    assertTrue(actualAllTermsInfo.hasBody());
    assertEquals(HttpStatus.OK, actualAllTermsInfo.getStatusCode());
    assertTrue(actualAllTermsInfo.getHeaders().isEmpty());
    verify(this.termRepository).findAll();
    verify(timestamp, times(2)).getTime();
    verify(timestamp1).getTime();
  }

  @Test
  public void testCreateNewTerm() {
    doNothing().when(this.appUserService).checkAdditionalManagers((java.util.List<String>) any());

    ArrayList<LoginNameDto> loginNameDtoList = new ArrayList<LoginNameDto>();
    loginNameDtoList.add(new LoginNameDto("Login", "Name And Degrees"));
    ArrayList<String> additionalTermManagersEmails = new ArrayList<String>();
    ArrayList<TermRunDto> termRuns = new ArrayList<TermRunDto>();

    NewTermDto newTermDto = new NewTermDto(loginNameDtoList, additionalTermManagersEmails, termRuns,
      new ArrayList<StudentsPlacementDto>(), true, "jane.doe@example.org");
    newTermDto.setSpacingBetweenStudents(SpacingBetweenStudents.ZERO);
    assertThrows(BadRequestException.class, () -> this.termServiceImpl.createNewTerm(newTermDto));
  }

  @Test
  public void testCreateNewTerm2() {
    Course course = new Course();
    course.setAbbreviation("Abbreviation");
    course.setAcademicYear(1);
    course.setName("Name");
    course.setTerms(new ArrayList<Term>());

    Term term = new Term();
    term.setDistribution(Distribution.RANDOM);
    term.setCourseReference(course);
    term.setTermRuns(new ArrayList<TermRun>());
    term.setTermName("Term Name");
    term.setTermManagers(new ArrayList<AppUser>());
    term.setDescription("The characteristics of someone or something");
    term.setSpacingBetweenStudents(SpacingBetweenStudents.ZERO);
    when(this.termRepository.save((Term) any())).thenReturn(term);

    Course course1 = new Course();
    course1.setAbbreviation("Abbreviation");
    course1.setAcademicYear(1);
    course1.setName("Name");
    course1.setTerms(new ArrayList<Term>());
    Optional<Course> ofResult = Optional.<Course>of(course1);
    when(this.courseService.findCourseById(anyInt())).thenReturn(ofResult);
    when(this.appUserService.createManagerList((List<String>) any())).thenReturn(new ArrayList<AppUser>());
    doNothing().when(this.appUserService).checkAdditionalManagers((List<String>) any());
    ArrayList<LoginNameDto> studentsList = new ArrayList<LoginNameDto>();
    ArrayList<String> additionalTermManagersEmails = new ArrayList<String>();
    ArrayList<TermRunDto> termRuns = new ArrayList<TermRunDto>();

    NewTermDto newTermDto = new NewTermDto(studentsList, additionalTermManagersEmails, termRuns,
      new ArrayList<StudentsPlacementDto>(), true, "jane.doe@example.org");
    newTermDto.setCourseId(123);
    newTermDto.setSpacingBetweenStudents(SpacingBetweenStudents.ZERO);
    ResponseEntity<MessageResponseDto> actualCreateNewTermResult = this.termServiceImpl.createNewTerm(newTermDto);
    assertTrue(actualCreateNewTermResult.getHeaders().isEmpty());
    assertTrue(actualCreateNewTermResult.hasBody());
    assertEquals(HttpStatus.OK, actualCreateNewTermResult.getStatusCode());
    assertEquals("Termín zkoušky byl úspěšně vytvořen", actualCreateNewTermResult.getBody().getMessage());
    verify(this.termRepository).save((Term) any());
    verify(this.courseService).findCourseById(anyInt());
    verify(this.appUserService).checkAdditionalManagers((List<String>) any());
    verify(this.appUserService).createManagerList((List<String>) any());
    assertTrue(newTermDto.getTermRuns().isEmpty());
    assertTrue(newTermDto.getStudentsList().isEmpty());
  }

  @Test
  public void testGetTermDetails() {
    when(this.termRepository.findById((Integer) any())).thenThrow(new NotFoundException("An error occurred"));
    assertThrows(NotFoundException.class, () -> this.termServiceImpl.getTermDetails(123, true));
    verify(this.termRepository).findById((Integer) any());
  }

  @Test
  public void testGetTermDetails2() {
    when(this.termRepository.findById((Integer) any())).thenReturn(Optional.<Term>empty());
    assertThrows(NotFoundException.class, () -> this.termServiceImpl.getTermDetails(123, true));
    verify(this.termRepository).findById((Integer) any());
  }

  @Test
  public void testGetTermDetails3() {
    Course course = new Course();
    course.setAbbreviation("Abbreviation");
    course.setAcademicYear(1);
    course.setName("Name");
    course.setTerms(new ArrayList<Term>());

    Term term = new Term();
    term.setDistribution(Distribution.RANDOM);
    term.setCourseReference(course);
    term.setTermRuns(new ArrayList<TermRun>());
    term.setTermName("Term Name");
    term.setTermManagers(new ArrayList<AppUser>());
    term.setDescription("The characteristics of someone or something");
    term.setSpacingBetweenStudents(SpacingBetweenStudents.ZERO);
    Optional<Term> ofResult = Optional.<Term>of(term);
    when(this.termRepository.findById((Integer) any())).thenReturn(ofResult);
    when(this.runningGeneratingService.getAllTermFailedGenerations(anyInt()))
      .thenThrow(new NotFoundException("An error occurred"));
    when(this.runningGeneratingService.getTermRunningTasks(anyInt())).thenReturn(new ArrayList<CompletableFuture<?>>());
    when(this.runningGeneratingService.doesTermHaveAnyRunningTask(anyInt())).thenReturn(true);
    assertThrows(NotFoundException.class, () -> this.termServiceImpl.getTermDetails(123, false));
    verify(this.termRepository).findById((Integer) any());
    verify(this.runningGeneratingService).doesTermHaveAnyRunningTask(anyInt());
    verify(this.runningGeneratingService).getAllTermFailedGenerations(anyInt());
    verify(this.runningGeneratingService).getTermRunningTasks(anyInt());
  }

  @Test
  public void testGetTermDetails4() {
    when(this.termRepository.findById((Integer) any())).thenReturn(Optional.<Term>empty());
    when(this.runningGeneratingService.getAllTermFailedGenerations(anyInt()))
      .thenReturn(new ArrayList<GeneratingFailDto>());
    when(this.runningGeneratingService.getTermRunningTasks(anyInt())).thenReturn(new ArrayList<CompletableFuture<?>>());
    when(this.runningGeneratingService.doesTermHaveAnyRunningTask(anyInt())).thenReturn(true);
    assertThrows(NotFoundException.class, () -> this.termServiceImpl.getTermDetails(123, false));
    verify(this.termRepository).findById((Integer) any());
  }
}

