package cz.vutbr.fit.exampreparation.features.appUser.dto;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class AccountInfoDtoTest {
  @Test
  public void testConstructor() {
    ArrayList<String> stringList = new ArrayList<String>();
    AccountInfoDto actualAccountInfoDto = new AccountInfoDto(123, "Name With Degrees", "jane.doe@example.org",
      stringList, true);

    assertEquals("jane.doe@example.org", actualAccountInfoDto.getEmail());
    assertEquals("AccountInfoDto(userId=123, locked=true)", actualAccountInfoDto.toString());
    assertTrue(actualAccountInfoDto.isLocked());
    assertEquals(123, actualAccountInfoDto.getUserId());
    List<String> roles = actualAccountInfoDto.getRoles();
    assertSame(stringList, roles);
    assertTrue(roles.isEmpty());
    assertEquals("Name With Degrees", actualAccountInfoDto.getNameWithDegrees());
    assertSame(roles, stringList);
  }
}

