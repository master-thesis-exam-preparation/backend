package cz.vutbr.fit.exampreparation.features.term;

import cz.vutbr.fit.exampreparation.features.appUser.AppUser;
import cz.vutbr.fit.exampreparation.features.course.Course;
import cz.vutbr.fit.exampreparation.features.roomPlan.SpacingBetweenStudents;
import cz.vutbr.fit.exampreparation.features.termRun.Distribution;
import cz.vutbr.fit.exampreparation.features.termRun.TermRun;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class TermTest {
  @Test
  public void testConstructor() {
    ArrayList<AppUser> appUserList = new ArrayList<AppUser>();
    Course course = new Course();
    ArrayList<TermRun> termRunList = new ArrayList<TermRun>();
    Term actualTerm = new Term("Term Name", SpacingBetweenStudents.ZERO, Distribution.RANDOM,
      "The characteristics of someone or something", appUserList, course, termRunList);

    Course courseReference = actualTerm.getCourseReference();
    assertSame(course, courseReference);
    List<TermRun> termRuns = actualTerm.getTermRuns();
    assertEquals(termRunList, termRuns);
    assertEquals(appUserList, termRuns);
    assertTrue(termRuns.isEmpty());
    assertEquals("The characteristics of someone or something", actualTerm.getDescription());
    assertEquals(0, actualTerm.getIdTerm());
    assertEquals(SpacingBetweenStudents.ZERO, actualTerm.getSpacingBetweenStudents());
    assertEquals(Distribution.RANDOM, actualTerm.getDistribution());
    List<AppUser> termManagers = actualTerm.getTermManagers();
    assertSame(appUserList, termManagers);
    assertEquals(termRunList, termManagers);
    assertEquals(termRuns, termManagers);
    assertTrue(termManagers.isEmpty());
    assertEquals("Term Name", actualTerm.getTermName());
    assertNull(courseReference.getAcademicYear());
    assertNull(courseReference.getName());
    assertNull(courseReference.getTerms());
    assertNull(courseReference.getAbbreviation());
    assertEquals(0, courseReference.getIdCourse());
    assertSame(termManagers, appUserList);
    assertSame(courseReference, course);
    assertEquals(termRuns, termRunList);
    assertEquals(termManagers, termRunList);
    assertTrue(termRunList.isEmpty());
  }

  @Test
  public void testGetTermRuns() {
    Term term = new Term();
    term.setTermRuns(new ArrayList<TermRun>());
    assertTrue(term.getTermRuns().isEmpty());
  }

  @Test
  public void testAddTermRun() {
    ArrayList<AppUser> termManagers = new ArrayList<AppUser>();
    Course courseReference = new Course();
    Term term = new Term("Term Name", SpacingBetweenStudents.ZERO, Distribution.RANDOM,
      "The characteristics of someone or something", termManagers, courseReference, new ArrayList<TermRun>());
    term.addTermRun(new TermRun());
    assertEquals(1, term.getTermRuns().size());
  }

  @Test
  public void testRemoveTermRun() {
    ArrayList<AppUser> termManagers = new ArrayList<AppUser>();
    Course courseReference = new Course();
    Term term = new Term("Term Name", SpacingBetweenStudents.ZERO, Distribution.RANDOM,
      "The characteristics of someone or something", termManagers, courseReference, new ArrayList<TermRun>());
    term.removeTermRun(new TermRun());
    assertTrue(term.getTermRuns().isEmpty());
  }

  @Test
  public void testRemoveAllTermRuns() {
    Term term = new Term();
    term.setTermRuns(new ArrayList<TermRun>());
    term.removeAllTermRuns();
    assertTrue(term.getTermRuns().isEmpty());
  }
}

