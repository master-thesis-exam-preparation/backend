package cz.vutbr.fit.exampreparation.features.appUser;

import cz.vutbr.fit.exampreparation.features.role.Role;
import cz.vutbr.fit.exampreparation.features.room.Room;
import cz.vutbr.fit.exampreparation.features.term.Term;
import cz.vutbr.fit.exampreparation.features.token.Token;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class AppUserTest {
  @Test
  public void testConstructor() {
    ArrayList<Role> roleList = new ArrayList<Role>();
    ArrayList<Term> termList = new ArrayList<Term>();
    ArrayList<Room> roomList = new ArrayList<Room>();
    ArrayList<Token> tokenList = new ArrayList<Token>();
    AppUser actualAppUser = new AppUser("jane.doe@example.org", "Jane", "Doe", "Degrees Before Name",
      "Degrees Behind Name", "iloveyou", true, true, roleList, termList, roomList, tokenList);

    assertNull(actualAppUser.getAssignments());
    assertTrue(actualAppUser.isVerified());
    assertTrue(actualAppUser.isLocked());
    List<Token> userTokens = actualAppUser.getUserTokens();
    assertSame(tokenList, userTokens);
    assertEquals(termList, userTokens);
    assertEquals(roleList, userTokens);
    assertEquals(roomList, userTokens);
    assertTrue(userTokens.isEmpty());
    List<Term> terms = actualAppUser.getTerms();
    assertSame(termList, terms);
    assertEquals(roleList, terms);
    assertEquals(userTokens, terms);
    assertEquals(roomList, terms);
    assertTrue(terms.isEmpty());
    List<Room> rooms = actualAppUser.getRooms();
    assertSame(roomList, rooms);
    assertEquals(terms, rooms);
    assertEquals(roleList, rooms);
    assertEquals(userTokens, rooms);
    assertTrue(rooms.isEmpty());
    List<Role> roles = actualAppUser.getRoles();
    assertSame(roleList, roles);
    assertEquals(terms, roles);
    assertEquals(userTokens, roles);
    assertEquals(rooms, roles);
    assertTrue(roles.isEmpty());
    assertEquals("iloveyou", actualAppUser.getPassword());
    assertEquals("Degrees Before Name Jane Doe Degrees Behind Name", actualAppUser.getNameWithDegrees());
    assertEquals("Doe", actualAppUser.getLastName());
    assertEquals(0, actualAppUser.getIdAppUser());
    assertEquals("Jane", actualAppUser.getFirstName());
    assertEquals("jane.doe@example.org", actualAppUser.getEmail());
    assertEquals("Degrees Behind Name", actualAppUser.getDegreesBehindName());
    assertEquals("Degrees Before Name", actualAppUser.getDegreesBeforeName());
    assertSame(roles, roleList);
    assertSame(terms, termList);
    assertSame(rooms, roomList);
    assertSame(userTokens, tokenList);
  }

  @Test
  public void testGetNameWithDegrees() {
    assertEquals("null null", (new AppUser()).getNameWithDegrees());
  }

  @Test
  public void testGetNameWithDegrees2() {
    ArrayList<Role> roles = new ArrayList<Role>();
    ArrayList<Term> terms = new ArrayList<Term>();
    ArrayList<Room> rooms = new ArrayList<Room>();
    assertEquals("Degrees Before Name Jane Doe Degrees Behind Name",
      (new AppUser("jane.doe@example.org", "Jane", "Doe", "Degrees Before Name", "Degrees Behind Name", "iloveyou",
        true, true, roles, terms, rooms, new ArrayList<Token>())).getNameWithDegrees());
  }
}

