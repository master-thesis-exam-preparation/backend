package cz.vutbr.fit.exampreparation.features.term.dto;

import cz.vutbr.fit.exampreparation.features.termRun.dto.TermRunDto;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class BasicTermRunDtoTest {
  @Test
  public void testCanEqual() {
    assertFalse((new BasicTermRunDto()).canEqual("Other"));
  }

  @Test
  public void testCanEqual2() {
    BasicTermRunDto basicTermRunDto = new BasicTermRunDto();
    assertTrue(basicTermRunDto.canEqual(new BasicTermRunDto()));
  }

  @Test
  public void testConstructor() {
    BasicTermRunDto actualBasicTermRunDto = new BasicTermRunDto();
    ArrayList<Integer> integerList = new ArrayList<Integer>();
    actualBasicTermRunDto.setAssignments(integerList);
    actualBasicTermRunDto.setEndTime(1L);
    actualBasicTermRunDto.setId(1);
    actualBasicTermRunDto.setStartTime(1L);
    assertSame(integerList, actualBasicTermRunDto.getAssignments());
    assertEquals(1L, actualBasicTermRunDto.getEndTime().longValue());
    assertEquals(1, actualBasicTermRunDto.getId().intValue());
    assertEquals(1L, actualBasicTermRunDto.getStartTime().longValue());
    assertEquals("BasicTermRunDto(id=1, startTime=1, endTime=1, assignments=[])", actualBasicTermRunDto.toString());
  }

  @Test
  public void testConstructor2() {
    ArrayList<Integer> integerList = new ArrayList<Integer>();
    BasicTermRunDto actualBasicTermRunDto = new BasicTermRunDto(1, 1L, 1L, integerList);
    ArrayList<Integer> integerList1 = new ArrayList<Integer>();
    actualBasicTermRunDto.setAssignments(integerList1);
    actualBasicTermRunDto.setEndTime(1L);
    actualBasicTermRunDto.setId(1);
    actualBasicTermRunDto.setStartTime(1L);
    List<Integer> assignments = actualBasicTermRunDto.getAssignments();
    assertSame(integerList1, assignments);
    assertEquals(integerList, assignments);
    assertEquals(1L, actualBasicTermRunDto.getEndTime().longValue());
    assertEquals(1, actualBasicTermRunDto.getId().intValue());
    assertEquals(1L, actualBasicTermRunDto.getStartTime().longValue());
    assertEquals("BasicTermRunDto(id=1, startTime=1, endTime=1, assignments=[])", actualBasicTermRunDto.toString());
  }

  @Test
  public void testEquals() {
    assertNotEquals((new BasicTermRunDto()), null);
    assertNotEquals((new BasicTermRunDto()), "Different type to BasicTermRunDto");
  }

  @Test
  public void testEquals10() {
    BasicTermRunDto basicTermRunDto = new BasicTermRunDto(1, 1L, 1L, new ArrayList<Integer>());
    BasicTermRunDto basicTermRunDto1 = new BasicTermRunDto(1, 1L, 1L, new ArrayList<Integer>());

    assertEquals(basicTermRunDto1, basicTermRunDto);
    int expectedHashCodeResult = basicTermRunDto.hashCode();
    assertEquals(expectedHashCodeResult, basicTermRunDto1.hashCode());
  }

  @Test
  public void testEquals11() {
    BasicTermRunDto basicTermRunDto = new BasicTermRunDto();

    BasicTermRunDto basicTermRunDto1 = new BasicTermRunDto();
    basicTermRunDto1.setStartTime(0L);
    assertNotEquals(basicTermRunDto1, basicTermRunDto);
  }

  @Test
  public void testEquals12() {
    BasicTermRunDto basicTermRunDto = new BasicTermRunDto();

    BasicTermRunDto basicTermRunDto1 = new BasicTermRunDto();
    basicTermRunDto1.setEndTime(0L);
    assertNotEquals(basicTermRunDto1, basicTermRunDto);
  }

  @Test
  public void testEquals13() {
    ArrayList<Integer> integerList = new ArrayList<Integer>();
    integerList.add(2);
    BasicTermRunDto basicTermRunDto = new BasicTermRunDto(1, 1L, 1L, integerList);
    assertNotEquals(new BasicTermRunDto(1, 1L, 1L, new ArrayList<Integer>()), basicTermRunDto);
  }

  @Test
  public void testEquals14() {
    BasicTermRunDto basicTermRunDto = new BasicTermRunDto(1, 1L, 1L, new ArrayList<Integer>());
    basicTermRunDto.setAssignments(null);
    assertNotEquals(new BasicTermRunDto(1, 1L, 1L, new ArrayList<Integer>()), basicTermRunDto);
  }

  @Test
  public void testEquals2() {
    BasicTermRunDto basicTermRunDto = new BasicTermRunDto();
    assertEquals(basicTermRunDto, basicTermRunDto);
    int expectedHashCodeResult = basicTermRunDto.hashCode();
    assertEquals(expectedHashCodeResult, basicTermRunDto.hashCode());
  }

  @Test
  public void testEquals3() {
    BasicTermRunDto basicTermRunDto = new BasicTermRunDto();
    BasicTermRunDto basicTermRunDto1 = new BasicTermRunDto();
    assertEquals(basicTermRunDto1, basicTermRunDto);
    int expectedHashCodeResult = basicTermRunDto.hashCode();
    assertEquals(expectedHashCodeResult, basicTermRunDto1.hashCode());
  }

  @Test
  public void testEquals4() {
    BasicTermRunDto basicTermRunDto = new BasicTermRunDto(1, 1L, 1L, new ArrayList<Integer>());
    assertNotEquals(new BasicTermRunDto(), basicTermRunDto);
  }

  @Test
  public void testEquals5() {
    TermRunDto termRunDto = mock(TermRunDto.class);
    assertNotEquals(new BasicTermRunDto(), termRunDto);
  }

  @Test
  public void testEquals6() {
    BasicTermRunDto basicTermRunDto = new BasicTermRunDto();
    assertNotEquals(new BasicTermRunDto(1, 1L, 1L, new ArrayList<Integer>()), basicTermRunDto);
  }

  @Test
  public void testEquals7() {
    BasicTermRunDto basicTermRunDto = new BasicTermRunDto();
    assertNotEquals(new TermRunDto(), basicTermRunDto);
  }

  @Test
  public void testEquals8() {
    BasicTermRunDto basicTermRunDto = new BasicTermRunDto();
    basicTermRunDto.setStartTime(0L);
    assertNotEquals(new BasicTermRunDto(), basicTermRunDto);
  }

  @Test
  public void testEquals9() {
    BasicTermRunDto basicTermRunDto = new BasicTermRunDto();
    basicTermRunDto.setEndTime(0L);
    assertNotEquals(new BasicTermRunDto(), basicTermRunDto);
  }
}

