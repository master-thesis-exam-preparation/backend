package cz.vutbr.fit.exampreparation.features.assignment.helpers;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Paths;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {StorageManager.class})
@ExtendWith(SpringExtension.class)
public class StorageManagerTest {
  @Autowired
  private StorageManager storageManager;

  @Test
  public void testCopyNonTexFiles() {
    assertThrows(RuntimeException.class, () -> this.storageManager.copyNonTexFiles("Src Dir Path", "Dest Dir Path"));
  }

  @Test
  public void testGetFilesData() {
    assertTrue(this.storageManager.getFilesData(123).isEmpty());
  }

  @Test
  public void testLoadAsString() {
    assertThrows(RuntimeException.class, () -> this.storageManager.loadAsString("/tmp/foo.txt"));
  }

  @Test
  public void testDownloadFile() {
    assertThrows(RuntimeException.class,
      () -> this.storageManager.downloadFile(Paths.get(System.getProperty("java.io.tmpdir"), "test.txt")));
    assertThrows(RuntimeException.class,
      () -> this.storageManager.downloadFile(Paths.get(System.getProperty("Key"), "")));
  }

  @Test
  public void testGetResourceProbeContentType() throws UnsupportedEncodingException {
    assertThrows(RuntimeException.class, () -> this.storageManager
      .getResourceProbeContentType(new ByteArrayResource("AAAAAAAAAAAAAAAAAAAAAAAA".getBytes("UTF-8"))));
  }

  @Test
  public void testGetResourceProbeContentType2() throws IOException {
    Resource resource = mock(Resource.class);
    when(resource.getFile()).thenReturn(Paths.get(System.getProperty("java.io.tmpdir"), "test.txt").toFile());
    assertEquals("text/plain", this.storageManager.getResourceProbeContentType(resource));
    verify(resource).getFile();
  }

  @Test
  public void testGetAssignmentDirPath() {
    String actualAssignmentDirPath = this.storageManager.getAssignmentDirPath(123);
    assertEquals(Paths.get(System.getProperty("user.dir"), "assignments", "templates", "123").toString(),
      actualAssignmentDirPath);
  }

  @Test
  public void testGetTermDirPath() {
    String actualTermDirPath = this.storageManager.getTermDirPath(123);
    assertEquals(Paths.get(System.getProperty("user.dir"), "assignments", "terms", "123").toString(),
      actualTermDirPath);
  }

  @Test
  public void testGetBackupDirPath() {
    String actualBackupDirPath = this.storageManager.getBackupDirPath(123);
    assertEquals(Paths.get(System.getProperty("user.dir"), "assignments", "backups", "123").toString(),
      actualBackupDirPath);
  }

  @Test
  public void testGetAssignmentFileContent() {
    assertThrows(RuntimeException.class, () -> this.storageManager.getAssignmentFileContent(123, "/tmp/foo.txt"));
  }

  @Test
  public void testMergePdfsIntoOne2() {
    ArrayList<String> stringList = new ArrayList<String>();
    stringList.add("foo");
    assertThrows(RuntimeException.class,
      () -> this.storageManager.mergePdfsIntoOne(stringList, "foo.txt", "Destination Dir", true));
  }

  @Test
  public void testMergePdfsIntoOne3() {
    ArrayList<String> stringList = new ArrayList<String>();
    stringList.add("foo");
    assertThrows(RuntimeException.class,
      () -> this.storageManager.mergePdfsIntoOne(stringList,
        "Chyba při vytváření PDF se zadáními.\nOpakujte pokus nebo kontaktujte administrátora.", "Destination Dir",
        true));
  }
}

