package cz.vutbr.fit.exampreparation.mail;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {MailNotificationSender.class})
@ExtendWith(SpringExtension.class)
public class MailNotificationSenderTest {
  @MockBean
  private JavaMailSender javaMailSender;

  @Autowired
  private MailNotificationSender mailNotificationSender;

  @Test
  public void testSendMail() throws MailException {
    doNothing().when(this.javaMailSender).send((org.springframework.mail.javamail.MimeMessagePreparator) any());
    this.mailNotificationSender.sendMail(new MailComponents(new String[]{"foo", "foo", "foo"},
      "Hello from the Dreaming Spires", "Not all who wander are lost"), "jane.doe@example.org");
    verify(this.javaMailSender).send((org.springframework.mail.javamail.MimeMessagePreparator) any());
  }
}

