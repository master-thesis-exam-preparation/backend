package cz.vutbr.fit.exampreparation.mail;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class MailComponentsTest {
  @Test
  public void testCanEqual() {
    assertFalse((new MailComponents(new String[]{"foo", "foo", "foo"}, "Hello from the Dreaming Spires",
      "Not all who wander are lost")).canEqual("Other"));
  }

  @Test
  public void testCanEqual2() {
    MailComponents mailComponents = new MailComponents(new String[]{"foo", "foo", "foo"},
      "Hello from the Dreaming Spires", "Not all who wander are lost");
    assertTrue(mailComponents.canEqual(new MailComponents(new String[]{"foo", "foo", "foo"},
      "Hello from the Dreaming Spires", "Not all who wander are lost")));
  }

  @Test
  public void testConstructor() {
    MailComponents actualMailComponents = new MailComponents(new String[]{"foo", "foo", "foo"},
      "Hello from the Dreaming Spires", "Not all who wander are lost");
    actualMailComponents.setMailBody("Not all who wander are lost");
    actualMailComponents.setRecipientEmails(new String[]{"jane.doe@example.org"});
    actualMailComponents.setSubject("Hello from the Dreaming Spires");
    assertEquals("Not all who wander are lost", actualMailComponents.getMailBody());
    assertEquals("Hello from the Dreaming Spires", actualMailComponents.getSubject());
    assertEquals("MailComponents(recipientEmails=[jane.doe@example.org], subject=Hello from the Dreaming Spires,"
      + " mailBody=Not all who wander are lost)", actualMailComponents.toString());
  }

  @Test
  public void testEquals() {
    assertNotEquals((new MailComponents(new String[]{"foo", "foo", "foo"}, "Hello from the Dreaming Spires",
      "Not all who wander are lost")), null);
    assertNotEquals((new MailComponents(new String[]{"foo", "foo", "foo"}, "Hello from the Dreaming Spires",
      "Not all who wander are lost")), "Different type to MailComponents");
  }

  @Test
  public void testEquals2() {
    MailComponents mailComponents = new MailComponents(new String[]{"foo", "foo", "foo"},
      "Hello from the Dreaming Spires", null);
    MailComponents mailComponents1 = new MailComponents(new String[]{"foo", "foo", "foo"},
      "Hello from the Dreaming Spires", null);

    assertEquals(mailComponents1, mailComponents);
    int expectedHashCodeResult = mailComponents.hashCode();
    assertEquals(expectedHashCodeResult, mailComponents1.hashCode());
  }

  @Test
  public void testEquals3() {
    MailComponents mailComponents = new MailComponents(new String[]{"foo", "foo", "foo"},
      "Hello from the Dreaming Spires", "Not all who wander are lost");
    MailComponents mailComponents1 = new MailComponents(new String[]{"foo", "foo", "foo"},
      "Hello from the Dreaming Spires", "Not all who wander are lost");

    assertEquals(mailComponents1, mailComponents);
    int expectedHashCodeResult = mailComponents.hashCode();
    assertEquals(expectedHashCodeResult, mailComponents1.hashCode());
  }

  @Test
  public void testEquals4() {
    MailComponents mailComponents = new MailComponents(new String[]{"jane.doe@example.org", "foo", "foo"},
      "Hello from the Dreaming Spires", "Not all who wander are lost");
    assertNotEquals(new MailComponents(new String[]{"foo", "foo", "foo"},
      "Hello from the Dreaming Spires", "Not all who wander are lost"), mailComponents);
  }

  @Test
  public void testEquals5() {
    MailComponents mailComponents = new MailComponents(new String[]{"foo", "foo", "foo"}, null,
      "Not all who wander are lost");
    assertNotEquals(new MailComponents(new String[]{"foo", "foo", "foo"},
      "Hello from the Dreaming Spires", "Not all who wander are lost"), mailComponents);
  }

  @Test
  public void testEquals6() {
    MailComponents mailComponents = new MailComponents(new String[]{"foo", "foo", "foo"}, "Not all who wander are lost",
      "Not all who wander are lost");
    assertNotEquals(new MailComponents(new String[]{"foo", "foo", "foo"},
      "Hello from the Dreaming Spires", "Not all who wander are lost"), mailComponents);
  }

  @Test
  public void testEquals7() {
    MailComponents mailComponents = new MailComponents(new String[]{"foo", "foo", "foo"},
      "Hello from the Dreaming Spires", "Hello from the Dreaming Spires");
    assertNotEquals(new MailComponents(new String[]{"foo", "foo", "foo"},
      "Hello from the Dreaming Spires", "Not all who wander are lost"), mailComponents);
  }

  @Test
  public void testEquals8() {
    MailComponents mailComponents = new MailComponents(new String[]{"foo", "foo", "foo"},
      "Hello from the Dreaming Spires", null);
    assertNotEquals(new MailComponents(new String[]{"foo", "foo", "foo"},
      "Hello from the Dreaming Spires", "Not all who wander are lost"), mailComponents);
  }

  @Test
  public void testEquals9() {
    MailComponents mailComponents = new MailComponents(new String[]{"foo", "foo", "foo"}, null,
      "Not all who wander are lost");
    MailComponents mailComponents1 = new MailComponents(new String[]{"foo", "foo", "foo"}, null,
      "Not all who wander are lost");

    assertEquals(mailComponents1, mailComponents);
    int expectedHashCodeResult = mailComponents.hashCode();
    assertEquals(expectedHashCodeResult, mailComponents1.hashCode());
  }
}

