package cz.vutbr.fit.exampreparation.common.exceptionHandling;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CustomErrorResponseDtoTest {
  @Test
  public void testConstructor() {
    CustomErrorResponseDto actualCustomErrorResponseDto = new CustomErrorResponseDto(HttpStatus.CONTINUE,
      "An error occurred");

    assertEquals("An error occurred", actualCustomErrorResponseDto.getMessage());
    assertEquals(100, actualCustomErrorResponseDto.getStatusCode());
    assertEquals("Continue", actualCustomErrorResponseDto.getStatus());
  }
}

