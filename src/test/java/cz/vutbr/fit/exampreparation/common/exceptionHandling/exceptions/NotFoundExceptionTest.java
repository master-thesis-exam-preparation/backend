package cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class NotFoundExceptionTest {
  @Test
  public void testConstructor() {
    NotFoundException actualNotFoundException = new NotFoundException("An error occurred");
    assertNull(actualNotFoundException.getCause());
    assertEquals(
      "cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.NotFoundException: An error" + " occurred",
      actualNotFoundException.toString());
    assertEquals(0, actualNotFoundException.getSuppressed().length);
    assertEquals("An error occurred", actualNotFoundException.getMessage());
    assertEquals("An error occurred", actualNotFoundException.getLocalizedMessage());
  }
}

