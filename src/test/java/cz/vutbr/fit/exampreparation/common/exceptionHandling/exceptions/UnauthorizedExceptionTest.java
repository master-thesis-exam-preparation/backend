package cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class UnauthorizedExceptionTest {
  @Test
  public void testConstructor() {
    UnauthorizedException actualUnauthorizedException = new UnauthorizedException("An error occurred");
    assertNull(actualUnauthorizedException.getCause());
    assertEquals("cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.UnauthorizedException: An error"
      + " occurred", actualUnauthorizedException.toString());
    assertEquals(0, actualUnauthorizedException.getSuppressed().length);
    assertEquals("An error occurred", actualUnauthorizedException.getMessage());
    assertEquals("An error occurred", actualUnauthorizedException.getLocalizedMessage());
  }
}

