package cz.vutbr.fit.exampreparation.common;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RegexTesterTest {
  @Test
  public void testTestRegex() {
    assertFalse(RegexTester.testRegex("Regex", "Text"));
    assertTrue(RegexTester.testRegex("foo", "foo"));
  }
}

