package cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class ForbiddenExceptionTest {
  @Test
  public void testConstructor() {
    ForbiddenException actualForbiddenException = new ForbiddenException("An error occurred");
    assertNull(actualForbiddenException.getCause());
    assertEquals(
      "cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.ForbiddenException: An error" + " occurred",
      actualForbiddenException.toString());
    assertEquals(0, actualForbiddenException.getSuppressed().length);
    assertEquals("An error occurred", actualForbiddenException.getMessage());
    assertEquals("An error occurred", actualForbiddenException.getLocalizedMessage());
  }
}

