package cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class BaseExceptionTest {
  @Test
  public void testConstructor() {
    BaseException actualBaseException = new BaseException("An error occurred");
    assertNull(actualBaseException.getCause());
    assertEquals("cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.BaseException: An error occurred",
      actualBaseException.toString());
    assertEquals(0, actualBaseException.getSuppressed().length);
    assertEquals("An error occurred", actualBaseException.getMessage());
    assertEquals("An error occurred", actualBaseException.getLocalizedMessage());
  }
}

