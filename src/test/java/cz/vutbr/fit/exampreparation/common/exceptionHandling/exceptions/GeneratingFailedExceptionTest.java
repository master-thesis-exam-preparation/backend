package cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GeneratingFailedExceptionTest {
  @Test
  public void testConstructor() {
    GeneratingFailedException actualGeneratingFailedException = new GeneratingFailedException("An error occurred", -1,
      "/tmp/foo.txt");

    assertEquals(-1, actualGeneratingFailedException.getErrorCode());
    assertEquals("/tmp/foo.txt", actualGeneratingFailedException.getLogFilePath());
  }
}

