package cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class BadRequestExceptionTest {
  @Test
  public void testConstructor() {
    BadRequestException actualBadRequestException = new BadRequestException("An error occurred");
    assertNull(actualBadRequestException.getCause());
    assertEquals(
      "cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.BadRequestException: An error" + " occurred",
      actualBadRequestException.toString());
    assertEquals(0, actualBadRequestException.getSuppressed().length);
    assertEquals("An error occurred", actualBadRequestException.getMessage());
    assertEquals("An error occurred", actualBadRequestException.getLocalizedMessage());
  }
}

