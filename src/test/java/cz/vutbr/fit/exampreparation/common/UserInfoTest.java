package cz.vutbr.fit.exampreparation.common;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

public class UserInfoTest {
  @Test
  public void testConstructor() {
    ArrayList<String> stringList = new ArrayList<String>();
    UserInfo actualUserInfo = new UserInfo("jane.doe@example.org", stringList);
    actualUserInfo.setEmail("jane.doe@example.org");
    ArrayList<String> stringList1 = new ArrayList<String>();
    actualUserInfo.setRoles(stringList1);
    assertEquals("jane.doe@example.org", actualUserInfo.getEmail());
    List<String> roles = actualUserInfo.getRoles();
    assertEquals(stringList, roles);
    assertSame(stringList1, roles);
  }
}

