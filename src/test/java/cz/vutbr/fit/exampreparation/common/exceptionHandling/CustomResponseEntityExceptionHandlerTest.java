package cz.vutbr.fit.exampreparation.common.exceptionHandling;

import cz.vutbr.fit.exampreparation.features.appUser.AppUserRepository;
import cz.vutbr.fit.exampreparation.features.appUser.AppUserServiceImpl;
import cz.vutbr.fit.exampreparation.features.role.RoleService;
import cz.vutbr.fit.exampreparation.features.token.TokenRepository;
import cz.vutbr.fit.exampreparation.features.token.TokenServiceImpl;
import cz.vutbr.fit.exampreparation.security.jwt.JwtConfig;
import cz.vutbr.fit.exampreparation.security.jwt.JwtTokenUtil;
import org.junit.jupiter.api.Test;
import org.slf4j.helpers.SubstituteLogger;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

public class CustomResponseEntityExceptionHandlerTest {
  @Test
  public void testHandleBindException() {
    TokenRepository tokenRepository = mock(TokenRepository.class);
    AppUserRepository appUserRepository = mock(AppUserRepository.class);
    AppUserServiceImpl appUserService = new AppUserServiceImpl(appUserRepository, new Argon2PasswordEncoder(),
      mock(RoleService.class));

    JwtConfig jwtConfig = new JwtConfig();
    AppUserServiceImpl appUserService1 = new AppUserServiceImpl(mock(AppUserRepository.class), null,
      mock(RoleService.class));

    JwtTokenUtil jwtTokenUtil = new JwtTokenUtil(jwtConfig, appUserService1, new SubstituteLogger("Name", null, true));

    CustomResponseEntityExceptionHandler customResponseEntityExceptionHandler = new CustomResponseEntityExceptionHandler(
      new TokenServiceImpl(tokenRepository, appUserService, jwtTokenUtil, new Argon2PasswordEncoder()));
    BindException ex = new BindException(
      new BindException(new BindException(new BindException(new BindException("Target", "Object Name")))));
    HttpHeaders headers = new HttpHeaders();
    ResponseEntity<Object> actualHandleBindExceptionResult = customResponseEntityExceptionHandler
      .handleBindException(ex, headers, HttpStatus.CONTINUE, new ServletWebRequest(new MockHttpServletRequest()));
    assertTrue(actualHandleBindExceptionResult.hasBody());
    assertTrue(actualHandleBindExceptionResult.getHeaders().isEmpty());
    assertEquals(100, actualHandleBindExceptionResult.getStatusCodeValue());
    assertEquals("", ((CustomErrorResponseDto) actualHandleBindExceptionResult.getBody()).getMessage());
    assertEquals("Continue", ((CustomErrorResponseDto) actualHandleBindExceptionResult.getBody()).getStatus());
    assertEquals(100, ((CustomErrorResponseDto) actualHandleBindExceptionResult.getBody()).getStatusCode());
  }

  @Test
  public void testHandleMissingServletRequestParameter() {
    TokenRepository tokenRepository = mock(TokenRepository.class);
    AppUserRepository appUserRepository = mock(AppUserRepository.class);
    AppUserServiceImpl appUserService = new AppUserServiceImpl(appUserRepository, new Argon2PasswordEncoder(),
      mock(RoleService.class));

    JwtConfig jwtConfig = new JwtConfig();
    AppUserServiceImpl appUserService1 = new AppUserServiceImpl(mock(AppUserRepository.class), null,
      mock(RoleService.class));

    JwtTokenUtil jwtTokenUtil = new JwtTokenUtil(jwtConfig, appUserService1, new SubstituteLogger("Name", null, true));

    CustomResponseEntityExceptionHandler customResponseEntityExceptionHandler = new CustomResponseEntityExceptionHandler(
      new TokenServiceImpl(tokenRepository, appUserService, jwtTokenUtil, new Argon2PasswordEncoder()));
    MissingServletRequestParameterException ex = new MissingServletRequestParameterException("Parameter Name",
      "Parameter Type");

    HttpHeaders headers = new HttpHeaders();
    ResponseEntity<Object> actualHandleMissingServletRequestParameterResult = customResponseEntityExceptionHandler
      .handleMissingServletRequestParameter(ex, headers, HttpStatus.CONTINUE,
        new ServletWebRequest(new MockHttpServletRequest()));
    assertTrue(actualHandleMissingServletRequestParameterResult.hasBody());
    assertTrue(actualHandleMissingServletRequestParameterResult.getHeaders().isEmpty());
    assertEquals(100, actualHandleMissingServletRequestParameterResult.getStatusCodeValue());
    assertEquals("Chybějící parametr \"Parameter Name\"",
      ((CustomErrorResponseDto) actualHandleMissingServletRequestParameterResult.getBody()).getMessage());
    assertEquals("Continue",
      ((CustomErrorResponseDto) actualHandleMissingServletRequestParameterResult.getBody()).getStatus());
    assertEquals(100,
      ((CustomErrorResponseDto) actualHandleMissingServletRequestParameterResult.getBody()).getStatusCode());
  }

  @Test
  public void testHandleTypeMismatch() {
    TokenRepository tokenRepository = mock(TokenRepository.class);
    AppUserRepository appUserRepository = mock(AppUserRepository.class);
    AppUserServiceImpl appUserService = new AppUserServiceImpl(appUserRepository, new Argon2PasswordEncoder(),
      mock(RoleService.class));

    JwtConfig jwtConfig = new JwtConfig();
    AppUserServiceImpl appUserService1 = new AppUserServiceImpl(mock(AppUserRepository.class), null,
      mock(RoleService.class));

    JwtTokenUtil jwtTokenUtil = new JwtTokenUtil(jwtConfig, appUserService1, new SubstituteLogger("Name", null, true));

    CustomResponseEntityExceptionHandler customResponseEntityExceptionHandler = new CustomResponseEntityExceptionHandler(
      new TokenServiceImpl(tokenRepository, appUserService, jwtTokenUtil, new Argon2PasswordEncoder()));
    TypeMismatchException ex = new TypeMismatchException("Value", Object.class);

    HttpHeaders headers = new HttpHeaders();
    ResponseEntity<Object> actualHandleTypeMismatchResult = customResponseEntityExceptionHandler.handleTypeMismatch(ex,
      headers, HttpStatus.CONTINUE, new ServletWebRequest(new MockHttpServletRequest()));
    assertTrue(actualHandleTypeMismatchResult.hasBody());
    assertTrue(actualHandleTypeMismatchResult.getHeaders().isEmpty());
    assertEquals(100, actualHandleTypeMismatchResult.getStatusCodeValue());
    assertEquals("Hodnota \"null\" by měla být typu \"class java.lang.Object\"",
      ((CustomErrorResponseDto) actualHandleTypeMismatchResult.getBody()).getMessage());
    assertEquals("Continue", ((CustomErrorResponseDto) actualHandleTypeMismatchResult.getBody()).getStatus());
    assertEquals(100, ((CustomErrorResponseDto) actualHandleTypeMismatchResult.getBody()).getStatusCode());
  }

  @Test
  public void testHandleMissingServletRequestPart() {
    TokenRepository tokenRepository = mock(TokenRepository.class);
    AppUserRepository appUserRepository = mock(AppUserRepository.class);
    AppUserServiceImpl appUserService = new AppUserServiceImpl(appUserRepository, new Argon2PasswordEncoder(),
      mock(RoleService.class));

    JwtConfig jwtConfig = new JwtConfig();
    AppUserServiceImpl appUserService1 = new AppUserServiceImpl(mock(AppUserRepository.class), null,
      mock(RoleService.class));

    JwtTokenUtil jwtTokenUtil = new JwtTokenUtil(jwtConfig, appUserService1, new SubstituteLogger("Name", null, true));

    CustomResponseEntityExceptionHandler customResponseEntityExceptionHandler = new CustomResponseEntityExceptionHandler(
      new TokenServiceImpl(tokenRepository, appUserService, jwtTokenUtil, new Argon2PasswordEncoder()));
    MissingServletRequestPartException ex = new MissingServletRequestPartException("Request Part Name");
    HttpHeaders headers = new HttpHeaders();
    ResponseEntity<Object> actualHandleMissingServletRequestPartResult = customResponseEntityExceptionHandler
      .handleMissingServletRequestPart(ex, headers, HttpStatus.CONTINUE,
        new ServletWebRequest(new MockHttpServletRequest()));
    assertTrue(actualHandleMissingServletRequestPartResult.hasBody());
    assertTrue(actualHandleMissingServletRequestPartResult.getHeaders().isEmpty());
    assertEquals(100, actualHandleMissingServletRequestPartResult.getStatusCodeValue());
    assertEquals("V požadavku chybí část \"Request Part Name\"",
      ((CustomErrorResponseDto) actualHandleMissingServletRequestPartResult.getBody()).getMessage());
    assertEquals("Continue",
      ((CustomErrorResponseDto) actualHandleMissingServletRequestPartResult.getBody()).getStatus());
    assertEquals(100, ((CustomErrorResponseDto) actualHandleMissingServletRequestPartResult.getBody()).getStatusCode());
  }

  @Test
  public void testHandleHttpMessageNotReadable() {
    TokenRepository tokenRepository = mock(TokenRepository.class);
    AppUserRepository appUserRepository = mock(AppUserRepository.class);
    AppUserServiceImpl appUserService = new AppUserServiceImpl(appUserRepository, new Argon2PasswordEncoder(),
      mock(RoleService.class));

    JwtConfig jwtConfig = new JwtConfig();
    AppUserServiceImpl appUserService1 = new AppUserServiceImpl(mock(AppUserRepository.class), null,
      mock(RoleService.class));

    JwtTokenUtil jwtTokenUtil = new JwtTokenUtil(jwtConfig, appUserService1, new SubstituteLogger("Name", null, true));

    CustomResponseEntityExceptionHandler customResponseEntityExceptionHandler = new CustomResponseEntityExceptionHandler(
      new TokenServiceImpl(tokenRepository, appUserService, jwtTokenUtil, new Argon2PasswordEncoder()));
    HttpMessageNotReadableException ex = new HttpMessageNotReadableException("https://example.org/example");
    HttpHeaders headers = new HttpHeaders();
    ResponseEntity<Object> actualHandleHttpMessageNotReadableResult = customResponseEntityExceptionHandler
      .handleHttpMessageNotReadable(ex, headers, HttpStatus.CONTINUE,
        new ServletWebRequest(new MockHttpServletRequest()));
    assertTrue(actualHandleHttpMessageNotReadableResult.hasBody());
    assertTrue(actualHandleHttpMessageNotReadableResult.getHeaders().isEmpty());
    assertEquals(100, actualHandleHttpMessageNotReadableResult.getStatusCodeValue());
    assertEquals("Hodnotu některého z atributů není možné přečíst. Zkontrolujte, zda je požadavek ve správném formátu.",
      ((CustomErrorResponseDto) actualHandleHttpMessageNotReadableResult.getBody()).getMessage());
    assertEquals("Continue", ((CustomErrorResponseDto) actualHandleHttpMessageNotReadableResult.getBody()).getStatus());
    assertEquals(100, ((CustomErrorResponseDto) actualHandleHttpMessageNotReadableResult.getBody()).getStatusCode());
  }

  @Test
  public void testHandleNoHandlerFoundException() {
    TokenRepository tokenRepository = mock(TokenRepository.class);
    AppUserRepository appUserRepository = mock(AppUserRepository.class);
    AppUserServiceImpl appUserService = new AppUserServiceImpl(appUserRepository, new Argon2PasswordEncoder(),
      mock(RoleService.class));

    JwtConfig jwtConfig = new JwtConfig();
    AppUserServiceImpl appUserService1 = new AppUserServiceImpl(mock(AppUserRepository.class), null,
      mock(RoleService.class));

    JwtTokenUtil jwtTokenUtil = new JwtTokenUtil(jwtConfig, appUserService1, new SubstituteLogger("Name", null, true));

    CustomResponseEntityExceptionHandler customResponseEntityExceptionHandler = new CustomResponseEntityExceptionHandler(
      new TokenServiceImpl(tokenRepository, appUserService, jwtTokenUtil, new Argon2PasswordEncoder()));
    NoHandlerFoundException ex = new NoHandlerFoundException("https://example.org/example",
      "https://example.org/example", new HttpHeaders());

    HttpHeaders headers = new HttpHeaders();
    ResponseEntity<Object> actualHandleNoHandlerFoundExceptionResult = customResponseEntityExceptionHandler
      .handleNoHandlerFoundException(ex, headers, HttpStatus.CONTINUE,
        new ServletWebRequest(new MockHttpServletRequest()));
    assertTrue(actualHandleNoHandlerFoundExceptionResult.hasBody());
    assertTrue(actualHandleNoHandlerFoundExceptionResult.getHeaders().isEmpty());
    assertEquals(404, actualHandleNoHandlerFoundExceptionResult.getStatusCodeValue());
    assertEquals("Neexistující záznam",
      ((CustomErrorResponseDto) actualHandleNoHandlerFoundExceptionResult.getBody()).getMessage());
    assertEquals("Not Found",
      ((CustomErrorResponseDto) actualHandleNoHandlerFoundExceptionResult.getBody()).getStatus());
    assertEquals(404, ((CustomErrorResponseDto) actualHandleNoHandlerFoundExceptionResult.getBody()).getStatusCode());
  }

  @Test
  public void testHandleMethodArgumentTypeMismatch() {
    TokenRepository tokenRepository = mock(TokenRepository.class);
    AppUserRepository appUserRepository = mock(AppUserRepository.class);
    AppUserServiceImpl appUserService = new AppUserServiceImpl(appUserRepository, new Argon2PasswordEncoder(),
      mock(RoleService.class));

    JwtConfig jwtConfig = new JwtConfig();
    AppUserServiceImpl appUserService1 = new AppUserServiceImpl(mock(AppUserRepository.class), null,
      mock(RoleService.class));

    JwtTokenUtil jwtTokenUtil = new JwtTokenUtil(jwtConfig, appUserService1, new SubstituteLogger("Name", null, true));

    CustomResponseEntityExceptionHandler customResponseEntityExceptionHandler = new CustomResponseEntityExceptionHandler(
      new TokenServiceImpl(tokenRepository, appUserService, jwtTokenUtil, new Argon2PasswordEncoder()));
    Class<?> requiredType = Object.class;
    MethodArgumentTypeMismatchException ex = new MethodArgumentTypeMismatchException("Value", requiredType,
      "0123456789ABCDEF", null, new Throwable());

    ResponseEntity<Object> actualHandleMethodArgumentTypeMismatchResult = customResponseEntityExceptionHandler
      .handleMethodArgumentTypeMismatch(ex, new ServletWebRequest(new MockHttpServletRequest()));
    assertTrue(actualHandleMethodArgumentTypeMismatchResult.hasBody());
    assertTrue(actualHandleMethodArgumentTypeMismatchResult.getHeaders().isEmpty());
    assertEquals(400, actualHandleMethodArgumentTypeMismatchResult.getStatusCodeValue());
    assertEquals("Argument by měl být typu java.lang.Object",
      ((CustomErrorResponseDto) actualHandleMethodArgumentTypeMismatchResult.getBody()).getMessage());
    assertEquals("Bad Request",
      ((CustomErrorResponseDto) actualHandleMethodArgumentTypeMismatchResult.getBody()).getStatus());
    assertEquals(400,
      ((CustomErrorResponseDto) actualHandleMethodArgumentTypeMismatchResult.getBody()).getStatusCode());
  }

  @Test
  public void testHandleConstraintViolation() {
    TokenRepository tokenRepository = mock(TokenRepository.class);
    AppUserRepository appUserRepository = mock(AppUserRepository.class);
    AppUserServiceImpl appUserService = new AppUserServiceImpl(appUserRepository, new Argon2PasswordEncoder(),
      mock(RoleService.class));

    JwtConfig jwtConfig = new JwtConfig();
    AppUserServiceImpl appUserService1 = new AppUserServiceImpl(mock(AppUserRepository.class), null,
      mock(RoleService.class));

    JwtTokenUtil jwtTokenUtil = new JwtTokenUtil(jwtConfig, appUserService1, new SubstituteLogger("Name", null, true));

    CustomResponseEntityExceptionHandler customResponseEntityExceptionHandler = new CustomResponseEntityExceptionHandler(
      new TokenServiceImpl(tokenRepository, appUserService, jwtTokenUtil, new Argon2PasswordEncoder()));
    ConstraintViolationException ex = new ConstraintViolationException(new HashSet<ConstraintViolation<?>>());
    ResponseEntity<CustomErrorResponseDto> actualHandleConstraintViolationResult = customResponseEntityExceptionHandler
      .handleConstraintViolation(ex, new ServletWebRequest(new MockHttpServletRequest()));
    assertTrue(actualHandleConstraintViolationResult.getHeaders().isEmpty());
    assertEquals(400, actualHandleConstraintViolationResult.getStatusCodeValue());
    assertTrue(actualHandleConstraintViolationResult.hasBody());
    CustomErrorResponseDto body = actualHandleConstraintViolationResult.getBody();
    assertEquals(400, body.getStatusCode());
    assertEquals("", body.getMessage());
    assertEquals("Bad Request", body.getStatus());
  }

  @Test
  public void testHandleConstraintViolation2() {
    TokenRepository tokenRepository = mock(TokenRepository.class);
    AppUserRepository appUserRepository = mock(AppUserRepository.class);
    AppUserServiceImpl appUserService = new AppUserServiceImpl(appUserRepository, new Argon2PasswordEncoder(),
      mock(RoleService.class));

    JwtConfig jwtConfig = new JwtConfig();
    AppUserServiceImpl appUserService1 = new AppUserServiceImpl(mock(AppUserRepository.class), null,
      mock(RoleService.class));

    JwtTokenUtil jwtTokenUtil = new JwtTokenUtil(jwtConfig, appUserService1, new SubstituteLogger("Name", null, true));

    CustomResponseEntityExceptionHandler customResponseEntityExceptionHandler = new CustomResponseEntityExceptionHandler(
      new TokenServiceImpl(tokenRepository, appUserService, jwtTokenUtil, new Argon2PasswordEncoder()));

    ConstraintViolationException constraintViolationException = new ConstraintViolationException("An error occurred",
      new HashSet<ConstraintViolation<?>>());
    constraintViolationException.addSuppressed(new Throwable());
    ResponseEntity<CustomErrorResponseDto> actualHandleConstraintViolationResult = customResponseEntityExceptionHandler
      .handleConstraintViolation(constraintViolationException, new ServletWebRequest(new MockHttpServletRequest()));
    assertTrue(actualHandleConstraintViolationResult.getHeaders().isEmpty());
    assertEquals(400, actualHandleConstraintViolationResult.getStatusCodeValue());
    assertTrue(actualHandleConstraintViolationResult.hasBody());
    CustomErrorResponseDto body = actualHandleConstraintViolationResult.getBody();
    assertEquals(400, body.getStatusCode());
    assertEquals("", body.getMessage());
    assertEquals("Bad Request", body.getStatus());
  }

  @Test
  public void testHandleBadRequest() {
    TokenRepository tokenRepository = mock(TokenRepository.class);
    AppUserRepository appUserRepository = mock(AppUserRepository.class);
    AppUserServiceImpl appUserService = new AppUserServiceImpl(appUserRepository, new Argon2PasswordEncoder(),
      mock(RoleService.class));

    JwtConfig jwtConfig = new JwtConfig();
    AppUserServiceImpl appUserService1 = new AppUserServiceImpl(mock(AppUserRepository.class), null,
      mock(RoleService.class));

    JwtTokenUtil jwtTokenUtil = new JwtTokenUtil(jwtConfig, appUserService1, new SubstituteLogger("Name", null, true));

    CustomResponseEntityExceptionHandler customResponseEntityExceptionHandler = new CustomResponseEntityExceptionHandler(
      new TokenServiceImpl(tokenRepository, appUserService, jwtTokenUtil, new Argon2PasswordEncoder()));
    Exception ex = new Exception("An error occurred");
    ResponseEntity<Object> actualHandleBadRequestResult = customResponseEntityExceptionHandler.handleBadRequest(ex,
      new ServletWebRequest(new MockHttpServletRequest()));
    assertTrue(actualHandleBadRequestResult.hasBody());
    assertTrue(actualHandleBadRequestResult.getHeaders().isEmpty());
    assertEquals(400, actualHandleBadRequestResult.getStatusCodeValue());
    assertEquals("An error occurred", ((CustomErrorResponseDto) actualHandleBadRequestResult.getBody()).getMessage());
    assertEquals("Bad Request", ((CustomErrorResponseDto) actualHandleBadRequestResult.getBody()).getStatus());
    assertEquals(400, ((CustomErrorResponseDto) actualHandleBadRequestResult.getBody()).getStatusCode());
  }

  @Test
  public void testHandleUnauthorized() {
    TokenRepository tokenRepository = mock(TokenRepository.class);
    AppUserRepository appUserRepository = mock(AppUserRepository.class);
    AppUserServiceImpl appUserService = new AppUserServiceImpl(appUserRepository, new Argon2PasswordEncoder(),
      mock(RoleService.class));

    JwtConfig jwtConfig = new JwtConfig();
    AppUserServiceImpl appUserService1 = new AppUserServiceImpl(mock(AppUserRepository.class), null,
      mock(RoleService.class));

    JwtTokenUtil jwtTokenUtil = new JwtTokenUtil(jwtConfig, appUserService1, new SubstituteLogger("Name", null, true));

    CustomResponseEntityExceptionHandler customResponseEntityExceptionHandler = new CustomResponseEntityExceptionHandler(
      new TokenServiceImpl(tokenRepository, appUserService, jwtTokenUtil, new Argon2PasswordEncoder()));
    Exception ex = new Exception("An error occurred");
    ResponseEntity<Object> actualHandleUnauthorizedResult = customResponseEntityExceptionHandler.handleUnauthorized(ex,
      new ServletWebRequest(new MockHttpServletRequest()));
    assertTrue(actualHandleUnauthorizedResult.hasBody());
    assertEquals(1, actualHandleUnauthorizedResult.getHeaders().size());
    assertEquals(HttpStatus.UNAUTHORIZED, actualHandleUnauthorizedResult.getStatusCode());
    assertEquals("Unauthorized", ((CustomErrorResponseDto) actualHandleUnauthorizedResult.getBody()).getStatus());
    assertEquals("An error occurred", ((CustomErrorResponseDto) actualHandleUnauthorizedResult.getBody()).getMessage());
    assertEquals(401, ((CustomErrorResponseDto) actualHandleUnauthorizedResult.getBody()).getStatusCode());
  }

  @Test
  public void testHandleAccessDenied() {
    TokenRepository tokenRepository = mock(TokenRepository.class);
    AppUserRepository appUserRepository = mock(AppUserRepository.class);
    AppUserServiceImpl appUserService = new AppUserServiceImpl(appUserRepository, new Argon2PasswordEncoder(),
      mock(RoleService.class));

    JwtConfig jwtConfig = new JwtConfig();
    AppUserServiceImpl appUserService1 = new AppUserServiceImpl(mock(AppUserRepository.class), null,
      mock(RoleService.class));

    JwtTokenUtil jwtTokenUtil = new JwtTokenUtil(jwtConfig, appUserService1, new SubstituteLogger("Name", null, true));

    CustomResponseEntityExceptionHandler customResponseEntityExceptionHandler = new CustomResponseEntityExceptionHandler(
      new TokenServiceImpl(tokenRepository, appUserService, jwtTokenUtil, new Argon2PasswordEncoder()));
    Exception ex = new Exception("An error occurred");
    ResponseEntity<Object> actualHandleAccessDeniedResult = customResponseEntityExceptionHandler.handleAccessDenied(ex,
      new ServletWebRequest(new MockHttpServletRequest()));
    assertTrue(actualHandleAccessDeniedResult.hasBody());
    assertTrue(actualHandleAccessDeniedResult.getHeaders().isEmpty());
    assertEquals(403, actualHandleAccessDeniedResult.getStatusCodeValue());
    assertEquals("An error occurred", ((CustomErrorResponseDto) actualHandleAccessDeniedResult.getBody()).getMessage());
    assertEquals("Forbidden", ((CustomErrorResponseDto) actualHandleAccessDeniedResult.getBody()).getStatus());
    assertEquals(403, ((CustomErrorResponseDto) actualHandleAccessDeniedResult.getBody()).getStatusCode());
  }

  @Test
  public void testHandleForbidden() {
    TokenRepository tokenRepository = mock(TokenRepository.class);
    AppUserRepository appUserRepository = mock(AppUserRepository.class);
    AppUserServiceImpl appUserService = new AppUserServiceImpl(appUserRepository, new Argon2PasswordEncoder(),
      mock(RoleService.class));

    JwtConfig jwtConfig = new JwtConfig();
    AppUserServiceImpl appUserService1 = new AppUserServiceImpl(mock(AppUserRepository.class), null,
      mock(RoleService.class));

    JwtTokenUtil jwtTokenUtil = new JwtTokenUtil(jwtConfig, appUserService1, new SubstituteLogger("Name", null, true));

    CustomResponseEntityExceptionHandler customResponseEntityExceptionHandler = new CustomResponseEntityExceptionHandler(
      new TokenServiceImpl(tokenRepository, appUserService, jwtTokenUtil, new Argon2PasswordEncoder()));
    Exception ex = new Exception("An error occurred");
    ResponseEntity<Object> actualHandleForbiddenResult = customResponseEntityExceptionHandler.handleForbidden(ex,
      new ServletWebRequest(new MockHttpServletRequest()));
    assertTrue(actualHandleForbiddenResult.hasBody());
    assertTrue(actualHandleForbiddenResult.getHeaders().isEmpty());
    assertEquals(403, actualHandleForbiddenResult.getStatusCodeValue());
    assertEquals("An error occurred", ((CustomErrorResponseDto) actualHandleForbiddenResult.getBody()).getMessage());
    assertEquals("Forbidden", ((CustomErrorResponseDto) actualHandleForbiddenResult.getBody()).getStatus());
    assertEquals(403, ((CustomErrorResponseDto) actualHandleForbiddenResult.getBody()).getStatusCode());
  }

  @Test
  public void testHandleNotFound() {
    TokenRepository tokenRepository = mock(TokenRepository.class);
    AppUserRepository appUserRepository = mock(AppUserRepository.class);
    AppUserServiceImpl appUserService = new AppUserServiceImpl(appUserRepository, new Argon2PasswordEncoder(),
      mock(RoleService.class));

    JwtConfig jwtConfig = new JwtConfig();
    AppUserServiceImpl appUserService1 = new AppUserServiceImpl(mock(AppUserRepository.class), null,
      mock(RoleService.class));

    JwtTokenUtil jwtTokenUtil = new JwtTokenUtil(jwtConfig, appUserService1, new SubstituteLogger("Name", null, true));

    CustomResponseEntityExceptionHandler customResponseEntityExceptionHandler = new CustomResponseEntityExceptionHandler(
      new TokenServiceImpl(tokenRepository, appUserService, jwtTokenUtil, new Argon2PasswordEncoder()));
    Exception ex = new Exception("An error occurred");
    ResponseEntity<Object> actualHandleNotFoundResult = customResponseEntityExceptionHandler.handleNotFound(ex,
      new ServletWebRequest(new MockHttpServletRequest()));
    assertTrue(actualHandleNotFoundResult.hasBody());
    assertTrue(actualHandleNotFoundResult.getHeaders().isEmpty());
    assertEquals(404, actualHandleNotFoundResult.getStatusCodeValue());
    assertEquals("An error occurred", ((CustomErrorResponseDto) actualHandleNotFoundResult.getBody()).getMessage());
    assertEquals("Not Found", ((CustomErrorResponseDto) actualHandleNotFoundResult.getBody()).getStatus());
    assertEquals(404, ((CustomErrorResponseDto) actualHandleNotFoundResult.getBody()).getStatusCode());
  }

  @Test
  public void testHandleConflict() {
    TokenRepository tokenRepository = mock(TokenRepository.class);
    AppUserRepository appUserRepository = mock(AppUserRepository.class);
    AppUserServiceImpl appUserService = new AppUserServiceImpl(appUserRepository, new Argon2PasswordEncoder(),
      mock(RoleService.class));

    JwtConfig jwtConfig = new JwtConfig();
    AppUserServiceImpl appUserService1 = new AppUserServiceImpl(mock(AppUserRepository.class), null,
      mock(RoleService.class));

    JwtTokenUtil jwtTokenUtil = new JwtTokenUtil(jwtConfig, appUserService1, new SubstituteLogger("Name", null, true));

    CustomResponseEntityExceptionHandler customResponseEntityExceptionHandler = new CustomResponseEntityExceptionHandler(
      new TokenServiceImpl(tokenRepository, appUserService, jwtTokenUtil, new Argon2PasswordEncoder()));
    Exception ex = new Exception("An error occurred");
    ResponseEntity<Object> actualHandleConflictResult = customResponseEntityExceptionHandler.handleConflict(ex,
      new ServletWebRequest(new MockHttpServletRequest()));
    assertTrue(actualHandleConflictResult.hasBody());
    assertTrue(actualHandleConflictResult.getHeaders().isEmpty());
    assertEquals(409, actualHandleConflictResult.getStatusCodeValue());
    assertEquals("An error occurred", ((CustomErrorResponseDto) actualHandleConflictResult.getBody()).getMessage());
    assertEquals("Conflict", ((CustomErrorResponseDto) actualHandleConflictResult.getBody()).getStatus());
    assertEquals(409, ((CustomErrorResponseDto) actualHandleConflictResult.getBody()).getStatusCode());
  }

  @Test
  public void testHandleAll() {
    TokenRepository tokenRepository = mock(TokenRepository.class);
    AppUserRepository appUserRepository = mock(AppUserRepository.class);
    AppUserServiceImpl appUserService = new AppUserServiceImpl(appUserRepository, new Argon2PasswordEncoder(),
      mock(RoleService.class));

    JwtConfig jwtConfig = new JwtConfig();
    AppUserServiceImpl appUserService1 = new AppUserServiceImpl(mock(AppUserRepository.class), null,
      mock(RoleService.class));

    JwtTokenUtil jwtTokenUtil = new JwtTokenUtil(jwtConfig, appUserService1, new SubstituteLogger("Name", null, true));

    CustomResponseEntityExceptionHandler customResponseEntityExceptionHandler = new CustomResponseEntityExceptionHandler(
      new TokenServiceImpl(tokenRepository, appUserService, jwtTokenUtil, new Argon2PasswordEncoder()));
    Exception ex = new Exception("An error occurred");
    ResponseEntity<Object> actualHandleAllResult = customResponseEntityExceptionHandler.handleAll(ex,
      new ServletWebRequest(new MockHttpServletRequest()));
    assertTrue(actualHandleAllResult.hasBody());
    assertTrue(actualHandleAllResult.getHeaders().isEmpty());
    assertEquals(500, actualHandleAllResult.getStatusCodeValue());
    assertEquals("An error occurred", ((CustomErrorResponseDto) actualHandleAllResult.getBody()).getMessage());
    assertEquals("Internal Server Error", ((CustomErrorResponseDto) actualHandleAllResult.getBody()).getStatus());
    assertEquals(500, ((CustomErrorResponseDto) actualHandleAllResult.getBody()).getStatusCode());
  }
}

