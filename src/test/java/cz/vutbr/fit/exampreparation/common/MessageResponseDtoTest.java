package cz.vutbr.fit.exampreparation.common;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MessageResponseDtoTest {
  @Test
  public void testConstructor() {
    MessageResponseDto actualMessageResponseDto = new MessageResponseDto("Not all who wander are lost");
    actualMessageResponseDto.setMessage("Not all who wander are lost");
    assertEquals("Not all who wander are lost", actualMessageResponseDto.getMessage());
  }
}

