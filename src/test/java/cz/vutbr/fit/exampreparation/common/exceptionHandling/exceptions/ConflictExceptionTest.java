package cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class ConflictExceptionTest {
  @Test
  public void testConstructor() {
    ConflictException actualConflictException = new ConflictException("An error occurred");
    assertNull(actualConflictException.getCause());
    assertEquals(
      "cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.ConflictException: An error" + " occurred",
      actualConflictException.toString());
    assertEquals(0, actualConflictException.getSuppressed().length);
    assertEquals("An error occurred", actualConflictException.getMessage());
    assertEquals("An error occurred", actualConflictException.getLocalizedMessage());
  }
}

