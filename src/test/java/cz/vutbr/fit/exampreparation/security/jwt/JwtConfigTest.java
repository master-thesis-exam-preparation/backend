package cz.vutbr.fit.exampreparation.security.jwt;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

@ContextConfiguration(classes = {JwtConfig.class})
@ExtendWith(SpringExtension.class)
public class JwtConfigTest {
  @Autowired
  private JwtConfig jwtConfig;

  @Test
  public void testCanEqual() {
    assertFalse(this.jwtConfig.canEqual("Other"));
    assertTrue(this.jwtConfig.canEqual(this.jwtConfig));
  }

  @Test
  public void testConstructor() {
    JwtConfig actualJwtConfig = new JwtConfig();
    actualJwtConfig.setAccessTokenExpirationAfterMinutes(1L);
    actualJwtConfig.setSecretKey("EXAMPLEKEYwjalrXUtnFEMI/K7MDENG/bPxRfiCY");
    assertEquals(1L, actualJwtConfig.getAccessTokenExpirationAfterMinutes().longValue());
    assertEquals("EXAMPLEKEYwjalrXUtnFEMI/K7MDENG/bPxRfiCY", actualJwtConfig.getSecretKey());
    assertEquals("JwtConfig(secretKey=EXAMPLEKEYwjalrXUtnFEMI/K7MDENG/bPxRfiCY, accessTokenExpirationAfterMinutes=1)",
      actualJwtConfig.toString());
  }

  @Test
  public void testEquals() {
    assertNotEquals((new JwtConfig()), null);
    assertNotEquals((new JwtConfig()), "Different type to JwtConfig");
    assertNotEquals((new JwtConfig()), null);
  }

  @Test
  public void testEquals2() {
    JwtConfig jwtConfig = new JwtConfig();
    assertEquals(jwtConfig, jwtConfig);
    int expectedHashCodeResult = jwtConfig.hashCode();
    assertEquals(expectedHashCodeResult, jwtConfig.hashCode());
  }

  @Test
  public void testEquals3() {
    JwtConfig jwtConfig = new JwtConfig();
    JwtConfig jwtConfig1 = new JwtConfig();
    assertEquals(jwtConfig1, jwtConfig);
    int expectedHashCodeResult = jwtConfig.hashCode();
    assertEquals(expectedHashCodeResult, jwtConfig1.hashCode());
  }

  @Test
  public void testEquals4() {
    JwtConfig jwtConfig = new JwtConfig();
    jwtConfig.setSecretKey("EXAMPLEKEYwjalrXUtnFEMI/K7MDENG/bPxRfiCY");
    assertNotEquals(new JwtConfig(), jwtConfig);
  }

  @Test
  public void testEquals5() {
    JwtConfig jwtConfig = new JwtConfig();
    jwtConfig.setAccessTokenExpirationAfterMinutes(0L);
    assertNotEquals(new JwtConfig(), jwtConfig);
  }

  @Test
  public void testEquals6() {
    JwtConfig jwtConfig = new JwtConfig();

    JwtConfig jwtConfig1 = new JwtConfig();
    jwtConfig1.setSecretKey("EXAMPLEKEYwjalrXUtnFEMI/K7MDENG/bPxRfiCY");
    assertNotEquals(jwtConfig1, jwtConfig);
  }

  @Test
  public void testEquals7() {
    JwtConfig jwtConfig = new JwtConfig();

    JwtConfig jwtConfig1 = new JwtConfig();
    jwtConfig1.setAccessTokenExpirationAfterMinutes(0L);
    assertNotEquals(jwtConfig1, jwtConfig);
  }

  @Test
  public void testEquals8() {
    JwtConfig jwtConfig = new JwtConfig();
    jwtConfig.setSecretKey("EXAMPLEKEYwjalrXUtnFEMI/K7MDENG/bPxRfiCY");

    JwtConfig jwtConfig1 = new JwtConfig();
    jwtConfig1.setSecretKey("EXAMPLEKEYwjalrXUtnFEMI/K7MDENG/bPxRfiCY");
    assertEquals(jwtConfig1, jwtConfig);
    int expectedHashCodeResult = jwtConfig.hashCode();
    assertEquals(expectedHashCodeResult, jwtConfig1.hashCode());
  }

  @Test
  public void testEquals9() {
    JwtConfig jwtConfig = new JwtConfig();
    jwtConfig.setAccessTokenExpirationAfterMinutes(0L);

    JwtConfig jwtConfig1 = new JwtConfig();
    jwtConfig1.setAccessTokenExpirationAfterMinutes(0L);
    assertEquals(jwtConfig1, jwtConfig);
    int expectedHashCodeResult = jwtConfig.hashCode();
    assertEquals(expectedHashCodeResult, jwtConfig1.hashCode());
  }
}

