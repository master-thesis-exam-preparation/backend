package cz.vutbr.fit.exampreparation.security.jwt;

import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.UnauthorizedException;
import cz.vutbr.fit.exampreparation.features.appUser.AppUserRepository;
import cz.vutbr.fit.exampreparation.features.appUser.AppUserServiceImpl;
import cz.vutbr.fit.exampreparation.features.role.RoleService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.impl.DefaultClaims;
import io.jsonwebtoken.impl.DefaultJws;
import io.jsonwebtoken.impl.DefaultJwsHeader;
import org.junit.jupiter.api.Test;
import org.slf4j.event.SubstituteLoggingEvent;
import org.slf4j.helpers.SubstituteLogger;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;

import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;

public class JwtTokenUtilTest {
  @Test
  public void testValidate() {
    JwtConfig jwtConfig = new JwtConfig();
    jwtConfig.setSecretKey("EXAMPLEKEYwjalrXUtnFEMI/K7MDENG/bPxRfiCY");
    AppUserRepository appUserRepository = mock(AppUserRepository.class);
    AppUserServiceImpl appUserService = new AppUserServiceImpl(appUserRepository, new Argon2PasswordEncoder(),
      mock(RoleService.class));

    assertThrows(UnauthorizedException.class, () -> (new JwtTokenUtil(jwtConfig, appUserService,
      new SubstituteLogger("Name", new LinkedList<SubstituteLoggingEvent>(), true))).validate("ABC123"));
  }

  @Test
  public void testValidate3() {
    JwtConfig jwtConfig = new JwtConfig();
    jwtConfig.setSecretKey("EXAMPLEKEYwjalrXUtnFEMI/K7MDENG/bPxRfiCY");
    AppUserRepository appUserRepository = mock(AppUserRepository.class);
    AppUserServiceImpl appUserService = new AppUserServiceImpl(appUserRepository, new Argon2PasswordEncoder(),
      mock(RoleService.class));

    assertThrows(UnauthorizedException.class,
      () -> (new JwtTokenUtil(jwtConfig, appUserService,
        new SubstituteLogger("Name", new LinkedList<SubstituteLoggingEvent>(), true)))
        .validate("io.jsonwebtoken.Claims"));
  }

  @Test
  public void testValidate4() {
    JwtConfig jwtConfig = new JwtConfig();
    jwtConfig.setSecretKey("EXAMPLEKEYwjalrXUtnFEMI/K7MDENG/bPxRfiCY");
    AppUserRepository appUserRepository = mock(AppUserRepository.class);
    AppUserServiceImpl appUserService = new AppUserServiceImpl(appUserRepository, new Argon2PasswordEncoder(),
      mock(RoleService.class));

    assertThrows(UnauthorizedException.class, () -> (new JwtTokenUtil(jwtConfig, appUserService,
      new SubstituteLogger("Name", new LinkedList<SubstituteLoggingEvent>(), true))).validate(""));
  }

  @Test
  public void testGetPayload() {
    JwtConfig jwtConfig = new JwtConfig();
    AppUserRepository appUserRepository = mock(AppUserRepository.class);
    AppUserServiceImpl appUserService = new AppUserServiceImpl(appUserRepository, new Argon2PasswordEncoder(),
      mock(RoleService.class));

    JwtTokenUtil jwtTokenUtil = new JwtTokenUtil(jwtConfig, appUserService,
      new SubstituteLogger("Name", new LinkedList<SubstituteLoggingEvent>(), true));
    DefaultJwsHeader header = new DefaultJwsHeader();
    assertNull(jwtTokenUtil.getPayload(new DefaultJws<Claims>(header, new DefaultClaims(), "Signature")));
  }

}

