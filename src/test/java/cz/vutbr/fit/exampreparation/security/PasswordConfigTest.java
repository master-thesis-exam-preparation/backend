package cz.vutbr.fit.exampreparation.security;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertTrue;

@ContextConfiguration(classes = {PasswordConfig.class})
@ExtendWith(SpringExtension.class)
public class PasswordConfigTest {
    @Autowired
    private PasswordConfig passwordConfig;

    @Test
    public void testPasswordEncoder() {
        assertTrue(this.passwordConfig
                .passwordEncoder() instanceof org.springframework.security.crypto.argon2.Argon2PasswordEncoder);
    }
}

