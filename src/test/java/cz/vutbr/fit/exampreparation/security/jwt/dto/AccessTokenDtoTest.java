package cz.vutbr.fit.exampreparation.security.jwt.dto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class AccessTokenDtoTest {
  @Test
  public void testCanEqual() {
    assertFalse((new AccessTokenDto("ABC123")).canEqual("Other"));
  }

  @Test
  public void testCanEqual2() {
    AccessTokenDto accessTokenDto = new AccessTokenDto("ABC123");
    assertTrue(accessTokenDto.canEqual(new AccessTokenDto("ABC123")));
  }

  @Test
  public void testConstructor() {
    AccessTokenDto actualAccessTokenDto = new AccessTokenDto();
    actualAccessTokenDto.setAccessToken("ABC123");
    assertEquals("ABC123", actualAccessTokenDto.getAccessToken());
    assertEquals("AccessTokenDto(accessToken=ABC123)", actualAccessTokenDto.toString());
  }

  @Test
  public void testConstructor2() {
    AccessTokenDto actualAccessTokenDto = new AccessTokenDto("ABC123");
    actualAccessTokenDto.setAccessToken("ABC123");
    assertEquals("ABC123", actualAccessTokenDto.getAccessToken());
    assertEquals("AccessTokenDto(accessToken=ABC123)", actualAccessTokenDto.toString());
  }

  @Test
  public void testEquals() {
    assertNotEquals((new AccessTokenDto("ABC123")), null);
    assertNotEquals((new AccessTokenDto("ABC123")), "Different type to AccessTokenDto");
  }

  @Test
  public void testEquals2() {
    AccessTokenDto accessTokenDto = new AccessTokenDto("ABC123");
    assertEquals(accessTokenDto, accessTokenDto);
    int expectedHashCodeResult = accessTokenDto.hashCode();
    assertEquals(expectedHashCodeResult, accessTokenDto.hashCode());
  }

  @Test
  public void testEquals3() {
    AccessTokenDto accessTokenDto = new AccessTokenDto("ABC123");
    AccessTokenDto accessTokenDto1 = new AccessTokenDto("ABC123");
    assertEquals(accessTokenDto1, accessTokenDto);
    int expectedHashCodeResult = accessTokenDto.hashCode();
    assertEquals(expectedHashCodeResult, accessTokenDto1.hashCode());
  }

  @Test
  public void testEquals4() {
    AccessTokenDto accessTokenDto = new AccessTokenDto("ExampleToken");
    assertNotEquals(new AccessTokenDto("ABC123"), accessTokenDto);
  }

  @Test
  public void testEquals5() {
    AccessTokenDto accessTokenDto = new AccessTokenDto(null);
    assertNotEquals(new AccessTokenDto("ABC123"), accessTokenDto);
  }

  @Test
  public void testEquals6() {
    AccessTokenDto accessTokenDto = new AccessTokenDto(null);
    AccessTokenDto accessTokenDto1 = new AccessTokenDto(null);
    assertEquals(accessTokenDto1, accessTokenDto);
    int expectedHashCodeResult = accessTokenDto.hashCode();
    assertEquals(expectedHashCodeResult, accessTokenDto1.hashCode());
  }
}

