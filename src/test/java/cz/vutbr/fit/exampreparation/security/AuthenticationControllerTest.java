package cz.vutbr.fit.exampreparation.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.vutbr.fit.exampreparation.common.MessageResponseDto;
import cz.vutbr.fit.exampreparation.features.appUser.account.AccountService;
import cz.vutbr.fit.exampreparation.features.appUser.dto.EmailDto;
import cz.vutbr.fit.exampreparation.features.appUser.dto.LoginUserDto;
import cz.vutbr.fit.exampreparation.features.appUser.dto.NewPasswordDto;
import cz.vutbr.fit.exampreparation.features.appUser.dto.RegisterUserDto;
import cz.vutbr.fit.exampreparation.security.jwt.dto.AccessTokenDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {AuthenticationController.class})
@ExtendWith(SpringExtension.class)
public class AuthenticationControllerTest {
  @MockBean
  private AccountService accountService;

  @Autowired
  private AuthenticationController authenticationController;

  @Test
  public void testLoginUser() throws Exception {
    when(this.accountService.loginUser((LoginUserDto) any()))
      .thenReturn(new ResponseEntity<AccessTokenDto>(HttpStatus.CONTINUE));

    LoginUserDto loginUserDto = new LoginUserDto();
    loginUserDto.setPassword("iloveyou");
    loginUserDto.setEmail("jane.doe@example.org");
    loginUserDto.setRememberMe(true);
    String content = (new ObjectMapper()).writeValueAsString(loginUserDto);
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/auth/login")
      .contentType(MediaType.APPLICATION_JSON)
      .content(content);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.authenticationController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testForgetPassword() throws Exception {
    when(this.accountService.forgetPassword((EmailDto) any()))
      .thenReturn(new ResponseEntity<MessageResponseDto>(HttpStatus.CONTINUE));

    EmailDto emailDto = new EmailDto();
    emailDto.setEmail("jane.doe@example.org");
    String content = (new ObjectMapper()).writeValueAsString(emailDto);
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/auth/forget-password")
      .contentType(MediaType.APPLICATION_JSON)
      .content(content);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.authenticationController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testVerifyAccount() throws Exception {
    when(this.accountService.verifyAccount(anyString()))
      .thenReturn(new ResponseEntity<MessageResponseDto>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/auth/verify/{token}", "ABC123");
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.authenticationController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testLogoutUser() throws Exception {
    when(this.accountService.logoutUser(anyString()))
      .thenReturn(new ResponseEntity<MessageResponseDto>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/auth/logout");
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.authenticationController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(200));
  }

  @Test
  public void testLogMeIn() throws Exception {
    when(this.accountService.logMeIn(anyString())).thenReturn(new ResponseEntity<AccessTokenDto>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/auth/log-me-in");
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.authenticationController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(200));
  }

  @Test
  public void testRefreshToken() throws Exception {
    when(this.accountService.generateNewKeys(anyString()))
      .thenReturn(new ResponseEntity<AccessTokenDto>(HttpStatus.CONTINUE));
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/auth/refresh-token");
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.authenticationController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(200));
  }

  @Test
  public void testRegisterUser() throws Exception {
    when(this.accountService.createNewUserAccount((RegisterUserDto) any()))
      .thenReturn(new ResponseEntity<MessageResponseDto>(HttpStatus.CONTINUE));

    RegisterUserDto registerUserDto = new RegisterUserDto();
    registerUserDto.setLastName("Doe");
    registerUserDto.setDegreesBeforeName("Degrees Before Name");
    registerUserDto.setPassword("iloveyou");
    registerUserDto.setEmail("jane.doe@example.org");
    registerUserDto.setDegreesBehindName("Degrees Behind Name");
    registerUserDto.setFirstName("Jane");
    String content = (new ObjectMapper()).writeValueAsString(registerUserDto);
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/auth/register")
      .contentType(MediaType.APPLICATION_JSON)
      .content(content);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.authenticationController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }

  @Test
  public void testVerifyForgetPasswordToken() throws Exception {
    when(this.accountService.resetPassword((NewPasswordDto) any()))
      .thenReturn(new ResponseEntity<MessageResponseDto>(HttpStatus.CONTINUE));

    NewPasswordDto newPasswordDto = new NewPasswordDto();
    newPasswordDto.setPassword("iloveyou");
    newPasswordDto.setToken("ABC123");
    String content = (new ObjectMapper()).writeValueAsString(newPasswordDto);
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/auth/reset-password")
      .contentType(MediaType.APPLICATION_JSON)
      .content(content);
    ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.authenticationController)
      .build()
      .perform(requestBuilder);
    actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
  }
}

