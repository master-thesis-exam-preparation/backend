package cz.vutbr.fit.exampreparation.security;

import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.NotFoundException;
import cz.vutbr.fit.exampreparation.features.appUser.AppUser;
import cz.vutbr.fit.exampreparation.features.appUser.AppUserService;
import cz.vutbr.fit.exampreparation.features.assignment.Assignment;
import cz.vutbr.fit.exampreparation.features.role.Role;
import cz.vutbr.fit.exampreparation.features.role.RoleName;
import cz.vutbr.fit.exampreparation.features.room.Room;
import cz.vutbr.fit.exampreparation.features.term.Term;
import cz.vutbr.fit.exampreparation.features.token.Token;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {UserDetailsServiceImpl.class})
@ExtendWith(SpringExtension.class)
public class UserDetailsServiceImplTest {
  @MockBean
  private AppUserService appUserService;

  @Autowired
  private UserDetailsServiceImpl userDetailsServiceImpl;

  @Test
  public void testLoadUserByUsername() throws UsernameNotFoundException {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());
    Optional<AppUser> ofResult = Optional.<AppUser>of(appUser);
    when(this.appUserService.findByEmail(anyString())).thenReturn(ofResult);
    UserDetails actualLoadUserByUsernameResult = this.userDetailsServiceImpl.loadUserByUsername("jane.doe@example.org");
    assertTrue(actualLoadUserByUsernameResult.getAuthorities().isEmpty());
    assertTrue(actualLoadUserByUsernameResult.isEnabled());
    assertTrue(actualLoadUserByUsernameResult.isCredentialsNonExpired());
    assertFalse(actualLoadUserByUsernameResult.isAccountNonLocked());
    assertTrue(actualLoadUserByUsernameResult.isAccountNonExpired());
    assertEquals("jane.doe@example.org", actualLoadUserByUsernameResult.getUsername());
    assertEquals("iloveyou", actualLoadUserByUsernameResult.getPassword());
    verify(this.appUserService).findByEmail(anyString());
  }

  @Test
  public void testLoadUserByUsername2() throws UsernameNotFoundException {
    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(false);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(new ArrayList<Role>());
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());
    Optional<AppUser> ofResult = Optional.<AppUser>of(appUser);
    when(this.appUserService.findByEmail(anyString())).thenReturn(ofResult);
    UserDetails actualLoadUserByUsernameResult = this.userDetailsServiceImpl.loadUserByUsername("jane.doe@example.org");
    assertTrue(actualLoadUserByUsernameResult.getAuthorities().isEmpty());
    assertFalse(actualLoadUserByUsernameResult.isEnabled());
    assertTrue(actualLoadUserByUsernameResult.isCredentialsNonExpired());
    assertFalse(actualLoadUserByUsernameResult.isAccountNonLocked());
    assertTrue(actualLoadUserByUsernameResult.isAccountNonExpired());
    assertEquals("jane.doe@example.org", actualLoadUserByUsernameResult.getUsername());
    assertEquals("iloveyou", actualLoadUserByUsernameResult.getPassword());
    verify(this.appUserService).findByEmail(anyString());
  }

  @Test
  public void testLoadUserByUsername3() throws UsernameNotFoundException {
    Role role = new Role();
    role.setRoleDescription("Role Description");
    role.setRoleName(RoleName.STUDENT);
    role.setAppUsers(new ArrayList<AppUser>());

    ArrayList<Role> roleList = new ArrayList<Role>();
    roleList.add(role);

    AppUser appUser = new AppUser();
    appUser.setLastName("Doe");
    appUser.setEmail("jane.doe@example.org");
    appUser.setDegreesBeforeName("Degrees Before Name");
    appUser.setPassword("iloveyou");
    appUser.setUserTokens(new ArrayList<Token>());
    appUser.setFirstName("Jane");
    appUser.setVerified(true);
    appUser.setRooms(new ArrayList<Room>());
    appUser.setDegreesBehindName("Degrees Behind Name");
    appUser.setRoles(roleList);
    appUser.setLocked(true);
    appUser.setTerms(new ArrayList<Term>());
    appUser.setAssignments(new ArrayList<Assignment>());
    Optional<AppUser> ofResult = Optional.<AppUser>of(appUser);
    when(this.appUserService.findByEmail(anyString())).thenReturn(ofResult);
    UserDetails actualLoadUserByUsernameResult = this.userDetailsServiceImpl.loadUserByUsername("jane.doe@example.org");
    assertEquals(1, actualLoadUserByUsernameResult.getAuthorities().size());
    assertEquals(
      "org.springframework.security.core.userdetails.User [Username=jane.doe@example.org, Password=[PROTECTED],"
        + " Enabled=true, AccountNonExpired=true, credentialsNonExpired=true, AccountNonLocked=false, Granted"
        + " Authorities=[ROLE_STUDENT]]",
      actualLoadUserByUsernameResult.toString());
    assertTrue(actualLoadUserByUsernameResult.isEnabled());
    assertTrue(actualLoadUserByUsernameResult.isCredentialsNonExpired());
    assertFalse(actualLoadUserByUsernameResult.isAccountNonLocked());
    assertTrue(actualLoadUserByUsernameResult.isAccountNonExpired());
    assertEquals("jane.doe@example.org", actualLoadUserByUsernameResult.getUsername());
    assertEquals("iloveyou", actualLoadUserByUsernameResult.getPassword());
    verify(this.appUserService).findByEmail(anyString());
  }

  @Test
  public void testLoadUserByUsername4() throws UsernameNotFoundException {
    when(this.appUserService.findByEmail(anyString())).thenReturn(Optional.<AppUser>empty());
    assertThrows(NotFoundException.class, () -> this.userDetailsServiceImpl.loadUserByUsername("jane.doe@example.org"));
    verify(this.appUserService).findByEmail(anyString());
  }
}

