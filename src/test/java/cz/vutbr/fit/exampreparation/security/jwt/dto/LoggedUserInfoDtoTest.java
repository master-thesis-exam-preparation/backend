package cz.vutbr.fit.exampreparation.security.jwt.dto;

import cz.vutbr.fit.exampreparation.features.appUser.dto.AccountInfoDto;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class LoggedUserInfoDtoTest {
  @Test
  public void testCanEqual() {
    assertFalse((new LoggedUserInfoDto()).canEqual("Other"));
  }

  @Test
  public void testCanEqual2() {
    LoggedUserInfoDto loggedUserInfoDto = new LoggedUserInfoDto();
    assertTrue(loggedUserInfoDto.canEqual(new LoggedUserInfoDto()));
  }

  @Test
  public void testConstructor() {
    LoggedUserInfoDto actualLoggedUserInfoDto = new LoggedUserInfoDto();
    actualLoggedUserInfoDto.setEmail("jane.doe@example.org");
    actualLoggedUserInfoDto.setNameWithDegrees("Name With Degrees");
    ArrayList<String> stringList = new ArrayList<String>();
    actualLoggedUserInfoDto.setRoles(stringList);
    assertEquals("jane.doe@example.org", actualLoggedUserInfoDto.getEmail());
    assertEquals("Name With Degrees", actualLoggedUserInfoDto.getNameWithDegrees());
    assertSame(stringList, actualLoggedUserInfoDto.getRoles());
    assertEquals("LoggedUserInfoDto(nameWithDegrees=Name With Degrees, email=jane.doe@example.org, roles=[])",
      actualLoggedUserInfoDto.toString());
  }

  @Test
  public void testConstructor2() {
    ArrayList<String> stringList = new ArrayList<String>();
    LoggedUserInfoDto actualLoggedUserInfoDto = new LoggedUserInfoDto("Name With Degrees", "jane.doe@example.org",
      stringList);
    actualLoggedUserInfoDto.setEmail("jane.doe@example.org");
    actualLoggedUserInfoDto.setNameWithDegrees("Name With Degrees");
    ArrayList<String> stringList1 = new ArrayList<String>();
    actualLoggedUserInfoDto.setRoles(stringList1);
    assertEquals("jane.doe@example.org", actualLoggedUserInfoDto.getEmail());
    assertEquals("Name With Degrees", actualLoggedUserInfoDto.getNameWithDegrees());
    List<String> roles = actualLoggedUserInfoDto.getRoles();
    assertSame(stringList1, roles);
    assertEquals(stringList, roles);
    assertEquals("LoggedUserInfoDto(nameWithDegrees=Name With Degrees, email=jane.doe@example.org, roles=[])",
      actualLoggedUserInfoDto.toString());
  }

  @Test
  public void testEquals() {
    assertNotEquals((new LoggedUserInfoDto()), null);
    assertNotEquals((new LoggedUserInfoDto()), "Different type to LoggedUserInfoDto");
  }

  @Test
  public void testEquals10() {
    LoggedUserInfoDto loggedUserInfoDto = new LoggedUserInfoDto();

    LoggedUserInfoDto loggedUserInfoDto1 = new LoggedUserInfoDto();
    loggedUserInfoDto1.setEmail("jane.doe@example.org");
    assertNotEquals(loggedUserInfoDto1, loggedUserInfoDto);
  }

  @Test
  public void testEquals11() {
    ArrayList<String> stringList = new ArrayList<String>();
    stringList.add("Name With Degrees");
    LoggedUserInfoDto loggedUserInfoDto = new LoggedUserInfoDto("Name With Degrees", "jane.doe@example.org",
      stringList);
    assertNotEquals(new LoggedUserInfoDto("Name With Degrees", "jane.doe@example.org", new ArrayList<String>()), loggedUserInfoDto);
  }

  @Test
  public void testEquals12() {
    LoggedUserInfoDto loggedUserInfoDto = new LoggedUserInfoDto("Name With Degrees", "jane.doe@example.org",
      new ArrayList<String>());
    loggedUserInfoDto.setRoles(null);
    assertNotEquals(new LoggedUserInfoDto("Name With Degrees", "jane.doe@example.org", new ArrayList<String>()), loggedUserInfoDto);
  }

  @Test
  public void testEquals2() {
    LoggedUserInfoDto loggedUserInfoDto = new LoggedUserInfoDto();
    assertEquals(loggedUserInfoDto, loggedUserInfoDto);
    int expectedHashCodeResult = loggedUserInfoDto.hashCode();
    assertEquals(expectedHashCodeResult, loggedUserInfoDto.hashCode());
  }

  @Test
  public void testEquals3() {
    LoggedUserInfoDto loggedUserInfoDto = new LoggedUserInfoDto();
    LoggedUserInfoDto loggedUserInfoDto1 = new LoggedUserInfoDto();
    assertEquals(loggedUserInfoDto1, loggedUserInfoDto);
    int expectedHashCodeResult = loggedUserInfoDto.hashCode();
    assertEquals(expectedHashCodeResult, loggedUserInfoDto1.hashCode());
  }

  @Test
  public void testEquals4() {
    LoggedUserInfoDto loggedUserInfoDto = new LoggedUserInfoDto("Name With Degrees", "jane.doe@example.org",
      new ArrayList<String>());
    assertNotEquals(new LoggedUserInfoDto(), loggedUserInfoDto);
  }

  @Test
  public void testEquals5() {
    AccountInfoDto accountInfoDto = mock(AccountInfoDto.class);
    assertNotEquals(new LoggedUserInfoDto(), accountInfoDto);
  }

  @Test
  public void testEquals6() {
    LoggedUserInfoDto loggedUserInfoDto = new LoggedUserInfoDto();
    assertNotEquals(new LoggedUserInfoDto("Name With Degrees", "jane.doe@example.org", new ArrayList<String>()), loggedUserInfoDto);
  }

  @Test
  public void testEquals7() {
    LoggedUserInfoDto loggedUserInfoDto = new LoggedUserInfoDto();
    assertNotEquals(new AccountInfoDto(123, "Name With Degrees", "jane.doe@example.org", new ArrayList<String>(), true), loggedUserInfoDto);
  }

  @Test
  public void testEquals8() {
    LoggedUserInfoDto loggedUserInfoDto = new LoggedUserInfoDto();
    loggedUserInfoDto.setEmail("jane.doe@example.org");
    assertNotEquals(new LoggedUserInfoDto(), loggedUserInfoDto);
  }

  @Test
  public void testEquals9() {
    LoggedUserInfoDto loggedUserInfoDto = new LoggedUserInfoDto("Name With Degrees", "jane.doe@example.org",
      new ArrayList<String>());
    LoggedUserInfoDto loggedUserInfoDto1 = new LoggedUserInfoDto("Name With Degrees", "jane.doe@example.org",
      new ArrayList<String>());

    assertEquals(loggedUserInfoDto1, loggedUserInfoDto);
    int expectedHashCodeResult = loggedUserInfoDto.hashCode();
    assertEquals(expectedHashCodeResult, loggedUserInfoDto1.hashCode());
  }
}

