package cz.vutbr.fit.exampreparation.security;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertTrue;

@ContextConfiguration(classes = {CorsConfig.class})
@ExtendWith(SpringExtension.class)
public class CorsConfigTest {
  @Autowired
  private CorsConfig corsConfig;

  @Test
  public void testCorsConfigurationSource() {
    assertTrue(this.corsConfig
      .corsConfigurationSource() instanceof org.springframework.web.cors.UrlBasedCorsConfigurationSource);
  }
}

