package cz.vutbr.fit.exampreparation.swagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * This class is responsible for setting the configuration of {@link Docket} bean.
 */
@Configuration
public class SwaggerConfiguration {

  /**
   * Creates {@link Docket} bean with specified config.
   * This bean is responsible for generating web documentation of the application REST API endpoints.
   *
   * @return configured {@link Docket} bean
   */
  @Bean
  public Docket backendApi() {
    return new Docket(DocumentationType.SWAGGER_2)
      .select()
      .apis(RequestHandlerSelectors.basePackage("cz.vutbr.fit.exampreparation"))
      .paths(PathSelectors.any())
      .build()
      .apiInfo(createApiInfo()
      );
  }

  private ApiInfo createApiInfo() {
    return new ApiInfoBuilder()
      .title("Application for Exam Preparation")
      .description("Master Thesis")
      .contact(new Contact("Bc. Tomáš Líbal", "https://gitlab.com/master-thesis-exam-preparation", "xlibal00@stud.fit.vutbr.cz"))
      .build();
  }
}
