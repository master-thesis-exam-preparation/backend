package cz.vutbr.fit.exampreparation;

import cz.vutbr.fit.exampreparation.swagger.SwaggerConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Class responsible for start of the whole application.
 */
@SpringBootApplication
@EnableAsync
@EnableScheduling
@Import(SwaggerConfiguration.class)
public class ExamPreparationApplication {

  /**
   * Starts this Spring application with given console inputs.
   *
   * @param args  applications console inputs
   */
	public static void main(String[] args) {
		SpringApplication.run(ExamPreparationApplication.class, args);
	}

}
