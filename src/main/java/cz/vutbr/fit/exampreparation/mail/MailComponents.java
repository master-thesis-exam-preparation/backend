package cz.vutbr.fit.exampreparation.mail;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Used to store information necessary to send an e-mail messsage.
 */
@Data
@AllArgsConstructor
public class MailComponents {

  private String[] recipientEmails;
  private String subject;
  private String mailBody;

}
