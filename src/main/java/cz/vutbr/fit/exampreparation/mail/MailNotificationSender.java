package cz.vutbr.fit.exampreparation.mail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * This service class is responsible for sending the e-mail messages via {@link JavaMailSender}.
 */
@Service
public class MailNotificationSender {

  private final JavaMailSender javaMailSender;

  /**
   * Creates new instance of {@link MailNotificationSender}.
   *
   * @param javaMailSender configured {@link JavaMailSender} bean
   */
  @Autowired
  public MailNotificationSender(JavaMailSender javaMailSender) {
    this.javaMailSender = javaMailSender;
  }

  /**
   * Asynchronously send e-mail message via {@link JavaMailSender}.
   *
   * @param mailComponents  object with necessary information to send email (recipients, message text etc.)
   * @param emailSender  senders e-mail address
   */
  @Async
  public void sendMail(MailComponents mailComponents, String emailSender) {
    MimeMessagePreparator messagePreparator = mimeMessage -> {
      MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
      messageHelper.setFrom(emailSender);
      messageHelper.setTo(mailComponents.getRecipientEmails());
      messageHelper.setSubject(mailComponents.getSubject());
      messageHelper.setText(mailComponents.getMailBody(), true);
    };
    javaMailSender.send(messagePreparator);
  }
}
