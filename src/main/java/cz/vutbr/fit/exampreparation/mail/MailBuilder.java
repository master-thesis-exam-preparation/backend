package cz.vutbr.fit.exampreparation.mail;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

/**
 * This service class is responsible for building an e-mail messages from HTML templates.
 */
@Service
@AllArgsConstructor
public class MailBuilder {

  private final TemplateEngine templateEngine;

  /**
   * Builds an e-mail message with account verification URL address.
   *
   * @param address  URL address with verification token
   * @return HTML template of e-mail message as String
   */
  public String buildAccountVerificationMail(String address) {
    Context context = new Context();
    context.setVariable("address", address);
    return templateEngine.process("accountVerificationMail", context);
  }

  /**
   * Builds an e-mail message with reset password URL address.
   *
   * @param address  URL address with reset password token
   * @return HTML template of e-mail message as String
   */
  public String buildForgetPasswordEmail(String address) {
    Context context = new Context();
    context.setVariable("address", address);
    return templateEngine.process("forgetPasswordMail", context);
  }

  /**
   * Builds a notification e-mail message with information about newly created term run.
   *
   * @param termName  name of the newly created term
   * @param courseAbbreviation  abbreviation of the terms course
   * @param courseName  name of the terms course
   * @param termRunStartTime  term run start time and date
   * @param roomAbbreviation  abbreviation of the term runs room
   * @param roomRow  number of row in which student has his place to sit
   * @param roomColumn  number of column in which student has his place to sit
   * @param frontendAddress  URL address of frontend application
   * @param emailMessage  message from teacher for students
   * @return HTML template of e-mail message as String
   */
  public String buildNewTermRunEmail(String termName, String courseAbbreviation, String courseName, String termRunStartTime,
                                     String roomAbbreviation, int roomRow, int roomColumn, String frontendAddress, String emailMessage) {
    Context context = createContext(termName, courseAbbreviation, courseName, termRunStartTime, roomAbbreviation, roomRow, roomColumn, frontendAddress, emailMessage);
    return templateEngine.process("newTermRunMail", context);
  }

  /**
   * Builds a notification e-mail message with information about new changes in existing term run.
   *
   * @param termName  name of the edited term
   * @param courseAbbreviation  abbreviation of the terms course
   * @param courseName  name of the terms course
   * @param termRunStartTime  term run start time and date
   * @param roomAbbreviation  abbreviation of the term runs room
   * @param roomRow  number of row in which student has his place to sit
   * @param roomColumn  number of column in which student has his place to sit
   * @param frontendAddress  URL address of frontend application
   * @param emailMessage  message from teacher for students
   * @return HTML template of e-mail message as String
   */
  public String buildEditTermRunEmail(String termName, String courseAbbreviation, String courseName, String termRunStartTime,
                                     String roomAbbreviation, int roomRow, int roomColumn, String frontendAddress, String emailMessage) {
    Context context = createContext(termName, courseAbbreviation, courseName, termRunStartTime, roomAbbreviation, roomRow, roomColumn, frontendAddress, emailMessage);
    return templateEngine.process("editTermRunMail", context);
  }

  /**
   * Builds a notification e-mail message with information about term from which was student removed.
   *
   * @param termName  name of the edited term
   * @param courseAbbreviation  abbreviation of the terms course
   * @param courseName  name of the terms course
   * @param termRunStartTime  term run start time and date
   * @param frontendAddress  URL address of frontend application
   * @return HTML template of e-mail message as String
   */
  public String buildRemovedFromTermEmail(String termName, String courseAbbreviation, String courseName, String termRunStartTime, String frontendAddress) {
    Context context = new Context();
    context.setVariable("termName", termName);
    context.setVariable("courseAbbreviation", courseAbbreviation);
    context.setVariable("courseName", courseName);
    context.setVariable("termRunStartTime", termRunStartTime);
    context.setVariable("frontendAddress", frontendAddress);
    return templateEngine.process("removedFromTermMail", context);
  }

  private Context createContext(String termName, String courseAbbreviation, String courseName, String termRunStartTime,
                                String roomAbbreviation, int roomRow, int roomColumn, String frontendAddress, String emailMessage) {
    Context context = new Context();
    context.setVariable("termName", termName);
    context.setVariable("courseAbbreviation", courseAbbreviation);
    context.setVariable("courseName", courseName);
    context.setVariable("termRunStartTime", termRunStartTime);
    context.setVariable("roomAbbreviation", roomAbbreviation);
    context.setVariable("roomRow", roomRow);
    context.setVariable("roomColumn", roomColumn);
    context.setVariable("frontendAddress", frontendAddress);
    context.setVariable("emailMessage", emailMessage);
    return context;
  }

}
