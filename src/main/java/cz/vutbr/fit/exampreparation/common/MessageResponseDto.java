package cz.vutbr.fit.exampreparation.common;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;


/**
 * Used to store message. Usually message is text response for user request.
 */
@Getter
@Setter
@AllArgsConstructor
public class MessageResponseDto {

  private String message;

}
