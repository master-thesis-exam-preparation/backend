package cz.vutbr.fit.exampreparation.common;

import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;

/**
 * This service class is responsible for asynchronous processing of {@link Runnable} operation.
 */
@Service
public class AsyncService {

  /**
   * Asynchronously processes provided operation.
   *
   * @param operation  operation which should be processed asynchronously
   * @return {@link CompletableFuture} with operation result
   */
  public CompletableFuture<?> process(Runnable operation) {
    return CompletableFuture.runAsync(operation);
  }

}
