package cz.vutbr.fit.exampreparation.common;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Used to store information about logged-in user.
 */
@Getter
@Setter
@AllArgsConstructor
public class UserInfo {

  private String email;
  private List<String> roles;

}
