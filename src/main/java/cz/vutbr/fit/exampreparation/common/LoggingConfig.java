package cz.vutbr.fit.exampreparation.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InjectionPoint;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * This class is responsible for setting the configuration of {@link Logger} bean.
 */
@Configuration
public class LoggingConfig {

  /**
   * Creates {@link Logger} bean with specified config.
   * This bean is responsible for printing log messages.
   *
   * @param injectionPoint {@link InjectionPoint} of logger
   * @return configured {@link Logger} bean
   */
  @Bean
  public Logger logger(final InjectionPoint injectionPoint) {
    if (injectionPoint.getMethodParameter() != null) {
      return LoggerFactory.getLogger(injectionPoint.getMethodParameter().getContainingClass());
    }

    if (injectionPoint.getField() != null) {
      return LoggerFactory.getLogger(injectionPoint.getField().getDeclaringClass());
    }

    throw new IllegalArgumentException();
  }

}
