package cz.vutbr.fit.exampreparation.common;

import cz.vutbr.fit.exampreparation.features.appUser.AppUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.List;
import java.util.stream.Collectors;

/**
 * This class is responsible for obtaining information about the currently logged-in user.
 */
public class AuthService {

  /**
   * Gets information about currently logged-in user from {@link SecurityContextHolder}.
   *
   * @return {@link UserInfo} object with information about currently logged-in user
   */
  public static UserInfo getLoggedUserInfo() {
    Authentication principal = SecurityContextHolder.getContext().getAuthentication();

    List<String> roles = principal.getAuthorities()
      .stream()
      .map(GrantedAuthority::getAuthority)
      .collect(Collectors.toList());

    return new UserInfo(principal.getName(), roles);
  }

  /**
   * Checks if currently logged-in is in list of managers.
   *
   * @param managers  list of managers
   * @return true if currently logged-in user is in list of managers, false otherwise
   */
  public static boolean isUserManager(List<AppUser> managers) {
    UserInfo userInfo = AuthService.getLoggedUserInfo();
    boolean isUserManager = managers.stream()
      .anyMatch(manager -> manager.getEmail().equals(userInfo.getEmail()));
    return isUserManager || userInfo.getRoles().contains("ROLE_ADMIN");
  }
}
