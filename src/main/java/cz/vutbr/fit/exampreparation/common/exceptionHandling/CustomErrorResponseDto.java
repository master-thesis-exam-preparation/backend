package cz.vutbr.fit.exampreparation.common.exceptionHandling;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import cz.vutbr.fit.exampreparation.common.MessageResponseDto;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;


/**
 * Used to store information about the occurred error.
 */
@Getter
@Setter
public class CustomErrorResponseDto extends MessageResponseDto {

  @JsonSerialize(using = LocalDateTimeSerializer.class)
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy HH:mm:ss")
  private LocalDateTime timestamp;
  private int statusCode;
  private String status;

  /**
   * Creates new instance of {@code CustomErrorResponseDto}.
   *
   * @param status  HttpStatus corresponding with thrown error
   * @param errorMessage  error message of thrown error
   */
  public CustomErrorResponseDto(final HttpStatus status, String errorMessage) {
    super(errorMessage);
    this.timestamp = LocalDateTime.now();
    this.statusCode = status.value();
    this.status = status.getReasonPhrase();
  }
}
