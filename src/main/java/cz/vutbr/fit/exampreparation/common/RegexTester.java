package cz.vutbr.fit.exampreparation.common;

import java.util.regex.Pattern;

/**
 * Class responsible for working with regular expressions.
 */
public class RegexTester {

  /**
   * Checks if text matches regular expression.
   *
   * @param regex  regular expression
   * @param text  text to check
   * @return true if text matches regular exception, false otherwise
   */
  public static boolean testRegex(String regex, String text) {
    Pattern p = Pattern.compile(regex);
    return p.matcher(text).find();
  }

}
