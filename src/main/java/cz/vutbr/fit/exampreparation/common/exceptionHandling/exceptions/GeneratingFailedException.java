package cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions;

/**
 * Thrown when an error occurred during assignment generating.
 */
public class GeneratingFailedException extends BaseException{

  private final int errorCode;
  private final String logFilePath;

  /**
   *
   * @return error code returned by latex compiler
   */
  public int getErrorCode() {
    return errorCode;
  }

  /**
   *
   * @return path to file, which contains information about error
   */
  public String getLogFilePath() {
    return logFilePath;
  }

  /**
   * Creates new instance of {@code GeneratingFailedException}.
   *
   * @param message  error message
   * @param errorCode  error code returned by latex compiler
   * @param logFilePath  path to file, which contains information about error
   */
  public GeneratingFailedException(String message, int errorCode, String logFilePath) {
    super(message);
    this.errorCode = errorCode;
    this.logFilePath = logFilePath;
  }
}
