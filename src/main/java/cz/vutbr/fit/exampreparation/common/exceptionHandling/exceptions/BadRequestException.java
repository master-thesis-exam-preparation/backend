package cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions;

/**
 * Thrown when an user request contains invalid data, missing mandatory data etc.
 */
public class BadRequestException extends BaseException {

  /**
   * Creates new instance of {@code BadRequestException}.
   *
   * @param message  error message
   */
  public BadRequestException(String message) {
    super(message);
  }
}
