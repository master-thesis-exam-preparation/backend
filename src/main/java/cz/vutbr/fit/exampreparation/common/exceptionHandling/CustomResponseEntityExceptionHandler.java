package cz.vutbr.fit.exampreparation.common.exceptionHandling;

import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.*;
import cz.vutbr.fit.exampreparation.features.token.TokenService;
import lombok.AllArgsConstructor;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.annotation.Nonnull;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

/**
 * Class used to capture all thrown exceptions.
 *
 * Exceptions are handled and corresponding {@link CustomErrorResponseDto} is created.
 * and send to user as response to failed request
 */
@AllArgsConstructor
@RestControllerAdvice
public class CustomResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

  private final TokenService tokenService;

  /* ****** 400 ****** */
  /**
   * {@inheritDoc}
   */
  @Override
  @Nonnull
  protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                HttpHeaders headers,
                                                                HttpStatus status,
                                                                WebRequest request) {
    return getErrorsCreateCustomResponse(ex, ex.getBindingResult(), status);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  @Nonnull
  protected ResponseEntity<Object> handleBindException(BindException ex,
                                                       HttpHeaders headers,
                                                       HttpStatus status,
                                                       WebRequest request) {
    return getErrorsCreateCustomResponse(ex, ex.getBindingResult(), status);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  @Nonnull
  protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex,
                                                                        HttpHeaders headers,
                                                                        HttpStatus status,
                                                                        WebRequest request) {
    String errorMessage = "Chybějící parametr \"" + ex.getParameterName() + "\"";
    return getCustomErrorResponseEntity(status, errorMessage);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  @Nonnull
  protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex,
                                                      HttpHeaders headers,
                                                      HttpStatus status,
                                                      WebRequest request) {
    String errorMessage = "Hodnota \"" + ex.getPropertyName() + "\" by měla být typu \"" + ex.getRequiredType() + "\"";
    return getCustomErrorResponseEntity(status, errorMessage);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  @Nonnull
  protected ResponseEntity<Object> handleMissingServletRequestPart(MissingServletRequestPartException ex,
                                                                   HttpHeaders headers,
                                                                   HttpStatus status,
                                                                   WebRequest request) {
    String errorMessage = "V požadavku chybí část \"" + ex.getRequestPartName() + "\"";
    return getCustomErrorResponseEntity(status, errorMessage);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  @Nonnull
  protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
                                                                HttpHeaders headers,
                                                                HttpStatus status,
                                                                WebRequest request) {
    String errorMessage = "Hodnotu některého z atributů není možné přečíst. Zkontrolujte, zda je požadavek ve správném formátu.";
    return getCustomErrorResponseEntity(status, errorMessage);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  @Nonnull
  protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
    return getCustomErrorResponseEntity(HttpStatus.NOT_FOUND, "Neexistující záznam");
  }

  /**
   * Capture {@link MethodArgumentTypeMismatchException} and creates {@link CustomErrorResponseDto} response.
   *
   * @param ex  captured {@link MethodArgumentTypeMismatchException} exception
   * @param request  user request
   * @return response to user request with {@link CustomErrorResponseDto} object
   */
  @ExceptionHandler({MethodArgumentTypeMismatchException.class})
  public ResponseEntity<Object> handleMethodArgumentTypeMismatch(MethodArgumentTypeMismatchException ex, WebRequest request) {
    String errorMessage = "Argument by měl být typu " + ex.getRequiredType().getName();
    return getCustomErrorResponseEntity(HttpStatus.BAD_REQUEST, errorMessage);
  }

  /**
   * Capture {@link ConstraintViolationException} and creates {@link CustomErrorResponseDto} response.
   *
   * @param ex  captured {@link ConstraintViolationException} exception
   * @param request  user request
   * @return response to user request with {@link CustomErrorResponseDto} object
   */
  @ExceptionHandler({ConstraintViolationException.class})
  public ResponseEntity<CustomErrorResponseDto> handleConstraintViolation(ConstraintViolationException ex, WebRequest request) {
    String newLine = "";
    StringBuilder errorMessage = new StringBuilder();

    // return all constraint violations in single response
    for (ConstraintViolation<?> violation : ex.getConstraintViolations()) {
      if (!violation.getMessage().equals("")) {
        errorMessage.append(newLine).append(violation.getMessage());
      } else {
        errorMessage.append(newLine)
          .append(violation.getRootBeanClass().getName())
          .append(" ")
          .append(violation.getPropertyPath())
          .append(": ")
          .append(violation.getMessage());
      }
      newLine = "\n";
    }

    CustomErrorResponseDto customErrorResponseDto = new CustomErrorResponseDto(HttpStatus.BAD_REQUEST, errorMessage.toString());
    return ResponseEntity.status(customErrorResponseDto.getStatusCode())
      .body(customErrorResponseDto);
  }

  /**
   * Capture {@link BadRequestException} and creates {@link CustomErrorResponseDto} response.
   *
   * @param ex  captured {@link BadRequestException} exception
   * @param request  user request
   * @return response to user request with {@link CustomErrorResponseDto} object
   */
  @ExceptionHandler(BadRequestException.class)
  public ResponseEntity<Object> handleBadRequest(Exception ex, WebRequest request) {
    return getCustomErrorResponseEntity(HttpStatus.BAD_REQUEST, ex.getMessage());
  }


  /* ****** 401 ****** */
  /**
   * Capture {@link UnauthorizedException} and creates {@link CustomErrorResponseDto} response.
   *
   * @param ex  captured {@link UnauthorizedException} exception
   * @param request  user request
   * @return response to user request with {@link CustomErrorResponseDto} object
   */
  @ExceptionHandler(UnauthorizedException.class)
  public ResponseEntity<Object> handleUnauthorized(Exception ex, WebRequest request) {
    SecurityContextHolder.clearContext();
    return getCustomErrorResponseEntity(HttpStatus.UNAUTHORIZED, ex.getMessage());
  }


  /* ****** 403 ****** */
  /**
   * Capture {@link AccessDeniedException} and creates {@link CustomErrorResponseDto} response.
   *
   * @param ex  captured {@link AccessDeniedException} exception
   * @param request  user request
   * @return response to user request with {@link CustomErrorResponseDto} object
   */
  @ExceptionHandler(AccessDeniedException.class)
  public ResponseEntity<Object> handleAccessDenied(Exception ex, WebRequest request) {
    return getCustomErrorResponseEntity(HttpStatus.FORBIDDEN, ex.getMessage());
  }

  /**
   * Capture {@link ForbiddenException} and creates {@link CustomErrorResponseDto} response.
   *
   * @param ex  captured {@link ForbiddenException} exception
   * @param request  user request
   * @return response to user request with {@link CustomErrorResponseDto} object
   */
  @ExceptionHandler(ForbiddenException.class)
  public ResponseEntity<Object> handleForbidden(Exception ex, WebRequest request) {
    return getCustomErrorResponseEntity(HttpStatus.FORBIDDEN, ex.getMessage());
  }


  /* ****** 404 ****** */
  /**
   * Capture {@link NotFoundException} and creates {@link CustomErrorResponseDto} response.
   *
   * @param ex  captured {@link NotFoundException} exception
   * @param request  user request
   * @return response to user request with {@link CustomErrorResponseDto} object
   */
  @ExceptionHandler(NotFoundException.class)
  public ResponseEntity<Object> handleNotFound(Exception ex, WebRequest request) {
    return getCustomErrorResponseEntity(HttpStatus.NOT_FOUND, ex.getMessage());
  }


  /* ****** 409 ****** */
  /**
   * Capture {@link ConflictException} and creates {@link CustomErrorResponseDto} response.
   *
   * @param ex  captured {@link ConflictException} exception
   * @param request  user request
   * @return response to user request with {@link CustomErrorResponseDto} object
   */
  @ExceptionHandler(ConflictException.class)
  public ResponseEntity<Object> handleConflict(Exception ex, WebRequest request) {
    return getCustomErrorResponseEntity(HttpStatus.CONFLICT, ex.getMessage());
  }


  /* ****** 500 ****** */
  /**
   * Capture {@link Exception} and creates {@link CustomErrorResponseDto} response.
   *
   * @param ex  captured {@link Exception} exception
   * @param request  user request
   * @return response to user request with {@link CustomErrorResponseDto} object
   */
  @ExceptionHandler(Exception.class)
  public ResponseEntity<Object> handleAll(Exception ex, WebRequest request) {
    return getCustomErrorResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
  }


  private ResponseEntity<Object> getErrorsCreateCustomResponse(Exception ex, BindingResult bindingResult, HttpStatus status) {
    StringBuilder errorMessage = new StringBuilder();
    String newLine = "";
    for (FieldError error : bindingResult.getFieldErrors()) {
      errorMessage.append(newLine)
        .append(error.getField())
        .append(": ")
        .append(error.getDefaultMessage());
      newLine = "\n";
    }
    for (ObjectError error : bindingResult.getGlobalErrors()) {
      errorMessage.append(newLine)
        .append(error.getObjectName())
        .append(": ")
        .append(error.getDefaultMessage());
      newLine = "\n";
    }

    return getCustomErrorResponseEntity(status, errorMessage.toString());
  }

  private ResponseEntity<Object> getCustomErrorResponseEntity(HttpStatus status, String errorMessage) {
    CustomErrorResponseDto customErrorResponseDto = new CustomErrorResponseDto(status, errorMessage);
    if (status == HttpStatus.UNAUTHORIZED) {
      return ResponseEntity.status(status)
        .header(HttpHeaders.SET_COOKIE, tokenService.deleteRefreshTokenCookie().toString())
        .body(customErrorResponseDto);
    } else {
      return ResponseEntity.status(customErrorResponseDto.getStatusCode())
        .body(customErrorResponseDto);
    }
  }
}
