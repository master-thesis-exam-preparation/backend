package cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions;

/**
 * Thrown when an user tries to access resources to which he does not have access.
 */
public class ConflictException extends BaseException {

  /**
   * Creates new instance of {@code ConflictException}.
   *
   * @param message  error message
   */
  public ConflictException(String message) {
    super(message);
  }
}
