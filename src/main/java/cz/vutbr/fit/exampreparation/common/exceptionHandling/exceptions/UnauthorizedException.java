package cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions;

/**
 * Thrown when unlogged user tries to access protected resources.
 */
public class UnauthorizedException extends BaseException {

  /**
   * Creates new instance of {@code UnauthorizedException}.
   *
   * @param message  error message
   */
  public UnauthorizedException(String message) {
    super(message);
  }
}
