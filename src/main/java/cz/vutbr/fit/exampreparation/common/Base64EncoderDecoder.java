package cz.vutbr.fit.exampreparation.common;

import java.util.Base64;

/**
 * This class is responsible for encoding and decoding open form text from/to Base64 encoding.
 */
public class Base64EncoderDecoder {

  /**
   * Transforms open form text to Base64 encoding.
   *
   * @param value  text in open form
   * @return encoded text
   */
  public static String encode(String value) {
    return Base64.getEncoder().encodeToString(value.getBytes());
  }

  /**
   * Transforms text in Base64 encoding to open form text.
   *
   * @param value  encoded text
   * @return text in open form
   */
  public static String decode(String value) {
    byte[] decodedBytes = Base64.getDecoder().decode(value);
    return new String(decodedBytes);
  }
}
