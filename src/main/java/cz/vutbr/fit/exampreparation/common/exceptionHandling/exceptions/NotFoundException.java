package cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions;

/**
 * Thrown when a resource which was requested by user does not exist.
 */
public class NotFoundException extends BaseException {

  /**
   * Creates new instance of {@code NotFoundException}.
   *
   * @param message  error message
   */
  public NotFoundException(String message) {
    super(message);
  }
}
