package cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions;

/**
 * This class is the superclass of those exceptions that can be thrown during user request processing.
 */
public class BaseException extends RuntimeException {

  /**
   * Creates new instance of {@code BaseException}.
   *
   * @param message  error message
   */
  public BaseException(String message) {
    super(message);
  }
}
