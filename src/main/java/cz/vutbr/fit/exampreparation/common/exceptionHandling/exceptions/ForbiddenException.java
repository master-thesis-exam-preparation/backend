package cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions;

/**
 * Thrown when the user create collision of data in database (same IDs of objects etc.).
 */
public class ForbiddenException extends BaseException {

  /**
   * Creates new instance of {@code ForbiddenException}.
   *
   * @param message  error message
   */
  public ForbiddenException(String message) {
    super(message);
  }
}
