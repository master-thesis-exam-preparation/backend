package cz.vutbr.fit.exampreparation.security.jwt.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Used to store information about currently logged-in user.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoggedUserInfoDto {

  private String nameWithDegrees;
  private String email;
  private List<String> roles;

}
