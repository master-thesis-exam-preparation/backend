package cz.vutbr.fit.exampreparation.security;

import cz.vutbr.fit.exampreparation.security.jwt.JwtTokenFilter;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * This class is responsible for setting the Spring Security configuration.
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@AllArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  private final UserDetailsService userDetailsService;
  private final PasswordEncoder passwordEncoder;
  private final JwtTokenFilter jwtTokenFilter;

  /**
   * Sets Spring Security configuration.
   *
   * @param httpSecurity {@link HttpSecurity} class to configure
   * @throws Exception if error occurs in {@link HttpSecurity}
   */
  @Override
  public void configure(HttpSecurity httpSecurity) throws Exception {
    httpSecurity.csrf()
      .disable();

    httpSecurity.cors();

    httpSecurity.sessionManagement()
      .sessionCreationPolicy(SessionCreationPolicy.STATELESS);  // never create any session

    httpSecurity.authorizeRequests()
      .antMatchers("/swagger-ui/**", "/v2/api-docs", "/configuration/ui", "/swagger-resources/**",
        "/configuration/security", "/webjars/**").permitAll()
      .antMatchers("/auth/**").permitAll()
      .antMatchers("/public/**").permitAll()
      .anyRequest().authenticated();

    httpSecurity
      .addFilterBefore(
        jwtTokenFilter,
        UsernamePasswordAuthenticationFilter.class
      );
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void configure(AuthenticationManagerBuilder auth) {
    auth.authenticationProvider(authenticationProvider());
  }

  /**
   * {@inheritDoc}
   */
  @Bean
  @Override
  public AuthenticationManager authenticationManagerBean() throws Exception {
    return super.authenticationManagerBean();
  }

  private DaoAuthenticationProvider authenticationProvider() {
    DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
    provider.setPasswordEncoder(passwordEncoder);
    provider.setUserDetailsService(userDetailsService);
    return provider;
  }
}
