package cz.vutbr.fit.exampreparation.security;

import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.NotFoundException;
import cz.vutbr.fit.exampreparation.features.appUser.AppUser;
import cz.vutbr.fit.exampreparation.features.appUser.AppUserService;
import cz.vutbr.fit.exampreparation.features.role.Role;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * This service class is responsible for obtaining information about users from database.
 */
@Service
@AllArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

  private final AppUserService appUserService;

  /**
   * Gets details about user from database
   *
   * @param email  users e-mail address
   * @return {@link UserDetails} object with information about user
   * @throws UsernameNotFoundException if user with specified e-mail address does not exist
   */
  @Override
  @Transactional
  public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
    AppUser appUser = appUserService
      .findByEmail(email)
      .orElseThrow(() -> new NotFoundException("Neznámý e-mail nebo heslo"));
    return org.springframework.security.core.userdetails.User.builder()
      .username(appUser.getEmail())
      .password(appUser.getPassword())
      .roles(getAccountRoles(appUser.getRoles()))
      .disabled(!appUser.isVerified())
      .accountExpired(false)
      .credentialsExpired(false)
      .accountLocked(appUser.isLocked())
      .build();
  }

  private String[] getAccountRoles(List<Role> roles) {
    List<String> roleNames = new ArrayList<>();
    for (Role role : roles) {
      roleNames.add(role.getRoleName().toString());
    }
    return roleNames.toArray(new String[0]);
  }
}
