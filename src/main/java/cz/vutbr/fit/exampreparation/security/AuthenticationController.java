package cz.vutbr.fit.exampreparation.security;

import cz.vutbr.fit.exampreparation.common.MessageResponseDto;
import cz.vutbr.fit.exampreparation.features.appUser.account.AccountService;
import cz.vutbr.fit.exampreparation.features.appUser.dto.EmailDto;
import cz.vutbr.fit.exampreparation.features.appUser.dto.LoginUserDto;
import cz.vutbr.fit.exampreparation.features.appUser.dto.NewPasswordDto;
import cz.vutbr.fit.exampreparation.features.appUser.dto.RegisterUserDto;
import cz.vutbr.fit.exampreparation.security.jwt.dto.AccessTokenDto;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * This class is responsible for managing authentication REST API endpoints.
 */
@Validated
@RestController
@RequestMapping("/auth")
@AllArgsConstructor
public class AuthenticationController {

  private final AccountService accountService;

  /**
   * Registers a new user into the application.
   *
   * @param userDto  information about new user (name, surname, password etc.)
   * @return response with success message
   */
  @PostMapping("/register")
  public ResponseEntity<MessageResponseDto> registerUser(@Valid @RequestBody RegisterUserDto userDto) {
    return accountService.createNewUserAccount(userDto);
  }

  /**
   * Logs an existing user into the application.
   *
   * @param userDto  user credentials
   * @return response with new access token
   */
  @PostMapping("/login")
  public ResponseEntity<AccessTokenDto> loginUser(@Valid @RequestBody LoginUserDto userDto) {
    return accountService.loginUser(userDto);
  }

  /**
   * Sends user an e-mail message with link to reset password.
   *
   * @param emailDto  e-mail address to which should be message with link send
   * @return response with success message
   */
  @PostMapping("/forget-password")
  public ResponseEntity<MessageResponseDto> forgetPassword(@Valid @RequestBody EmailDto emailDto) {
    return accountService.forgetPassword(emailDto);
  }

  /**
   * Sets a new password for the user.
   *
   * @param newPasswordDto  token from link and a new password
   * @return response with success message
   */
  @PostMapping("/reset-password")
  public ResponseEntity<MessageResponseDto> verifyForgetPasswordToken(@Valid @RequestBody NewPasswordDto newPasswordDto) {
    return accountService.resetPassword(newPasswordDto);
  }

  /**
   * Creates a new access token from refresh token cookie.
   *
   * @param refreshToken  refresh token cookie value
   * @return response with new access token and with new refresh token cookie
   */
  @GetMapping("/refresh-token")
  public ResponseEntity<AccessTokenDto> refreshToken(@CookieValue(name = "refreshToken", required = false) String refreshToken) {
    return accountService.generateNewKeys(refreshToken);
  }

  /**
   * Activates an user account if verification token is valid.
   *
   * @param token  verification token from e-mail message
   * @return response with success message
   */
  @GetMapping("/verify/{token}")
  public ResponseEntity<MessageResponseDto> verifyAccount(@PathVariable String token) {
    return accountService.verifyAccount(token);
  }

  /**
   * Logs out an user from the application.
   *
   * @param refreshToken  refresh token cookie value
   * @return response with success message
   */
  @GetMapping("/logout")
  public ResponseEntity<MessageResponseDto> logoutUser(@CookieValue(name = "refreshToken", required = false) String refreshToken) {
    return accountService.logoutUser(refreshToken);
  }

  /**
   * Logs an existing user into the application via refresh token cookie.
   *
   * @param refreshToken  refresh token cookie value
   * @return response with new access token
   */
  @GetMapping("/log-me-in")
  public ResponseEntity<AccessTokenDto> logMeIn(@CookieValue(name = "refreshToken", required = false) String refreshToken) {
    return accountService.logMeIn(refreshToken);
  }
}
