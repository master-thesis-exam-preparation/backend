package cz.vutbr.fit.exampreparation.security.jwt;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Used to store configuration data for JSON Web Token library.
 */
@Configuration
@ConfigurationProperties(prefix = "application.jwt")
@NoArgsConstructor
@Data
public class JwtConfig {

  private String secretKey;
  private Long accessTokenExpirationAfterMinutes;

}
