package cz.vutbr.fit.exampreparation.security.jwt.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * Used to store access token value.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccessTokenDto {

  private String accessToken;

}
