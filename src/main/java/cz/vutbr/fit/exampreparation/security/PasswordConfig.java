package cz.vutbr.fit.exampreparation.security;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * This class is responsible for setting the configuration of {@link PasswordEncoder} bean.
 */
@Configuration
@ConfigurationProperties(prefix = "application.jwt.argon2")
@NoArgsConstructor
@Data
public class PasswordConfig {

  private int saltLength;
  private int hashLength;
  private int parallelism;
  private int memory;
  private int iterations;

  /**
   * Creates {@link PasswordEncoder} bean with specified config.
   * This bean is responsible for hashing passwords and other text via Argon2 algorithm.
   *
   * @return configured {@link PasswordEncoder} bean
   */
  @Bean
  PasswordEncoder passwordEncoder() {
    return new Argon2PasswordEncoder(saltLength, hashLength, parallelism, memory, iterations);
  }
}
