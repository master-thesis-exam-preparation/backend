package cz.vutbr.fit.exampreparation.security.jwt;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.NotFoundException;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.UnauthorizedException;
import cz.vutbr.fit.exampreparation.features.appUser.AppUser;
import cz.vutbr.fit.exampreparation.features.appUser.AppUserService;
import cz.vutbr.fit.exampreparation.security.jwt.dto.LoggedUserInfoDto;
import io.jsonwebtoken.*;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.security.SignatureException;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * This class is responsible for parsing JSON Web Tokens from user request headers.
 */
@Component
@RequiredArgsConstructor
public class JwtTokenUtil {

  private final JwtConfig jwtConfig;
  private final AppUserService appUserService;

  private final Logger logger;

  /**
   * Create new JWT token.
   *
   * @param email  e-mail address of currently logged-in user
   * @return new access token
   */
  public String generateAccessToken(String email) {
    return Jwts.builder()
      .claim("loggedUserInfo", getLoggedUserInfo(email))
      .setIssuedAt(Date.from(Instant.now()))
      .setExpiration(Date.from(Instant.now().plusSeconds(jwtConfig.getAccessTokenExpirationAfterMinutes() * 60)))
      .signWith(Keys.hmacShaKeyFor(jwtConfig.getSecretKey().getBytes()))
      .compact();
  }

  /**
   * Checks if given JWT token is valid.
   *
   * @param token  token to validate
   * @return JWT token {@link Claims}
   */
  public Jws<Claims> validate(String token) {
    try {
      return Jwts.parserBuilder()
        .setSigningKey(Keys.hmacShaKeyFor(jwtConfig.getSecretKey().getBytes()))
        .build()
        .parseClaimsJws(token);
    } catch (SignatureException ex) {
      logger.error("Invalid JWT signature - {}", ex.getMessage());
      throw new UnauthorizedException("Nesprávný podpis access tokenu");
    } catch (MalformedJwtException ex) {
      logger.error("Invalid JWT token - {}", ex.getMessage());
      throw new UnauthorizedException("Nesprávný formát access tokenu");
    } catch (ExpiredJwtException ex) {
      logger.error("Expired JWT token - {}", ex.getMessage());
      throw new UnauthorizedException("Vypršela platnost access tokenu");
    } catch (UnsupportedJwtException ex) {
      logger.error("Unsupported JWT token - {}", ex.getMessage());
      throw new UnauthorizedException("Nepodporovaný access token");
    } catch (IllegalArgumentException ex) {
      logger.error("JWT claims string is empty - {}", ex.getMessage());
      throw new UnauthorizedException("Obsah access tokenu je prázdný");
    }
  }

  /**
   * Gets JWT token payload part.
   *
   * @param claims JWT token {@link Claims}
   * @return information about user stored in {@link LoggedUserInfoDto} object
   */
  public LoggedUserInfoDto getPayload(Jws<Claims> claims) {
    Claims body = claims.getBody();
    ObjectMapper mapper = new ObjectMapper();
    return mapper.convertValue(body.get("loggedUserInfo"), LoggedUserInfoDto.class);
  }

  /**
   * Gets users e-mail address from JWT token.
   *
   * @param claims JWT token {@link Claims}
   * @return users e-mail address
   */
  public String getLoggedUserEmail(Jws<Claims> claims) {
    return getPayload(claims).getEmail();
  }

  /**
   * Gets users roles from JWT token.
   *
   * @param claims JWT token {@link Claims}
   * @return users roles
   */
  public List<String> getLoggedUserRoles(Jws<Claims> claims) {
    return getPayload(claims).getRoles();
  }

  private LoggedUserInfoDto getLoggedUserInfo(String email) {
    AppUser appUser = appUserService.findByEmail(email)
      .orElseThrow(() -> new NotFoundException("Uživatel s e-mailem \"" + email + "\" nebyl nalezen"));

    LoggedUserInfoDto loggedUserInfoDto = new LoggedUserInfoDto();
    loggedUserInfoDto.setNameWithDegrees(appUser.getNameWithDegrees());

    loggedUserInfoDto.setEmail(appUser.getEmail());

    loggedUserInfoDto.setRoles(
      appUser.getRoles()
        .stream()
        .map(role -> role.getRoleName().toString())
        .collect(Collectors.toList())
    );

    return loggedUserInfoDto;
  }
}
