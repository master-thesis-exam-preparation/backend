package cz.vutbr.fit.exampreparation.security.jwt;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.CustomErrorResponseDto;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.BaseException;
import cz.vutbr.fit.exampreparation.features.token.TokenService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.util.Strings;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * This class is responsible for filtering of all input requests.
 */
@Service
@AllArgsConstructor
public class JwtTokenFilter extends OncePerRequestFilter {

  private final JwtTokenUtil jwtTokenUtil;
  private final TokenService tokenService;
  private final UserDetailsService userDetailsService;

  /**
   * Check if request should go through filter.
   *
   * @param request  user request
   * @return true if request should be excluded from filtering, false otherwise
   */
  @Override
  protected boolean shouldNotFilter(HttpServletRequest request) {
    List<String> excludedUrl = new ArrayList<>(
      Arrays.asList(
        "\\/swagger-ui.*",
        "\\/v2/api-docs",
        "\\/configuration\\/ui",
        "\\/swagger-resources.*",
        "\\/configuration/security",
        "\\/webjars.*",
        "\\/auth\\/.*",
        "\\/public\\/.*",
        "favicon.ico"
      )
    );

    return excludedUrl.stream()
      .anyMatch(regex -> request.getRequestURI().matches(regex));
  }

  /**
   * Tries to log user in via authentication token in request header.
   *
   * @param request  user request
   * @param response  response for user request
   * @param chain specific chain from {@link FilterChain}
   * @throws IOException if error occur during error response creation
   * @throws ServletException if some error occur in servlet
   */
  @Override
  protected void doFilterInternal(HttpServletRequest request,
                                  HttpServletResponse response,
                                  FilterChain chain) throws IOException, ServletException {
    final String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
    if (Strings.isEmpty(authHeader) || !authHeader.startsWith("Bearer ")) {
      returnInvalidTokenResponse(response);
      return;
    }


    String token = authHeader.replace("Bearer ", "");
    try {
      Jws<Claims> claims = jwtTokenUtil.validate(token);

      Set<SimpleGrantedAuthority> setOfAuthorities = jwtTokenUtil.getLoggedUserRoles(claims).stream()
        .map(role -> new SimpleGrantedAuthority("ROLE_" + role))
        .collect(Collectors.toSet());

      UserDetails appUser = userDetailsService.loadUserByUsername(jwtTokenUtil.getLoggedUserEmail(claims));
      if (!appUser.isEnabled()) {
        returnExceptionResponse(response,
          "Účet není aktivován. Aktivujte ho pomocí odkazu v obdržené e-mailové zprávě.");
        return;
      }

      if (!appUser.isAccountNonLocked()) {
        returnExceptionResponse(response, "Učet je zablokován. Kontaktujte administrátora systému.");
        return;
      }

      // log in user via authentication token
      UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
        appUser, null, setOfAuthorities
      );
      SecurityContextHolder.getContext().setAuthentication(authentication);
    } catch (BaseException e) {
      returnExceptionResponse(response, e.getMessage());
      return;
    }
    chain.doFilter(request, response);
  }

  private void returnExceptionResponse(HttpServletResponse response, String errorMessage)
    throws IOException {
    CustomErrorResponseDto customErrorResponseDto = new CustomErrorResponseDto(HttpStatus.UNAUTHORIZED, errorMessage);
    populateResponse(customErrorResponseDto, response);
  }

  private void returnInvalidTokenResponse(HttpServletResponse response) throws IllegalAccessError, IOException {
    CustomErrorResponseDto customErrorResponseDto = new CustomErrorResponseDto(
      HttpStatus.UNAUTHORIZED,
      "Neplatný nebo chybějící access token"
    );
    populateResponse(customErrorResponseDto, response);
  }

  private void populateResponse(CustomErrorResponseDto customErrorResponseDto, HttpServletResponse response) throws IOException {
    // delete refresh token cookie and write message to response
    response.setStatus(customErrorResponseDto.getStatusCode());
    response.addHeader(HttpHeaders.CONTENT_TYPE, "text/json; charset=UTF-16LE");
    response.addHeader(HttpHeaders.SET_COOKIE, tokenService.deleteRefreshTokenCookie().toString());
    response.getWriter().write(new ObjectMapper().writeValueAsString(customErrorResponseDto));
    response.getWriter().flush();
  }
}
