package cz.vutbr.fit.exampreparation.features.testSchedule;

import cz.vutbr.fit.exampreparation.features.assignment.Assignment;
import cz.vutbr.fit.exampreparation.features.cell.Cell;
import cz.vutbr.fit.exampreparation.features.termRun.TermRun;
import cz.vutbr.fit.exampreparation.features.termStudent.TermStudent;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * This entity class is responsible for object-relational mapping of an objects
 * with information about where who sits, what is their assignment etc. into a database table TestSchedule.
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
public class TestSchedule {

  @Id
  @Setter(AccessLevel.PRIVATE)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int idTestSchedule;

  @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
  @JoinColumn(name = "idStudent")
  private TermStudent student;

  @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
  @JoinColumn(name = "idTermRun")
  private TermRun termRun;

  @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
  @JoinColumn(name = "idCell")
  private Cell cell;

  @NotNull(message = "Chybějící pořadové číslo studenta")
  private Integer orderNumber;

  @NotNull(message = "Chybějící pořadové číslo sedadla")
  private Integer seatNumber;

  @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
  @JoinColumn(name = "idAssignment")
  private Assignment assignment;

  private String pdfPath;

  /**
   * Creates new instance of {@code TestSchedule} without assignment, pdfPath and seat number.
   * ID attribute will be set after storing the object into database.
   *
   * @param student  student which which attends the exam
   * @param termRun  term run in which will student be seated
   * @param cell  seat on which will student be seated
   * @param orderNumber  student order in alphabetical order
   */
  public TestSchedule(
    TermStudent student,
    TermRun termRun,
    Cell cell,
    Integer orderNumber
  ) {
    this.student = student;
    this.termRun = termRun;
    this.cell = cell;
    this.orderNumber = orderNumber;
  }

  /**
   * Creates new instance of {@code TestSchedule} without assignment, pdfPath.
   * ID attribute will be set after storing the object into database.
   *
   * @param student  student which which attends the exam
   * @param termRun  term run in which will student be seated
   * @param cell  seat on which will student be seated
   * @param orderNumber  student order in alphabetical order
   * @param seatNumber  order number of seat on which will student be seated
   * @param assignment  assignment which will be generated for student
   */
  public TestSchedule(
    TermStudent student,
    TermRun termRun,
    Cell cell,
    Integer orderNumber,
    Integer seatNumber,
    Assignment assignment
  ) {
    this.student = student;
    this.termRun = termRun;
    this.cell = cell;
    this.orderNumber = orderNumber;
    this.seatNumber = seatNumber;
    this.assignment = assignment;
  }

}
