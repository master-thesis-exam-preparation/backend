package cz.vutbr.fit.exampreparation.features.roomPlan.dto;

import cz.vutbr.fit.exampreparation.features.term.dto.SeatingPlanCell;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * Used to store information about room in edit term response.
 */
@Data
@AllArgsConstructor
public class EditTermRoomDto {

  private int roomId;
  private int capacity;
  private List<Integer> freeSeatIds;
  private List<Integer> freeSeatsAtTheEndOfRoom;

  @NotEmpty(message = "Chybí plán rozsazení")
  private List<List<SeatingPlanCell>> seatingPlan;
}
