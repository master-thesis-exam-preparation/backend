package cz.vutbr.fit.exampreparation.features.cell;

import cz.vutbr.fit.exampreparation.features.roomPlan.RoomPlan;
import cz.vutbr.fit.exampreparation.features.termRun.TermRun;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * This service class is responsible for management of cells of all room plans.
 */
@Service
@AllArgsConstructor
public class CellServiceImpl implements CellService {

  CellRepository cellRepository;

  /**
   * {@inheritDoc}
   */
  @Override
  public List<Integer> getRoomPlanSeatIds(RoomPlan roomPlan) {
    List<Cell> cells = cellRepository.findCellsByRoomPlanAndCellType(roomPlan, CellType.SEAT);
    return cells.stream()
      .map(Cell::getIdCell)
      .collect(Collectors.toList());
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<List<Cell>> getRoomPlanSeatsInSnakePattern(RoomPlan roomPlan) {
    List<List<Cell>> rows = new ArrayList<>(
      cellRepository.findCellsByRoomPlanAndCellType(roomPlan, CellType.SEAT)
        .stream()
        .sorted(
          Comparator.comparing(Cell::getSeatRowNumber)
          .thenComparing(Cell::getSeatColumnNumber)
        )
        .collect(
          Collectors.groupingBy(Cell::getSeatRowNumber)
        )
        .values()
    );

    for(int i = 0; i < rows.size(); i++) {
      if ((i % 2) == 1) {
        Collections.reverse(rows.get(i));
      }
    }

    return rows;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Optional<Cell> getCellById(Integer cellId) {
    return cellRepository.findById(cellId);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int findFirstRowNumberWithSeatInRoomPlan(RoomPlan roomPlan) {
    return cellRepository.findFirstRowNumberWithSeatInRoomPlan(roomPlan);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<Cell> findFreeSeatsInRoomPlan(TermRun termRun, RoomPlan roomPlan) {
    return cellRepository.findFreeSeatsInTermRunsRoomPlan(termRun, roomPlan);
  }
}
