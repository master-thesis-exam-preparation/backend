package cz.vutbr.fit.exampreparation.features.role;

/**
 * There can be 3 different user roles.
 *
 * <ul>
 *   <li>STUDENT</li>
 *   <li>EMPLOYEE</li>
 *   <li>ADMIN</li>
 * </ul>
 */
public enum RoleName {
  STUDENT,
  EMPLOYEE,
  ADMIN
}
