package cz.vutbr.fit.exampreparation.features.token;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

/**
 * This class is responsible for querying database table Token
 */
@Repository
public interface TokenRepository extends JpaRepository<Token, Integer> {
  void deleteByIdToken(int id);

  Optional<Token> findByIdTokenAndTokenType(int id, TokenType tokenType);

  List<Token> findAllByExpireAtLessThan(Instant now);

  void deleteAllByAppUserReference_EmailAndTokenType(String email, TokenType tokenType);
}
