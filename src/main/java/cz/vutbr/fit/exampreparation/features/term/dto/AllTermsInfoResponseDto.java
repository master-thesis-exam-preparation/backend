package cz.vutbr.fit.exampreparation.features.term.dto;

import cz.vutbr.fit.exampreparation.features.termRun.dto.TermRunBasicRoomDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Used to store information about term run which will be returned as response to get information about all terms request.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AllTermsInfoResponseDto extends BasicTermDto {

  @NotNull(message = "Chybějící identifikátor termínu zkoušky")
  private Integer termId;

  @NotEmpty(message = "Chybějící e-maily správců zkoušky")
  private List<String> managersEmails;

  @NotEmpty(message = "Chybějící běhy zkoušky")
  private List<TermRunBasicRoomDto> termRuns;

  /**
   Creates new instance of {@code AllTermsInfoResponseDto}.
   *
   * @param courseAbbreviation  abbreviation of the course from which the term exam takes place
   * @param termName  name of the term
   * @param academicYear  academic year in which term exam takes place
   * @param termDates  list of the start times of the term runs of the term
   * @param termId  unique ID of the term
   * @param managersEmails  list of e-mail addresses of application users that can manage this term
   * @param termRuns  list of term runs of this term
   */
  public AllTermsInfoResponseDto(String courseAbbreviation, String termName, Integer academicYear,
                                 List<Long> termDates, Integer termId, List<String> managersEmails,
                                 List<TermRunBasicRoomDto> termRuns) {
    super(courseAbbreviation, termName, academicYear, termDates);
    this.termId = termId;
    this.managersEmails = managersEmails;
    this.termRuns = termRuns;
  }
}
