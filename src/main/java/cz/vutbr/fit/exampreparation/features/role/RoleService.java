package cz.vutbr.fit.exampreparation.features.role;

import java.util.Optional;

public interface RoleService {

  /**
   * Tries to find the role by provided role name in database.
   *
   * @param roleName  name of the role
   * @return the found {@link Role}, if not found it will return null
   */
  Optional<Role> findRole(RoleName roleName);
}
