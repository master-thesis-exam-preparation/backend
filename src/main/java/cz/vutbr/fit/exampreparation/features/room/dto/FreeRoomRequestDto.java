package cz.vutbr.fit.exampreparation.features.room.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Used to store data to find free rooms at given time.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FreeRoomRequestDto {

  @NotNull(message = "Chybějící čas začátku běhu")
  private Long startTime;

  @NotNull(message = "Chybějící počet navazujících běhů")
  @Min(value = 1, message = "Běh musí být alespoň jeden")
  private Integer numberOfTermRuns;

  @NotNull(message = "Chybějící délka běhu")
  @Min(value = 1, message = "Běh musí trvat alespoň jednu minutu")
  private Integer termRunLength;

  private Integer termId;
}
