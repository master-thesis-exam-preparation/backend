package cz.vutbr.fit.exampreparation.features.term.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import cz.vutbr.fit.exampreparation.features.testSchedule.dto.TestScheduleDetailDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * Used to store information about seating plan of the specific room plan.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SeatingPlanDto {

  boolean highlight;

  @NotBlank(message = "Chybí zkratka místnosti")
  private String roomAbbreviation;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private List<TestScheduleDetailDto> testSchedules;

  @NotEmpty(message = "Chybí plán rozsazení")
  private List<List<SeatingPlanCell>> seatingPlan;
}
