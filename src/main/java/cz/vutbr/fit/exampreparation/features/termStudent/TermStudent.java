package cz.vutbr.fit.exampreparation.features.termStudent;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * This entity class is responsible for object-relational mapping of an objects
 * with information about students who will participate in the exam into a database table TermStudent.
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
public class TermStudent {

  @Id
  @Setter(AccessLevel.PRIVATE)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int idStudent;

  @NotBlank(message = "Chybí login studenta")
  @Size(max = 255, message = "Příliš dlouhý vstup")
  @Column(unique = true)
  private String login;

  @NotBlank(message = "Chybí příjmení a jméno studenta")
  @Size(max = 255, message = "Příliš dlouhý vstup")
  private String nameWithDegrees;

  public TermStudent(String login, String nameWithDegrees) {
    this.login = login;
    this.nameWithDegrees = nameWithDegrees;
  }
}
