package cz.vutbr.fit.exampreparation.features.room;

import cz.vutbr.fit.exampreparation.common.MessageResponseDto;
import cz.vutbr.fit.exampreparation.features.room.dto.*;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * This class is responsible for managing room REST API endpoints.
 */
@Validated
@RestController
@AllArgsConstructor
public class RoomController {

  private final RoomService roomService;

  /**
   * Gets information about all rooms.
   *
   * @return response with information about all rooms
   */
  @GetMapping("/public/rooms")
  public ResponseEntity<List<RoomInfoDto>> getAllRoomsInfo() {
    return roomService.getAllRoomsInfo();
  }

  /**
   * Creates new room based on given data.
   *
   * @param newRoomDto  information necessary to create new room
   * @return response with information about newly created room
   */
  @PreAuthorize("hasAnyRole('EMPLOYEE','ADMIN')")
  @PostMapping("/internal/rooms")
  public ResponseEntity<RoomInfoDto> createNewRoom(@Valid @RequestBody NewRoomDto newRoomDto) {
    return roomService.createNewRoom(newRoomDto);
  }

  /**
   * Gets information about room that can be viewed by unlogged users.
   *
   * @param roomId  unique ID of the room
   * @return response with public information about room
   */
  @GetMapping("/public/rooms/{roomId:[\\d]+}")
  public ResponseEntity<RoomDetailDto> getRoomDetailsPublic(@PathVariable int roomId) {
    return roomService.getRoomDetails(roomId, true);
  }

  /**
   * Gets information about room that cannot be viewed by unlogged users.
   *
   * @param roomId  unique ID of the room
   * @return response with private information about room
   */
  @PreAuthorize("hasAnyRole('EMPLOYEE','ADMIN','STUDENT')")
  @GetMapping("/internal/rooms/{roomId:[\\d]+}")
  public ResponseEntity<RoomDetailDto> getRoomDetailsInternal(@PathVariable int roomId) {
    return roomService.getRoomDetails(roomId, false);
  }

  /**
   * Gets information about room that will be used to fill edit form.
   *
   * @param roomId  unique ID of the room
   * @return response with edit information about room
   */
  @PreAuthorize("hasAnyRole('EMPLOYEE','ADMIN')")
  @GetMapping("/internal/rooms/{roomId:[\\d]+}/edit")
  public ResponseEntity<EditRoomDto> getEditRoomDetails(@PathVariable int roomId) {
    return roomService.getEditRoomDetails(roomId, true);
  }

  /**
   * Gets information about room that will be used to fill form with data of requested room.
   *
   * @param roomId  unique ID of the room
   * @return response with information about room
   */
  @PreAuthorize("hasAnyRole('EMPLOYEE','ADMIN')")
  @GetMapping("/internal/rooms/{roomId:[\\d]+}/form-info")
  public ResponseEntity<EditRoomDto> getEditRoomDetailsForCreation(@PathVariable int roomId) {
    return roomService.getEditRoomDetails(roomId, false);
  }

  /**
   * Edits existing room data based on new given data.
   *
   * @param roomId  unique ID of the room
   * @param newRoomDto  information about changes
   * @return response with success message
   */
  @PreAuthorize("hasAnyRole('EMPLOYEE','ADMIN')")
  @PutMapping("/internal/rooms/{roomId:[\\d]+}")
  public ResponseEntity<MessageResponseDto> editRoom(@PathVariable int roomId, @Valid @RequestBody NewRoomDto newRoomDto) {
    return roomService.editRoom(roomId, newRoomDto);
  }

  /**
   * Deletes specified room, its room plan and cells from database.
   *
   * @param roomId  unique ID of the room that should be deleted
   * @return response with success message
   */
  @PreAuthorize("hasAnyRole('EMPLOYEE','ADMIN')")
  @DeleteMapping("/internal/rooms/{roomId:[\\d]+}")
  public ResponseEntity<MessageResponseDto> deleteRoom(@PathVariable int roomId) {
    return roomService.deleteRoom(roomId);
  }

  /**
   * Gets list of room that does not have any exam in it at given time.
   *
   * @param freeRoomRequestDto  information about start time, length etc.
   * @return response with list of free rooms
   */
  @PreAuthorize("hasAnyRole('EMPLOYEE','ADMIN')")
  @PostMapping("/internal/rooms/free")
  public ResponseEntity<List<FreeRoomResponseDto>> getFreeRooms(@Valid @RequestBody FreeRoomRequestDto freeRoomRequestDto) {
    return roomService.getFreeRooms(freeRoomRequestDto);
  }
}
