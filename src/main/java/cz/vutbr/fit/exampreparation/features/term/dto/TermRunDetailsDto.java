package cz.vutbr.fit.exampreparation.features.term.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import cz.vutbr.fit.exampreparation.features.assignment.dto.BasicAssignmentInfoDto;
import cz.vutbr.fit.exampreparation.features.termRun.dto.BasicTermRun;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * Used to store more specific information about term run.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TermRunDetailsDto extends BasicTermRun {

  boolean highlight;

  @NotEmpty(message = "Chybí seznam zasedacích plánů běhu")
  private List<SeatingPlanDto> seatingPlans;

  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  private List<BasicAssignmentInfoDto> assignments;

  /**
   * Creates new instance of {@code TermRunDetailsDto}.
   *
   * @param id  unique ID of the term run
   * @param startTime  term run start time and date
   * @param endTime term run end time and date
   * @param seatingPlans  list of seating plans of the rooms of this term run
   * @param assignments  list of assignments that was assigned to this term run
   */
  public TermRunDetailsDto(Integer id, Long startTime, Long endTime, List<SeatingPlanDto> seatingPlans,
                           List<BasicAssignmentInfoDto> assignments) {
    super(id, startTime, endTime);
    this.seatingPlans = seatingPlans;
    this.assignments = assignments;
  }
}
