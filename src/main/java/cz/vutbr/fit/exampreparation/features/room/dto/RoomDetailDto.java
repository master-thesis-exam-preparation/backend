package cz.vutbr.fit.exampreparation.features.room.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import cz.vutbr.fit.exampreparation.features.appUser.dto.EmployeeDetailsDto;
import cz.vutbr.fit.exampreparation.features.roomPlan.dto.RoomPlanWithCellsDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * Used to store detailed information about room (basic info + room creator, room description etc.).
 */
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class RoomDetailDto extends BasicRoomDto {

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private EmployeeDetailsDto creator;

  private String description;

  @NotEmpty(message = "Místnost nemá žádný zasedací plán")
  private List<RoomPlanWithCellsDto> roomPlans;   // list of 2D arrays od cells

  /**
   * Creates new instance of {@code RoomDetailDto}.
   *
   * @param abbreviation  room abbreviation
   * @param name room name
   * @param creator  reference to user which created this room
   * @param description  room description text
   * @param roomPlans  all 3 room plans of the given room (number of free cells between students - 0, 1, 2)
   */
  public RoomDetailDto(String abbreviation, String name, EmployeeDetailsDto creator,
                       String description, List<RoomPlanWithCellsDto> roomPlans) {
    super(abbreviation, name);
    this.creator = creator;
    this.description = description;
    this.roomPlans = roomPlans;
  }
}
