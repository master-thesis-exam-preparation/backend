package cz.vutbr.fit.exampreparation.features.appUser.dto;

import cz.vutbr.fit.exampreparation.security.jwt.dto.LoggedUserInfoDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * Used to store information about logged user and his account.
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class AccountInfoDto extends LoggedUserInfoDto {
  private int userId;
  private boolean locked;

  /**
   * Creates new instance of {@link AccountInfoDto}.
   *
   * @param userId  unique ID of the logged-in user
   * @param nameWithDegrees  combination of degrees, name and surname of logged-in user
   * @param email  e-mail address of the logged-in user
   * @param roles  roles of the logged-in user
   * @param locked  information about that users account is blocked or not
   */
  public AccountInfoDto(int userId, String nameWithDegrees, String email, List<String> roles, boolean locked) {
    super(nameWithDegrees, email, roles);
    this.userId = userId;
    this.locked = locked;
  }
}
