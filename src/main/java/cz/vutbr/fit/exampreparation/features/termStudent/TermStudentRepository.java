package cz.vutbr.fit.exampreparation.features.termStudent;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * This class is responsible for querying database table TermStudent
 */
@Repository
public interface TermStudentRepository extends JpaRepository<TermStudent, Integer> {

  TermStudent findByLogin(String login);

}
