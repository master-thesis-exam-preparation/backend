package cz.vutbr.fit.exampreparation.features.termRun.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * Used to store basic information about term run (unique ID, start time and end time of the term run).
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BasicTermRun {

  @NotNull(message = "Chybí pomocný identifikátor běhu termínu zkoušky")
  private Integer id;

  @NotNull(message = "Chybí čas začátku termínu zkoušky")
  private Long startTime;

  @NotNull(message = "Chybí čas konce běhu termínu zkoušky")
  private Long endTime;

}
