package cz.vutbr.fit.exampreparation.features.termRun.dto;

import cz.vutbr.fit.exampreparation.features.roomPlan.dto.EditTermRoomDto;
import cz.vutbr.fit.exampreparation.features.term.dto.BasicTermRunDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * Used to store information to fill edit term run form.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class TermRunEditInfoDto extends BasicTermRunDto {

  @NotEmpty(message = "Chybí seznam místností a volných míst u běhu termínu zkoušky")
  private List<EditTermRoomDto> rooms;

  /**
   * Creates new instance of {@link TermRunEditInfoDto}
   *
   * @param id  unique ID of the term run
   * @param startTime  start date time of the term run
   * @param endTime  end date time of the term run
   * @param rooms  list of selected rooms
   * @param assignments  list of selected assignments
   */
  public TermRunEditInfoDto(Integer id, Long startTime,  Long endTime, List<EditTermRoomDto> rooms, List<Integer> assignments) {
    super(id, startTime, endTime, assignments);
    this.rooms = rooms;
  }

}
