package cz.vutbr.fit.exampreparation.features.term;

import cz.vutbr.fit.exampreparation.features.appUser.AppUser;
import cz.vutbr.fit.exampreparation.features.course.Course;
import cz.vutbr.fit.exampreparation.features.roomPlan.SpacingBetweenStudents;
import cz.vutbr.fit.exampreparation.features.termRun.Distribution;
import cz.vutbr.fit.exampreparation.features.termRun.TermRun;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This entity class is responsible for object-relational mapping of an objects
 * with information about terms into a database table Term.
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
public class Term {

  @Id
  @Setter(AccessLevel.PRIVATE)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int idTerm;

  @NotBlank(message = "Chybějící název zkoušky")
  @Size(max = 255, message = "Příliš dlouhý vstup")
  private String termName;

  @NotNull(message = "Chybějící údaj o počtu volných míst mezi studenty")
  @Enumerated(EnumType.STRING)
  private SpacingBetweenStudents spacingBetweenStudents;

  @NotNull(message = "Chybějící údaj o způsobu rozřazení studentů")
  @Enumerated(EnumType.STRING)
  private Distribution distribution;

  @Size(max = 65535, message = "Příliš dlouhý vstup")
  private String description;

  @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
  @JoinTable(
    name = "app_user_term_link",
    joinColumns = {@JoinColumn(name = "id_term")},
    inverseJoinColumns = {@JoinColumn(name = "id_app_user")}
  )
  private List<AppUser> termManagers = new ArrayList<>();

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "idCourse", referencedColumnName = "idCourse")
  @OnDelete(action = OnDeleteAction.CASCADE)
  private Course courseReference;

  @OneToMany(mappedBy = "termReference", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @OnDelete(action = OnDeleteAction.CASCADE)
  private List<TermRun> termRuns;

  /**
   * Creates new instance of {@code Term}.
   *
   * @param termName  name of the term
   * @param spacingBetweenStudents  selected spacing between students
   * @param distribution  selected students distribution in the rooms
   * @param description  text with term description
   * @param termManagers  list of application users that can manage this term
   * @param courseReference  course from which the term exam takes place
   * @param termRuns  list of terms term runs
   */
  public Term(
    String termName,
    SpacingBetweenStudents spacingBetweenStudents,
    Distribution distribution,
    String description,
    List<AppUser> termManagers,
    Course courseReference,
    List<TermRun> termRuns
  ) {
    this.termName = termName;
    this.spacingBetweenStudents = spacingBetweenStudents;
    this.distribution = distribution;
    this.description = description;
    this.termManagers = termManagers;
    this.courseReference = courseReference;
    this.termRuns = termRuns;
  }

  /**
   * Gets all terms term runs
   *
   * @return list of terms term runs
   */
  public List<TermRun> getTermRuns() {
    return termRuns.stream()
      .sorted(
        Comparator.comparing(TermRun::getStartDateTime)
        .thenComparing(TermRun::getIdTermRun)
      )
      .collect(Collectors.toList());
  }

  /**
   * Adds the provided term run to terms list of term runs.
   *
   * @param termRun  term run to add
   */
  public void addTermRun(TermRun termRun) {
    this.termRuns.add(termRun);
  }

  /**
   * Removes the provided term run from terms list of term runs.
   *
   * @param termRun  term run to remove
   */
  public void removeTermRun(TermRun termRun) {
    this.termRuns.remove(termRun);
  }

  /**
   * Removes all term runs from terms list of term runs.
   */
  public void removeAllTermRuns() {
    this.termRuns.clear();
  }
}
