package cz.vutbr.fit.exampreparation.features.term.dto.term;

import cz.vutbr.fit.exampreparation.features.roomPlan.SpacingBetweenStudents;
import cz.vutbr.fit.exampreparation.features.termRun.Distribution;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Used to store basic information about term (name, selected spacing between students, distribution of students etc.).
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BasicTermDto {

  @NotBlank(message = "Chybí název termínu zkoušky")
  @Size(max = 255, message = "Příliš dlouhý vstup")
  private String termName;

  @NotNull(message = "Chybí předmět, ze kterého se zkouška koná")
  private Integer courseId;

  @NotNull(message = "Chybí volba počtu volných míst mezi studenty")
  private SpacingBetweenStudents spacingBetweenStudents;

  @NotNull(message = "Chybí volba způsobu rozdělení studentů do místností")
  private Distribution distribution;

  @Size(max = 65535, message = "Příliš dlouhý vstup")
  private String informationText;
}
