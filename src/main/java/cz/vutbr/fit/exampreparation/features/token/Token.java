package cz.vutbr.fit.exampreparation.features.token;

import cz.vutbr.fit.exampreparation.features.appUser.AppUser;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.time.Instant;

/**
 * This entity class is responsible for object-relational mapping of an objects
 * with information about application users tokens into a database table Token.
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
public class Token {

  @Id
  @Setter(AccessLevel.PRIVATE)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int idToken;

  @NotBlank(message = "Chybějící token")
  private String token;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "idAppUser", referencedColumnName = "idAppUser")
  private AppUser appUserReference;

  private Instant expireAt;

  @Enumerated(EnumType.STRING)
  private TokenType tokenType;

  private boolean remember;

}
