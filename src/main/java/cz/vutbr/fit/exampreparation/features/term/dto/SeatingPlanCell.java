package cz.vutbr.fit.exampreparation.features.term.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import cz.vutbr.fit.exampreparation.features.appUser.dto.LoginNameDto;
import cz.vutbr.fit.exampreparation.features.cell.CellType;
import cz.vutbr.fit.exampreparation.features.cell.dto.CellDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Used to store information about cell on which is some student seated.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SeatingPlanCell extends CellDto {

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private Integer seatNumber;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private LoginNameDto student;

  /**
   * Creates new instance of {@code SeatingPlanCell}.
   *
   * @param cellId  unique ID of the cell
   * @param cellType  selected type of the cell
   * @param seatNumber  seats order number in term, if cell is not a seat set this to null
   * @param student  student which is seated on this cell
   */
  public SeatingPlanCell(Integer cellId, CellType cellType,
                         Integer seatNumber, LoginNameDto student) {
    super(cellId, cellType);
    this.seatNumber = seatNumber;
    this.student = student;
  }
}
