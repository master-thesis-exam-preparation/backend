package cz.vutbr.fit.exampreparation.features.term;

import cz.vutbr.fit.exampreparation.common.AuthService;
import cz.vutbr.fit.exampreparation.common.MessageResponseDto;
import cz.vutbr.fit.exampreparation.common.UserInfo;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.BadRequestException;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.ForbiddenException;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.NotFoundException;
import cz.vutbr.fit.exampreparation.features.appUser.AppUser;
import cz.vutbr.fit.exampreparation.features.appUser.AppUserService;
import cz.vutbr.fit.exampreparation.features.appUser.dto.EmployeeDetailsDto;
import cz.vutbr.fit.exampreparation.features.appUser.dto.LoginNameDto;
import cz.vutbr.fit.exampreparation.features.assignment.Assignment;
import cz.vutbr.fit.exampreparation.features.assignment.AssignmentService;
import cz.vutbr.fit.exampreparation.features.assignment.dto.BasicAssignmentInfoDto;
import cz.vutbr.fit.exampreparation.features.assignment.dto.GeneratingFailDto;
import cz.vutbr.fit.exampreparation.features.assignment.helpers.RunningGeneratingService;
import cz.vutbr.fit.exampreparation.features.cell.Cell;
import cz.vutbr.fit.exampreparation.features.cell.CellService;
import cz.vutbr.fit.exampreparation.features.cell.CellType;
import cz.vutbr.fit.exampreparation.features.course.Course;
import cz.vutbr.fit.exampreparation.features.course.CourseService;
import cz.vutbr.fit.exampreparation.features.role.Role;
import cz.vutbr.fit.exampreparation.features.role.RoleName;
import cz.vutbr.fit.exampreparation.features.room.Room;
import cz.vutbr.fit.exampreparation.features.room.RoomService;
import cz.vutbr.fit.exampreparation.features.room.dto.BasicRoomDto;
import cz.vutbr.fit.exampreparation.features.roomPlan.RoomPlan;
import cz.vutbr.fit.exampreparation.features.roomPlan.RoomPlanService;
import cz.vutbr.fit.exampreparation.features.roomPlan.SpacingBetweenStudents;
import cz.vutbr.fit.exampreparation.features.roomPlan.dto.EditTermRoomDto;
import cz.vutbr.fit.exampreparation.features.term.dto.*;
import cz.vutbr.fit.exampreparation.features.term.dto.term.EditTermDto;
import cz.vutbr.fit.exampreparation.features.term.dto.term.EditTermInfoDto;
import cz.vutbr.fit.exampreparation.features.term.dto.term.NewTermDto;
import cz.vutbr.fit.exampreparation.features.term.helpers.EditAction;
import cz.vutbr.fit.exampreparation.features.term.helpers.EditActionWithIdsList;
import cz.vutbr.fit.exampreparation.features.term.helpers.FreeSeatsAtTheEndOfRoomPlan;
import cz.vutbr.fit.exampreparation.features.termRun.Distribution;
import cz.vutbr.fit.exampreparation.features.termRun.TermRun;
import cz.vutbr.fit.exampreparation.features.termRun.dto.TermRunBasicRoomDto;
import cz.vutbr.fit.exampreparation.features.termRun.dto.TermRunDto;
import cz.vutbr.fit.exampreparation.features.termRun.dto.TermRunEditInfoDto;
import cz.vutbr.fit.exampreparation.features.termStudent.TermStudent;
import cz.vutbr.fit.exampreparation.features.termStudent.TermStudentService;
import cz.vutbr.fit.exampreparation.features.termStudent.dto.TermStudentOrderDto;
import cz.vutbr.fit.exampreparation.features.testSchedule.TestSchedule;
import cz.vutbr.fit.exampreparation.features.testSchedule.TestScheduleService;
import cz.vutbr.fit.exampreparation.features.testSchedule.dto.TestScheduleDetailDto;
import cz.vutbr.fit.exampreparation.mail.MailBuilder;
import cz.vutbr.fit.exampreparation.mail.MailComponents;
import cz.vutbr.fit.exampreparation.mail.MailNotificationSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * This service class is responsible for management of all terms.
 */
@Service
@PropertySource(value = "application.properties")
public class TermServiceImpl implements TermService {

  private final TermRepository termRepository;
  private final RoomService roomService;
  private final CellService cellService;
  private final AppUserService appUserService;
  private final CourseService courseService;
  private final RoomPlanService roomPlanService;
  private final AssignmentService assignmentService;
  private final TermStudentService termStudentService;
  private final TestScheduleService testScheduleService;
  private final RunningGeneratingService runningGenerationService;

  private final MailNotificationSender mailNotificationSender;
  private final MailBuilder mailBuilder;

  @Value("${frontend.url}")
  private String frontendUrl;

  @Value("${send_email}")
  private boolean sendEmail;

  @Autowired
  public TermServiceImpl(TermRepository termRepository, RoomService roomService, CellService cellService,
                         AppUserService appUserService, CourseService courseService, RoomPlanService roomPlanService,
                         AssignmentService assignmentService, TermStudentService termStudentService,
                         TestScheduleService testScheduleService, RunningGeneratingService runningGenerationService,
                         MailNotificationSender mailNotificationSender, MailBuilder mailBuilder) {
    this.termRepository = termRepository;
    this.roomService = roomService;
    this.cellService = cellService;
    this.appUserService = appUserService;
    this.courseService = courseService;
    this.roomPlanService = roomPlanService;
    this.assignmentService = assignmentService;
    this.termStudentService = termStudentService;
    this.testScheduleService = testScheduleService;
    this.runningGenerationService = runningGenerationService;
    this.mailNotificationSender = mailNotificationSender;
    this.mailBuilder = mailBuilder;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseEntity<List<AllTermsInfoResponseDto>> getAllTermsInfo(boolean isPublic) {
    List<Term> rawTerms = getExistingTerms(isPublic);

    List<AllTermsInfoResponseDto> termsInformation = getInformationAboutTerms(isPublic, rawTerms);

    return ResponseEntity.ok()
      .body(termsInformation);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  @Transactional
  public ResponseEntity<MessageResponseDto> createNewTerm(NewTermDto newTermDto) {
    // input DTO should be valid after this call
    doNewTermValidations(newTermDto);

    Course course = courseService.findCourseById(newTermDto.getCourseId())
      .orElseThrow(() -> new BadRequestException("Předmět neexistuje"));

    Term newTerm = new Term(
      newTermDto.getTermName(),
      newTermDto.getSpacingBetweenStudents(),
      newTermDto.getDistribution(),
      newTermDto.getInformationText(),
      this.appUserService.createManagerList(newTermDto.getAdditionalTermManagersEmails()),
      course,
      null
    );

    List<TermRun> newTermRuns = createNewTermRuns(newTermDto, newTerm);
    newTerm.setTermRuns(newTermRuns);

    Term savedTerm = termRepository.save(newTerm);

    // send an e-mail about the creation of the term to all students who will participate in this term
    if (sendEmail && newTermDto.isSendEmail()) {
      for (TermRun termRun : savedTerm.getTermRuns()) {
        for (TestSchedule testSchedule : termRun.getTestSchedules()) {
          sendNewTermEmails(newTermDto.getEmailMessage(), savedTerm, testSchedule);
        }
      }
    }

    return ResponseEntity.ok()
      .body(new MessageResponseDto("Termín zkoušky byl úspěšně vytvořen"));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseEntity<TermDetailDto> getTermDetails(int termId, boolean isPublic) {
    Term term = termRepository.findById(termId)
      .orElseThrow(() -> new NotFoundException("Termín zkoušky s ID " + termId + " nebyl nalezen"));

    List<EmployeeDetailsDto> managers = new ArrayList<>();
    Integer remainingGenerating = null;
    List<GeneratingFailDto> generatingFails = new ArrayList<>();
    if (!isPublic) {
      managers = term.getTermManagers()
        .stream()
        .map(appUser -> new EmployeeDetailsDto(appUser.getEmail(), appUser.getNameWithDegrees()))
        .collect(Collectors.toList());
      if (runningGenerationService.doesTermHaveAnyRunningTask(termId)) {
        remainingGenerating = runningGenerationService.getTermRunningTasks(termId).size();
      }
      generatingFails = runningGenerationService.getAllTermFailedGenerations(termId);
    }

    UserInfo userInfo = AuthService.getLoggedUserInfo();
    boolean isUserManager = managers.stream()
      .anyMatch(manager -> manager.getEmail().equals(userInfo.getEmail()));
    checkTermDetailAccessRights(term, userInfo, isUserManager);
    List<TermRunDetailsDto> termRuns = createTermRunsForResponse(term, isPublic, isUserManager);

    Set<Long> termDates = getTermsTermRunsStartingDates(term);

    TermDetailDto termDetail = new TermDetailDto(
      term.getCourseReference().getAbbreviation(),
      term.getTermName(),
      term.getCourseReference().getAcademicYear(),
      new ArrayList<>(termDates),
      term.getDescription(),
      managers,
      remainingGenerating,
      generatingFails,
      termRuns
    );
    return ResponseEntity.ok()
      .body(termDetail);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseEntity<MessageResponseDto> deleteTerm(int termId) {
    UserInfo userInfo = AuthService.getLoggedUserInfo();
    Term term = termRepository.findById(termId)
      .orElseThrow(() -> new NotFoundException("Termín zkoušky s ID " + termId + " nebyl nalezen"));

    boolean isUserManager = term.getTermManagers().stream()
      .anyMatch(manager -> manager.getEmail().equals(userInfo.getEmail()));

    if (!isUserManager && !userInfo.getRoles().contains("ROLE_ADMIN")) {
      throw new ForbiddenException("Termín zkoušky může smazat pouze její správce");
    }

    try {
      termRepository.deleteById(termId);
    } catch (Exception e) {
      throw new BadRequestException("Termín zkoušky se nepodařilo smazat. Ujistěte se, že existuje");
    }
    return ResponseEntity.ok()
      .body(new MessageResponseDto("Termín zkoušky úspěšně odstraněn"));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseEntity<EditTermInfoDto> getEditTermDetails(int termId) {
    UserInfo userInfo = AuthService.getLoggedUserInfo();
    Term term = termRepository.findById(termId)
      .orElseThrow(() -> new NotFoundException("Termín zkoušky s ID " + termId + " nebyl nalezen"));

    boolean isUserManager = term.getTermManagers().stream()
      .anyMatch(manager -> manager.getEmail().equals(userInfo.getEmail()));

    if (!isUserManager && !userInfo.getRoles().contains("ROLE_ADMIN")) {
      throw new ForbiddenException("Detaily termínu zkoušky může zobrazit pouze její správce");
    }

    List<EmployeeDetailsDto> managers = term.getTermManagers()
      .stream()
      .filter(appUser -> !appUser.getEmail().equals(userInfo.getEmail()))
      .map(appUser -> new EmployeeDetailsDto(appUser.getEmail(), appUser.getNameWithDegrees()))
      .collect(Collectors.toList());

    List<LoginNameDto> studentList = testScheduleService.findAllTestSchedulesInTerm(termId)
      .stream()
      .map(testSchedule -> {
        TermStudent termStudent = testSchedule.getStudent();
        return new LoginNameDto(termStudent.getLogin(), termStudent.getNameWithDegrees());
      })
      .sorted(Comparator.comparing(LoginNameDto::getLogin))
      .collect(Collectors.toList());

    List<TermRunEditInfoDto> termRuns = getEditInformationAboutTermRuns(userInfo, term);

    EditTermInfoDto editTermInfoDto = new EditTermInfoDto(
      term.getTermName(),
      term.getCourseReference().getIdCourse(),
      studentList,
      term.getSpacingBetweenStudents(),
      termRuns,
      term.getDistribution(),
      managers,
      term.getDescription()
    );

    return ResponseEntity.ok()
      .body(editTermInfoDto);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseEntity<MessageResponseDto> editTerm(int termId, EditTermDto editTermDto) {
    UserInfo userInfo = AuthService.getLoggedUserInfo();
    Term term = termRepository.findById(termId)
      .orElseThrow(() -> new NotFoundException("Termín zkoušky s ID " + termId + " nebyl nalezen"));

    boolean isUserManager = term.getTermManagers().stream()
      .anyMatch(manager -> manager.getEmail().equals(userInfo.getEmail()));

    if (!isUserManager && !userInfo.getRoles().contains("ROLE_ADMIN")) {
      throw new ForbiddenException("Termín zkoušky může upravit pouze její správce");
    }

    // input DTO should be valid after this call
    doEditTermValidations(editTermDto, termId);

    Course course = courseService.findCourseById(editTermDto.getCourseId())
      .orElseThrow(() -> new BadRequestException("Předmět neexistuje"));

    boolean sendEmailAboutChangesToAllStudents = false;
    Set<String> recipientLogins = new HashSet<>();

    if (!term.getTermName().equals(editTermDto.getTermName()) || term.getCourseReference() != course) {
      // important information change -> send e-mail to all students
      term.setTermName(editTermDto.getTermName());
      term.setCourseReference(course);
      sendEmailAboutChangesToAllStudents = true;
    }

    term.setDescription(editTermDto.getInformationText());
    term.setTermManagers(this.appUserService.createManagerList(editTermDto.getAdditionalTermManagersEmails()));

    List<LoginNameDto> studentsList = new ArrayList<>(editTermDto.getStudentsList());
    studentsList.addAll(editTermDto.getOldStudentsList());

    List<TermRun> existingTermRuns = term.getTermRuns();
    List<TermRunDto> editedTermRuns = editTermDto.getTermRuns();
    EditActionWithIdsList termRunsAction = getTermRunAction(existingTermRuns, editedTermRuns);

    boolean spacingChange = term.getSpacingBetweenStudents() != editTermDto.getSpacingBetweenStudents();
    boolean distributionChange = term.getDistribution() != editTermDto.getDistribution();

    List<TestSchedule> testSchedulesToAdd = new ArrayList<>();
    List<TestSchedule> testSchedulesToDelete = new ArrayList<>();

    if (spacingChange || distributionChange || !editTermDto.isAddStudentsAtTheEnd()
      || termRunsAction.getEditAction() == EditAction.NEW) {
      // huge change -> new seating plans must be created
      createNewSeatingPlans(editTermDto, term, studentsList);
      term.setSpacingBetweenStudents(editTermDto.getSpacingBetweenStudents());
      term.setDistribution(editTermDto.getDistribution());
      sendEmailAboutChangesToAllStudents = true;
    } else if (termRunsAction.getEditAction() == EditAction.REMOVE) {
      // some term run was deleted
      sendEmailAboutChangesToAllStudents = removeExistingTermRuns(editTermDto, term, sendEmailAboutChangesToAllStudents,
        studentsList, termRunsAction);
    } else {
      // only fraction of seating plans must be updated
      List<TestSchedule> currentTestSchedules = testScheduleService.findAllTestSchedulesInTerm(termId);
      if (currentTestSchedules.size() > editTermDto.getOldStudentsList().size()) {
        // some students was removed
        testSchedulesToDelete = removeExistingTestSchedules(editTermDto, existingTermRuns, currentTestSchedules);
      }

      sendEmailAboutChangesToAllStudents = editExistingTermRuns(editTermDto, term, sendEmailAboutChangesToAllStudents,
        recipientLogins, studentsList, existingTermRuns, editedTermRuns);

      if (termRunsAction.getEditAction() == EditAction.ADD) {
        // added new term runs -> just create and add then and do nothing more
        addNewTermRuns(editTermDto, term, editedTermRuns, termRunsAction);
      }

      List<StudentsPlacementDto> studentsPlacements = editTermDto.getStudentsPlacements();
      int initStudentOrderValue = testScheduleService.findLastOrderNumber(term);
      List<TermStudentOrderDto> newlyAddedStudentsList = createStudentListFromLoginNameDtoList(editTermDto.getStudentsList(), initStudentOrderValue);

      if (studentsPlacements.size() > 0 || (editTermDto.getStudentsList().size() > 0 && editTermDto.isAddStudentsAtTheEnd())) {
        manuallyPlaceStudentsAndAddNewStudentsAtTheEnd(editTermDto, term, recipientLogins, testSchedulesToAdd,
          studentsPlacements, newlyAddedStudentsList);
      }
    }

    Term savedTerm = termRepository.save(term);

    if (sendEmail && editTermDto.isSendEmail()) {
      sendChangesInTermEmails(editTermDto, sendEmailAboutChangesToAllStudents, recipientLogins, testSchedulesToAdd, testSchedulesToDelete, savedTerm);
    }

    return ResponseEntity.ok()
      .body(new MessageResponseDto("Změny úspěšně uloženy"));
  }

  private List<TermRunEditInfoDto> getEditInformationAboutTermRuns(UserInfo userInfo, Term term) {
    List<TermRunEditInfoDto> termRuns = new ArrayList<>();
    for (TermRun termRun : term.getTermRuns()) {
      List<EditTermRoomDto> rooms = getEditInformationAboutTermRunRoomPlans(userInfo, termRun);

      List<Integer> assignmentIds = termRun.getAssignments()
        .stream()
        .map(Assignment::getIdAssignment)
        .sorted()
        .collect(Collectors.toList());

      termRuns.add(new TermRunEditInfoDto(
        termRun.getIdTermRun(),
        termRun.getStartDateTime().getTime(),
        termRun.getEndDateTime().getTime(),
        rooms,
        assignmentIds
      ));
    }
    return termRuns;
  }

  private List<EditTermRoomDto> getEditInformationAboutTermRunRoomPlans(UserInfo userInfo, TermRun termRun) {
    return termRun.getRoomPlans()
      .stream()
      .map(roomPlan -> {
        int roomPlanRoomId = roomPlan.getRoomReference().getIdRoom();
        List<Cell> freeSeatsInRoomPlan = cellService.findFreeSeatsInRoomPlan(termRun, roomPlan);

        List<List<SeatingPlanCell>> seatingPlan = createSeatingPlan(roomPlan, termRun, userInfo, false, null);

        if (freeSeatsInRoomPlan.size() == 0) {
          return new EditTermRoomDto(roomPlanRoomId, roomPlan.getCapacity(), new ArrayList<>(), new ArrayList<>(), seatingPlan);
        } else {
          List<Integer> freeSeatsIdsInRoomPlan = freeSeatsInRoomPlan.stream()
            .map(Cell::getIdCell)
            .collect(Collectors.toList());
          List<Integer> freeSeatsAtTheEndOfRoom = getFreeSeatsAtTheEndOfRoom(roomPlan, freeSeatsInRoomPlan).stream()
            .map(Cell::getIdCell)
            .collect(Collectors.toList());
          return new EditTermRoomDto(roomPlanRoomId, roomPlan.getCapacity(), freeSeatsIdsInRoomPlan, freeSeatsAtTheEndOfRoom, seatingPlan);
        }
      })
      .collect(Collectors.toList());
  }

  private List<Cell> getFreeSeatsAtTheEndOfRoom(RoomPlan roomPlan, List<Cell> freeSeatsInRoomPlan) {
    List<Cell> roomPlanSeats = cellService.getRoomPlanSeatsInSnakePattern(roomPlan)
      .stream()
      .flatMap(List::stream)
      .collect(Collectors.toList());

    Collections.reverse(roomPlanSeats);

    List<Cell> freeRoomPlanSeatsInSnakePattern = new ArrayList<>();
    for (int i = 0; i < freeSeatsInRoomPlan.size(); i++) {
      Cell seat = roomPlanSeats.get(i);
      if (freeSeatsInRoomPlan.stream().anyMatch(cell -> cell.getIdCell() == seat.getIdCell())) {
        freeRoomPlanSeatsInSnakePattern.add(seat);
      } else {
        break;
      }
    }
    Collections.reverse(freeRoomPlanSeatsInSnakePattern);
    return freeRoomPlanSeatsInSnakePattern;
  }

  private void checkTermDetailAccessRights(Term term, UserInfo userInfo, boolean isUserManager) {
    String[] emailParts = userInfo.getEmail().split("@");
    boolean doesStudentAttend = termRepository.findAllTermsInWhichTheStudentAttends(emailParts[0])
      .stream()
      .anyMatch(attTerm -> attTerm.equals(term));
    if (userInfo.getRoles().contains("ROLE_EMPLOYEE") && !isUserManager) {
      throw new ForbiddenException("Přístup zamítnut: Tento termín zkoušky nespravujete.");
    } else if (userInfo.getRoles().contains("ROLE_STUDENT") && !doesStudentAttend) {
      throw new ForbiddenException("Přístup zamítnut: Tohoto termínu zkoušky se neúčastníte.");
    }
  }

  private List<TermRunDetailsDto> createTermRunsForResponse(Term term, boolean isPublic, boolean isUserManager) {
    List<TermRunDetailsDto> termRuns = new ArrayList<>();
    for (TermRun termRun : term.getTermRuns()) {
      AtomicBoolean highLight = new AtomicBoolean(false);
      List<SeatingPlanDto> seatingPlans = getSeatingPlansForTermRunResponse(termRun, isPublic, highLight);
      List<BasicAssignmentInfoDto> assignments = new ArrayList<>();
      if (isUserManager) {
        assignments = getAssignmentsForTermRunResponse(termRun);
      }

      TermRunDetailsDto termRunDetailsDto = new TermRunDetailsDto(
        termRun.getIdTermRun(),
        termRun.getStartDateTime().getTime(),
        termRun.getEndDateTime().getTime(),
        seatingPlans,
        assignments
      );
      termRunDetailsDto.setHighlight(highLight.get());
      termRuns.add(termRunDetailsDto);
    }
    return termRuns;
  }

  private Set<Long> getTermsTermRunsStartingDates(Term term) {
    Set<Long> termDates = new HashSet<>();
    for (TermRun termRun : term.getTermRuns()) {
      LocalDateTime localDateTime = LocalDateTime.ofInstant(
        new Date(termRun.getStartDateTime().getTime()).toInstant(),
        ZoneId.systemDefault()
      );
      LocalDateTime startOfDay = localDateTime.with(LocalTime.MIN);
      termDates.add(Date.from(startOfDay.atZone(ZoneId.systemDefault()).toInstant()).getTime());
    }
    return termDates;
  }

  private List<SeatingPlanDto> getSeatingPlansForTermRunResponse(TermRun termRun, boolean isPublic, AtomicBoolean highlight) {
    List<SeatingPlanDto> seatingPlans = new ArrayList<>();
    boolean roomPlanHighlighted = false;
    for (RoomPlan roomPlan : termRun.getRoomPlans()) {
      String roomAbbreviation = roomPlan.getRoomReference().getAbbreviation();
      UserInfo userInfo = AuthService.getLoggedUserInfo();
      List<List<SeatingPlanCell>> seatingPlan = createSeatingPlan(roomPlan, termRun, userInfo, isPublic, highlight);
      List<TestScheduleDetailDto> testSchedules = createTestScheduleOfRoomPlan(roomPlan, termRun, userInfo, isPublic);

      SeatingPlanDto seatingPlanDto = new SeatingPlanDto(false, roomAbbreviation, testSchedules, seatingPlan);
      if (highlight.get() && !roomPlanHighlighted) {
        seatingPlanDto.setHighlight(true);
        roomPlanHighlighted = true;
      }
      seatingPlans.add(seatingPlanDto);
    }

    return seatingPlans;
  }

  private List<BasicAssignmentInfoDto> getAssignmentsForTermRunResponse(TermRun termRun) {
    return termRun.getAssignments()
      .stream()
      .map(assignment -> new BasicAssignmentInfoDto(assignment.getIdAssignment(), assignment.getName(), assignment.getStatus()))
      .collect(Collectors.toList());
  }

  private List<List<SeatingPlanCell>> createSeatingPlan(RoomPlan roomPlan, TermRun termRun, UserInfo userInfo,
                                                        boolean isPublic, AtomicBoolean highlight) {
    List<List<SeatingPlanCell>> cellRows = new ArrayList<>();
    while (cellRows.size() < roomPlan.getRoomReference().getNumberOfRows()) {
      cellRows.add(new ArrayList<>());
    }

    for (Cell cell : roomPlan.getCells()) {
      SeatingPlanCell seatingPlanCell = createSeatingPlanCell(termRun, userInfo, isPublic, highlight, cell);
      cellRows.get(cell.getRowNumber() - 1)
        .add(cell.getColumnNumber() - 1, seatingPlanCell);
    }
    return cellRows;
  }

  private List<TestScheduleDetailDto> createTestScheduleOfRoomPlan(RoomPlan roomPlan, TermRun termRun,
                                                                   UserInfo userInfo, boolean isPublic) {
    List<TestSchedule> testSchedules = this.testScheduleService.findAllTestSchedulesInTermRunsRoomPlan(termRun, roomPlan);
    if (!testSchedules.isEmpty() && !isPublic) {
      if (userInfo.getRoles().contains("ROLE_ADMIN") || userInfo.getRoles().contains("ROLE_EMPLOYEE")) {
        return testSchedules.stream()
          .map(testSchedule -> new TestScheduleDetailDto(
            testSchedule.getStudent().getLogin(),
            testSchedule.getStudent().getNameWithDegrees(),
            testSchedule.getIdTestSchedule(),
            testSchedule.getSeatNumber(),
            testSchedule.getOrderNumber(),
            testSchedule.getAssignment(),
            testSchedule.getPdfPath() != null
          ))
          .collect(Collectors.toList());
      }
    }
    return null;
  }

  private SeatingPlanCell createSeatingPlanCell(TermRun termRun, UserInfo userInfo, boolean isPublic, AtomicBoolean highlight, Cell cell) {
    List<TestSchedule> testSchedules = cell.getTestSchedules()
      .stream()
      .filter(schedule -> schedule.getTermRun().equals(termRun))
      .collect(Collectors.toList());

    CellType cellType = cell.getCellType();

    LoginNameDto student = null;
    Integer seatNumber = null;
    if (!testSchedules.isEmpty() && !isPublic) {
      seatNumber = testSchedules.get(0).getSeatNumber();
      TermStudent termStudent = testSchedules.get(0).getStudent();
      if (userInfo.getRoles().contains("ROLE_ADMIN") || userInfo.getRoles().contains("ROLE_EMPLOYEE")) {
        student = new LoginNameDto(termStudent.getLogin(), termStudent.getNameWithDegrees());
      }

      if (userInfo.getEmail().equals(termStudent.getLogin() + "@stud.fit.vutbr.cz")) {
        cellType = CellType.SELECTED;
        if (highlight != null) {
          highlight.set(true);
        }
      }
    }

    return new SeatingPlanCell(cell.getIdCell(), cellType, seatNumber, student);
  }

  private List<Term> getExistingTerms(boolean isPublic) {
    List<Term> rawTerms;
    if (!isPublic) {
      // internal info
      UserInfo userInfo = AuthService.getLoggedUserInfo();
      if (userInfo.getRoles().contains("ROLE_ADMIN")) {
        rawTerms = termRepository.findAll();
      } else if (userInfo.getRoles().contains("ROLE_EMPLOYEE")) {
        rawTerms = termRepository.findAllTermsWhereUserIsManager(userInfo.getEmail());
      } else {
        String[] emailParts = userInfo.getEmail().split("@");
        rawTerms = termRepository.findAllTermsInWhichTheStudentAttends(emailParts[0]);
      }
    } else {
      rawTerms = termRepository.findAll();
    }
    rawTerms.sort(Comparator.comparing(t -> t.getTermRuns().get(0).getStartDateTime()));
    return rawTerms;
  }

  private List<AllTermsInfoResponseDto> getInformationAboutTerms(boolean isPublic, List<Term> rawTerms) {
    List<AllTermsInfoResponseDto> responseTerms = new ArrayList<>();
    for (Term term : rawTerms) {
      String courseAbbreviation = term.getCourseReference().getAbbreviation();
      Integer academicYear = term.getCourseReference().getAcademicYear();

      List<String> managersEmails = term.getTermManagers()
        .stream()
        .map(AppUser::getEmail)
        .collect(Collectors.toList());

      Set<Long> termDates = new HashSet<>();

      List<TermRunBasicRoomDto> termRuns = getInformationAboutTermRuns(isPublic, term, termDates);

      List<Long> sortedTermDates = termDates.stream()
        .sorted()
        .collect(Collectors.toList());

      responseTerms.add(
        new AllTermsInfoResponseDto(courseAbbreviation, term.getTermName(), academicYear, sortedTermDates,
          term.getIdTerm(), managersEmails, termRuns)
      );
    }
    return responseTerms;
  }

  private List<TermRunBasicRoomDto> getInformationAboutTermRuns(boolean isPublic, Term term, Set<Long> termDates) {
    List<TermRunBasicRoomDto> termRuns = new ArrayList<>();
    for (TermRun termRun : term.getTermRuns()) {
      LocalDateTime localDateTime = LocalDateTime.ofInstant(
        new Date(termRun.getStartDateTime().getTime()).toInstant(),
        ZoneId.systemDefault()
      );
      LocalDateTime startOfDay = localDateTime.with(LocalTime.MIN);
      termDates.add(Date.from(startOfDay.atZone(ZoneId.systemDefault()).toInstant()).getTime());

      List<BasicRoomDto> rooms = getInformationAboutTermRunRoomPlans(termRun);
      List<BasicAssignmentInfoDto> assignments = getInformationAboutTermRunAssignments(isPublic, termRun);

      termRuns.add(
        new TermRunBasicRoomDto(termRun.getIdTermRun(), termRun.getStartDateTime().getTime(),
          termRun.getEndDateTime().getTime(), rooms, assignments)
      );
    }
    return termRuns;
  }

  private List<BasicRoomDto> getInformationAboutTermRunRoomPlans(TermRun termRun) {
    List<BasicRoomDto> rooms = new ArrayList<>();
    for (RoomPlan roomPlan : termRun.getRoomPlans()) {
      String abbreviation = roomPlan.getRoomReference().getAbbreviation();
      String name = roomPlan.getRoomReference().getName();
      rooms.add(new BasicRoomDto(abbreviation, name));
    }
    return rooms;
  }

  private List<BasicAssignmentInfoDto> getInformationAboutTermRunAssignments(boolean isPublic, TermRun termRun) {
    List<BasicAssignmentInfoDto> assignments = new ArrayList<>();
    if (!isPublic) {
      for (Assignment assignment : termRun.getAssignments()) {
        assignments.add(
          new BasicAssignmentInfoDto(assignment.getIdAssignment(), assignment.getName(), assignment.getStatus())
        );
      }
    }
    return assignments;
  }

  private void doNewTermValidations(NewTermDto newTermDto) {
    doStudentListValidations(newTermDto.getStudentsList(), newTermDto.getTermRuns(), newTermDto.getSpacingBetweenStudents());
    doTermRunValidations(newTermDto.getTermRuns(), null);
    doStudentsPlacementsValidations(newTermDto.getStudentsPlacements(), newTermDto.getSpacingBetweenStudents(),
      newTermDto.getTermRuns(), newTermDto.getStudentsList());
    this.appUserService.checkAdditionalManagers(newTermDto.getAdditionalTermManagersEmails());
  }

  private List<TermRun> createNewTermRuns(NewTermDto newTermDto, Term newTerm) {
    List<TermRun> newTermRuns = new ArrayList<>();

    List<TermStudentOrderDto> sortedStudentsList = createStudentListFromLoginNameDtoList(newTermDto.getStudentsList(), 0);

    List<TermStudentOrderDto> manuallyPlacedStudentsList =
      extractManuallyPlacedStudents(sortedStudentsList, newTermDto.getStudentsPlacements());

    List<TermRunDto> sortedTermRunDtos = newTermDto.getTermRuns();
    sortedTermRunDtos.sort(Comparator.comparing(TermRunDto::getStartTime));

    // counter, which will assign number to each seat, where students seat
    AtomicInteger seatCounter = new AtomicInteger(1);
    for (TermRunDto termRunDto : sortedTermRunDtos) {
      TermRun newTermRun = createNewTermRun(newTermDto, newTerm, sortedStudentsList, manuallyPlacedStudentsList, seatCounter, termRunDto);
      newTermRuns.add(newTermRun);
    }
    return newTermRuns;
  }

  private void sendNewTermEmails(String emailMessage, Term savedTerm, TestSchedule testSchedule) {
    DateFormat f = new SimpleDateFormat("dd. MM. yyyy HH:mm");
//    System.out.println("NEW TERM EMAIL " + testSchedule.getStudent().getLogin());
      mailNotificationSender.sendMail(new MailComponents(
      new String[]{testSchedule.getStudent().getLogin() + "@stud.fit.vutbr.cz"},
      "Nový termín zkoušky",
      mailBuilder.buildNewTermRunEmail(
        savedTerm.getTermName(),
        savedTerm.getCourseReference().getAbbreviation(),
        savedTerm.getCourseReference().getName(),
        f.format(new Date(testSchedule.getTermRun().getStartDateTime().getTime())),
        testSchedule.getCell().getRoomPlanReference().getRoomReference().getAbbreviation(),
        testSchedule.getCell().getSeatRowNumber(),
        testSchedule.getCell().getSeatColumnNumber(),
        frontendUrl + "/rozsazeni",
        emailMessage
      )
    ), AuthService.getLoggedUserInfo().getEmail());
  }

  private boolean removeExistingTermRuns(EditTermDto editTermDto, Term term, boolean sendEmailAboutChangesToAllStudents,
                                         List<LoginNameDto> studentsList, EditActionWithIdsList termRunsAction) {
    List<TermRun> termRunsToDelete = term.getTermRuns()
      .stream()
      .filter(termRun -> termRunsAction.getChangedObjectIdsList()
        .stream()
        .anyMatch(id -> id == termRun.getIdTermRun())
      )
      .collect(Collectors.toList());

    for (TermRun termRun : termRunsToDelete) {
      if (testScheduleService.findAllTestSchedulesInTermRun(termRun).size() == 0) {
        // term run is empty -> can be safely deleted
        term.removeTermRun(termRun);  // TODO pokud muze byt v pohode odebrat, validace by mely pokracovat (negeneruji totiz vse znovu)
      } else {
        // deleted term run contains test schedules -> new seating plans must be created
        createNewSeatingPlans(editTermDto, term, studentsList);
        sendEmailAboutChangesToAllStudents = true;
        break;
      }
    }
    return sendEmailAboutChangesToAllStudents;
  }

  private List<TestSchedule> removeExistingTestSchedules(EditTermDto editTermDto, List<TermRun> existingTermRuns, List<TestSchedule> currentTestSchedules) {
    List<TestSchedule> testSchedulesToDelete;
    testSchedulesToDelete = currentTestSchedules
      .stream()
      .filter(testSchedule ->
        editTermDto.getOldStudentsList()
          .stream()
          .noneMatch(student -> student.getLogin().equals(testSchedule.getStudent().getLogin()))
      )
      .collect(Collectors.toList());
    for (TermRun termRun : existingTermRuns) {
      termRun.getTestSchedules()
        .removeIf(testSchedulesToDelete::contains);
    }
    return testSchedulesToDelete;
  }

  private boolean editExistingTermRuns(EditTermDto editTermDto, Term term, boolean sendEmailAboutChangesToAllStudents, Set<String> recipientLogins, List<LoginNameDto> studentsList, List<TermRun> existingTermRuns, List<TermRunDto> editedTermRuns) {
    for (int i = 0; i < term.getTermRuns().size(); i++) {
      TermRun existingTermRun = existingTermRuns.get(i);
      TermRunDto editedTermRun = editedTermRuns.get(i);

      boolean isNewSeatingPlansCreated = editTermRunsRoomPlans(editTermDto, term, studentsList, existingTermRun, editedTermRun);
      if (isNewSeatingPlansCreated) {
        sendEmailAboutChangesToAllStudents = true;
        break;
      }

      editTermRunsAssignments(existingTermRun, editedTermRun);
      editTermRunsStartTimeAndEndTime(recipientLogins, existingTermRun, editedTermRun);
    }
    return sendEmailAboutChangesToAllStudents;
  }

  private boolean editTermRunsRoomPlans(EditTermDto editTermDto, Term term, List<LoginNameDto> studentsList, TermRun existingTermRun, TermRunDto editedTermRun) {
    EditActionWithIdsList roomsAction = getRoomAction(existingTermRun.getRoomPlans(), editedTermRun.getRooms(),
      term.getSpacingBetweenStudents());
    if (roomsAction.getEditAction() == EditAction.NEW) {
      // huge change -> new seating plans must be created
      createNewSeatingPlans(editTermDto, term, studentsList);
      return true;
    } else if (roomsAction.getEditAction() == EditAction.REMOVE) {
      return removeRoomPlans(editTermDto, term, studentsList, existingTermRun, roomsAction);
    } else if (roomsAction.getEditAction() == EditAction.ADD) {
      // added additional empty rooms to term run -> just add them and do nothing more with them
      addNewRoomPlans(existingTermRun, roomsAction);
    }
    return false;
  }

  private boolean removeRoomPlans(EditTermDto editTermDto, Term term, List<LoginNameDto> studentsList, TermRun existingTermRun, EditActionWithIdsList roomsAction) {
    List<RoomPlan> roomPlansToDelete = existingTermRun.getRoomPlans()
      .stream()
      .filter(roomPlan -> roomsAction.getChangedObjectIdsList()
        .stream()
        .anyMatch(id -> id == roomPlan.getIdRoomPlan())
      )
      .collect(Collectors.toList());

    for (RoomPlan roomPlan : roomPlansToDelete) {
      if (testScheduleService.findAllTestSchedulesInTermRunsRoomPlan(existingTermRun, roomPlan).size() == 0) {
        // room plan is empty -> can be safely deleted
        existingTermRun.removeRoomPlan(roomPlan);
      } else {
        // deleted room plan contains test schedules -> new seating plans must be created
        createNewSeatingPlans(editTermDto, term, studentsList);
        return true;
      }
    }
    return false;
  }

  private void addNewRoomPlans(TermRun existingTermRun, EditActionWithIdsList roomsAction) {
    List<RoomPlan> roomPlansToAdd = roomsAction.getChangedObjectIdsList()
      .stream()
      .map(roomPlanId -> roomPlanService.findRoomPlanById(roomPlanId)
        .orElseThrow(() -> new BadRequestException("Plán místnosti s ID " + roomPlanId + " neexistuje."))
      )
      .collect(Collectors.toList());

    for (RoomPlan roomPlan : roomPlansToAdd) {
      existingTermRun.addRoomPlan(roomPlan);
    }
  }

  private void editTermRunsAssignments(TermRun existingTermRun, TermRunDto editedTermRun) {
    List<Integer> existingAssignmentIds = existingTermRun.getAssignments()
      .stream()
      .map(Assignment::getIdAssignment)
      .sorted()
      .collect(Collectors.toList());

    Collections.sort(editedTermRun.getAssignments());

    if (!existingAssignmentIds.equals(editedTermRun.getAssignments())) {
      List<Assignment> assignments = getAssignmentsByIds(editedTermRun);
      for (RoomPlan roomPlan : existingTermRun.getRoomPlans()) {
        List<TestSchedule> testSchedules = testScheduleService.findAllTestSchedulesInTermRunsRoomPlan(existingTermRun, roomPlan);
        addAssignmentsToTestSchedules(assignments, roomPlan, testSchedules, true);
      }
      existingTermRun.setAssignments(assignments);
    }
  }

  private void editTermRunsStartTimeAndEndTime(Set<String> recipientLogins, TermRun existingTermRun, TermRunDto editedTermRun) {
    boolean startTimeChange = existingTermRun.getStartDateTime().getTime() != editedTermRun.getStartTime();
    boolean endTimeChange = existingTermRun.getEndDateTime().getTime() != editedTermRun.getEndTime();
    if (startTimeChange || endTimeChange) {
      existingTermRun.setStartDateTime(new Timestamp(editedTermRun.getStartTime()));
      existingTermRun.setEndDateTime(new Timestamp(editedTermRun.getEndTime()));
      addAllTermRunStudentLoginsToSet(existingTermRun, recipientLogins);
    }
  }

  private void addNewTermRuns(EditTermDto editTermDto, Term term, List<TermRunDto> editedTermRuns, EditActionWithIdsList termRunsAction) {
    List<TermRunDto> editTermRunsToAdd = editedTermRuns.stream()
      .filter(termRunDto -> termRunsAction.getChangedObjectIdsList().contains(termRunDto.getId()))
      .collect(Collectors.toList());

    for (TermRunDto editTermRunDto : editTermRunsToAdd) {
      List<Assignment> assignments = getAssignmentsByIds(editTermRunDto);

      List<RoomPlan> roomPlans = editTermRunDto.getRooms()
        .stream()
        .map(roomId -> roomPlanService.findRoomPlanByRoomIdAndSpacing(roomId, editTermDto.getSpacingBetweenStudents()))
        .collect(Collectors.toList());

      term.addTermRun(
        new TermRun(new Timestamp(editTermRunDto.getStartTime()), new Timestamp(editTermRunDto.getEndTime()),
          term, assignments, roomPlans, new ArrayList<>())
      );
    }
  }

  private void manuallyPlaceStudentsAndAddNewStudentsAtTheEnd(EditTermDto editTermDto, Term term, Set<String> recipientLogins, List<TestSchedule> testSchedulesToAdd, List<StudentsPlacementDto> studentsPlacements, List<TermStudentOrderDto> newlyAddedStudentsList) {
    List<TermStudentOrderDto> manuallyPlacedStudentsList = extractManuallyPlacedStudents(newlyAddedStudentsList, studentsPlacements);
    List<TestSchedule> removedTestSchedules = freeSeatsOfManuallyReplacedStudents(term, recipientLogins, studentsPlacements);
    removedTestSchedules.stream()
      .map(testSchedule -> new TermStudentOrderDto(testSchedule.getStudent(), testSchedule.getOrderNumber()))
      .forEach(manuallyPlacedStudentsList::add);

    List<FreeSeatsAtTheEndOfRoomPlan> freeSeatsAtTheEndOfRooms = manuallyPlaceStudentsEdit(editTermDto, term,
      testSchedulesToAdd, studentsPlacements, manuallyPlacedStudentsList);

    if (newlyAddedStudentsList.size() > 0) {
      // add new students at the end
      addNewStudentsAtTheEndOfTerm(editTermDto, testSchedulesToAdd, newlyAddedStudentsList, freeSeatsAtTheEndOfRooms);
    }

    // newly created test schedules does not have assignments and seat numbers
    addAssignmentsAndSeatNumbersToNewTestSchedules(term, testSchedulesToAdd, removedTestSchedules);

    for (TestSchedule testSchedule : testSchedulesToAdd) {
      testSchedule.getTermRun().addTestSchedule(testSchedule);
    }
  }

  private List<FreeSeatsAtTheEndOfRoomPlan> manuallyPlaceStudentsEdit(EditTermDto editTermDto, Term term,
                                                                      List<TestSchedule> testSchedulesToAdd,
                                                                      List<StudentsPlacementDto> studentsPlacements,
                                                                      List<TermStudentOrderDto> manuallyPlacedStudentsList) {
    List<FreeSeatsAtTheEndOfRoomPlan> freeSeatsAtTheEndOfRooms = new ArrayList<>();
    for (TermRun termRun : term.getTermRuns()) {
      List<StudentsPlacementDto> studentPlacementsForCurrentTermRun =
        getManuallyPlacedStudentsForTermRunAllRoomPlans(studentsPlacements, termRun.getIdTermRun());
      for (RoomPlan roomPlan : termRun.getRoomPlans()) {
        manuallyPlaceStudentsAtTheEndOfRoomPlan(freeSeatsAtTheEndOfRooms, termRun, studentPlacementsForCurrentTermRun, roomPlan);
      }

      List<FreeSeatsAtTheEndOfRoomPlan> freeSeatsAtTheEndOfTermRun = freeSeatsAtTheEndOfRooms.stream()
        .filter(freeSeatsAtTheEndOfRoomPlan -> freeSeatsAtTheEndOfRoomPlan.getTermRun().equals(termRun))
        .collect(Collectors.toList());

      StudentsPlacementDto studentPlacementEndOfTermRun = getManuallyPlacedStudentsForTermRunTermRunEnd(studentPlacementsForCurrentTermRun);
      if (studentPlacementEndOfTermRun != null) {
        manuallyPlaceStudentsAtTheEndOfTermRun(termRun, studentPlacementsForCurrentTermRun, freeSeatsAtTheEndOfTermRun, studentPlacementEndOfTermRun);
      }

      testSchedulesToAdd.addAll(doManualPlacementOfStudentsAtSpecificSeats(
        manuallyPlacedStudentsList, studentPlacementsForCurrentTermRun, termRun, editTermDto.getDistribution())
      );
    }
    return freeSeatsAtTheEndOfRooms;
  }

  private List<StudentsPlacementDto> getManuallyPlacedStudentsForTermRunAllRoomPlans(List<StudentsPlacementDto> studentsPlacements, int termRunId) {
    List<StudentsPlacementDto> termRunStudentPlacements = new ArrayList<>();
    if (studentsPlacements != null) {
      termRunStudentPlacements = studentsPlacements
        .stream()
        .filter(sp -> sp.getTermRunId().equals(termRunId))
        .collect(Collectors.toList());
    }
    return termRunStudentPlacements;
  }

  private void manuallyPlaceStudentsAtTheEndOfRoomPlan(List<FreeSeatsAtTheEndOfRoomPlan> freeSeatsAtTheEndOfRooms, TermRun termRun,
                                                       List<StudentsPlacementDto> studentPlacementsForCurrentTermRun, RoomPlan roomPlan) {
    List<Cell> freeSeatsInRoomPlan = cellService.findFreeSeatsInRoomPlan(termRun, roomPlan);
    List<Cell> freeSeatsAtTheEndOfRoom = getFreeSeatsAtTheEndOfRoom(roomPlan, freeSeatsInRoomPlan);

    List<StudentsPlacementDto> studentsPlacementsEndOfRoomPlan =
      getManuallyPlacedStudentsForCurrentRoomPlan(studentPlacementsForCurrentTermRun, roomPlan);
    if (studentsPlacementsEndOfRoomPlan.size() == 1) {
      StudentsPlacementDto roomStudentPlacement = studentsPlacementsEndOfRoomPlan.get(0);

      int numberOfStudentsToPlace = roomStudentPlacement.getStudentLogins().size();
      int numberOfFreeSeats = freeSeatsAtTheEndOfRoom.size();
      if (numberOfStudentsToPlace > numberOfFreeSeats) {
        Room room = roomPlan.getRoomReference();
        throw new BadRequestException("U běhu s ID " + termRun.getIdTermRun() + " u místnosti " +
          "se zkratkou a názvem \"" + room.getAbbreviation() + " - " + room.getName() + "\" byl pro usazení na konec " +
          "místnosti zvolen větší počet studentů (" + numberOfStudentsToPlace + "), než je dostupná volná kapacita " +
          "(" + numberOfFreeSeats + ").");
      }

      List<Cell> seatSubList = freeSeatsAtTheEndOfRoom.subList(0, numberOfStudentsToPlace);
      List<Integer> seatIds = seatSubList.stream()
        .map(Cell::getIdCell)
        .collect(Collectors.toList());
      roomStudentPlacement.setSeatIds(seatIds);
      seatSubList.clear();
    }

    freeSeatsAtTheEndOfRooms.add(new FreeSeatsAtTheEndOfRoomPlan(termRun, roomPlan, freeSeatsAtTheEndOfRoom));
  }

  private void addAssignmentsAndSeatNumbersToNewTestSchedules(Term term, List<TestSchedule> testSchedulesToAdd, List<TestSchedule> removedTestSchedules) {
    int currentSeatNumber = 0;
    for (TermRun termRun : term.getTermRuns()) {
      for (RoomPlan roomPlan : termRun.getRoomPlans()) {
        List<TestSchedule> newTestSchedulesForCurrentTermRunAndRoomPlan = testSchedulesToAdd.stream()
          .filter(testSchedule -> testSchedule.getTermRun().equals(termRun) && testSchedule.getCell().getRoomPlanReference().equals(roomPlan))
          .collect(Collectors.toList());

        List<TestSchedule> testSchedules = testScheduleService.findAllTestSchedulesInTermRunsRoomPlan(termRun, roomPlan);
        testSchedules.removeAll(removedTestSchedules);
        testSchedules.addAll(newTestSchedulesForCurrentTermRunAndRoomPlan);
        addAssignmentsToTestSchedules(termRun.getAssignments(), roomPlan, testSchedules, false);

        List<List<Cell>> roomPlanSeats = cellService.getRoomPlanSeatsInSnakePattern(roomPlan);
        for (List<Cell> row : roomPlanSeats) {
          for (Cell seat : row) {
            currentSeatNumber = addMissingSeatNumberToTestScheduleAtSeat(currentSeatNumber, testSchedules, seat);
          }
        }
      }
    }
  }

  private void manuallyPlaceStudentsAtTheEndOfTermRun(TermRun termRun, List<StudentsPlacementDto> studentPlacementsForCurrentTermRun, List<FreeSeatsAtTheEndOfRoomPlan> freeSeatsAtTheEndOfTermRun, StudentsPlacementDto studentPlacementEndOfTermRun) {
    studentPlacementsForCurrentTermRun.remove(studentPlacementEndOfTermRun);

    List<String> loginsToSeat = studentPlacementEndOfTermRun.getStudentLogins();
    int numberOfStudentsToPlace = loginsToSeat.size();
    int startIndex = getIndexOfLastRoomPlanWithFreeSeatsAtTheEnd(freeSeatsAtTheEndOfTermRun);
    int numberOfFreeSeats = 0;
    for (int i = startIndex; i < freeSeatsAtTheEndOfTermRun.size(); i++) {
      numberOfFreeSeats += freeSeatsAtTheEndOfTermRun.get(i)
        .getFreeSeatsAtTheEndOfRoom()
        .size();
    }

    if (numberOfStudentsToPlace > numberOfFreeSeats) {
      throw new BadRequestException("U běhu s ID " + termRun.getIdTermRun() + " byl pro usazení na konec " +
        "místnosti zvolen větší počet studentů (" + numberOfStudentsToPlace + "), než je dostupná volná kapacita " +
        "(" + numberOfFreeSeats + ").");
    }

    for (int i = startIndex; i < freeSeatsAtTheEndOfTermRun.size(); i++) {
      if (numberOfStudentsToPlace <= 0) {
        break;
      }
      FreeSeatsAtTheEndOfRoomPlan f = freeSeatsAtTheEndOfTermRun.get(i);

      numberOfStudentsToPlace = createStudentPlacement(termRun, studentPlacementsForCurrentTermRun, loginsToSeat, numberOfStudentsToPlace, f);
    }
  }

  private int createStudentPlacement(TermRun termRun, List<StudentsPlacementDto> studentPlacementsForCurrentTermRun,
                                     List<String> loginsToSeat, int numberOfStudentsToPlace, FreeSeatsAtTheEndOfRoomPlan f) {
    StudentsPlacementDto studentsPlacement = new StudentsPlacementDto();
    studentsPlacement.setTermRunId(termRun.getIdTermRun());
    studentsPlacement.setRoomId(f.getRoomPlan().getRoomReference().getIdRoom());

    List<Cell> freeSeatsAtTheEndOfRoom = f.getFreeSeatsAtTheEndOfRoom();
    List<Cell> seatSubList = freeSeatsAtTheEndOfRoom.subList(0, numberOfStudentsToPlace);
    List<String> loginsSubList = loginsToSeat.subList(0, numberOfStudentsToPlace);
    numberOfStudentsToPlace -= seatSubList.size();
    List<Integer> seatIds = seatSubList.stream()
      .map(Cell::getIdCell)
      .collect(Collectors.toList());
    studentsPlacement.setSeatIds(seatIds);
    studentsPlacement.setStudentLogins(new ArrayList<>(loginsSubList));
    seatSubList.clear();
    loginsSubList.clear();
    studentPlacementsForCurrentTermRun.add(studentsPlacement);
    return numberOfStudentsToPlace;
  }

  private int addMissingSeatNumberToTestScheduleAtSeat(int currentSeatNumber, List<TestSchedule> testSchedules, Cell seat) {
    Optional<TestSchedule> testScheduleOpt = testSchedules.stream()
      .filter(ts -> ts.getCell().equals(seat))
      .findFirst();
    if (testScheduleOpt.isPresent()) {
      TestSchedule testSchedule = testScheduleOpt.get();
      if (testSchedule.getSeatNumber() != null) {
        currentSeatNumber = testSchedule.getSeatNumber();
      } else {
        currentSeatNumber++;
        testSchedule.setSeatNumber(currentSeatNumber);
      }
    }
    return currentSeatNumber;
  }

  private void addNewStudentsAtTheEndOfTerm(EditTermDto editTermDto, List<TestSchedule> testSchedulesToAdd, List<TermStudentOrderDto> newlyAddedStudentsList, List<FreeSeatsAtTheEndOfRoomPlan> freeSeatsAtTheEndOfRooms) {
    int startIndex = getIndexOfLastRoomPlanWithFreeSeatsAtTheEnd(freeSeatsAtTheEndOfRooms);
    AtomicInteger seatCounter = new AtomicInteger(0);
    AtomicInteger alreadySeatedInRoom = new AtomicInteger(0);
    AtomicInteger alreadySeatedInTermRun = new AtomicInteger(0);

    for (int i = startIndex; i < freeSeatsAtTheEndOfRooms.size(); i++) {
      FreeSeatsAtTheEndOfRoomPlan f = freeSeatsAtTheEndOfRooms.get(i);
      for (Cell seat : f.getFreeSeatsAtTheEndOfRoom()) {
        placeNewStudentAtSeat(editTermDto.getDistribution(), newlyAddedStudentsList, f.getTermRun(), seat, testSchedulesToAdd,
          seatCounter, alreadySeatedInRoom, alreadySeatedInTermRun);
        testSchedulesToAdd.get(testSchedulesToAdd.size() - 1).setSeatNumber(null); // will be added at the and
      }
    }

    if (newlyAddedStudentsList.size() != 0) {
      throw new BadRequestException("Nepodařilo se usadit všechny dodatečně přidané studenty - za posledním studentem není dostatek místa.\n" +
        "Zkuste další místa vytvořit nebo zrušte možnost \"Přidat nové studenty na konec\" a vytvořte nové rozsazení.");
    }
  }

  private void sendChangesInTermEmails(EditTermDto editTermDto, boolean sendEmailAboutChangesToAllStudents, Set<String> recipientLogins, List<TestSchedule> testSchedulesToAdd, List<TestSchedule> testSchedulesToDelete, Term savedTerm) {
    String emailMessage = editTermDto.getEmailMessage();
    if (sendEmailAboutChangesToAllStudents || !recipientLogins.isEmpty()) {
      sendEmailsAboutChanges(sendEmailAboutChangesToAllStudents, recipientLogins, savedTerm, emailMessage);
    } else if (testSchedulesToDelete.size() == 0 && !emailMessage.isBlank()) {
      sendEmailsAboutChanges(true, recipientLogins, savedTerm, emailMessage);
    }

    testSchedulesToAdd = testSchedulesToAdd.stream()
      .filter(testSchedule -> !recipientLogins.contains(testSchedule.getStudent().getLogin()))
      .collect(Collectors.toList());

    for (TestSchedule testSchedule : testSchedulesToAdd) {
      sendNewTermEmails(emailMessage, savedTerm, testSchedule);
    }

    for (TestSchedule testSchedule : testSchedulesToDelete) {
      sendStudentRemovedFromTermEmail(testSchedule.getStudent().getLogin(), testSchedule.getTermRun());
    }
  }

  private void sendStudentRemovedFromTermEmail(String login, TermRun termRun) {
    DateFormat f = new SimpleDateFormat("dd. MM. yyyy HH:mm");
    Term term = termRun.getTermReference();
//    System.out.println("REMOVED EMAIL " + login);
    mailNotificationSender.sendMail(new MailComponents(
      new String[]{login + "@stud.fit.vutbr.cz"},
      "Upozornění - odebrání z termínu zkoušky",
      mailBuilder.buildRemovedFromTermEmail(
        term.getTermName(),
        term.getCourseReference().getAbbreviation(),
        term.getCourseReference().getName(),
        f.format(new Date(termRun.getStartDateTime().getTime())),
        frontendUrl
      )
    ), AuthService.getLoggedUserInfo().getEmail());
  }

  private List<TestSchedule> freeSeatsOfManuallyReplacedStudents(Term term, Set<String> recipientLogins,
                                                                 List<StudentsPlacementDto> studentsPlacements) {
    List<String> logins = new ArrayList<>();
    for (StudentsPlacementDto studentsPlacementDto : studentsPlacements) {
      logins.addAll(studentsPlacementDto.getStudentLogins());
    }
    recipientLogins.addAll(logins);

    List<TestSchedule> removedTestSchedules = new ArrayList<>();
    for (TermRun termRun : term.getTermRuns()) {
      termRun.getTestSchedules().removeIf(testSchedule -> {
        if (logins.contains(testSchedule.getStudent().getLogin())) {
          removedTestSchedules.add(testSchedule);
          return true;
        } else {
          return false;
        }
      });
    }
    return removedTestSchedules;
  }

  private int getIndexOfLastRoomPlanWithFreeSeatsAtTheEnd(List<FreeSeatsAtTheEndOfRoomPlan> freeSeatsAtTheEndOfRooms) {
    int resultIndex = 0;
    for (int i = freeSeatsAtTheEndOfRooms.size() - 1; i >= 0; i--) {
      FreeSeatsAtTheEndOfRoomPlan freeSeatsAtTheEndOfRoomPlan = freeSeatsAtTheEndOfRooms.get(i);
      List<Cell> freeSeatsAtTheEndOfRoom = freeSeatsAtTheEndOfRoomPlan.getFreeSeatsAtTheEndOfRoom();
      if (freeSeatsAtTheEndOfRoom.size() != freeSeatsAtTheEndOfRoomPlan.getRoomPlan().getCapacity()) {
        if (freeSeatsAtTheEndOfRoom.size() == 0) {
          resultIndex = i + 1;
        } else {
          resultIndex = i;
        }
        break;
      }
    }
    if (resultIndex > (freeSeatsAtTheEndOfRooms.size() - 1)) {
      throw new BadRequestException("Není dostatečná kapacita pro usazení všech vybraných studentů na konec.");
    }
    return resultIndex;
  }

  private List<Assignment> getAssignmentsByIds(TermRunDto editedTermRun) {
    return editedTermRun.getAssignments()
      .stream()
      .map(assignmentId -> assignmentService.findAssignmentById(assignmentId)
        .orElseThrow(() -> new BadRequestException("Šablona zadání s ID " + assignmentId + " neexistuje"))
      )
      .collect(Collectors.toList());
  }

  private EditActionWithIdsList getTermRunAction(List<TermRun> existingTermRuns, List<TermRunDto> editedTermRuns) {
    List<Integer> existingTermRunsIds = existingTermRuns.stream()
      .map(TermRun::getIdTermRun)
      .sorted()
      .collect(Collectors.toList());

    List<Integer> editedTermRunsIds = editedTermRuns.stream()
      .map(BasicTermRunDto::getId)
      .sorted()
      .collect(Collectors.toList());

    return compareIdsLists(existingTermRunsIds, editedTermRunsIds);
  }

  private EditActionWithIdsList getRoomAction(List<RoomPlan> existingRoomPlans, List<Integer> editedRoomIds,
                                              SpacingBetweenStudents spacingBetweenStudents) {
    List<Integer> existingRoomPlansIds = existingRoomPlans.stream()
      .map(RoomPlan::getIdRoomPlan)
      .sorted()
      .collect(Collectors.toList());

    List<Integer> editedRoomPlanIds = editedRoomIds.stream()
      .map(roomId -> roomPlanService.findRoomPlanByRoomIdAndSpacing(roomId, spacingBetweenStudents).getIdRoomPlan())
      .collect(Collectors.toList());

    return compareIdsLists(existingRoomPlansIds, editedRoomPlanIds);
  }

  private EditActionWithIdsList compareIdsLists(List<Integer> existingIds, List<Integer> editedIds) {
    if (existingIds.equals(editedIds)) {
      return new EditActionWithIdsList(EditAction.NOTHING, null);
    } else if (editedIds.containsAll(existingIds) && editedIds.size() > existingIds.size()) {
      List<Integer> idsToAdd = new ArrayList<>(editedIds);
      idsToAdd.removeAll(existingIds);
      return new EditActionWithIdsList(EditAction.ADD, idsToAdd);
    } else if (existingIds.containsAll(editedIds) && editedIds.size() < existingIds.size()) {
      List<Integer> idsToRemove = new ArrayList<>(existingIds);
      idsToRemove.removeAll(editedIds);
      return new EditActionWithIdsList(EditAction.REMOVE, idsToRemove);
    } else {
      return new EditActionWithIdsList(EditAction.NEW, editedIds);
    }
  }

  private void createNewSeatingPlans(EditTermDto editTermDto, Term term, List<LoginNameDto> studentsList) {
    term.removeAllTermRuns();
    NewTermDto newTermDto = new NewTermDto(editTermDto.getTermName(), editTermDto.getCourseId(),
      editTermDto.getSpacingBetweenStudents(), editTermDto.getTermRuns(), editTermDto.getStudentsPlacements(),
      editTermDto.getDistribution(), editTermDto.getInformationText(), studentsList,
      editTermDto.getAdditionalTermManagersEmails(), editTermDto.isSendEmail(), editTermDto.getEmailMessage());
    for (TermRun newTermRun : createNewTermRuns(newTermDto, term)) {
      term.addTermRun(newTermRun);
    }
  }

  private void sendEmailsAboutChanges(boolean sendToAll, Set<String> recipientsLogins, Term savedTerm, String emailMessage) {
    DateFormat f = new SimpleDateFormat("dd. MM. yyyy HH:mm");
    for (TermRun termRun : savedTerm.getTermRuns()) {
      List<TestSchedule> filteredTestSchedules;
      if (sendToAll) {
        filteredTestSchedules = termRun.getTestSchedules();
      } else {
        filteredTestSchedules = termRun.getTestSchedules()
          .stream()
          .filter(testSchedule ->
            recipientsLogins.size() == 0 ||
              recipientsLogins.stream()
                .anyMatch(login -> login.equals(testSchedule.getStudent().getLogin()))
          )
          .collect(Collectors.toList());
      }

      for (TestSchedule testSchedule : filteredTestSchedules) {
//        System.out.println("EMAIL SEND: " + testSchedule.getStudent().getLogin());
        mailNotificationSender.sendMail(new MailComponents(
          new String[]{testSchedule.getStudent().getLogin() + "@stud.fit.vutbr.cz"},
          "Upozornění - změny v termínu zkoušky",
          mailBuilder.buildEditTermRunEmail(
            savedTerm.getTermName(),
            savedTerm.getCourseReference().getAbbreviation(),
            savedTerm.getCourseReference().getName(),
            f.format(new Date(termRun.getStartDateTime().getTime())),
            testSchedule.getCell().getRoomPlanReference().getRoomReference().getAbbreviation(),
            testSchedule.getCell().getSeatRowNumber(),
            testSchedule.getCell().getSeatColumnNumber(),
            frontendUrl,
            emailMessage
          )
        ), AuthService.getLoggedUserInfo().getEmail());
      }
    }
  }

  private List<TermStudentOrderDto> createStudentListFromLoginNameDtoList(List<LoginNameDto> studentsList, int startValue) {
    // Sorting
    studentsList.sort(Comparator.comparing(LoginNameDto::getLogin));

    List<TermStudentOrderDto> students = new ArrayList<>();
    for (int i = 0; i < studentsList.size(); i++) {
      LoginNameDto dto = studentsList.get(i);
      TermStudent student = termStudentService.findUserByLogin(dto.getLogin());
      if (student == null) {
        student = new TermStudent(dto.getLogin(), dto.getNameAndDegrees());
      }
      students.add(new TermStudentOrderDto(student, startValue + i + 1));
    }

    return students;
  }

  private TermRun createNewTermRun(NewTermDto newTermDto, Term newTerm, List<TermStudentOrderDto> sortedStudentsList,
                                   List<TermStudentOrderDto> manuallyPlacedStudentsList, AtomicInteger seatCounter,
                                   TermRunDto termRunDto) {
    // get manually placed students for current term run (all room plans)
    List<StudentsPlacementDto> studentPlacementsForCurrentTermRun =
      getManuallyPlacedStudentsForTermRunAllRoomPlans(newTermDto.getStudentsPlacements(), termRunDto.getId());

    // get manually placed students for current term run (with no room plan, just place them on the end of term run)
    StudentsPlacementDto studentPlacementEndOfTermRun = getManuallyPlacedStudentsForTermRunTermRunEnd(studentPlacementsForCurrentTermRun);

    List<Assignment> assignments = new ArrayList<>();
    for (Integer assignmentId : termRunDto.getAssignments()) {
      Assignment assignment = this.assignmentService.findAssignmentById(assignmentId)
        .orElseThrow(() -> new NotFoundException("Zadání neexistuje"));
      assignments.add(assignment);
    }

    TermRun newTermRun = new TermRun(new Timestamp(termRunDto.getStartTime()), new Timestamp(termRunDto.getEndTime()),
      newTerm, assignments, null, null);

    List<RoomPlan> sortedRoomPlans = getSortedRoomPlansFromTermRunDto(termRunDto.getRooms(), newTermDto.getSpacingBetweenStudents());

    // place students, which have explicitly specified seats to sit
    List<TestSchedule> testSchedules = doManualPlacementOfStudentsAtSpecificSeats(manuallyPlacedStudentsList,
      studentPlacementsForCurrentTermRun, newTermRun, newTermDto.getDistribution());

    // counter of already seated students in whole term run (to determine, when to start placing manually placed students)
    int termRunCapacity = sortedRoomPlans.stream().mapToInt(RoomPlan::getCapacity).sum();
    boolean roomPlansLoopStop = false;
    AtomicInteger alreadySeatedInTermRun = new AtomicInteger(0);
    for (RoomPlan roomPlan : sortedRoomPlans) {
      roomPlansLoopStop = placeStudentsToRoomPlan(newTermDto, sortedStudentsList, manuallyPlacedStudentsList, seatCounter,
        studentPlacementsForCurrentTermRun, studentPlacementEndOfTermRun, assignments, newTermRun, testSchedules,
        termRunCapacity, roomPlansLoopStop, alreadySeatedInTermRun, roomPlan);
    }

    newTermRun.setRoomPlans(sortedRoomPlans);
    newTermRun.setTestSchedules(testSchedules);
    return newTermRun;
  }

  private boolean placeStudentsToRoomPlan(NewTermDto newTermDto, List<TermStudentOrderDto> sortedStudentsList, List<TermStudentOrderDto> manuallyPlacedStudentsList, AtomicInteger seatCounter, List<StudentsPlacementDto> studentPlacementsForCurrentTermRun, StudentsPlacementDto studentPlacementEndOfTermRun, List<Assignment> assignments, TermRun newTermRun, List<TestSchedule> testSchedules, int termRunCapacity, boolean roomPlansLoopStop, AtomicInteger alreadySeatedInTermRun, RoomPlan roomPlan) {
    // get students, which should be manually placed on the end of the current room
    List<StudentsPlacementDto> studentsPlacementsEndOfRoomPlan =
      getManuallyPlacedStudentsForCurrentRoomPlan(studentPlacementsForCurrentTermRun, roomPlan);
    StudentsPlacementDto roomStudentPlacement = null;
    if (studentsPlacementsEndOfRoomPlan.size() == 1) {
      roomStudentPlacement = studentsPlacementsEndOfRoomPlan.get(0);
    }

    List<Cell> cells = roomPlan.getCells();
    int numRows = roomPlan.getRoomReference().getNumberOfRows();
    int numCols = roomPlan.getRoomReference().getNumberOfColumns();
    int rowStart = cellService.findFirstRowNumberWithSeatInRoomPlan(roomPlan);
    boolean goRight = true; // go through room in snake pattern

    // counter of already seated students in room (to determine, when to start placing manually placed students)
    AtomicInteger alreadySeatedInRoom = new AtomicInteger(0);
    if (!roomPlansLoopStop) {
      roomPlansLoopStop = placeStudentsToRoomPlanInSnakePattern(newTermDto, sortedStudentsList, seatCounter, studentPlacementEndOfTermRun, newTermRun, testSchedules, termRunCapacity, roomPlansLoopStop, alreadySeatedInTermRun, roomPlan, roomStudentPlacement, cells, numRows, numCols, rowStart, goRight, alreadySeatedInRoom);
    }

    // if there are any students to manually place on the end of the room or term run, place them in snake pattern
    manuallyPlaceStudents(newTermDto, sortedStudentsList, manuallyPlacedStudentsList, seatCounter,
      studentPlacementsForCurrentTermRun, studentPlacementEndOfTermRun, newTermRun, testSchedules, roomPlansLoopStop,
      alreadySeatedInTermRun, roomPlan, studentsPlacementsEndOfRoomPlan, roomStudentPlacement, cells, numRows,
      numCols, rowStart, alreadySeatedInRoom);

    if (assignments.size() > 0) {
      addAssignmentsToTestSchedules(assignments, roomPlan, testSchedules, true);
    }
    return roomPlansLoopStop;
  }

  private boolean placeStudentsToRoomPlanInSnakePattern(NewTermDto newTermDto, List<TermStudentOrderDto> sortedStudentsList, AtomicInteger seatCounter, StudentsPlacementDto studentPlacementEndOfTermRun, TermRun newTermRun, List<TestSchedule> testSchedules, int termRunCapacity, boolean roomPlansLoopStop, AtomicInteger alreadySeatedInTermRun, RoomPlan roomPlan, StudentsPlacementDto roomStudentPlacement, List<Cell> cells, int numRows, int numCols, int rowStart, boolean goRight, AtomicInteger alreadySeatedInRoom) {
    outerLoop:
    for (int row = rowStart - 1; row < numRows; row++) {
      int oldSeatCounterValue = seatCounter.get();
      if (goRight) {
        for (int column = 0; column < numCols; column++) {
          if ((studentPlacementEndOfTermRun != null && studentPlacementEndOfTermRun.getRoomId() == null) &&
            (termRunCapacity - studentPlacementEndOfTermRun.getStudentLogins().size() == alreadySeatedInTermRun.get())) {
            // all students are seated or it is necessary to stop, because manually placed students
            // must be placed on the end of the term run
            roomPlansLoopStop = true;
            break outerLoop;
          } else if (sortedStudentsList.size() == 0) {
            // all students are seated, but we still need to loop, because there are still manual placements
            // without seat number
            isSeatOccupied(testSchedules, newTermRun, cells.get(numCols * row + column), seatCounter);
            continue;
          } else if (roomStudentPlacement != null && (roomPlan.getCapacity() - roomStudentPlacement.getStudentLogins().size()) == alreadySeatedInRoom.get()) {
            // it is necessary to stop, because manually placed students must be placed on the end of the room
            break outerLoop;
          }
          placeNewStudentAtSeat(newTermDto.getDistribution(), sortedStudentsList, newTermRun,
            cells.get(numCols * row + column), testSchedules, seatCounter, alreadySeatedInRoom, alreadySeatedInTermRun);
        }
      } else {
        for (int column = numCols - 1; column >= 0; column--) {
          if ((studentPlacementEndOfTermRun != null && studentPlacementEndOfTermRun.getRoomId() == null) &&
            (termRunCapacity - studentPlacementEndOfTermRun.getStudentLogins().size() == alreadySeatedInTermRun.get())) {
            // all students are seated or it is necessary to stop, because manually placed students
            // must be placed on the end of the term run
            roomPlansLoopStop = true;
            break outerLoop;
          } else if (sortedStudentsList.size() == 0) {
            // all students are seated, but we still need to loop, because there are still manual placements
            // without seat number
            isSeatOccupied(testSchedules, newTermRun, cells.get(numCols * row + column), seatCounter);
            continue;
          } else if (roomStudentPlacement != null && (roomPlan.getCapacity() - roomStudentPlacement.getStudentLogins().size()) == alreadySeatedInRoom.get()) {
            // it is necessary to stop, because manually placed students must be placed on the end of the room
            break outerLoop;
          }
          placeNewStudentAtSeat(newTermDto.getDistribution(), sortedStudentsList, newTermRun,
            cells.get(numCols * row + column), testSchedules, seatCounter, alreadySeatedInRoom, alreadySeatedInTermRun);
        }
      }
      if (oldSeatCounterValue != seatCounter.get()) {
        goRight = !goRight;
      }
    }
    return roomPlansLoopStop;
  }

  private List<StudentsPlacementDto> getManuallyPlacedStudentsForCurrentRoomPlan(List<StudentsPlacementDto> studentPlacementsForCurrentTermRun, RoomPlan roomPlan) {
    return studentPlacementsForCurrentTermRun.stream()
      .filter(studentsPlacement -> (studentsPlacement.getRoomId() != null && studentsPlacement.getRoomId() == roomPlan.getRoomReference().getIdRoom()) &&
        (studentsPlacement.getSeatIds() == null || studentsPlacement.getSeatIds().size() == 0))
      .collect(Collectors.toList());
  }

  private StudentsPlacementDto getManuallyPlacedStudentsForTermRunTermRunEnd(List<StudentsPlacementDto> termRunStudentPlacements) {
    StudentsPlacementDto termRunStudentPlacement = null;
    if (termRunStudentPlacements.size() == 1) {
      termRunStudentPlacement = termRunStudentPlacements.get(0);
      if (termRunStudentPlacement.getRoomId() != null) {
        termRunStudentPlacement = null;
      }
    }
    return termRunStudentPlacement;
  }

  private void manuallyPlaceStudents(NewTermDto newTermDto, List<TermStudentOrderDto> sortedStudentsList,
                                     List<TermStudentOrderDto> manuallyPlacedStudentsList, AtomicInteger seatCounter,
                                     List<StudentsPlacementDto> termRunStudentPlacements, StudentsPlacementDto termRunStudentPlacement,
                                     TermRun newTermRun, List<TestSchedule> testSchedules, boolean roomPlansLoopStop,
                                     AtomicInteger alreadySeatedInTermRun, RoomPlan roomPlan,
                                     List<StudentsPlacementDto> roomStudentsPlacements, StudentsPlacementDto roomStudentPlacement,
                                     List<Cell> cells, int numRows, int numCols, int rowStart, AtomicInteger alreadySeatedInRoom) {
    if (roomStudentPlacement != null || (termRunStudentPlacement != null && roomPlansLoopStop) || sortedStudentsList.size() == 0) {
      List<TermStudentOrderDto> studentsToManuallyPlace = getStudentsToManuallyPlace(manuallyPlacedStudentsList, termRunStudentPlacements,
        termRunStudentPlacement, roomPlan, roomStudentsPlacements, roomStudentPlacement, alreadySeatedInRoom);

      if (studentsToManuallyPlace.size() == 0) {
        return;
      }

      manuallyPlaceStudentsInSnakePattern(newTermDto, seatCounter, newTermRun, testSchedules, alreadySeatedInTermRun,
        cells, numRows, numCols, rowStart, alreadySeatedInRoom, studentsToManuallyPlace);
    }
  }

  private List<TermStudentOrderDto> getStudentsToManuallyPlace(List<TermStudentOrderDto> manuallyPlacedStudentsList,
                                                               List<StudentsPlacementDto> termRunStudentPlacements,
                                                               StudentsPlacementDto termRunStudentPlacement, RoomPlan roomPlan,
                                                               List<StudentsPlacementDto> roomStudentsPlacements,
                                                               StudentsPlacementDto roomStudentPlacement, AtomicInteger alreadySeatedInRoom) {
    List<TermStudentOrderDto> studentsToManuallyPlace = new ArrayList<>();
    if (roomStudentPlacement != null) {
      studentsToManuallyPlace = extractManuallyPlacedStudents(manuallyPlacedStudentsList, roomStudentsPlacements);
    }
    if (termRunStudentPlacement != null) {
      int numberOfStudents = roomPlan.getCapacity() - alreadySeatedInRoom.get();
      studentsToManuallyPlace = extractTermRunManuallyPlacedStudents(manuallyPlacedStudentsList, termRunStudentPlacements, numberOfStudents);
    }
    return studentsToManuallyPlace;
  }

  private void manuallyPlaceStudentsInSnakePattern(NewTermDto newTermDto, AtomicInteger seatCounter, TermRun newTermRun,
                                                   List<TestSchedule> testSchedules, AtomicInteger alreadySeatedInTermRun,
                                                   List<Cell> cells, int numRows, int numCols, int rowStart,
                                                   AtomicInteger alreadySeatedInRoom, List<TermStudentOrderDto> studentsToManuallyPlace) {
    boolean goRight = true;
    for (int row = rowStart - 1; row < numRows; row++) {
      int oldSeatCounterValue = seatCounter.get();
      if (goRight) {
        for (int column = 0; column < numCols; column++) {
          placeNewStudentAtSeat(newTermDto.getDistribution(), studentsToManuallyPlace, newTermRun,
            cells.get(numCols * row + column), testSchedules, seatCounter, alreadySeatedInRoom, alreadySeatedInTermRun);
        }
      } else {
        for (int column = numCols - 1; column >= 0; column--) {
          placeNewStudentAtSeat(newTermDto.getDistribution(), studentsToManuallyPlace, newTermRun,
            cells.get(numCols * row + column), testSchedules, seatCounter, alreadySeatedInRoom, alreadySeatedInTermRun);
        }
      }
      if (oldSeatCounterValue != seatCounter.get()) {
        goRight = !goRight;
      }
    }
  }

  private void placeNewStudentAtSeat(Distribution distribution, List<TermStudentOrderDto> studentsList, TermRun newTermRun,
                                     Cell cell, List<TestSchedule> testSchedules, AtomicInteger seatCounter,
                                     AtomicInteger alreadySeatedInRoom, AtomicInteger alreadySeatedInTermRun) {
    if (cell.getCellType() == CellType.SEAT) {
      if (isSeatOccupied(testSchedules, newTermRun, cell, seatCounter) || studentsList.size() == 0) {
        return;
      }
      TermStudentOrderDto nextStudent;
      if (distribution == Distribution.RANDOM) {
        nextStudent = getRandomStudentFromList(studentsList);
      } else {
        nextStudent = getNextStudentFromList(studentsList);
      }
      testSchedules.add(
        new TestSchedule(nextStudent.getStudent(), newTermRun, cell, nextStudent.getOrderNumber(),
          seatCounter.getAndIncrement(), null)
      );
      alreadySeatedInRoom.incrementAndGet();
      alreadySeatedInTermRun.incrementAndGet();
    }
  }

  private void addAssignmentsToTestSchedules(List<Assignment> assignments, RoomPlan roomPlan,
                                             List<TestSchedule> testSchedules, boolean overrideExisting) {
    Room room = roomPlan.getRoomReference();
    List<Cell> cells = roomPlan.getCells();
    List<Assignment> localCopyOfAssignments = new ArrayList<>(assignments);
    for (int column = 0; column < room.getNumberOfColumns(); column++) {
      List<Assignment> columnAssignments = new ArrayList<>(localCopyOfAssignments);
      boolean isAnyAssignmentAddedInColumn = false;
      for (int row = 0; row < room.getNumberOfRows(); row++) {
        Cell cell = cells.get(row * room.getNumberOfColumns() + column);
        if (cell.getCellType() == CellType.SEAT) {
          isAnyAssignmentAddedInColumn = addAssignmentToTestScheduleAtSeat(testSchedules, overrideExisting,
            columnAssignments, isAnyAssignmentAddedInColumn, cell);
        }
        if (roomPlan.getSpacing() != SpacingBetweenStudents.ZERO) {
          shiftAssignments(columnAssignments);
        }
      }
      if (isAnyAssignmentAddedInColumn)
        shiftAssignments(localCopyOfAssignments);
    }
  }

  private boolean addAssignmentToTestScheduleAtSeat(List<TestSchedule> testSchedules, boolean overrideExisting,
                                                    List<Assignment> columnAssignments, boolean isAnyAssignmentAddedInColumn, Cell cell) {
    Optional<TestSchedule> testScheduleOpt = testSchedules.stream()
      .filter(ts -> ts.getCell().equals(cell))
      .findFirst();
    if (testScheduleOpt.isPresent()) {
      TestSchedule testSchedule = testScheduleOpt.get();
      if (testSchedule.getAssignment() == null || overrideExisting) {
        if (columnAssignments.size() == 0) {
          testSchedule.setAssignment(null);
        } else {
          testSchedule.setAssignment(columnAssignments.get(0));
        }
        testSchedule.setPdfPath(null);
        isAnyAssignmentAddedInColumn = true;
      }
    }
    return isAnyAssignmentAddedInColumn;
  }

  private void shiftAssignments(List<Assignment> assignments) {
    Collections.rotate(assignments, 1);
  }

  private boolean isSeatOccupied(List<TestSchedule> testSchedules, TermRun currentTermRun, Cell currentCell,
                                 AtomicInteger seatCounter) {
    for (TestSchedule testSchedule : testSchedules) {
      if (testSchedule.getCell() == currentCell && testSchedule.getTermRun() == currentTermRun) {
        if (testSchedule.getSeatNumber() == null) {
          testSchedule.setSeatNumber(seatCounter.getAndIncrement());
        }
        return true;
      }
    }
    return false;
  }

  private List<TermStudentOrderDto> extractManuallyPlacedStudents(List<TermStudentOrderDto> studentsList,
                                                                  List<StudentsPlacementDto> studentsPlacements) {
    List<TermStudentOrderDto> manuallyPlacedStudents = new ArrayList<>();
    if (studentsPlacements != null) {
      for (StudentsPlacementDto studentsPlacement : studentsPlacements) {
        for (String login : studentsPlacement.getStudentLogins()) {
          TermStudentOrderDto student = getAndRemoveStudentFromStudentList(studentsList, login);
          if (student == null) {
            continue;
          }
          manuallyPlacedStudents.add(student);
        }
      }
    }
    return manuallyPlacedStudents;
  }

  private List<TermStudentOrderDto> extractTermRunManuallyPlacedStudents(List<TermStudentOrderDto> studentsList,
                                                                         List<StudentsPlacementDto> studentsPlacements,
                                                                         int numberOfStudentsToExtract) {
    List<TermStudentOrderDto> manuallyPlacedStudents = new ArrayList<>();
    if (studentsPlacements != null) {
      outerLoop:
      for (StudentsPlacementDto studentsPlacement : studentsPlacements) {
        for (String login : studentsPlacement.getStudentLogins()) {
          TermStudentOrderDto student = getAndRemoveStudentFromStudentList(studentsList, login);
          if (student == null) {
            continue;
          }
          manuallyPlacedStudents.add(student);
          if (manuallyPlacedStudents.size() == numberOfStudentsToExtract) {
            break outerLoop;
          }
        }
      }
    }
    return manuallyPlacedStudents;
  }

  private TermStudentOrderDto getRandomStudentFromList(List<TermStudentOrderDto> students) {
    Random random = new Random();
    int randomNumber = random.nextInt(students.size());
    TermStudentOrderDto student = students.get(randomNumber);
    students.remove(randomNumber);
    return student;
  }

  private TermStudentOrderDto getNextStudentFromList(List<TermStudentOrderDto> students) {
    TermStudentOrderDto student = students.get(0);
    students.remove(0);
    return student;
  }

  private List<TestSchedule> doManualPlacementOfStudentsAtSpecificSeats(List<TermStudentOrderDto> studentList,
                                                                        List<StudentsPlacementDto> termRunStudentPlacements,
                                                                        TermRun newTermRun, Distribution distribution) {
    List<TestSchedule> testSchedules = new ArrayList<>();
    for (StudentsPlacementDto studentsPlacement : termRunStudentPlacements) {
      if (studentsPlacement.getRoomId() != null && studentsPlacement.getSeatIds() != null && studentsPlacement.getSeatIds().size() != 0) {
        List<String> logins = studentsPlacement.getStudentLogins();
        if (distribution == Distribution.RANDOM) {
          Collections.shuffle(logins);
        } else {
          Collections.sort(logins);
        }

        List<Integer> seatIds = studentsPlacement.getSeatIds();
        Collections.sort(seatIds);
        for (int i = 0; i < logins.size(); i++) {
          Cell cell = cellService.getCellById(studentsPlacement.getSeatIds().get(i))
            .orElseThrow(() -> new BadRequestException("Sedadlo neexistuje"));

          TermStudentOrderDto student = getAndRemoveStudentFromStudentList(studentList, logins.get(i));
          assert student != null;
          testSchedules.add(new TestSchedule(student.getStudent(), newTermRun, cell, student.getOrderNumber()));
        }
      }
    }
    return testSchedules;
  }

  private TermStudentOrderDto getAndRemoveStudentFromStudentList(List<TermStudentOrderDto> studentsList, String login) {
    List<TermStudentOrderDto> students = findStudentInInputStudentList(studentsList, login);
    if (students.size() > 0) {
      removeStudentFromInputStudentList(studentsList, login);
      return students.get(0);
    } else {
      return null;
    }
  }

  private List<TermStudentOrderDto> findStudentInInputStudentList(List<TermStudentOrderDto> students, String login) {
    return students.stream()
      .filter(dto -> dto.getStudent().getLogin().equals(login))
      .collect(Collectors.toList());
  }

  private void removeStudentFromInputStudentList(List<TermStudentOrderDto> students, String login) {
    students.removeIf(termStudentOrderDto ->
      termStudentOrderDto.getStudent().getLogin().equals(login)
    );
  }

  private List<RoomPlan> getSortedRoomPlansFromTermRunDto(List<Integer> roomIds, SpacingBetweenStudents spacing) {
    return roomIds.stream()
      .map(roomId -> this.roomPlanService.findRoomPlanByRoomIdAndSpacing(roomId, spacing))
      .sorted(
        Comparator.comparing(RoomPlan::getCapacity).reversed()
          .thenComparing(roomPlan -> roomPlan.getRoomReference().getAbbreviation())
          .thenComparing(roomPlan -> roomPlan.getRoomReference().getName())
      )
      .collect(Collectors.toList());
  }

  private void doEditTermValidations(EditTermDto editTermDto, Integer termId) {
    List<LoginNameDto> studentsList = new ArrayList<>(editTermDto.getStudentsList());
    studentsList.addAll(editTermDto.getOldStudentsList());

    doStudentListValidations(studentsList, editTermDto.getTermRuns(), editTermDto.getSpacingBetweenStudents());
    doTermRunValidations(editTermDto.getTermRuns(), termId);
    doStudentsPlacementsValidations(editTermDto.getStudentsPlacements(), editTermDto.getSpacingBetweenStudents(),
      editTermDto.getTermRuns(), studentsList);
    this.appUserService.checkAdditionalManagers(editTermDto.getAdditionalTermManagersEmails());
  }

  private void doStudentListValidations(List<LoginNameDto> studentsList, List<TermRunDto> termRuns,
                                        SpacingBetweenStudents spacingBetweenStudents) {
    checkIfStudentsFitsToRooms(studentsList, termRuns, spacingBetweenStudents);
    checkStudentListDuplicates(studentsList);
    checkIfStudentsAreStudents(studentsList);
  }

  private void checkIfStudentsFitsToRooms(List<LoginNameDto> students, List<TermRunDto> termRuns,
                                          SpacingBetweenStudents spacing) {
    int totalRoomCapacity = 0;
    for (TermRunDto termRun : termRuns) {
      for (Integer roomId : termRun.getRooms()) {
        RoomPlan roomPlan = this.roomPlanService.findRoomPlanByRoomIdAndSpacing(roomId, spacing);
        if (roomPlan != null) {
          totalRoomCapacity += roomPlan.getCapacity();
        }
      }
    }
    if (students.size() > totalRoomCapacity) {
      throw new BadRequestException("Není možné usadit všechny studenty, nebyla vybrána dostatečná kapacita míst v místnostech");
    }
  }

  private void checkStudentListDuplicates(List<LoginNameDto> studentsList) {
    Set<String> lump = new HashSet<>();
    for (LoginNameDto dto : studentsList) {
      if (lump.contains(dto.getLogin())) {
        throw new BadRequestException("Student s loginem '" + dto.getLogin() + "' je v seznamu vícekrát.\n" +
          "Odstraňte prosím duplicitní záznamy.");
      }
      lump.add(dto.getLogin());
    }
  }

  private void checkIfStudentsAreStudents(List<LoginNameDto> studentsList) {
    for (LoginNameDto dto : studentsList) {
      AppUser student = appUserService.findUserByLogin(dto.getLogin());
      if (student != null) {
        for (Role role : student.getRoles()) {
          if (role.getRoleName() != RoleName.STUDENT) {
            throw new BadRequestException("Uživatel s loginem " + dto.getLogin() + " není student");
          }
        }
      }
    }
  }

  private void doTermRunValidations(List<TermRunDto> termRuns, Integer termId) {
    checkIfTermRunIdsAreUnique(termRuns);
    checkSelectedRoomsDuplicates(termRuns);
    checkIfTimesIsCorrect(termRuns);
    checkTermRunCollisions(termRuns, termId);
    checkIfSelectedRoomsExist(termRuns);
    checkSelectedAssignmentsDuplicates(termRuns);
    checkIfSelectedAssignmentsExist(termRuns);
  }

  private void checkIfTermRunIdsAreUnique(List<TermRunDto> termRuns) {
    Set<Integer> lump = new HashSet<>();
    for (TermRunDto termRun : termRuns) {
      if (lump.contains(termRun.getId())) {
        throw new BadRequestException("Duplicitní ID běhu: " + termRun.getId() + ". ID běhu musí být unikátní");
      } else {
        lump.add(termRun.getId());
      }
    }
  }

  private void checkSelectedRoomsDuplicates(List<TermRunDto> termRuns) {
    for (TermRunDto termRun : termRuns) {
      Set<Integer> lump = new HashSet<>();
      for (Integer roomId : termRun.getRooms()) {
        if (lump.contains(roomId)) {
          throw new BadRequestException("Místnost s ID " + roomId + " je u běhu s ID " + termRun.getId() +
            " vybrána vícekrát.\nOdstraňte prosím duplicitní záznamy.");
        }
        lump.add(roomId);
      }
    }
  }

  private void checkTermRunCollisions(List<TermRunDto> termRuns, Integer termId) {
    for (int i = 0; i < termRuns.size(); i++) {
      TermRunDto currentTr = termRuns.get(i);
      long currentTermRunStartTime = currentTr.getStartTime();
      long currentTermRunEndTime = currentTr.getEndTime();
      List<Integer> currentTermRunsRoomIds = currentTr.getRooms();
      for (int j = i + 1; j < termRuns.size(); j++) {
        checkSameRoomOverlap(termRuns, currentTr, currentTermRunStartTime, currentTermRunEndTime, currentTermRunsRoomIds, j);
      }
      List<Room> freeRooms = this.roomService.findFreeRooms(currentTermRunStartTime, currentTermRunEndTime, termId);
      List<Integer> freeRoomsIds = freeRooms.stream()
        .map(Room::getIdRoom)
        .collect(Collectors.toList());
      boolean isTermRunInFreeRoom = currentTermRunsRoomIds.stream().anyMatch(freeRoomsIds::contains);
      if (!isTermRunInFreeRoom) {
        throw new BadRequestException("Běh s ID " + currentTr.getId() + " je v kolizi s jiným již existujícím během");
      }
    }
  }

  private void checkSameRoomOverlap(List<TermRunDto> termRuns, TermRunDto currentTr, long currentTermRunStartTime, long currentTermRunEndTime, List<Integer> currentTermRunsRoomIds, int j) {
    TermRunDto tr = termRuns.get(j);
    List<Integer> roomIds = tr.getRooms();
    boolean sameRoom = roomIds.stream().anyMatch(currentTermRunsRoomIds::contains);
    if (sameRoom) {
      boolean startTimeOverlap = (tr.getStartTime() > currentTermRunStartTime && tr.getStartTime() < currentTermRunEndTime);
      boolean endTimeOverlap = (tr.getEndTime() > currentTermRunStartTime && tr.getEndTime() < currentTermRunEndTime);
      boolean insideOverlap = (tr.getStartTime() < currentTermRunStartTime && tr.getEndTime() > currentTermRunEndTime);
      boolean sameOverlap = (tr.getStartTime() == currentTermRunStartTime && tr.getEndTime() == currentTermRunEndTime);
      if (startTimeOverlap || endTimeOverlap || insideOverlap || sameOverlap) {
        throw new BadRequestException("Běh s ID " + currentTr.getId() + ": Kolize s během s ID " + tr.getId() + ".\n" +
          "Zkontrolujte, zda se běhy nekonají ve stejný čas ve stejné místnosti");
      }
    }
  }

  private void checkIfSelectedRoomsExist(List<TermRunDto> termRuns) {
    for (TermRunDto tr : termRuns) {
      for (Integer roomId : tr.getRooms()) {
        if (this.roomService.findRoomById(roomId).isEmpty()) {
          throw new BadRequestException("Místnost s ID " + roomId + " u běhu s ID " + tr.getId() + " neexistuje");
        }
      }
    }
  }

  private void checkSelectedAssignmentsDuplicates(List<TermRunDto> termRuns) {
    for (TermRunDto termRun : termRuns) {
      Set<Integer> lump = new HashSet<>();
      if (termRun.getAssignments() == null) {
        continue;
      }
      for (Integer assignmentId : termRun.getAssignments()) {
        if (lump.contains(assignmentId)) {
          throw new BadRequestException("Šablona zadání s ID " + assignmentId + " je u běhu s ID " + termRun.getId() +
            " vybráno vícekrát.\nOdstraňte prosím duplicitní záznamy.");
        }
        lump.add(assignmentId);
      }
    }
  }

  private void checkIfSelectedAssignmentsExist(List<TermRunDto> termRuns) {
    for (TermRunDto tr : termRuns) {
      if (tr.getAssignments() == null) {
        continue;
      }
      for (Integer assignmentId : tr.getAssignments()) {
        if (this.assignmentService.findAssignmentById(assignmentId).isEmpty()) {
          throw new BadRequestException("Šablona zadání s ID " + assignmentId + " u běhu s ID " + tr.getId() + " neexistuje");
        }
      }
    }
  }

  private void checkIfTimesIsCorrect(List<TermRunDto> termRuns) {
    for (TermRunDto tr : termRuns) {
      if (tr.getStartTime() >= tr.getEndTime()) {
        throw new BadRequestException("Běh s ID " + tr.getId() + ": Čas začátku termínu zkoušky nesmí být později, než čas konce termínu zkoušky");
      } else if (tr.getStartTime() < new Date().getTime()) {
        throw new BadRequestException("Běh s ID " + tr.getId() + ": Termín zkoušky nesmí začínat v minulosti");
      }
    }
  }

  private void doStudentsPlacementsValidations(List<StudentsPlacementDto> studentsPlacements,
                                               SpacingBetweenStudents spacingBetweenStudents,
                                               List<TermRunDto> termRuns, List<LoginNameDto> studentsList) {
    if (studentsPlacements == null) {
      return;
    }
    checkAttributesCombination(studentsPlacements);
    checkIfStudentExist(studentsList, studentsPlacements);
    checkIfTermRunExist(termRuns, studentsPlacements);
    checkIfTermRunHasRoomId(termRuns, studentsPlacements);
    checkIfTermWasSelectedMultipleTimesWithoutRoom(studentsPlacements);
    checkIfTermRunHasRoomSelectedMultipleTimes(studentsPlacements);
    checkIfStudentsFitsToTermRunRoom(studentsPlacements, spacingBetweenStudents);
    checkIfStudentsFitsToTermRun(studentsPlacements, spacingBetweenStudents, termRuns);
    checkIfRoomHasSeatIds(studentsPlacements, spacingBetweenStudents);
    checkIfThereIsSameNumberOfStudentsAndSelectedSeats(studentsPlacements);
    checkSeatIdDuplicates(studentsPlacements);
    checkStudentPlacementDuplicates(studentsPlacements);
  }

  private void checkAttributesCombination(List<StudentsPlacementDto> studentsPlacements) {
    for (StudentsPlacementDto sp : studentsPlacements) {
      if (sp.getSeatIds() != null && sp.getSeatIds().size() > 0 && sp.getRoomId() == null) {
        throw new BadRequestException("Studenty nelze usadit na konkrétní místa, pokud nebyla vybrána i odpovídající místnost");
      }
    }
  }

  private void checkIfStudentExist(List<LoginNameDto> studentsList, List<StudentsPlacementDto> studentsPlacements) {
    List<String> logins = studentsList.stream().map(LoginNameDto::getLogin).collect(Collectors.toList());
    for (StudentsPlacementDto sp : studentsPlacements) {
      for (String login : sp.getStudentLogins()) {
        if (!logins.contains(login)) {
          throw new BadRequestException("Studenta nelze manuálně usadit. Student s loginem '" + login + "' neexistuje. " +
            "Ujistěte se, že student s loginem '" + login + "' nebyl odstraněn ze vstupního seznamu.");
        }
      }
    }
  }

  private void checkIfTermRunExist(List<TermRunDto> termRuns, List<StudentsPlacementDto> studentsPlacements) {
    List<Integer> termRunIds = termRuns.stream().map(TermRunDto::getId).collect(Collectors.toList());
    for (StudentsPlacementDto sp : studentsPlacements) {
      if (!termRunIds.contains(sp.getTermRunId())) {
        throw new BadRequestException("Studenty nelze manuálně usadit. Běh s ID " + sp.getTermRunId() + " neexistuje");
      }
    }
  }

  private void checkIfTermRunHasRoomId(List<TermRunDto> termRuns, List<StudentsPlacementDto> studentsPlacements) {
    for (StudentsPlacementDto sp : studentsPlacements) {
      if (sp.getRoomId() == null) {
        continue;
      }
      for (TermRunDto tr : termRuns) {
        if (tr.getId().equals(sp.getTermRunId())) {
          if (!tr.getRooms().contains(sp.getRoomId())) {
            throw new BadRequestException("Studenty nelze manuálně usadit. Místnost s ID " + sp.getRoomId() +
              " v běhu s ID " + tr.getId() + " nebyla vybrána.");
          }
        }
      }
    }
  }

  private void checkIfTermWasSelectedMultipleTimesWithoutRoom(List<StudentsPlacementDto> studentsPlacement) {
    Set<Integer> studentsPlacementsWithoutRoom = studentsPlacement.stream()
      .filter(placement -> placement.getRoomId() == null)
      .map(StudentsPlacementDto::getTermRunId)
      .collect(Collectors.toSet());

    Set<Integer> lump = new HashSet<>();
    for (StudentsPlacementDto placement : studentsPlacement) {
      if (lump.contains(placement.getTermRunId())) {
        if (placement.getRoomId() == null || studentsPlacementsWithoutRoom.contains(placement.getTermRunId())) {
          throw new BadRequestException("Studenty nelze manuálně usadit. Běh s ID: " + placement.getTermRunId() +
            " již byl alespoň jednou vybrán, a to bez místnosti.\n" +
            "Pokud chcete tento běh vybrat vícekrát, vyberte pokaždé různou místnost");
        }
      } else {
        lump.add(placement.getTermRunId());
      }
    }
  }

  private void checkIfTermRunHasRoomSelectedMultipleTimes(List<StudentsPlacementDto> studentsPlacement) {
    for (StudentsPlacementDto studentPlacement : studentsPlacement) {
      List<StudentsPlacementDto> studentsPlacementsInSameTermRun = studentsPlacement.stream()
        .filter(placement -> placement.getTermRunId().equals(studentPlacement.getTermRunId()))
        .collect(Collectors.toList());

      Set<Integer> lump = new HashSet<>();
      for (StudentsPlacementDto sameTermPlacement : studentsPlacementsInSameTermRun) {
        if (lump.contains(sameTermPlacement.getRoomId())) {
          throw new BadRequestException("Studenty nelze manuálně usadit. Běh s ID: " + sameTermPlacement.getTermRunId() +
            ". Místnost s ID " + sameTermPlacement.getRoomId() + " byla u tohoto běhu již vybrána");
        } else {
          lump.add(sameTermPlacement.getRoomId());
        }
      }
    }
  }

  private void checkIfStudentsFitsToTermRunRoom(List<StudentsPlacementDto> studentsPlacements, SpacingBetweenStudents spacing) {
    for (StudentsPlacementDto placement : studentsPlacements) {
      if (placement.getRoomId() == null) {
        continue;
      }
      RoomPlan roomPlan = this.roomPlanService.findRoomPlanByRoomIdAndSpacing(placement.getRoomId(), spacing);
      if (roomPlan != null && placement.getStudentLogins().size() > roomPlan.getCapacity()) {
        throw new BadRequestException("Studenty nelze manuálně usadit. Běh s ID: " + placement.getTermRunId() +
          ", vybraní studenti se nevejdou do vybrané místnosti.");
      }
    }
  }

  private void checkIfStudentsFitsToTermRun(List<StudentsPlacementDto> studentsPlacements, SpacingBetweenStudents spacing,
                                            List<TermRunDto> termRuns) {
    for (StudentsPlacementDto placement : studentsPlacements) {
      if (placement.getRoomId() != null) {
        continue;
      }
      TermRunDto termRunDto = termRuns.stream()
        .filter(termRun -> termRun.getId().equals(placement.getTermRunId()))
        .collect(Collectors.toList()).get(0);

      int termRunCapacity = 0;
      for (Integer roomId : termRunDto.getRooms()) {
        RoomPlan roomPlan = this.roomPlanService.findRoomPlanByRoomIdAndSpacing(roomId, spacing);
        termRunCapacity += roomPlan.getCapacity();
      }

      if (placement.getStudentLogins().size() > termRunCapacity) {
        throw new BadRequestException("Studenty nelze manuálně usadit. Běh s ID: " + placement.getTermRunId() +
          ", vybraní studenti se nevejdou do vybraného běhu.");
      }
    }
  }

  private void checkIfRoomHasSeatIds(List<StudentsPlacementDto> studentsPlacements, SpacingBetweenStudents spacingBetweenStudents) {
    for (StudentsPlacementDto sp : studentsPlacements) {
      if (sp.getRoomId() == null) {
        continue;
      }
      Room room = this.roomService.findRoomById(sp.getRoomId())
        .orElseThrow(() -> new BadRequestException("Místnost neexistuje"));
      List<Integer> roomSeatIds = cellService.getRoomPlanSeatIds(
        room.getRoomPlans().get(spacingBetweenStudents.getSpacing())
      );
      if (sp.getSeatIds() == null || sp.getSeatIds().size() == 0) {
        continue;
      }
      for (int seatId : sp.getSeatIds()) {
        if (!roomSeatIds.contains(seatId)) {
          throw new BadRequestException("Studenty nelze manuálně usadit. Sedadlo s ID " + seatId
            + " v místnosti s ID " + sp.getRoomId() + " neexistuje");
        }
      }
    }
  }

  private void checkIfThereIsSameNumberOfStudentsAndSelectedSeats(List<StudentsPlacementDto> studentsPlacements) {
    for (StudentsPlacementDto sp : studentsPlacements) {
      if (sp.getSeatIds() == null || sp.getSeatIds().size() == 0) {
        continue;
      }
      if (sp.getRoomId() != null && sp.getSeatIds().size() > 0 &&
        sp.getStudentLogins().size() != sp.getSeatIds().size()) {
        throw new BadRequestException("Studenty nelze manuálně usadit. Běh s ID " + sp.getTermRunId() +
          ", místnost s ID " + sp.getRoomId() + ": Počet usazovaných studentů neodpovídá počtu vybraných míst");
      }
    }
  }

  private void checkSeatIdDuplicates(List<StudentsPlacementDto> studentsPlacements) {
    for (StudentsPlacementDto sp : studentsPlacements) {
      if (sp.getSeatIds() == null || sp.getSeatIds().size() == 0) {
        continue;
      }
      Set<Integer> lump = new HashSet<>();
      for (Integer seatId : sp.getSeatIds()) {
        if (lump.contains(seatId)) {
          throw new BadRequestException("Studenty nelze manuálně usadit. Běh s ID " + sp.getTermRunId() +
            ", místnost s ID " + sp.getRoomId() + ": Sedadlo s ID " + seatId + " je vybráno vícekrát.");
        }
        lump.add(seatId);
      }
    }
  }

  private void checkStudentPlacementDuplicates(List<StudentsPlacementDto> studentsPlacements) {
    List<String> logins = new ArrayList<>();
    for (StudentsPlacementDto sp : studentsPlacements) {
      logins.addAll(sp.getStudentLogins());
    }
    Set<String> lump = new HashSet<>();
    for (String login : logins) {
      if (lump.contains(login)) {
        throw new BadRequestException("Studenty nelze manuálně usadit. Student s loginem '" + login +
          "' je usazen vícekrát.");
      }
      lump.add(login);
    }
  }

  private void addAllTermRunStudentLoginsToSet(TermRun termRun, Set<String> recipientLogins) {
    List<String> logins = termRun.getTestSchedules()
      .stream()
      .map(testSchedule -> testSchedule.getStudent().getLogin())
      .collect(Collectors.toList());
    recipientLogins.addAll(logins);
  }
}
