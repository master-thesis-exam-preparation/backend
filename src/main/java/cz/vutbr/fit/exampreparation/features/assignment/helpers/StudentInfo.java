package cz.vutbr.fit.exampreparation.features.assignment.helpers;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Used to store personal information about students that will be added to .tex files
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentInfo {
  private String login;
  private String studentName;
  private String term;
  private String date;
  private int studentOrderNumber;
  private String roomAbbreviation;
  private int seatNumber;
  private int rowNumber;
  private int columnNumber;
}
