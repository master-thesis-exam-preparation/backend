package cz.vutbr.fit.exampreparation.features.appUser.account;

import cz.vutbr.fit.exampreparation.common.AuthService;
import cz.vutbr.fit.exampreparation.common.MessageResponseDto;
import cz.vutbr.fit.exampreparation.common.UserInfo;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.BadRequestException;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.BaseException;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.NotFoundException;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.UnauthorizedException;
import cz.vutbr.fit.exampreparation.features.appUser.AppUser;
import cz.vutbr.fit.exampreparation.features.appUser.AppUserService;
import cz.vutbr.fit.exampreparation.features.appUser.dto.*;
import cz.vutbr.fit.exampreparation.features.role.Role;
import cz.vutbr.fit.exampreparation.features.role.RoleName;
import cz.vutbr.fit.exampreparation.features.role.RoleService;
import cz.vutbr.fit.exampreparation.features.token.Token;
import cz.vutbr.fit.exampreparation.features.token.TokenService;
import cz.vutbr.fit.exampreparation.features.token.TokenType;
import cz.vutbr.fit.exampreparation.mail.MailBuilder;
import cz.vutbr.fit.exampreparation.mail.MailComponents;
import cz.vutbr.fit.exampreparation.mail.MailNotificationSender;
import cz.vutbr.fit.exampreparation.security.jwt.JwtTokenUtil;
import cz.vutbr.fit.exampreparation.security.jwt.dto.AccessTokenDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import static cz.vutbr.fit.exampreparation.common.RegexTester.testRegex;
import static org.springframework.http.HttpStatus.*;

/**
 * This service class is responsible for management of accounts of all application users.
 */
@Service
@PropertySource(value = "application.properties")
public class AccountServiceImpl implements AccountService {

  private final TokenService tokenService;
  private final MailNotificationSender mailNotificationSender;
  private final MailBuilder mailBuilder;
  private final AppUserService appUserService;
  private final AuthenticationManager authenticationManager;
  private final JwtTokenUtil jwtTokenUtil;
  private final PasswordEncoder passwordEncoder;
  private final RoleService roleService;

  /**
   * Creates new instance of {@code AccountServiceImpl}.
   *
   * @param tokenService  service class that works with tokens stored in database
   * @param mailNotificationSender  service that provides sending e-mail messages
   * @param mailBuilder  that provides creating e-mail messages
   * @param appUserService  service that works with users data stored in database
   * @param authenticationManager  class that is responsible for users authentication
   * @param jwtTokenUtil  class that is responsible for parsing JSON Web Tokens
   * @param passwordEncoder  class that is responsible for encoding passwords and other text
   * @param roleService  service class that works with user roles stored in database
   */
  @Autowired
  public AccountServiceImpl(TokenService tokenService, MailNotificationSender mailNotificationSender,
                            MailBuilder mailBuilder, AppUserService appUserService,
                            AuthenticationManager authenticationManager, JwtTokenUtil jwtTokenUtil,
                            PasswordEncoder passwordEncoder, RoleService roleService) {
    this.tokenService = tokenService;
    this.mailNotificationSender = mailNotificationSender;
    this.mailBuilder = mailBuilder;
    this.appUserService = appUserService;
    this.authenticationManager = authenticationManager;
    this.jwtTokenUtil = jwtTokenUtil;
    this.passwordEncoder = passwordEncoder;
    this.roleService = roleService;
  }

  @Value("${server.port:}")
  private String serverPort;

  @Value("${frontend.url}")
  private String frontendUrl;

  /**
   * {@inheritDoc}
   */
  @Override
  @Transactional
  public ResponseEntity<MessageResponseDto> createNewUserAccount(RegisterUserDto userDto) {
    boolean isPasswordSafe = isPasswordSave(userDto.getPassword());

    if (!isPasswordSafe) {
      return ResponseEntity.badRequest()
        .body(new MessageResponseDto("Heslo musí obsahovat číslici, malé a velké písmeno a speciální znak a musí být delší než 8 znaků"));
    }

    if (!testRegex("^.*@((?:(?:stud)\\.)?(?:(?:fit|feec|fce|fbm|fch|favu|fme|usi|fa)\\.))?(?:vut|vutbr)\\.cz$", userDto.getEmail())) {
      return ResponseEntity.badRequest()
        .body(new MessageResponseDto("E-mail neodpovídá tvaru VUT"));
    }

    if (appUserService.findByEmail(userDto.getEmail()).isPresent()) {
      return ResponseEntity.status(CONFLICT)
        .body(new MessageResponseDto("Tento e-mail je již zaregistrován."));
    }

    try {
      AppUser newAppUser = appUserService.encodePasswordSetRoleAndSave(getUserFromDto(userDto));
      String token = tokenService.generateToken(newAppUser, TokenType.VERIFICATION, 48, false);
      mailNotificationSender.sendMail(new MailComponents(
        new String[]{newAppUser.getEmail()},
        "Potrzení registrace",
        mailBuilder.buildAccountVerificationMail("http://" + InetAddress.getLocalHost().getCanonicalHostName() + ":" + serverPort + "/auth/verify/" + token)
      ), "zkousky@minerva3.fit.vutbr.cz");
      return ResponseEntity.ok()
        .body(new MessageResponseDto("Na vaši e-mailovou adresu " + newAppUser.getEmail() + " byl zaslán odkaz pro potvrzení vaší registrace."));
    } catch (Exception e) {
      throw new RuntimeException(e.getMessage());
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  @Transactional
  public ResponseEntity<MessageResponseDto> verifyAccount(String verificationToken) {
    try {
      Token token = tokenService.getTokenIfIsValid(verificationToken);
      if (token.getTokenType() == TokenType.VERIFICATION) {
        enableAccount(token.getAppUserReference());
        return ResponseEntity.status(FOUND)
          .header(HttpHeaders.LOCATION, frontendUrl + "/rozsazeni/auth/login")
          .body(new MessageResponseDto("Vaše registrace byla potvrzena a účet aktivován.\nMůžete se přihlásit."));
      } else {
        throw new UnauthorizedException("Neexistuje token pro verifikování účtu");
      }
    } catch (BaseException e) {
      throw new UnauthorizedException("Tento token neexistuje nebo je neplatný. Zaregistrujte se prosím znovu.");
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseEntity<AccessTokenDto> generateNewKeys(String refreshToken) {
    return tokenService.generateNewKeys(refreshToken);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseEntity<AccessTokenDto> loginUser(LoginUserDto userDto) {
    try {
      Authentication auth = authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(userDto.getEmail(), userDto.getPassword())
      );

      SecurityContextHolder.getContext().setAuthentication(auth);
    } catch (BadCredentialsException e) {
      throw new UnauthorizedException("Neznámý e-mail nebo heslo");
    } catch (DisabledException e) {
      throw new UnauthorizedException("Účet není aktivován. Aktivujte ho pomocí odkazu v obdržené e-mailové zprávě.");
    } catch (LockedException e) {
      throw new UnauthorizedException("Učet je zablokován. Kontaktujte administrátora systému.");
    } catch (Exception e) {
      throw new UnauthorizedException(e.getMessage());
    }

    return setTokensToResponse(userDto);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseEntity<MessageResponseDto> logoutUser(String refreshToken) {
    if (refreshToken != null) {
      try {
        Token token = tokenService.getTokenIfIsValid(refreshToken);
        if (token.getTokenType() == TokenType.REFRESH) {
          tokenService.deleteByTokenId(token.getIdToken());
        }
      } catch (BaseException e) {
        throw new RuntimeException(e.getMessage());
      }
    }

    SecurityContextHolder.clearContext();

    return ResponseEntity.ok()
      .header(HttpHeaders.SET_COOKIE, tokenService.deleteRefreshTokenCookie().toString())
      .body(new MessageResponseDto("Odhlášení proběhlo úspěšně"));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseEntity<AccessTokenDto> logMeIn(String refreshToken) {
    try {
      return this.generateNewKeys(refreshToken);
    } catch (BaseException e) {
      return ResponseEntity.ok()
        .body(null);
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseEntity<MessageResponseDto> forgetPassword(EmailDto emailDto) {
    if (!testRegex("^.*@((?:(?:stud)\\.)?(?:(?:fit|feec|fce|fbm|fch|favu|fme|usi|fa)\\.))?(?:vut|vutbr)\\.cz$", emailDto.getEmail())) {
      return ResponseEntity.badRequest()
        .body(new MessageResponseDto("E-mail neodpovídá tvaru VUT"));
    }

    AppUser appUser = appUserService.findByEmail(emailDto.getEmail())
      .orElseThrow(() -> new BadRequestException("Pro tento e-mail neexistuje žádný účet"));

    for (Token token : appUser.getUserTokens()) {
      if (token.getTokenType() == TokenType.FORGET_PASSWORD) {
        throw new BadRequestException("E-mail s odkazem pro obnovení hesla vám jíž byl zaslán.");
      }
    }

    try {
      String token = tokenService.generateToken(appUser, TokenType.FORGET_PASSWORD, 48, false);
      mailNotificationSender.sendMail(new MailComponents(
        new String[]{emailDto.getEmail()},
        "Odkaz pro obnovení hesla",
        mailBuilder.buildForgetPasswordEmail(frontendUrl + "/rozsazeni/auth/reset-password?token=" + token)
      ), "zkousky@minerva3.fit.vutbr.cz");
      return ResponseEntity.ok()
        .body(new MessageResponseDto("Na vaši e-mailovou adresu " + emailDto.getEmail() + " byl zaslán odkaz pro obnovení hesla."));
    } catch (Exception e) {
      throw new RuntimeException(e.getMessage());
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseEntity<MessageResponseDto> resetPassword(NewPasswordDto newPasswordDto) {
    boolean isPasswordSafe = isPasswordSave(newPasswordDto.getPassword());

    if (!isPasswordSafe) {
      return ResponseEntity.badRequest()
        .body(new MessageResponseDto("Heslo musí obsahovat číslici, malé a velké písmeno a speciální znak a musí být delší než 8 znaků"));
    }

    try {
      Token token = tokenService.getTokenIfIsValid(newPasswordDto.getToken());
      if (token.getTokenType() != TokenType.FORGET_PASSWORD) {
        throw new BadRequestException("Neplatný token pro obnovení hesla.");
      }
      AppUser appUser = token.getAppUserReference();
      appUser.setPassword(passwordEncoder.encode(newPasswordDto.getPassword()));
      appUserService.save(appUser);
      tokenService.deleteByTokenId(token.getIdToken());
      return ResponseEntity.ok()
        .body(new MessageResponseDto("Vaše heslo bylo změněno, můžete se přihlásit"));
    } catch (BaseException e) {
      throw new UnauthorizedException("Token pro obnovení hesla neexistuje nebo je neplatný.");
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseEntity<UserDataDto> getUserData() {
    UserInfo userInfo = AuthService.getLoggedUserInfo();
    AppUser appUser = appUserService.findByEmail(userInfo.getEmail())
      .orElseThrow(() -> new NotFoundException("Uživatel s e-mailem " + userInfo.getEmail() + " neexistuje"));

    return ResponseEntity.ok()
      .body(
        new UserDataDto(appUser.getDegreesBeforeName(), appUser.getDegreesBehindName(), appUser.getFirstName(), appUser.getLastName()
      ));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseEntity<MessageResponseDto> changePassword(ChangePasswordDto changePasswordDto) {
    UserInfo userInfo = AuthService.getLoggedUserInfo();
    try {
      authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(userInfo.getEmail(), changePasswordDto.getCurrentPassword())
      );
    } catch (BadCredentialsException e) {
      throw new UnauthorizedException("Současné heslo je nesprávné.");
    } catch (DisabledException e) {
      throw new UnauthorizedException("Účet není aktivován. Aktivujte ho pomocí odkazu v obdržené e-mailové zprávě.");
    } catch (LockedException e) {
      throw new UnauthorizedException("Učet je zablokován. Kontaktujte administrátora systému.");
    } catch (Exception e) {
      throw new UnauthorizedException(e.getMessage());
    }

    boolean isPasswordSafe = isPasswordSave(changePasswordDto.getNewPassword());

    if (!isPasswordSafe) {
      return ResponseEntity.badRequest()
        .body(new MessageResponseDto("Heslo musí obsahovat číslici, malé a velké písmeno a speciální znak a musí být delší než 8 znaků"));
    }
    AppUser appUser = appUserService.findByEmail(userInfo.getEmail())
      .orElseThrow(() -> new NotFoundException("Uživatel s e-mailem " + userInfo.getEmail() + " neexistuje"));

    appUser.setPassword(passwordEncoder.encode(changePasswordDto.getNewPassword()));
    appUserService.save(appUser);
    return ResponseEntity.ok()
      .body(new MessageResponseDto("Vaše heslo bylo změněno."));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseEntity<MessageResponseDto> changeUserDetails(UserDataDto userDataDto) {
    UserInfo userInfo = AuthService.getLoggedUserInfo();

    AppUser appUser = appUserService.findByEmail(userInfo.getEmail())
      .orElseThrow(() -> new NotFoundException("Uživatel s e-mailem " + userInfo.getEmail() + " neexistuje"));
    appUser.setDegreesBeforeName(userDataDto.getDegreesBeforeName());
    appUser.setDegreesBehindName(userDataDto.getDegreesBehindName());
    appUser.setFirstName(userDataDto.getFirstName());
    appUser.setLastName(userDataDto.getLastName());

    appUserService.save(appUser);
    return ResponseEntity.ok()
      .body(new MessageResponseDto("Vaše údaje byly úspěšně změněny."));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseEntity<List<AccountInfoDto>> getAllUsersData() {
    return ResponseEntity.ok()
      .body(appUserService.getAllAccountsInfo());
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseEntity<MessageResponseDto> changeAccountRole(NewRoleDto newRole, int userId) {
    AppUser appUser = appUserService.findById(userId)
      .orElseThrow(() -> new NotFoundException("Uživatel s e-mailem + userInfo.getEmail() neexistuje"));
    Role role = roleService.findRole(RoleName.valueOf(newRole.getRole()))
      .orElseThrow(() -> new NotFoundException("Role s názvem " + newRole.getRole() + " nebyla nalezena."));
    List<Role> roles = new ArrayList<>();
    roles.add(role);
    appUser.setRoles(roles);

    appUserService.save(appUser);
    return ResponseEntity.ok()
      .body(new MessageResponseDto("Role uživatele s e-mailem " + appUser.getEmail() + " byla úspěšně změněna."));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseEntity<MessageResponseDto> deleteAccount(int userId) {
    appUserService.deleteUserById(userId);
    return ResponseEntity.ok()
      .body(new MessageResponseDto("Uživatelský účet byl úspěšně odstraněn."));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseEntity<MessageResponseDto> lockAccount(int userId) {
    AppUser appUser = appUserService.findById(userId)
      .orElseThrow(() -> new NotFoundException("Uživatel neexistuje"));
    appUser.setLocked(true);

    appUserService.save(appUser);
    return ResponseEntity.ok()
      .body(new MessageResponseDto("Uživatelský účet byl úspěšně zablokován."));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseEntity<MessageResponseDto> unlockAccount(int userId) {
    AppUser appUser = appUserService.findById(userId)
      .orElseThrow(() -> new NotFoundException("Uživatel neexistuje"));
    appUser.setLocked(false);

    appUserService.save(appUser);
    return ResponseEntity.ok()
      .body(new MessageResponseDto("Uživatelský účet byl úspěšně odblokován."));
  }

  private boolean isPasswordSave(String password) {
    return testRegex("\\d", password) &&
      testRegex("[A-Z]", password) &&
      testRegex("[a-z]", password) &&
      testRegex("\\W|_", password) &&
      password.length() >= 8;
  }

  private ResponseEntity<AccessTokenDto> setTokensToResponse(LoginUserDto userDto) {
    AccessTokenDto accessTokenDto = new AccessTokenDto(
      jwtTokenUtil.generateAccessToken(userDto.getEmail())
    );

    tokenService.deleteAllUserTokens(userDto.getEmail(), TokenType.REFRESH);

    ResponseCookie refreshTokenCookie = tokenService.createRefreshTokenCookie(userDto.getEmail(), userDto.isRememberMe());

    return ResponseEntity.status(OK)
      .header(HttpHeaders.SET_COOKIE, refreshTokenCookie.toString())
      .body(accessTokenDto);
  }


  private void enableAccount(AppUser appUser) {
    appUser.setVerified(true);
    appUserService.save(appUser);
  }

  private AppUser getUserFromDto(RegisterUserDto userDto) {
    AppUser appUser = new AppUser();
    appUser.setEmail(userDto.getEmail());
    appUser.setFirstName(userDto.getFirstName());
    appUser.setLastName(userDto.getLastName());
    appUser.setDegreesBeforeName(userDto.getDegreesBeforeName());
    appUser.setDegreesBehindName(userDto.getDegreesBehindName());
    appUser.setPassword(userDto.getPassword());
    appUser.setVerified(false);
    return appUser;
  }
}
