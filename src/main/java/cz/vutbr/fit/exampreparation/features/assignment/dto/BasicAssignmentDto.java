package cz.vutbr.fit.exampreparation.features.assignment.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * Used to store name of assignment and list of users that has access to assignment management.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BasicAssignmentDto {

  @NotBlank(message = "Chybějící název zadání")
  private String name;

  private List<String> additionalAssignmentManagersEmails;
}
