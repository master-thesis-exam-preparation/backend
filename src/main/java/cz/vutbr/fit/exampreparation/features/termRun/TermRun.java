package cz.vutbr.fit.exampreparation.features.termRun;

import cz.vutbr.fit.exampreparation.features.assignment.Assignment;
import cz.vutbr.fit.exampreparation.features.roomPlan.RoomPlan;
import cz.vutbr.fit.exampreparation.features.term.Term;
import cz.vutbr.fit.exampreparation.features.testSchedule.TestSchedule;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This entity class is responsible for object-relational mapping of an objects
 * with information about term runs into a database table TermRun.
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
public class TermRun {

  @Id
  @Setter(AccessLevel.PRIVATE)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int idTermRun;

  @NotNull(message = "Chybějící čas začátku zkoušky")
  private Timestamp startDateTime;

  @NotNull(message = "Chybějící čas konce zkoušky")
  private Timestamp endDateTime;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "idTerm", referencedColumnName = "idTerm")
  @OnDelete(action = OnDeleteAction.CASCADE)
  private Term termReference;

  @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
  @JoinTable(
    name = "term_run_assignment_link",
    joinColumns = {@JoinColumn(name = "id_term_run")},
    inverseJoinColumns = {@JoinColumn(name = "id_assignment")}
  )
  private List<Assignment> assignments = new ArrayList<>();

  @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
  @JoinTable(
    name = "term_run_room_plan_link",
    joinColumns = {@JoinColumn(name = "id_term_run")},
    inverseJoinColumns = {@JoinColumn(name = "id_room_plan")}
  )
  private List<RoomPlan> roomPlans;

  @OneToMany(mappedBy = "termRun", cascade = CascadeType.ALL, orphanRemoval = true)
  @OnDelete(action = OnDeleteAction.CASCADE)
  private List<TestSchedule> testSchedules;

  /**
   * Creates new instance of {@code TermRun}.
   * ID attribute will be set after storing the object into database.
   *
   * @param startDateTime  time and date when term run starts
   * @param endDateTime  time and date when term run ends
   * @param termReference  reference to term to which this term run belongs
   * @param assignments  list of assignments selected for this term run
   * @param roomPlans  list of room plan selected for this term run
   * @param testSchedules  list of test schedules assigned to this term run
   */
  public TermRun(
    Timestamp startDateTime,
    Timestamp endDateTime,
    Term termReference,
    List<Assignment> assignments,
    List<RoomPlan> roomPlans,
    List<TestSchedule> testSchedules
  ) {
    this.startDateTime = startDateTime;
    this.endDateTime = endDateTime;
    this.termReference = termReference;
    this.assignments = assignments;
    this.roomPlans = roomPlans;
    this.testSchedules = testSchedules;
  }

  /**
   * Adds new assignment to list of term runs selected assignments.
   *
   * @param assignment  assignment to add
   */
  public void addAssignment(Assignment assignment) {
    this.assignments.add(assignment);
    assignment.getTermRuns().add(this);
  }

  /**
   * Removes the provided assignment from list of term runs selected assignments.
   *
   * @param assignment  assignment to remove
   */
  public void removeAssignment(Assignment assignment) {
    this.assignments.remove(assignment);
    assignment.getTermRuns().remove(this);
  }

  /**
   * Adds new room plan to list of term runs selected room plans.
   *
   * @param roomPlan  room plan to add
   */
  public void addRoomPlan(RoomPlan roomPlan) {
    this.roomPlans.add(roomPlan);
    roomPlan.getTermRuns().add(this);
  }

  /**
   * Removes the provided room plan from list of term runs selected room plans.
   *
   * @param roomPlan  room plan to remove
   */
  public void removeRoomPlan(RoomPlan roomPlan) {
    this.roomPlans.remove(roomPlan);
    roomPlan.getTermRuns().remove(this);
  }

  /**
   * Gets term runs room plans from its list of rooms.
   *
   * @return list of term runs room plans
   */
  public List<RoomPlan> getRoomPlans() {
    return roomPlans.stream()
      .sorted(
        Comparator.comparing(RoomPlan::getCapacity).reversed()
        .thenComparing(roomPlan -> roomPlan.getRoomReference().getAbbreviation())
        .thenComparing(roomPlan -> roomPlan.getRoomReference().getName())
      )
      .collect(Collectors.toList());
  }

  /**
   * Adds new test schedule to list of term runs assigned test schedules.
   *
   * @param testSchedule  test schedule to add
   */
  public void addTestSchedule(TestSchedule testSchedule) {
    this.testSchedules.add(testSchedule);
    testSchedule.setTermRun(this);
  }

  /**
   * Removes the provided test schedule from list of term runs assigned test schedules.
   *
   * @param testSchedule  test schedule to remove
   */
  public void removeTestSchedule(TestSchedule testSchedule) {
    this.testSchedules.remove(testSchedule);
    testSchedule.setTermRun(null);
  }
}
