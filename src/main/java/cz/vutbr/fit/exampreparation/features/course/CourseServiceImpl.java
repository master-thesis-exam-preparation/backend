package cz.vutbr.fit.exampreparation.features.course;

import cz.vutbr.fit.exampreparation.common.MessageResponseDto;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.BadRequestException;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.ConflictException;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.ForbiddenException;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.NotFoundException;
import cz.vutbr.fit.exampreparation.features.course.dto.BasicCourseInfo;
import cz.vutbr.fit.exampreparation.features.course.dto.CourseInfoDto;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * This service class is responsible for management of all courses.
 */
@Service
@AllArgsConstructor
public class CourseServiceImpl implements CourseService {

  private final CourseRepository courseRepository;

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseEntity<List<CourseInfoDto>> getAllCoursesInfo(boolean getOnlyCurrent) {
    List<Course> courses;
    if (getOnlyCurrent) {
      LocalDate localDate = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
      int currentYear, startYear;
      currentYear = startYear = localDate.getYear();
      int month = localDate.getMonthValue();
      int day = localDate.getDayOfMonth();
      if (month == 9 || (month == 10 && day <= 15)) { // 1.9. - 15.10.
        startYear -= 1;
      } else if (month <= 8) {
        startYear -= 1;
        currentYear -= 1;
      }
      courses = courseRepository.findAllCurrentAndSorted(startYear, currentYear);
    } else {
      courses = courseRepository.findAllSorted();
    }

    List<CourseInfoDto> allCoursesInfo = courses.stream()
      .map(this::mapCourseToCourseInfoDto)
      .collect(Collectors.toList());

    return ResponseEntity.ok()
      .body(allCoursesInfo);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseEntity<CourseInfoDto> createNewCourse(BasicCourseInfo courseInfoDto) {
    if (checkIfCourseExist(courseInfoDto)) {
      throwCourseConflictException(courseInfoDto);
    }
    Course savedCourse = createAndSaveNewCourse(courseInfoDto);
    return ResponseEntity.ok()
      .body(mapCourseToCourseInfoDto(savedCourse));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseEntity<MessageResponseDto> editCourse(int courseId, BasicCourseInfo courseInfoDto) {
    Course course = courseRepository.findById(courseId)
      .orElseThrow(() -> new NotFoundException("Předmět nebyl nalezen"));

    if (checkIfCourseExist(courseInfoDto)) {
      throwCourseConflictException(courseInfoDto);
    }

    course.setAbbreviation(courseInfoDto.getAbbreviation());
    course.setName(courseInfoDto.getName());
    course.setAcademicYear(courseInfoDto.getAcademicYear());
    try {
      courseRepository.save(course);
    } catch (Exception e) {
      throw new BadRequestException(e.getMessage());
    }

    return ResponseEntity.ok()
      .body(new MessageResponseDto("Změny úspěšně uloženy"));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseEntity<MessageResponseDto> deleteCourse(int courseId) {
    Course course = courseRepository.findById(courseId)
      .orElseThrow(() -> new NotFoundException("Předmět nebyl nalezen"));

    if (course.getTerms().size() > 0) {
      throw new ForbiddenException("Tento předmět nelze odstranit, jelikož existuje zkouška, která se z něj koná.\n" +
        "Pokud potřebujete tento předmět smazat smazat, smažte i všechny z něj probíhající termíny zkoušek.");
    }

    try {
      courseRepository.deleteById(courseId);
    } catch (Exception e) {
      throw new BadRequestException("Nepodařilo se smazat předmět. Ujistěte se, že tento předmět existuje.");
    }
    return ResponseEntity.ok()
      .body(new MessageResponseDto("Předmět úspěšně odstraněn"));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseEntity<MessageResponseDto> importCourses(List<BasicCourseInfo> courses) {
    List<Course> createdCourses = new ArrayList<>();
    for (int i = 0; i < courses.size(); i++) {
      if (checkIfCourseExist(courses.get(i))) {
        continue;
      }

      try {
        createdCourses.add(createAndSaveNewCourse(courses.get(i)));
      } catch (BadRequestException e) {
        createdCourses.forEach(courseRepository::delete);

        String append = (i + 1) + ". předmět v pořadí\nCHYBA: " + e.getMessage() + "\nOpravte chyby a opakujte pokus.";
        throw new BadRequestException(append);
      }
    }

    return ResponseEntity.ok()
      .body(new MessageResponseDto("Předměty byly úspěšně importovány"));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Optional<Course> findCourseById(int courseId) {
    return this.courseRepository.findById(courseId);
  }

  private CourseInfoDto mapCourseToCourseInfoDto(Course course) {
    return new CourseInfoDto(
      course.getAbbreviation(),
      course.getName(),
      course.getAcademicYear(),
      course.getIdCourse());
  }

  private Course createAndSaveNewCourse(BasicCourseInfo courseInfoDto) {
    Course newCourse = new Course(courseInfoDto.getAbbreviation(), courseInfoDto.getAcademicYear(), courseInfoDto.getName(), null);

    Course savedCourse;
    try {
      savedCourse = courseRepository.save(newCourse);
    } catch (Exception e) {
      throw new BadRequestException(e.getMessage());
    }
    return savedCourse;
  }

  private boolean checkIfCourseExist(BasicCourseInfo courseInfoDto) {
    return !courseRepository.findAllByAbbreviationAndNameAndAcademicYear(
      courseInfoDto.getAbbreviation(),
      courseInfoDto.getName(),
      courseInfoDto.getAcademicYear()).isEmpty();
  }

  private void throwCourseConflictException(BasicCourseInfo courseInfoDto) {
    throw new ConflictException("Kombinace zkratky, názvu a akademického roku musí být unikátní.\n" +
      "Předmět " + courseInfoDto.getAbbreviation() + " - " + courseInfoDto.getName() + " " +
      courseInfoDto.getAcademicYear() + " již existuje");
  }
}
