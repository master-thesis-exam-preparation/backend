package cz.vutbr.fit.exampreparation.features.testSchedule;

import cz.vutbr.fit.exampreparation.common.AuthService;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.BadRequestException;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.ForbiddenException;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.NotFoundException;
import cz.vutbr.fit.exampreparation.features.assignment.dto.FileContentDto;
import cz.vutbr.fit.exampreparation.features.assignment.dto.GeneratingFailDto;
import cz.vutbr.fit.exampreparation.features.assignment.helpers.RunningGeneratingService;
import cz.vutbr.fit.exampreparation.features.assignment.helpers.StorageManager;
import cz.vutbr.fit.exampreparation.features.room.Room;
import cz.vutbr.fit.exampreparation.features.roomPlan.RoomPlan;
import cz.vutbr.fit.exampreparation.features.term.Term;
import cz.vutbr.fit.exampreparation.features.termRun.TermRun;
import cz.vutbr.fit.exampreparation.features.termRun.TermRunService;
import lombok.AllArgsConstructor;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * This service class is responsible for management of all test schedules.
 */
@Service
@AllArgsConstructor
public class TestScheduleServiceImpl implements TestScheduleService {

  private final TestScheduleRepository testScheduleRepository;
  private final TermRunService termRunService;
  private final StorageManager storageManager;
  private final RunningGeneratingService runningGeneratingService;

  /**
   * {@inheritDoc}
   */
  @Override
  public List<TestSchedule> findAllTestSchedulesInTermRunsRoomPlan(TermRun termRun, RoomPlan roomPlan) {
    return testScheduleRepository.findAllTestSchedulesInTermRunsRoomPlan(termRun, roomPlan);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<TestSchedule> findAllTestSchedulesInTermRunsRoomPlan(int termRunId, String roomPlanAbbreviation) {
    return testScheduleRepository.findAllTestSchedulesInTermRunsRoomPlan(termRunId, roomPlanAbbreviation);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<TestSchedule> findAllTestSchedulesInTermRun(TermRun termRun) {
    return testScheduleRepository.findAllByTermRun(termRun);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<TestSchedule> findAllTestSchedulesWithAssignment(int assignmentId) {
    return testScheduleRepository.findAllByAssignment_IdAssignment(assignmentId);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isAnyStudentSeatedInRoom(Room room) {
    return testScheduleRepository.findTestSchedulesInRoom(room).size() > 0;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Optional<TestSchedule> findTestScheduleById(int testScheduleId) {
    return testScheduleRepository.findById(testScheduleId);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void updatePdfPath(TestSchedule testSchedule, String pdfPath) {
    testSchedule.setPdfPath(pdfPath);
    testScheduleRepository.save(testSchedule);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<TestSchedule> findAllTestSchedulesInTerm(int termId) {
    return testScheduleRepository.findAllByTermRun_TermReference_IdTerm(termId);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getRoomAbbreviationFromTestSchedule(TestSchedule testSchedule) {
    return testSchedule.getCell()
      .getRoomPlanReference()
      .getRoomReference()
      .getAbbreviation();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getTermRunDir(TestSchedule testSchedule) {
    int termId = getTermIdFromTestSchedule(testSchedule);
    int termRunId = getTermRunIdFromTestSchedule(testSchedule);
    return storageManager.getTermDirPath(termId) + File.separator + "term-run-" + termRunId;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getRoomPlanDir(String termRunDir, String roomAbbreviation) {
    return termRunDir + File.separator + roomAbbreviation;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseEntity<Resource> downloadPdfs(Integer termId, Integer termRunId, String roomAbbreviation,
                                               Integer testScheduleId, boolean pack, boolean doubleSided) {
    if (termId == null && termRunId == null && roomAbbreviation == null && testScheduleId == null) {
      throw new BadRequestException("Nebylo specifikováno jaká zadání mají být stažena.");
    }

    if (roomAbbreviation != null && termRunId == null) {
      throw new BadRequestException("Se zkratkou místnosti musí být specifikováno i ID odpovídajícího běhu");
    }

    List<TestSchedule> testSchedules = new ArrayList<>();
    String zipFileName = null;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy-HH-mm");
    if (testScheduleId != null) {
      TestSchedule testSchedule = findTestScheduleById(testScheduleId)
        .orElseThrow(() -> new NotFoundException("Usazení s ID " + testScheduleId + " neexistuje"));

      if (!AuthService.isUserManager(testSchedule.getTermRun().getTermReference().getTermManagers())) {
        throw new ForbiddenException("Nejste správce termínu zkoušky. Toto zadání nemůžete stáhnout.");
      }

      testSchedules.add(testSchedule);
    } else if (termRunId != null) {
      TermRun termRun = termRunService.findTermRunById(termRunId)
        .orElseThrow(() -> new NotFoundException("Běh s ID " + termRunId + " neexistuje"));

      if (!AuthService.isUserManager(termRun.getTermReference().getTermManagers())) {
        throw new ForbiddenException("Nejste správce termínu zkoušky. Tato zadání nemůžete stáhnout.");
      }

      if (roomAbbreviation != null) {
        testSchedules = findAllTestSchedulesInTermRunsRoomPlan(termRunId, roomAbbreviation);
        zipFileName = "beh-" + dateFormat.format(new Date(termRun.getStartDateTime().getTime()))
          + "_mistnost-" + roomAbbreviation;
      } else {
        testSchedules = findAllTestSchedulesInTermRun(termRun);
        zipFileName = "beh-" + dateFormat.format(new Date(termRun.getStartDateTime().getTime()));
      }
    } else {
      List<TermRun> termRuns = termRunService.getTermTermRuns(termId);
      zipFileName = "termin-" + dateFormat.format(new Date(termRuns.get(0).getStartDateTime().getTime()));

      if (!AuthService.isUserManager(termRuns.get(0).getTermReference().getTermManagers())) {
        throw new ForbiddenException("Nejste správce termínu zkoušky. Tato zadání nemůžete stáhnout.");
      }

      testSchedules = findAllTestSchedulesInTerm(termId);
    }

    Resource file = getTestSchedulesPdfs(testSchedules, pack, doubleSided, zipFileName);
    String probeContentType = storageManager.getResourceProbeContentType(file);

    return ResponseEntity.ok()
      .header(HttpHeaders.CONTENT_TYPE, probeContentType)
      .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
      .body(file);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void saveAll(List<TestSchedule> testSchedules) {
    testScheduleRepository.saveAll(testSchedules);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseEntity<FileContentDto> getLogFileContent(int testScheduleId) {
    TestSchedule testSchedule = findTestScheduleById(testScheduleId)
      .orElseThrow(() -> new NotFoundException("Usazení s ID " + testScheduleId + " neexistuje."));

    if (!AuthService.isUserManager(testSchedule.getTermRun().getTermReference().getTermManagers())) {
      throw new ForbiddenException("Nejste správce termínu zkoušky. Nemůžete zobrazit obsah chybového logu.");
    }

    GeneratingFailDto generatingFail = runningGeneratingService.getGeneratingError(testSchedule);
    if (generatingFail == null) {
      throw new BadRequestException("Usazení s ID " + testScheduleId + " neobsahuje žádnou chybu při generování.");
    }
    String fileContent = storageManager.loadAsString(generatingFail.getLogFilePath());
    return ResponseEntity.ok()
      .body(new FileContentDto(fileContent));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseEntity<Resource> downloadStudentsCsv(Integer termId, Integer termRunId, String roomAbbreviation) {
    if (termId == null || termRunId == null || roomAbbreviation == null) {
      throw new BadRequestException("Nebylo jasně specifikováno pro jakou místnost má být soubor .CSV stažen.");
    }

    TermRun termRun = termRunService.findTermRunById(termRunId)
      .orElseThrow(() -> new NotFoundException("Běh s ID " + termRunId + " neexistuje"));

    String csvFileName = "beh-" +
      new SimpleDateFormat("dd-MM-yyyy-HH-mm").format(new Date(termRun.getStartDateTime().getTime())) +
      "_mistnost-" + roomAbbreviation;

    StringBuilder fileContent = new StringBuilder("Číslo sedadla,Login,Jméno a tituly,Pořadové číslo\n");
    for (TestSchedule testSchedule : findAllTestSchedulesInTermRunsRoomPlan(termRunId, roomAbbreviation)) {
      fileContent
        .append(testSchedule.getSeatNumber()).append(",")
        .append(testSchedule.getStudent().getLogin()).append(",")
        .append(testSchedule.getStudent().getNameWithDegrees()).append(",")
        .append(testSchedule.getOrderNumber())
        .append("\n");
    }
    Resource csvFile = new ByteArrayResource(fileContent.toString().getBytes(Charset.forName("CP1250")));

    return ResponseEntity.ok()
      .header(HttpHeaders.CONTENT_TYPE, "text/csv")
      .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + csvFileName + ".csv\"")
      .body(csvFile);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int findLastOrderNumber(Term term) {
    Integer orderNumber = testScheduleRepository.findTermLastOrderNumber(term);
    return Objects.requireNonNullElse(orderNumber, 0);
  }

  private Resource getTestSchedulesPdfs(List<TestSchedule> testSchedules, boolean pack, boolean doubleSided, String zipFileName) {
    testSchedules.sort(Comparator.comparing(TestSchedule::getSeatNumber));

    List<String> pdfPaths = new ArrayList<>();
    for (TestSchedule testSchedule : testSchedules) {
      if (testSchedule.getPdfPath() == null) {
        throw new BadRequestException("Usazení s ID " + testSchedule.getIdTestSchedule() + " nemá doposud vygenerované zadání");
      }
      pdfPaths.add(testSchedule.getPdfPath());
    }

    if (pdfPaths.size() == 1) {
      return storageManager.downloadFile(Paths.get(pdfPaths.get(0)));
    }

    int termId = getTermIdFromTestSchedule(testSchedules.get(0));
    String termRootPath = storageManager.getTermDirPath(termId);

    String downloadFilePath;
    if (pack) {
      downloadFilePath = storageManager.mergePdfsIntoOne(pdfPaths, zipFileName, termRootPath, doubleSided);
    } else {
      downloadFilePath = storageManager.zipFiles(pdfPaths, zipFileName, termRootPath);
    }

    return storageManager.downloadFile(Paths.get(downloadFilePath));
  }

  private int getTermIdFromTestSchedule(TestSchedule testSchedule) {
    return testSchedule.getTermRun()
      .getTermReference()
      .getIdTerm();
  }

  private int getTermRunIdFromTestSchedule(TestSchedule testSchedule) {
    return testSchedule.getTermRun()
      .getIdTermRun();
  }
}
