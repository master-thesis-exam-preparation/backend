package cz.vutbr.fit.exampreparation.features.assignment.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Used to store text content of assignment file.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FileContentDto {
  private String fileContent;
}
