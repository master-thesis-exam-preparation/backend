package cz.vutbr.fit.exampreparation.features.appUser.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * Used to store information necessary to register user to application (e-mail address, password, name and degrees).
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegisterUserDto extends EmailPasswordDto {

  @NotBlank(message = "Chybějící jméno")
  @Size(max = 255, message = "Příliš dlouhý vstup")
  private String firstName;

  @NotBlank(message = "Chybějící příjmení")
  @Size(max = 255, message = "Příliš dlouhý vstup")
  private String lastName;

  @Size(max = 255, message = "Příliš dlouhý vstup")
  private String degreesBeforeName;

  @Size(max = 255, message = "Příliš dlouhý vstup")
  private String degreesBehindName;

}
