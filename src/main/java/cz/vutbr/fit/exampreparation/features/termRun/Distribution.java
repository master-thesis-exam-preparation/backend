package cz.vutbr.fit.exampreparation.features.termRun;

/**
 * There can be two different types of student distribution into rooms.
 *
 * <ul>
 *   <li>RANDOM - students are seated into rooms in a random order.</li>
 *   <li>ALPHABETIC - students are seated into rooms in an alphabetical order.</li>
 * </ul>
 */
public enum Distribution {
  RANDOM,
  ALPHABETIC
}
