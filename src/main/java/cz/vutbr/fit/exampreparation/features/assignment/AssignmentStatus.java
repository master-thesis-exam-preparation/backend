package cz.vutbr.fit.exampreparation.features.assignment;

/**
 * The state in which the assignment can be
 *
 * <ul>
 *   <li>UPLOADED - assignment is created but no PDFs was generated</li>
 *   <li>LOCKED - generating of assignment is in progress and assignment cannot be manipulated.</li>
 *   <li>GENERATED - assignment was generated and PDF files can be downloaded.</li>
 *   <li>EDITED - changes was made and assignment should be regenerated.</li>
 * </ul>
 */
public enum AssignmentStatus {
  UPLOADED,
  LOCKED,
  GENERATED,
  EDITED
}
