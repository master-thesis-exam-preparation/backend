package cz.vutbr.fit.exampreparation.features.term.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Used to store information about manually placed students.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentsPlacementDto {

  @NotEmpty(message = "Chybí seznam studentů, kteří mají být manuálně usazeni")
  private List<String> studentLogins;

  @NotNull(message = "Chybí termín, do kterého mají být studenti usazeni")
  private Integer termRunId;

  private Integer roomId;

  private List<Integer> seatIds;
}
