package cz.vutbr.fit.exampreparation.features.assignment.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Used to store information about assignment files (type of file, name and size).
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FileDataDto {
  private String type;
  private String name;
  private Long size;
}
