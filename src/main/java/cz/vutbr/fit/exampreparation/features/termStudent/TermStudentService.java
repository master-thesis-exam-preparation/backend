package cz.vutbr.fit.exampreparation.features.termStudent;

public interface TermStudentService {

  /**
   * Tries to find the student data by provided login in database.
   *
   * @param login login of the searched student
   * @return data about student
   */
  TermStudent findUserByLogin(String login);
}
