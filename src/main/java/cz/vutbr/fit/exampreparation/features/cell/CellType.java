package cz.vutbr.fit.exampreparation.features.cell;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Cell can be of multiple types.
 *
 * <ul>
 *   <li>SEAT - student can be seated on this cell.</li>
 *   <li>AISLE - used to visualize free space in room.</li>
 *   <li>DESK - used to visualize teachers desk.</li>
 *   <li>BLOCKED - seat but no one can be seated on it.</li>
 *   <li>SELECTED - used to distinguish seat that was assigned to the students.</li>
 * </ul>
 */
public enum CellType {
  @JsonProperty("SEAT")
  SEAT,
  @JsonProperty("AISLE")
  AISLE,
  @JsonProperty("DESK")
  DESK,
  @JsonProperty("BLOCKED")
  BLOCKED,
  @JsonProperty("SELECTED")
  SELECTED
}
