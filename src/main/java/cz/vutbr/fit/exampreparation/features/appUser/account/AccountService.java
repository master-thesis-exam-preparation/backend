package cz.vutbr.fit.exampreparation.features.appUser.account;

import cz.vutbr.fit.exampreparation.common.MessageResponseDto;
import cz.vutbr.fit.exampreparation.features.appUser.dto.*;
import cz.vutbr.fit.exampreparation.security.jwt.dto.AccessTokenDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface AccountService {

  /**
   * Registers a new user into the application.
   *
   * @param userDto  information about new user (name, surname, password etc.)
   * @return response with success message
   */
  ResponseEntity<MessageResponseDto> createNewUserAccount(RegisterUserDto userDto);

  /**
   * Activates an user account if verification token is valid.
   *
   * @param token  verification token from e-mail message
   * @return response with success message
   */
  ResponseEntity<MessageResponseDto> verifyAccount(String token);

  /**
   * Creates a new access token from refresh token cookie.
   *
   * @param refreshToken  refresh token cookie value
   * @return response with new access token and with new refresh token cookie
   */
  ResponseEntity<AccessTokenDto> generateNewKeys(String refreshToken);

  /**
   * Logs an existing user into the application.
   *
   * @param userDto  user credentials
   * @return response with new access token
   */
  ResponseEntity<AccessTokenDto> loginUser(LoginUserDto userDto);

  /**
   * Logs out an user from the application.
   *
   * @param refreshToken  refresh token cookie value
   * @return response with success message
   */
  ResponseEntity<MessageResponseDto> logoutUser(String refreshToken);

  /**
   * Logs an existing user into the application via refresh token cookie.
   *
   * @param refreshToken  refresh token cookie value
   * @return response with new access token
   */
  ResponseEntity<AccessTokenDto> logMeIn(String refreshToken);

  /**
   * Sends user an e-mail message with link to reset password.
   *
   * @param emailDto  e-mail address to which should be message with link send
   * @return response with success message
   */
  ResponseEntity<MessageResponseDto> forgetPassword(EmailDto emailDto);

  /**
   * Sets a new password for the user.
   *
   * @param newPasswordDto  token from link and a new password
   * @return response with success message
   */
  ResponseEntity<MessageResponseDto> resetPassword(NewPasswordDto newPasswordDto);

  /**
   * Gets information about currently logged-in user.
   *
   * @return response with information about logged-in user
   */
  ResponseEntity<UserDataDto> getUserData();

  /**
   * Changes current password of logged-in user.
   *
   * @param changePasswordDto  object with old password to confirmation and new password to set
   * @return response with success message
   */
  ResponseEntity<MessageResponseDto> changePassword(ChangePasswordDto changePasswordDto);

  /**
   * Changes current information of logged-in user.
   *
   * @param userDataDto  object with new information about user (name, degrees etc.)
   * @return response with success message
   */
  ResponseEntity<MessageResponseDto> changeUserDetails(UserDataDto userDataDto);

  /**
   * Gets information about all users accounts.
   *
   * @return response with information about all user accounts
   */
  ResponseEntity<List<AccountInfoDto>> getAllUsersData();

  /**
   * Changes role of a specific user to new one.
   *
   * @param userId  unique ID of the user whose account should get different role
   * @param newRole  object with name of the new role
   * @return response with success message
   */
  ResponseEntity<MessageResponseDto> changeAccountRole(NewRoleDto newRole, int userId);

  /**
   * Deletes account of a specific user.
   *
   * @param userId  unique ID of the user whose account is to be deleted
   * @return response with success message
   */
  ResponseEntity<MessageResponseDto> deleteAccount(int userId);

  /**
   * Locks account of a blocked user.
   *
   * @param userId unique ID of the user whose account is to be locked
   * @return response with success message
   */
  ResponseEntity<MessageResponseDto> lockAccount(int userId);

  /**
   * Unlocks account of a blocked user.
   *
   * @param userId  unique ID of the user whose account is to be unlocked
   * @return response with success message
   */
  ResponseEntity<MessageResponseDto> unlockAccount(int userId);
}
