package cz.vutbr.fit.exampreparation.features.course;

import cz.vutbr.fit.exampreparation.common.MessageResponseDto;
import cz.vutbr.fit.exampreparation.features.course.dto.BasicCourseInfo;
import cz.vutbr.fit.exampreparation.features.course.dto.CourseInfoDto;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * This class is responsible for managing course REST API endpoints.
 */
@Validated
@RestController
@AllArgsConstructor
public class CourseController {

  private final CourseService courseService;

  /**
   * Gets information about all courses.
   *
   * @return response with information about all courses.
   */
  @PreAuthorize("hasAnyRole('EMPLOYEE','ADMIN')")
  @GetMapping("/internal/courses")
  public ResponseEntity<List<CourseInfoDto>> getAllCoursesInfo() {
    return courseService.getAllCoursesInfo(false);
  }

  /**
   * Creates new ceurse based on given data.
   *
   * @param courseInfoDto  information necessary to create new course
   * @return response with information about created course
   */
  @PreAuthorize("hasRole('ADMIN')")
  @PostMapping("/internal/courses")
  public ResponseEntity<CourseInfoDto> createNewCourse(@Valid @RequestBody BasicCourseInfo courseInfoDto) {
    return courseService.createNewCourse(courseInfoDto);
  }

  /**
   * Edits existing course data based on new given data.
   *
   * @param courseId  unique ID of the course which should be edited
   * @param courseInfoDto  information about changes
   * @return response with success message
   */
  @PreAuthorize("hasRole('ADMIN')")
  @PutMapping("/internal/courses/{courseId:[\\d]+}")
  public ResponseEntity<MessageResponseDto> editCourse(@PathVariable int courseId, @Valid @RequestBody BasicCourseInfo courseInfoDto) {
    return courseService.editCourse(courseId, courseInfoDto);
  }

  /**
   * Deletes specified course from database.
   *
   * @param courseId  unique ID of the course that should be deleted
   * @return response with success message
   */
  @PreAuthorize("hasRole('ADMIN')")
  @DeleteMapping("/internal/courses/{courseId:[\\d]+}")
  public ResponseEntity<MessageResponseDto> deleteCourse(@PathVariable int courseId) {
    return courseService.deleteCourse(courseId);
  }

  /**
   * Creates multiple courses at once.
   *
   * @param courses  list of information necessary to create new courses
   * @return response with success message
   */
  @PreAuthorize("hasRole('ADMIN')")
  @PostMapping("/internal/courses/import")
  public ResponseEntity<MessageResponseDto> importNewCourses(@RequestBody @NotEmpty(message = "Seznam předmětů je prázdný") List<@Valid BasicCourseInfo> courses) {
    return courseService.importCourses(courses);
  }

  /**
   * Gets all courses from which the exam can take place at current time.
   *
   * @return response with list of courses from which the exam can take place at current time
   */
  @PreAuthorize("hasAnyRole('EMPLOYEE','ADMIN')")
  @GetMapping("/internal/current-courses")
  public ResponseEntity<List<CourseInfoDto>> getAllCurrentCoursesInfo() {
    return courseService.getAllCoursesInfo(true);
  }

}
