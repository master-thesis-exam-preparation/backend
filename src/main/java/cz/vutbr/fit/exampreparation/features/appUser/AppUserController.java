package cz.vutbr.fit.exampreparation.features.appUser;

import cz.vutbr.fit.exampreparation.features.appUser.dto.EmployeeDetailsDto;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * This class is responsible for managing user REST API endpoints.
 */
@Validated
@RestController
@AllArgsConstructor
public class AppUserController {

  private final AppUserService appUserService;

  /**
   * Gets list of users that have employee role.
   *
   * @return response with list of objects with information about employees
   */
  @PreAuthorize("hasAnyRole('EMPLOYEE','ADMIN')")
  @GetMapping("/internal/employees")
  public ResponseEntity<List<EmployeeDetailsDto>> getAllEmployeesList() {
    return appUserService.getAllEmployeesList();
  }
}
