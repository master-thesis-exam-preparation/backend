package cz.vutbr.fit.exampreparation.features.assignment.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.vutbr.fit.exampreparation.features.testSchedule.dto.TestScheduleDetailDto;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Used to store information about error that occurs during assignment generating.
 */
@Data
@AllArgsConstructor
public class GeneratingFailDto {

  @NotNull(message = "Chybí usazení, u kterého došlo k chybě při generování")
  private TestScheduleDetailDto testSchedule;

  @NotNull(message = "Chybí chybový kód")
  private Integer errorCode;

  @NotBlank(message = "Chybí chybová hláška")
  private String errorMessage;

  private boolean hasLogFile;

  @JsonIgnore
  @NotBlank(message = "Chybí cesta k log souboru")
  private String logFilePath;

}
