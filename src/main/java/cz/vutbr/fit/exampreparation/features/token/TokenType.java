package cz.vutbr.fit.exampreparation.features.token;

/**
 * Token can be of multiple types.
 *
 * <ul>
 *   <li>VERIFICATION - this type of token is used to users e-mail address verification.</li>
 *   <li>REFRESH - this token is used to periodically obtain new access tokens.</li>
 *   <li>FORGET_PASSWORD - this token is used to password renewal.</li>
 * </ul>
 */
public enum TokenType {
  VERIFICATION,
  REFRESH,
  FORGET_PASSWORD
}
