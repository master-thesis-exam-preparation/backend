package cz.vutbr.fit.exampreparation.features.room.dto;

import cz.vutbr.fit.exampreparation.features.cell.CellType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * Used to store information necessary to create new room (room size, cell types etc.).
 */
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class NewRoomDto extends BasicRoomDto {

  private String description;

  @NotEmpty(message = "Plán místnosti nesmí být prázdný")
  private List<List<CellType>> cells;

}
