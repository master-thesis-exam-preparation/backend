package cz.vutbr.fit.exampreparation.features.role;

import com.fasterxml.jackson.annotation.JsonBackReference;
import cz.vutbr.fit.exampreparation.features.appUser.AppUser;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * This entity class is responsible for object-relational mapping of an objects
 * with information about assigned roles of application users into a database table Role.
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Role {

  @Id
  @NotBlank(message = "Chybějící název uživatelské role")
  @Enumerated(EnumType.STRING)
  private RoleName roleName;

  private String roleDescription;

  @ManyToMany(mappedBy = "roles", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JsonBackReference
  private List<AppUser> appUsers;

}
