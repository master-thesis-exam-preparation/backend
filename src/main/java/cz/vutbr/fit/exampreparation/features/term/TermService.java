package cz.vutbr.fit.exampreparation.features.term;

import cz.vutbr.fit.exampreparation.common.MessageResponseDto;
import cz.vutbr.fit.exampreparation.features.term.dto.AllTermsInfoResponseDto;
import cz.vutbr.fit.exampreparation.features.term.dto.TermDetailDto;
import cz.vutbr.fit.exampreparation.features.term.dto.term.EditTermDto;
import cz.vutbr.fit.exampreparation.features.term.dto.term.EditTermInfoDto;
import cz.vutbr.fit.exampreparation.features.term.dto.term.NewTermDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface TermService {

  /**
   * Gets information about all terms.
   *
   * @param isPublic  if set to false this method return only that information that can be accessed by logged-in users
   * @return response with information about all terms
   */
  ResponseEntity<List<AllTermsInfoResponseDto>> getAllTermsInfo(boolean isPublic);

  /**
   * Creates new term based on given data.
   *
   * @param newTermDto  information necessary to create new term
   * @return response with success message
   */
  ResponseEntity<MessageResponseDto> createNewTerm(NewTermDto newTermDto);

  /**
   * Gets information about specified term.
   *
   * @param termId  unique ID of the term
   * @param isPublic  if set to false this method return only that information that can be accessed by logged-in users
   * @return response with information about term
   */
  ResponseEntity<TermDetailDto> getTermDetails(int termId, boolean isPublic);

  /**
   * Deletes specified term from database.
   *
   * @param termId  unique ID of the term that should be deleted
   * @return response with success message
   */
  ResponseEntity<MessageResponseDto> deleteTerm(int termId);

  /**
   * Gets information about term that will be used to fill edit forms.
   *
   * @param termId  unique ID of the term
   * @return response with edit information about term
   */
  ResponseEntity<EditTermInfoDto> getEditTermDetails(int termId);

  /**
   * Edits existing term data based on new given data.
   *
   * @param editTermDto  information about changes
   * @param termId  unique ID of the term
   * @return response with success message
   */
  ResponseEntity<MessageResponseDto> editTerm(int termId, EditTermDto editTermDto);
}
