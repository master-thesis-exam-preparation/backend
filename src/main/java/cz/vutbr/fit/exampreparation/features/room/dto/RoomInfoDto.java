package cz.vutbr.fit.exampreparation.features.room.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Used to store more detailed information about room, but not too specific.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class RoomInfoDto extends BasicRoomDto {

  @NotNull(message = "Chybějící identifikátor místnosti")
  private Integer roomId;

  @NotNull(message = "Chybějící kapacity plánů mistností")
  private List<Integer> capacities;

  @NotBlank(message = "Chybějící jméno autora místnosti")
  private String creatorNameWithDegrees;

  @NotBlank(message = "Chybějící e-mail autora")
  @Email(message = "Zkontrolujte, zda je e-mail ve správném formátu")
  private String creatorEmail;

  /**
   * Creates new instance of {@code RoomInfoDto}.
   *
   * @param roomId  unique ID of the room
   * @param abbreviation  room abbreviation
   * @param name  room name
   * @param capacities  number of seats in each room plan of this room
   * @param creatorNameWithDegrees  name and degrees of the user who created this room
   * @param creatorEmail  e-mail address of the user who created this room.
   */
  public RoomInfoDto(Integer roomId, String abbreviation, String name, List<Integer> capacities,
                     String creatorNameWithDegrees, String creatorEmail) {
    super(abbreviation, name);
    this.roomId = roomId;
    this.capacities = capacities;
    this.creatorNameWithDegrees = creatorNameWithDegrees;
    this.creatorEmail = creatorEmail;
  }
}
