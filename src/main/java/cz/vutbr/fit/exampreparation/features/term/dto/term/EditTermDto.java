package cz.vutbr.fit.exampreparation.features.term.dto.term;

import cz.vutbr.fit.exampreparation.features.appUser.dto.LoginNameDto;
import cz.vutbr.fit.exampreparation.features.term.dto.StudentsPlacementDto;
import cz.vutbr.fit.exampreparation.features.termRun.dto.TermRunDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Used to store basic information about term and other data necessary to edit existing term.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EditTermDto extends BasicTermDto  {

  @NotEmpty(message = "Chybí seznam běhů termínů zkoušky")
  private List<@Valid TermRunDto> termRuns;

  private List<@Valid StudentsPlacementDto> studentsPlacements;

  private List<@Valid LoginNameDto> studentsList;

  private List<@Valid LoginNameDto> oldStudentsList;

  private boolean addStudentsAtTheEnd;

  private List<String> additionalTermManagersEmails;

  private boolean sendEmail;

  @Size(max = 65535, message = "Příliš dlouhý vstup")
  private String emailMessage;

}
