package cz.vutbr.fit.exampreparation.features.appUser.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * Used to store information necessary to set a new role to user.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NewRoleDto {

  @NotBlank(message = "Chybějící název role")
  private String role;
}
