package cz.vutbr.fit.exampreparation.features.room;

import cz.vutbr.fit.exampreparation.common.AuthService;
import cz.vutbr.fit.exampreparation.common.MessageResponseDto;
import cz.vutbr.fit.exampreparation.common.UserInfo;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.BadRequestException;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.ConflictException;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.ForbiddenException;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.NotFoundException;
import cz.vutbr.fit.exampreparation.features.appUser.AppUser;
import cz.vutbr.fit.exampreparation.features.appUser.AppUserService;
import cz.vutbr.fit.exampreparation.features.appUser.dto.EmployeeDetailsDto;
import cz.vutbr.fit.exampreparation.features.cell.Cell;
import cz.vutbr.fit.exampreparation.features.cell.CellType;
import cz.vutbr.fit.exampreparation.features.cell.dto.CellDto;
import cz.vutbr.fit.exampreparation.features.room.dto.*;
import cz.vutbr.fit.exampreparation.features.roomPlan.RoomPlan;
import cz.vutbr.fit.exampreparation.features.roomPlan.RoomPlanService;
import cz.vutbr.fit.exampreparation.features.roomPlan.SpacingBetweenStudents;
import cz.vutbr.fit.exampreparation.features.roomPlan.dto.RoomPlanDto;
import cz.vutbr.fit.exampreparation.features.roomPlan.dto.RoomPlanWithCellsDto;
import cz.vutbr.fit.exampreparation.features.testSchedule.TestScheduleService;
import lombok.AllArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * This service class is responsible for management of all rooms.
 */
@Service
@AllArgsConstructor
public class RoomServiceImpl implements RoomService {

  private final AppUserService appUserService;
  private final RoomPlanService roomPlanService;
  private final TestScheduleService testScheduleService;
  private final RoomRepository roomRepository;

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseEntity<RoomInfoDto> createNewRoom(NewRoomDto newRoomDto) {
    UserInfo userInfo = AuthService.getLoggedUserInfo();

    if (!roomRepository.findAllByAbbreviationAndName(newRoomDto.getAbbreviation(), newRoomDto.getName()).isEmpty()) {
      throw new BadRequestException("Tato kombinace zkratky a názvu místnosti již existuje");
    }

    checkIfRoomPlanRowsHasSameLength(newRoomDto);

    AppUser roomCreator = appUserService.findByEmail(userInfo.getEmail())
      .orElseThrow(() -> new NotFoundException("Tvůrce místnosti s e-mailem \"" + userInfo.getEmail() + "\" nebyl nalezen"));

    Room newRoom = new Room(newRoomDto.getAbbreviation(), newRoomDto.getName(), newRoomDto.getDescription(),
      newRoomDto.getCells().size(), newRoomDto.getCells().get(0).size(), roomCreator, null);

    List<RoomPlan> roomPlans = generateRoomPlans(newRoomDto.getCells(), newRoom);
    newRoom.setRoomPlans(roomPlans);

    Room savedRoom;
    try {
      savedRoom = roomRepository.save(newRoom);
    } catch (Exception e) {
      throw new BadRequestException(e.getMessage());
    }
    return ResponseEntity.ok()
      .body(mapRoomToRoomInfoDto(savedRoom));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseEntity<List<RoomInfoDto>> getAllRoomsInfo() {
    List<RoomInfoDto> allRoomsInfo = new ArrayList<>();

    List<Room> allRooms = roomRepository.findAllSorted();
    for (Room room : allRooms) {
      RoomInfoDto roomInfoDto = mapRoomToRoomInfoDto(room);

      allRoomsInfo.add(roomInfoDto);
    }

    return ResponseEntity.ok()
      .body(allRoomsInfo);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseEntity<RoomDetailDto> getRoomDetails(int roomId, boolean isPublic) {
    Room room = roomRepository.findById(roomId)
      .orElseThrow(() -> new NotFoundException("Místnost nebyla nalezena"));

    List<RoomPlanWithCellsDto> roomPlans = new ArrayList<>();

    room.getRoomPlans()
      .forEach(roomPlan -> {
        List<List<CellDto>> roomPlanCells = formatPlanCells(room.getNumberOfRows(), roomPlan.getCells());
        roomPlans.add(
          new RoomPlanWithCellsDto(roomPlan.getIdRoomPlan(), roomPlan.getCapacity(), roomPlan.getSpacing(), roomPlanCells)
        );
      });

    EmployeeDetailsDto creator = null;
    if (!isPublic) {
      AppUser roomCreator = room.getAppUserReference();
      creator = new EmployeeDetailsDto(
        roomCreator.getEmail(),
        roomCreator.getNameWithDegrees()
      );
    }
    RoomDetailDto responseRoomDetail = new RoomDetailDto(
      room.getAbbreviation(),
      room.getName(),
      creator,
      room.getDescription(),
      roomPlans
    );

    return ResponseEntity.ok()
      .body(responseRoomDetail);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseEntity<EditRoomDto> getEditRoomDetails(int roomId, boolean isEditForm) {
    Room room = roomRepository.findById(roomId)
      .orElseThrow(() -> new NotFoundException("Místnost nebyla nalezena"));

    if (room.getRoomPlans().isEmpty()) {
      throw new RuntimeException("Místnost nemá žádný plán");
    }

    if (isEditForm) {
      if (testScheduleService.isAnyStudentSeatedInRoom(room)) {
        throw new ConflictException("V této místnosti již probíhá termín zkoušky a nelze ji tudíž editovat.\n" +
          "Můžete ale vytvořit novou místnost, která z této bude vycházet.");
      }
    }

    EditRoomDto responseDto = createNewRoomDtoFromRoom(room);

    return ResponseEntity.ok()
      .body(responseDto);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseEntity<MessageResponseDto> editRoom(int roomId, NewRoomDto newRoomDto) {
    UserInfo userInfo = AuthService.getLoggedUserInfo();
    Room room = roomRepository.findById(roomId)
      .orElseThrow(() -> new NotFoundException("Místnost nebyla nalezena"));

    boolean canBeEdited = roomRepository.findAllByAbbreviationAndName(newRoomDto.getAbbreviation(), newRoomDto.getName())
        .isEmpty() || room.getIdRoom() == roomId;
    if (!canBeEdited) {
      throw new BadRequestException("Tato kombinace zkratky a názvu místnosti již existuje");
    }

    // user is not author and user is not admin
    if (!(room.getAppUserReference().getEmail().equals(userInfo.getEmail()) || userInfo.getRoles().contains("ROLE_ADMIN"))) {
      throw new ForbiddenException("Místnost může upravovat pouze její autor");
    }

    checkIfRoomPlanRowsHasSameLength(newRoomDto);

    roomPlanService.deleteRoomPlans(room.getRoomPlans());

    room.setAbbreviation(newRoomDto.getAbbreviation());
    room.setName(newRoomDto.getName());
    room.setDescription(newRoomDto.getDescription());
    room.setNumberOfRows(newRoomDto.getCells().size());
    room.setNumberOfColumns(newRoomDto.getCells().get(0).size());

    List<RoomPlan> roomPlans = generateRoomPlans(newRoomDto.getCells(), room);
    room.setRoomPlans(roomPlans);

    try {
      roomRepository.save(room);
    } catch (DataIntegrityViolationException e) {
      throw new BadRequestException("Tato kombinace zkratky a jména místnosti již existuje");
    }

    return ResponseEntity.ok()
      .body(new MessageResponseDto("Změny úspěšně uloženy"));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseEntity<MessageResponseDto> deleteRoom(int roomId) {
    UserInfo userInfo = AuthService.getLoggedUserInfo();
    Room room = roomRepository.findById(roomId)
      .orElseThrow(() -> new NotFoundException("Místnost nebyla nalezena"));

    if (!(room.getAppUserReference().getEmail().equals(userInfo.getEmail()) || userInfo.getRoles().contains("ROLE_ADMIN"))) {
      throw new ForbiddenException("Místnost může smazat pouze její autor");
    }

    for (RoomPlan plan : room.getRoomPlans()) {
      if (plan.getTermRuns().size() != 0) {
        throw new ForbiddenException("Tuto místnost nelze odstranit, jelikož existuje termín zkoušky, který se v ní koná.\n" +
          "Pokud potřebujete místnost smazat, odeberte jí ze všech termínů zkoušek.");
      }
    }

    try {
      roomRepository.deleteById(roomId);
    } catch (Exception e) {
      throw new BadRequestException("Nepodařilo se smazat místnost. Ujistěte se, že tato místnost existuje.");
    }
    return ResponseEntity.ok()
      .body(new MessageResponseDto("Místnost úspěšně odstraněna"));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseEntity<List<FreeRoomResponseDto>> getFreeRooms(FreeRoomRequestDto request) {
    List<FreeRoomResponseDto> freeRooms = new ArrayList<>();

    long endTime = request.getStartTime() + ((long) request.getTermRunLength() * request.getNumberOfTermRuns() * 60 * 1000);

    List<Room> rooms = findFreeRooms(request.getStartTime(), endTime, request.getTermId());
    for (Room room : rooms) {
      freeRooms.add(createFreeRoomResponseDtoFromRoom(room));
    }

    return ResponseEntity.ok()
      .body(freeRooms);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Optional<Room> findRoomById(int id) {
    return this.roomRepository.findById(id);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<Room> findFreeRooms(long startTime, long endTime, Integer termId) {
    if (termId == null) {
      return roomRepository.findFreeRooms(new Date(startTime), new Date(endTime));
    } else {
      return roomRepository.findFreeRooms(new Date(startTime), new Date(endTime), termId);
    }
  }

  private void checkIfRoomPlanRowsHasSameLength(NewRoomDto newRoomDto) {
    boolean correctNumberOfColumns = true;
    int fstRowLength = newRoomDto.getCells().get(0).size();
    for (List<CellType> row: newRoomDto.getCells()) {
      if (row.size() != fstRowLength) {
        correctNumberOfColumns = false;
        break;
      }
    }

    if (!correctNumberOfColumns) {
      throw new BadRequestException("Všechny řady v místnosti musí mít stejný počet buněk.");
    }
  }

  private FreeRoomResponseDto createFreeRoomResponseDtoFromRoom(Room room) {
    List<RoomPlanDto> roomPlans = new ArrayList<>();
    room.getRoomPlans().forEach(roomPlan ->
      roomPlans.add(
        new RoomPlanDto(roomPlan.getIdRoomPlan(), roomPlan.getCapacity(), roomPlan.getSpacing())
      )
    );

    FreeRoomResponseDto freeRoomResponse = new FreeRoomResponseDto();
    freeRoomResponse.setRoomId(room.getIdRoom());
    freeRoomResponse.setAbbreviation(room.getAbbreviation());
    freeRoomResponse.setName(room.getName());
    freeRoomResponse.setRoomPlans(roomPlans);
    return freeRoomResponse;
  }

  private List<RoomPlan> generateRoomPlans(List<List<CellType>> cells, Room newRoom) {
    int[] numberOfSeatsInColumns = getNumberOfSeatsInColumns(newRoom, cells);
    RoomPlan baseRoomPlan = createRoomPlan(SpacingBetweenStudents.ZERO, newRoom, numberOfSeatsInColumns, cells);
    RoomPlan oneSpaceRoomPlan = createRoomPlan(SpacingBetweenStudents.ONE, newRoom, numberOfSeatsInColumns, cells);
    RoomPlan twoSpaceRoomPlan = createRoomPlan(SpacingBetweenStudents.TWO, newRoom, numberOfSeatsInColumns, cells);

    return new ArrayList<>(Arrays.asList(baseRoomPlan, oneSpaceRoomPlan, twoSpaceRoomPlan));
  }

  private RoomPlan createRoomPlan(SpacingBetweenStudents spacing, Room newRoom, int[] numberOfSeatsInColumns, List<List<CellType>> cellTypes) {
    boolean[] selectedColumns = selectColumns(numberOfSeatsInColumns, spacing);
    int capacity = 0;
    for (int i = 0; i < numberOfSeatsInColumns.length; i++) {
      if (selectedColumns[i]) {
        capacity += numberOfSeatsInColumns[i];
      }
    }
    RoomPlan newRoomPlan = new RoomPlan(capacity, spacing, null, null, newRoom);

    List<Cell> cells = createRoomPlanCells(cellTypes, selectedColumns, newRoomPlan);
    newRoomPlan.setCells(cells);

    return newRoomPlan;
  }

  private int[] getNumberOfSeatsInColumns(Room newRoom, List<List<CellType>> cells) {
    int[] numberOfSeatsInColumns = new int[newRoom.getNumberOfColumns()];
    for (int row = 0; row < newRoom.getNumberOfRows(); row++) {
      for (int column = 0; column < newRoom.getNumberOfColumns(); column++) {
        CellType cellType = cells.get(row).get(column);
        if (cellType == CellType.SEAT) {
          numberOfSeatsInColumns[column]++;
        }
      }
    }
    return numberOfSeatsInColumns;
  }

  private boolean[] selectColumns(int[] numberOfSeatsInColumns, SpacingBetweenStudents spacing) {
    int length = numberOfSeatsInColumns.length;
    boolean[] finalResults = new boolean[length];

    if (spacing == SpacingBetweenStudents.ZERO) {
      Arrays.fill(finalResults, true);
      return finalResults;
    }

    int numberOfSkippedColumns = spacing.getSpacing() + 1;
    boolean[] candidatesForSelection = new boolean[length];
    int[] dp = new int[length];

    // set first value
    if (length > 0) {
      dp[0] = Math.max(0, numberOfSeatsInColumns[0]);   // just in case if all columns have negative value
      candidatesForSelection[0] = true;
    }

    // dynamic programming
    // calculate values for single columns
    for (int i = 1; i < numberOfSkippedColumns; i++) {
      if (length > i) {
        if (dp[i - 1] >= numberOfSeatsInColumns[i]) { // >= because we want to use the most left columns so more columns on the right can fit
          dp[i] = dp[i - 1];
        } else {
          dp[i] = numberOfSeatsInColumns[i];
          candidatesForSelection[i] = true;
        }
      }
    }

    // calculate values for combination of two or more columns
    // at each iteration get the maximum possible sum for the current length and save it for later use
    for (int i = numberOfSkippedColumns; i < length; i++) {
      int maxSumOfPrevious = dp[i - 1];
      int maxSumOfCurrentValuePlusPreviousAfterSkipped = numberOfSeatsInColumns[i] + dp[i - numberOfSkippedColumns];
      if (maxSumOfPrevious > maxSumOfCurrentValuePlusPreviousAfterSkipped) {
        dp[i] = maxSumOfPrevious;
      } else {
        dp[i] = maxSumOfCurrentValuePlusPreviousAfterSkipped;
        candidatesForSelection[i] = true;
      }
    }

    // get final finalResults array
    for (int i = length - 1; i >= 0; ) {
      if (candidatesForSelection[i]) {
        finalResults[i] = true;
        i -= numberOfSkippedColumns;
      } else {
        i--;
      }
    }
    return finalResults;
  }

  private List<Cell> createRoomPlanCells(List<List<CellType>> cellTypes, boolean[] selectedColumns, RoomPlan roomPlan) {
    List<List<Cell>> allRowsResult = new ArrayList<>();
    int rowCounter = 1;
    boolean[] columnsWithSeat = new boolean[cellTypes.get(0).size()];
    for (int i = 0; i < cellTypes.size(); i++) {
      List<CellType> row = cellTypes.get(i);
      List<Cell> rowResult = new ArrayList<>();
      boolean containsSeatOrBlockedSeat = false;
      for (int j = 0; j < row.size(); j++) {
        CellType cell = row.get(j);
        CellType cellType;
        if (!selectedColumns[j] && cell == CellType.SEAT) {
          cellType = CellType.BLOCKED;
        } else {
          cellType = cell;
        }
        if (cellType == CellType.SEAT || cellType == CellType.BLOCKED) {
          containsSeatOrBlockedSeat = true;
          columnsWithSeat[j] = true;
          rowResult.add(new Cell(cellType, i + 1, j + 1, roomPlan, null, rowCounter, null));
        } else {
          rowResult.add(new Cell(cellType, i + 1, j + 1, roomPlan, null, null, null));
        }
      }
      if (containsSeatOrBlockedSeat) {
        rowCounter++;
      }
      allRowsResult.add(rowResult);
    }

    return parseResultCellList(columnsWithSeat, allRowsResult);
  }

  private List<Cell> parseResultCellList(boolean[] columnsWithSeat, List<List<Cell>> allRowsResult) {
    int columnCounter = 1;
    Integer[] columnNumbers = new Integer[columnsWithSeat.length];
    for (int i = 0; i < columnsWithSeat.length; i++) {
      if (columnsWithSeat[i]) {
        columnNumbers[i] = columnCounter;
        columnCounter++;
      } else {
        columnNumbers[i] = null;
      }
    }

    List<Cell> resultList = new ArrayList<>();
    for (List<Cell> row : allRowsResult) {
      for (int i = 0; i < row.size(); i++) {
        Cell cell = row.get(i);
        if (cell.getCellType() == CellType.SEAT || cell.getCellType() == CellType.BLOCKED) {
          cell.setSeatColumnNumber(columnNumbers[i]);
        }
      }
      resultList.addAll(row);
    }
    return resultList;
  }

  private List<List<CellDto>> formatPlanCells(int rows, List<Cell> cells) {
    List<List<CellDto>> cellRows = new ArrayList<>();
    while (cellRows.size() < rows) {
      cellRows.add(new ArrayList<>());
    }

    cells.forEach(cell -> {
      CellDto cellDto = new CellDto(cell.getIdCell(), cell.getCellType());
      cellRows.get(cell.getRowNumber() - 1)
        .add(cell.getColumnNumber() - 1, cellDto);
    });

    return cellRows;
  }

  private EditRoomDto createNewRoomDtoFromRoom(Room room) {
    List<List<CellType>> cells = new ArrayList<>();
    for(int i = 0; i < room.getNumberOfRows(); i++) {
      cells.add(new ArrayList<>());
    }
    room.getRoomPlans().get(0)
      .getCells()
      .forEach(cell -> {
        CellType cellType = cell.getCellType();
        cells.get(cell.getRowNumber() - 1).add(cell.getColumnNumber() - 1, cellType);
      });


    return new EditRoomDto(
      room.getAbbreviation(),
      room.getName(),
      room.getDescription(),
      room.getNumberOfRows(),
      room.getNumberOfColumns(),
      cells
    );
  }

  private RoomInfoDto mapRoomToRoomInfoDto(Room room) {
    List<Integer> roomPlansCapacities = room.getRoomPlans()
      .stream()
      .map(RoomPlan::getCapacity)
      .collect(Collectors.toList());

    AppUser roomCreator = room.getAppUserReference();

    return new RoomInfoDto(room.getIdRoom(), room.getAbbreviation(), room.getName(),
      roomPlansCapacities, roomCreator.getNameWithDegrees(), roomCreator.getEmail());
  }
}
