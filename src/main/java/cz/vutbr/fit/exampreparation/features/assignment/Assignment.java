package cz.vutbr.fit.exampreparation.features.assignment;

import cz.vutbr.fit.exampreparation.features.appUser.AppUser;
import cz.vutbr.fit.exampreparation.features.termRun.TermRun;
import cz.vutbr.fit.exampreparation.features.testSchedule.TestSchedule;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

/**
 * This entity class is responsible for object-relational mapping of an objects
 * with information about assignments into a database table Assignment.
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
public class Assignment {

  @Id
  @Setter(AccessLevel.PRIVATE)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int idAssignment;

  @NotBlank(message = "Chybějící název šablony zadání")
  @Size(max = 255, message = "Příliš dlouhý vstup")
  @Column(unique = true)
  private String name;

  @NotNull(message = "Chybějící stav šablony zadání")
  @Enumerated(EnumType.STRING)
  private AssignmentStatus status;

  @ManyToMany(mappedBy = "assignments", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
  private List<TermRun> termRuns = new ArrayList<>();

  @OneToMany(mappedBy = "assignment", cascade = CascadeType.ALL, orphanRemoval = true)
  @OnDelete(action = OnDeleteAction.CASCADE)
  private List<TestSchedule> testSchedules = new ArrayList<>();

  @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
  @JoinTable(
    name = "app_user_assignment_link",
    joinColumns = {@JoinColumn(name = "id_assignment")},
    inverseJoinColumns = {@JoinColumn(name = "id_app_user")}
  )
  private List<AppUser> assignmentManagers = new ArrayList<>();

  /**
   * Creates new instance of {@code Assignment}.
   * ID attribute will be set after storing the object into database.
   *
   * @param name  name of the assignment
   * @param status  assignment status
   * @param termRuns  list of term runs to which the assignment is assigned
   * @param assignmentManagers  list of users that have access to assignment management
   * @param testSchedules  list of test schedules which use this assignment
   */
  public Assignment(String name, AssignmentStatus status, List<TermRun> termRuns, List<AppUser> assignmentManagers,
                    List<TestSchedule> testSchedules) {
    this.name = name;
    this.status = status;
    this.termRuns = termRuns;
    this.assignmentManagers = assignmentManagers;
    this.testSchedules = testSchedules;
  }
}
