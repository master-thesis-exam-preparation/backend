package cz.vutbr.fit.exampreparation.features.assignment.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import cz.vutbr.fit.exampreparation.features.appUser.dto.EmployeeDetailsDto;
import cz.vutbr.fit.exampreparation.features.assignment.AssignmentStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * Used to store information about assignment (name of assignment, list of assignment files etc.).
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AssignmentInfoDto extends BasicAssignmentInfoDto{

  @NotEmpty(message = "Chybějící soubory šablony zadání")
  private List<FileDataDto> files;

  @NotEmpty(message = "Chybí seznam správců šablony zadání")
  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  private List<EmployeeDetailsDto> managers;

  /**
   * Creates new instance of {@link AssignmentInfoDto}.
   *
   * @param assignmentId  unique ID of the assignment
   * @param name  assignment name
   * @param status  status of the assignment (uploaded, generated, ...)
   * @param files  list of assignment files
   * @param managers  list of users that has access to the assignment management
   */
  public AssignmentInfoDto(Integer assignmentId, String name, AssignmentStatus status, List<FileDataDto> files, List<EmployeeDetailsDto> managers) {
    super(assignmentId, name, status);
    this.files = files;
    this.managers = managers;
  }
}
