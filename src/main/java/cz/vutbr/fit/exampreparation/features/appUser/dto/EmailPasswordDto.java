package cz.vutbr.fit.exampreparation.features.appUser.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * Used to store information about the e-mail address and password of the logged-in user.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmailPasswordDto extends EmailDto {

  @NotBlank(message = "Chybějící heslo")
  private String password;

}
