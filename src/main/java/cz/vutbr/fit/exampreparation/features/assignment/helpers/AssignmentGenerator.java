package cz.vutbr.fit.exampreparation.features.assignment.helpers;

import cz.vutbr.fit.exampreparation.common.AsyncService;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.BadRequestException;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.ConflictException;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.GeneratingFailedException;
import cz.vutbr.fit.exampreparation.features.assignment.Assignment;
import cz.vutbr.fit.exampreparation.features.assignment.AssignmentRepository;
import cz.vutbr.fit.exampreparation.features.assignment.AssignmentStatus;
import cz.vutbr.fit.exampreparation.features.termRun.TermRun;
import cz.vutbr.fit.exampreparation.features.termStudent.TermStudent;
import cz.vutbr.fit.exampreparation.features.testSchedule.TestSchedule;
import cz.vutbr.fit.exampreparation.features.testSchedule.TestScheduleService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * This service class is responsible for generating assignment PDF files.
 */
@Service
@AllArgsConstructor
public class AssignmentGenerator {

  private final StorageManager storageManager;
  private final TestScheduleService testScheduleService;
  private final AssignmentRepository assignmentRepository;
  private final AsyncService asyncService;
  private final RunningGeneratingService runningGeneratingService;

  /**
   * Asynchronously and in parallel generates assignment PDF files.
   *
   * For each provided {@link TestSchedule} will generate his PDF file with provided personal information.
   *
   * @param termId  unique ID of the term whose assignments will be generated
   * @param testSchedules  list of personal information that will be added to assignment TEX files and from which will be generated PDF files
   * @param assignments  set of assignment that will be generated
   */
  public void generateAssignments(int termId, List<TestSchedule> testSchedules, Set<Assignment> assignments) {
    Set<String> termRunsDirPaths = createWorkingDirs(testSchedules);
    lockAssignments(assignments);
    clearTestSchedulesPdfPaths(testSchedules);
    runningGeneratingService.removeAllTermFailedGenerations(termId);

    List<String> tmpDirs;
    try {
      tmpDirs = prepareTmpDirs(termRunsDirPaths);
    } catch (Exception e) {
      unlockAssignments(assignments);
      throw e;
    }

    asyncService.process(() -> generateProcess(termId, testSchedules, assignments, tmpDirs));
  }

  /**
   * Stops all assignment generating in provided term.
   *
   * @param termId  unique ID of the term whose assignments should be stop generating
   */
  public void cancelAssignmentGenerating(int termId) {
    runningGeneratingService.cancelTermTasks(termId);
  }

  /**
   * Creates output directories for files, that will be temporary created during assignment generating.
   * If any directory exists, all files in it will be deleted.
   *
   * @param termRunsDirPaths  list of directories paths to be created
   * @return list of paths to created directories
   */
  public List<String> prepareTmpDirs(Set<String> termRunsDirPaths) {
    List<String> tmpDirPaths = new ArrayList<>();
    for (String path : termRunsDirPaths) {
      String tmpDirPath = path + File.separator + "tmp";
      storageManager.createOrClearDir(tmpDirPath);
      tmpDirPaths.add(tmpDirPath);
    }
    return tmpDirPaths;
  }

  private void clearTestSchedulesPdfPaths(List<TestSchedule> testSchedules) {
    testSchedules.forEach(testSchedule -> testSchedule.setPdfPath(null));
    testScheduleService.saveAll(testSchedules);
  }

  private void generateProcess(int termId, List<TestSchedule> testSchedules, Set<Assignment> assignments, List<String> tmpDirs) {
    try {
      for (CompletableFuture<?> f : generatePdfs(testSchedules)) {
        try {
          f.get();
        } catch (CancellationException e) {
          throw e;
        } catch (Exception e) {
          TestSchedule failedTestSchedule = runningGeneratingService.getFutureTestSchedule(f);
          if (failedTestSchedule == null) {
            // all task already canceled, no need to do anything more
            throw e;
          }
          cancelAssignmentGenerating(termId);
          if (e.getCause() instanceof GeneratingFailedException) {
            GeneratingFailedException ex = (GeneratingFailedException) e.getCause();
            runningGeneratingService.addGeneratingError(failedTestSchedule, ex.getErrorCode(), ex.getMessage(), ex.getLogFilePath());
          } else {
            runningGeneratingService.addGeneratingError(failedTestSchedule, 1, e.getMessage(), null);
          }
          throw e;
        }
      }
      markAssignmentsAsGenerated(assignments);
      deleteTmpDirs(tmpDirs);
    } catch (Exception e) {
      unlockAssignments(assignments);
      throw new RuntimeException(e);
    }
  }

  private void deleteTmpDirs(List<String> tmpDirs) {
    tmpDirs.forEach(storageManager::deleteDirRecursively);
  }

  private void lockAssignments(Set<Assignment> assignments) {
    for (Assignment assignment : assignments) {
      if (assignment.getStatus() == AssignmentStatus.LOCKED) {
        throw new ConflictException("Šablona zadání \"" + assignment.getName() + "\" je uzamčena.\n" +
          "Již probíhá jiné generování zadání.");
      } else {
        lockAssignment(assignment);
      }
    }
  }

  private void unlockAssignments(Set<Assignment> assignments) {
    assignments.forEach(this::unlockAssignment);
  }

  private void markAssignmentsAsGenerated(Set<Assignment> assignments) {
    assignments.forEach(this::markAsGenerated);
  }

  private Set<String> createWorkingDirs(List<TestSchedule> testSchedules) {
    Set<String> termRunsWorkingDirs = new HashSet<>();
    Set<String> roomPlanWorkingDirs = new HashSet<>();
    for (TestSchedule testSchedule : testSchedules) {
      Assignment assignment = testSchedule.getAssignment();
      if (assignment == null) {
        throw new BadRequestException("Usazení s ID " + testSchedule.getIdTestSchedule() + " nemá doposud přidělené zadání");
      }

      String roomAbbreviation = testScheduleService.getRoomAbbreviationFromTestSchedule(testSchedule);

      String termRunDir = testScheduleService.getTermRunDir(testSchedule);
      String roomPlanDir = testScheduleService.getRoomPlanDir(termRunDir, roomAbbreviation);
      termRunsWorkingDirs.add(termRunDir);
      roomPlanWorkingDirs.add(roomPlanDir);
    }

    storageManager.createTestScheduleDirs(roomPlanWorkingDirs, testSchedules.size() != 1);
    return termRunsWorkingDirs;
  }

  private void lockAssignment(Assignment assignment) {
    assignment.setStatus(AssignmentStatus.LOCKED);
    assignmentRepository.save(assignment);
  }

  private void unlockAssignment(Assignment assignment) {
    assignment.setStatus(AssignmentStatus.UPLOADED);
    assignmentRepository.save(assignment);
  }

  private void markAsGenerated(Assignment assignment) {
    assignment.setStatus(AssignmentStatus.GENERATED);
    assignmentRepository.save(assignment);
  }

  private List<CompletableFuture<?>> generatePdfs(List<TestSchedule> testSchedules) {
    return testSchedules.stream()
      .map(testSchedule -> {
        CompletableFuture<?> process = asyncService.process(() -> generatePdf(testSchedule));
        runningGeneratingService.addTask(testSchedule, process);
        process.thenRun(() -> runningGeneratingService.removeTask(testSchedule));
        return process;
      })
      .collect(Collectors.toList());
  }

  private void generatePdf(TestSchedule testSchedule) {
    Assignment testScheduleAssignment = testSchedule.getAssignment();
    String assignmentDirPath = storageManager.getAssignmentDirPath(testScheduleAssignment.getIdAssignment());
    List<Path> texFilesPaths = getTexFiles(assignmentDirPath);

    String roomAbbreviation = testScheduleService.getRoomAbbreviationFromTestSchedule(testSchedule);
    TermRun termRun = testSchedule.getTermRun();

    String datetime = new SimpleDateFormat("dd. MM. yyyy HH:mm")
      .format(new Date(termRun.getStartDateTime().getTime()));

    String date = new SimpleDateFormat("dd. MM. yyyy")
      .format(new Date(termRun.getStartDateTime().getTime()));

    TermStudent student = testSchedule.getStudent();

    StudentInfo studentInfo = new StudentInfo();
    studentInfo.setLogin(student.getLogin());
    studentInfo.setStudentName(student.getNameWithDegrees());
    studentInfo.setStudentOrderNumber(testSchedule.getOrderNumber());
    studentInfo.setTerm(datetime);
    studentInfo.setDate(date);
    studentInfo.setRoomAbbreviation(roomAbbreviation);
    studentInfo.setSeatNumber(testSchedule.getSeatNumber());
    studentInfo.setRowNumber(testSchedule.getCell().getSeatRowNumber());
    studentInfo.setColumnNumber(testSchedule.getCell().getSeatColumnNumber());

    String assignmentName = replaceWhiteSpaces(testScheduleAssignment.getName());
    String jobname = studentInfo.getSeatNumber() + "_" + studentInfo.getRoomAbbreviation() + "_" +
      studentInfo.getLogin() + "_" + studentInfo.getStudentOrderNumber() + "_" + assignmentName;

    String termRunDir = testScheduleService.getTermRunDir(testSchedule);
    String pdfsDirPath = testScheduleService.getRoomPlanDir(termRunDir, roomAbbreviation);
    String tmpDirPath = termRunDir + File.separator + "tmp" + File.separator + testScheduleAssignment.getIdAssignment() +
      File.separator + jobname;
    storageManager.createOrClearDir(tmpDirPath);

    storageManager.copyNonTexFiles(assignmentDirPath, tmpDirPath);
    for (Path file : texFilesPaths) {
      replaceKeywordsByData(studentInfo, assignmentDirPath, file.toString(), tmpDirPath);
    }

    String pdfFilePath = generatePdf(tmpDirPath, jobname);
    String pdfPath = moveFile(pdfFilePath, tmpDirPath, pdfsDirPath);
    testScheduleService.updatePdfPath(testSchedule, pdfPath);
  }

  private String replaceWhiteSpaces(String fileName) {
    return fileName.replaceAll("\\s+", "-");
  }

  private List<Path> getTexFiles(String srcDirPath) {
    try {
      return Files.walk(Paths.get(srcDirPath))
        .filter(Files::isRegularFile)
        .filter(path -> path.getFileName().toString().toLowerCase().endsWith(".tex"))
        .collect(Collectors.toList());
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException("Chyba při procházení souborů");
    }
  }

  private void replaceKeywordsByData(StudentInfo studentInfo, String srcDirPath, String srcTexFilePath, String destDirPath) {
    String originalText = storageManager.loadAsString(srcTexFilePath);

    originalText = originalText.replaceAll("@@LOGIN@@", studentInfo.getLogin());
    originalText = originalText.replaceAll("@@JMENO@@", studentInfo.getStudentName());
    originalText = originalText.replaceAll("@@TERMIN@@", studentInfo.getTerm());
    originalText = originalText.replaceAll("@@DATUM@@", studentInfo.getDate());
    originalText = originalText.replaceAll("@@CISLO-STUDENTA@@", String.valueOf(studentInfo.getStudentOrderNumber()));
    originalText = originalText.replaceAll("@@MISTNOST@@", studentInfo.getRoomAbbreviation());
    originalText = originalText.replaceAll("@@MISTO@@", String.valueOf(studentInfo.getSeatNumber()));
    originalText = originalText.replaceAll("@@RADA@@", String.valueOf(studentInfo.getRowNumber()));
    originalText = originalText.replaceAll("@@SLOUPEC@@", String.valueOf(studentInfo.getColumnNumber()));

    String tmpFilePath = destDirPath + srcTexFilePath.replace(srcDirPath, "");

    File tmpTexFile = new File(tmpFilePath);
    PrintWriter writer;
    try {
      writer = new PrintWriter(tmpTexFile, StandardCharsets.UTF_8);   // TODO check encoding
      writer.print(originalText);
      writer.close();
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException("Chyba při zápisu dat do souboru");
    }
  }

  private String generatePdf(String dirPath, String jobname) {
    String outputFileNameWithoutExtension = dirPath + File.separator + jobname;
    try {
      Process p = new ProcessBuilder("latexmk", "-pdf", "-silent", "-jobname=" + jobname).directory(new File(dirPath)).start();
      boolean finishedNormally = p.waitFor(20, TimeUnit.SECONDS);
      if (!finishedNormally) {
        throw new GeneratingFailedException("Generování zadání \"" + jobname + "\" trvalo příliš dlouho a muselo být ukončeno.\n" +
          "Nahlédněte do chybového logu a zkuste opakovat pokus.", p.exitValue(), outputFileNameWithoutExtension + ".log");
      }

      if (p.exitValue() != 0) {
        throw new GeneratingFailedException("Při generování zadání \"" + jobname + "\" nastala chyba.\n" +
          "Nahlédněte do chybového logu a zkuste opakovat pokus.", p.exitValue(), outputFileNameWithoutExtension + ".log");
      }

      BufferedReader stdInput = new BufferedReader(new
        InputStreamReader(p.getInputStream()));

      BufferedReader stdError = new BufferedReader(new
        InputStreamReader(p.getErrorStream()));

      // Read the output from the command
      System.out.println("Here is the standard output of the command:\n");
      String s = null;
      while ((s = stdInput.readLine()) != null) {
        System.out.println(s);
      }

      // Read any errors from the attempted command
      System.out.println("Here is the standard error of the command (if any):\n");
      while ((s = stdError.readLine()) != null) {
        System.out.println(s);
      }
      return outputFileNameWithoutExtension + ".pdf";
    } catch (InterruptedException | IOException e) {
      e.printStackTrace();
      String message = "Chyba při generování PDF." +
        "\nOpakujte pokus nebo kontaktujte administrátora.";
      throw new RuntimeException(message);
    }
  }

  private String moveFile(String srcFilePath, String srcDirPath, String destDirPath) {
    Path file = Paths.get(srcFilePath);
    Path src = Paths.get(srcDirPath);
    Path dest = Paths.get(destDirPath);
    try {
      return Files.move(file, dest.resolve(src.relativize(file)), StandardCopyOption.REPLACE_EXISTING).toString();
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException("Chyba při přesunu souboru");
    }
  }
}
