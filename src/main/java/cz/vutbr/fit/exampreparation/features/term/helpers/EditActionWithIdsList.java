package cz.vutbr.fit.exampreparation.features.term.helpers;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * Used to store IDs of the changed objects and edit scenario that will be applied.
 */
@Data
@AllArgsConstructor
public class EditActionWithIdsList {

  private EditAction editAction;

  private List<Integer> changedObjectIdsList;
}
