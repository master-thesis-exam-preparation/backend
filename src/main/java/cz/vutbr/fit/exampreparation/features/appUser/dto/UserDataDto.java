package cz.vutbr.fit.exampreparation.features.appUser.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * Used to store information about name and degrees of the user.
 */
@Data
@AllArgsConstructor
public class UserDataDto {
  private String degreesBeforeName;
  private String degreesBehindName;

  @NotBlank(message = "Chybějící jméno")
  private String firstName;

  @NotBlank(message = "Chybějící příjmení")
  private String lastName;
}
