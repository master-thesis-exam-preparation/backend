package cz.vutbr.fit.exampreparation.features.assignment.dto;

import cz.vutbr.fit.exampreparation.features.assignment.AssignmentStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Used to store name of assignment, his unique ID and his status.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BasicAssignmentInfoDto {

  @NotNull(message = "Chybějící identifikátor šablony zadání")
  private Integer assignmentId;

  @NotBlank(message = "Chybějící název šablony zadání")
  private String name;

  @NotNull(message = "Chybějící stav šablony zadání")
  private AssignmentStatus status;
}
