package cz.vutbr.fit.exampreparation.features.assignment.helpers;

import cz.vutbr.fit.exampreparation.features.assignment.dto.GeneratingFailDto;
import cz.vutbr.fit.exampreparation.features.testSchedule.TestSchedule;
import cz.vutbr.fit.exampreparation.features.testSchedule.dto.TestScheduleDetailDto;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * This service class is responsible for management of running assignments generating.
 */
@Service
public class RunningGeneratingService {

  private final Map<TestSchedule, CompletableFuture<?>> runningTasks = new HashMap<>();

  private final Map<TestSchedule, GeneratingFailDto> errors = new HashMap<>();

  /**
   * Adds additional assignment to the assignment queue that will be generated in parallel.
   *
   * @param testSchedule  personal information and assignment to be generated in parallel.
   * @param future  process ready to run (or already running process)
   */
  public void addTask(TestSchedule testSchedule, CompletableFuture<?> future) {
    runningTasks.put(testSchedule, future);
  }

  /**
   * Removes finished (or still running) process from assignment queue. If process was still running it will be killed.
   *
   * @param testSchedule  process of this test schedule will be removed from queue
   */
  public void removeTask(TestSchedule testSchedule) {
    runningTasks.remove(testSchedule);
  }

  /**
   * Finds {@link TestSchedule} object of running process with personal information.
   *
   * @param future  running process
   * @return personal information and assignment of running generating process
   */
  public TestSchedule getFutureTestSchedule(CompletableFuture<?> future) {
    List<TestSchedule> testSchedules = runningTasks.entrySet()
      .stream()
      .filter(entry -> future.equals(entry.getValue()))
      .map(Map.Entry::getKey)
      .collect(Collectors.toList());

    if (testSchedules.size() == 0) {
      return null;
    } else {
      return testSchedules.get(0);
    }
  }

  /**
   * Checks if any generating processes runs for provided term.
   *
   * @param termId  unique ID of the term that will be checked
   * @return true if any generating tasks
   */
  public boolean doesTermHaveAnyRunningTask(int termId) {
    return runningTasks.keySet()
      .stream()
      .anyMatch(getTermTestSchedulePredicate(termId));
  }

  /**
   * Gets list of running generating processes for provided term.
   *
   * @param termId  unique ID of the term for which the tasks will be searched
   * @return list of running generating processes
   */
  public List<CompletableFuture<?>> getTermRunningTasks(int termId) {
    return runningTasks.keySet()
      .stream()
      .filter(getTermTestSchedulePredicate(termId))
      .map(runningTasks::get)
      .collect(Collectors.toList());
  }

  /**
   * Cancels all running generating processes for provided term.
   *
   * @param termId  unique ID of the term for which the tasks will be canceled
   */
  public void cancelTermTasks(int termId) {
    List<TestSchedule> testSchedules = runningTasks.keySet()
      .stream()
      .filter(getTermTestSchedulePredicate(termId))
      .collect(Collectors.toList());

    testSchedules.forEach(testSchedule -> {
      runningTasks.get(testSchedule).cancel(true);
      removeTask(testSchedule);
    });
  }

  /**
   * Adds new generating error to map of occurred errors.
   *
   * @param testSchedule  information about failed generating process
   * @param errorCode  error code returned by the LaTeX compiler
   * @param errorMessage  error message (e.g. from thrown exception)
   * @param logFilePath  path to file with information about error
   */
  public void addGeneratingError(TestSchedule testSchedule, int errorCode, String errorMessage, String logFilePath) {
    TestScheduleDetailDto testScheduleDetail = new TestScheduleDetailDto(
      testSchedule.getStudent().getLogin(),
      testSchedule.getStudent().getNameWithDegrees(),
      testSchedule.getIdTestSchedule(),
      testSchedule.getSeatNumber(),
      testSchedule.getOrderNumber(),
      null,
      false
    );
    errors.put(testSchedule, new GeneratingFailDto(testScheduleDetail, errorCode, errorMessage, logFilePath != null, logFilePath));
  }

  /**
   * Removes error that relates to given personal information.
   *
   * @param testSchedule  information for which should be error removed
   */
  public void removeGeneratingError(TestSchedule testSchedule) {
    errors.remove(testSchedule);
  }

  /**
   * Finds error that relates to given personal information.
   *
   * @param testSchedule  information for which should be error found
   * @return founded error, null if not found
   */
  public GeneratingFailDto getGeneratingError(TestSchedule testSchedule) {
    List<GeneratingFailDto> generatingFails = errors.keySet()
      .stream()
      .filter(ts -> ts.getIdTestSchedule() == testSchedule.getIdTestSchedule())
      .map(errors::get)
      .collect(Collectors.toList());

    if (generatingFails.size() == 0) {
      return null;
    } else {
      return generatingFails.get(0);
    }
  }

  /**
   * Finds all errors that relates to given term.
   *
   * @param termId  unique ID of the term for which the errors should be searched
   * @return list of found errors
   */
  public List<GeneratingFailDto> getAllTermFailedGenerations(int termId) {
    return errors.keySet()
      .stream()
      .filter(getTermTestSchedulePredicate(termId))
      .map(errors::get)
      .collect(Collectors.toList());
  }

  /**
   * Removes all errors that relates to given term.
   *
   * @param termId  unique ID of the term for which the errors should be canceled
   */
  public void removeAllTermFailedGenerations(int termId) {
    List<TestSchedule> testSchedules = errors.keySet()
      .stream()
      .filter(getTermTestSchedulePredicate(termId))
      .collect(Collectors.toList());

    testSchedules.forEach(this::removeGeneratingError);
  }

  private Predicate<TestSchedule> getTermTestSchedulePredicate(int termId) {
    return testSchedule -> testSchedule.getTermRun()
      .getTermReference()
      .getIdTerm() == termId;
  }
}
