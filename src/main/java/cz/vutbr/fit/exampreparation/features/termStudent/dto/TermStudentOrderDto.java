package cz.vutbr.fit.exampreparation.features.termStudent.dto;

import cz.vutbr.fit.exampreparation.features.termStudent.TermStudent;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * Used to store information about student and his order in alphabetical order is students list.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TermStudentOrderDto {

  @NotNull(message = "Chybí student")
  private TermStudent student;

  @NotNull(message = "Chybí pořadové číslo studenta")
  private Integer orderNumber;
}
