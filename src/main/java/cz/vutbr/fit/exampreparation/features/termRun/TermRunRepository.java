package cz.vutbr.fit.exampreparation.features.termRun;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * This class is responsible for querying database table TermRun
 */
@Repository
public interface TermRunRepository extends JpaRepository<TermRun, Integer> {

  List<TermRun> findByTermReference_IdTerm(int termId);
}
