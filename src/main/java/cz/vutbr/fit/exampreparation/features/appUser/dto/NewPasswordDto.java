package cz.vutbr.fit.exampreparation.features.appUser.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * Used to store information necessary to reset user password (token for validation and new password value).
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NewPasswordDto {

  @NotBlank(message = "Chybějící token pro obnovení hesla")
  private String token;

  @NotBlank(message = "Chybějící heslo")
  private String password;

}
