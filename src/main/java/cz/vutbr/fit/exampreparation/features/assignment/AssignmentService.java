package cz.vutbr.fit.exampreparation.features.assignment;

import cz.vutbr.fit.exampreparation.common.MessageResponseDto;
import cz.vutbr.fit.exampreparation.features.assignment.dto.*;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

public interface AssignmentService {

  /**
   * Creates new assignment based on given data.
   *
   * @param assignmentDto  information necessary to create new assignment
   * @param assignmentFiles  list of assignment files
   * @return response with success message
   */
  ResponseEntity<MessageResponseDto> createNewAssignment(BasicAssignmentDto assignmentDto, MultipartFile[] assignmentFiles);

  /**
   * Gets more specific information about provided assignment.
   *
   * @param assignmentId  unique ID of the assignment
   * @return response with information about assignment
   */
  ResponseEntity<AssignmentInfoDto> getAssignmentDetails(int assignmentId);

  /**
   * Gets information about all assignments.
   *
   * @return response with information about all assignments.
   */
  ResponseEntity<List<BasicAssignmentInfoDto>> getAllAssignmentsInfo();

  /**
   * Deletes specified assignment from database.
   *
   * @param assignmentId  unique ID of the assignment which should be deleted
   * @return response with success message
   */
  ResponseEntity<MessageResponseDto> deleteAssignment(int assignmentId);

  /**
   * Reads content of specified assignment file
   *
   * @param assignmentId  unique ID of the assignment to which the file belong
   * @param filePath  path to file which should be read
   * @return response with content of a file as String
   */
  ResponseEntity<FileContentDto> getAssignmentFileContent(int assignmentId, String filePath);

  /**
   * Gets URI address of specified file path.
   *
   * @param assignmentId  unique ID of the assignment to which the file belong
   * @param filePath  path to file which should be downloaded
   * @return response with URI to download the file
   */
  ResponseEntity<Resource> downloadAssignmentFile(int assignmentId, String filePath);

  /**
   * Edits existing assignment data based on new given data.
   *
   * @param assignmentId  unique ID of the assignment which should be edited
   * @param assignmentDto  information about changes
   * @param assignmentFiles  new assignment files
   * @return response with success message
   */
  ResponseEntity<MessageResponseDto> editAssignment(int assignmentId, EditAssignmentDto assignmentDto,
                                                    MultipartFile[] assignmentFiles);

  /**
   * Starts generating of given assignments.
   *
   * <ul>
   *   <li>If only {@code termId} is provided assignments for all students will be generated.</li>
   *   <li>If {@code termId} and {@code termRunId} are provided only assignments of specified term run will be generated.</li>
   *   <li>If {@code termId}, {@code termRunId} and {@code roomAbbreviation} are provided only assignments of specified room of specified term run will be generated.</li>
   *   <li>If {@code testScheduleId} is provided only its assignment will be generated.</li>
   * </ul>
   *
   * @param termId  unique ID of the term
   * @param termRunId  unique ID of the term run
   * @param roomAbbreviation  term runs room abbreviation
   * @param testScheduleId  unique ID of the test schedule
   * @return response with success message
   */
  ResponseEntity<MessageResponseDto> generateAssignment(Integer termId, Integer termRunId, String roomAbbreviation, Integer testScheduleId);

  /**
   * Cancels all generating processes of given term.
   *
   * @param termId  unique ID of the term which generating processes should be canceled
   * @return response with success message
   */
  ResponseEntity<MessageResponseDto> cancelAssignmentGenerating(Integer termId);

  /**
   * Tries to find the assignment by provided assignment ID in database.
   *
   * @param assignmentId unique ID of the searched assignment
   * @return the found {@link Assignment}, if not found it will return null
   */
  Optional<Assignment> findAssignmentById(Integer assignmentId);
}
