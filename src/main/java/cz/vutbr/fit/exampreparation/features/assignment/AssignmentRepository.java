package cz.vutbr.fit.exampreparation.features.assignment;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * This class is responsible for querying database table Assignment
 */
@Repository
public interface AssignmentRepository extends JpaRepository<Assignment, Integer> {
  List<Assignment> findByTermRuns_idTermRun(int termId);

  Optional<Assignment> findByName(String name);

  @Query( "SELECT a FROM Assignment a JOIN a.assignmentManagers am " +
          "WHERE am.email = ?1")
  List<Assignment> findAllAssignmentsWhereUserIsManager(String email);
}
