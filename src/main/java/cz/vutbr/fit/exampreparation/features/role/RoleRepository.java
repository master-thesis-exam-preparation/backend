package cz.vutbr.fit.exampreparation.features.role;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * This class is responsible for querying database table Role
 */
@Repository
public interface RoleRepository extends JpaRepository<Role, String> {
  Optional<Role> findByRoleName(RoleName roleName);

}
