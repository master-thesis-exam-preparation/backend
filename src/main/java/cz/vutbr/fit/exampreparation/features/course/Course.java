package cz.vutbr.fit.exampreparation.features.course;

import cz.vutbr.fit.exampreparation.features.term.Term;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.List;

/**
 * This entity class is responsible for object-relational mapping of an objects
 * with information about taught courses into a database table Course.
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(uniqueConstraints = {@UniqueConstraint(columnNames =
  {"abbreviation", "name", "academicYear"})})
public class Course {

  @Id
  @Setter(AccessLevel.PRIVATE)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int idCourse;

  @NotBlank(message = "Chybějící zkratka předmětu")
  @Size(max = 50, message = "Příliš dlouhý vstup")
  @Column(length = 50)
  private String abbreviation;

  @NotNull(message = "Chybějící akademický rok")
  @Min(value = 2010, message = "Neplatný rok (2000-2050)")
  @Max(value = 2050, message = "Neplatný rok (2000-2050)")
  private Integer academicYear;

  @NotBlank(message = "Chybějící název předmětu")
  @Size(max = 255, message = "Příliš dlouhý vstup")
  private String name;

  @OneToMany(mappedBy = "courseReference", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
  private List<Term> terms;

  /**
   * Creates new instance of {@code Course}.
   * ID attribute will be set after storing the object into database.
   *
   * @param abbreviation  course abbreviation (e.g. WAP)
   * @param academicYear  academic year when course is taught (e.g. if the course is taught in years 2020-2021 use 2020)
   * @param name  name of the course
   * @param terms  list of terms that take exam of this course
   */
  public Course(String abbreviation, Integer academicYear, String name, List<Term> terms) {
    this.abbreviation = abbreviation;
    this.academicYear = academicYear;
    this.name = name;
    this.terms = terms;
  }
}
