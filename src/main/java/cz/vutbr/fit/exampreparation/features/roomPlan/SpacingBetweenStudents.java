package cz.vutbr.fit.exampreparation.features.roomPlan;

/**
 * There can be 3 different numbers of free cell between students.
 *
 * <ul>
 *   <li>ZERO - each student is seated next to each other.</li>
 *   <li>ONE - between students is one free cell.</li>
 *   <li>TWO - between students are two free cells.</li>
 * </ul>
 */
public enum SpacingBetweenStudents {
  ZERO(0),
  ONE(1),
  TWO(2);

  private final int spacing;

  SpacingBetweenStudents(int spacing) {
    this.spacing = spacing;
  }

  /**
   * Gets numeric value of enum
   * @return numeric value
   */
  public int getSpacing() {
    return spacing;
  }
}
