package cz.vutbr.fit.exampreparation.features.roomPlan;

import java.util.List;
import java.util.Optional;

public interface RoomPlanService {

  /**
   * Deletes all specified room plan and their cell from database.
   *
   * @param roomPlans  list of room plans to be deleted
   */
  void deleteRoomPlans(List<RoomPlan> roomPlans);

  /**
   * Find the room plan by provided room ID and number of free cells between students in database.
   *
   * @param roomId  unique ID of the room
   * @param spacing  number of free cells between students
   * @return found room plan, if not found return null
   */
  RoomPlan findRoomPlanByRoomIdAndSpacing(Integer roomId, SpacingBetweenStudents spacing);

  /**
   * Tries to find the assignment by provided room plan ID in database.
   *
   * @param roomPlanId  unique ID of the searched room plan
   * @return the found {@link RoomPlan}, if not found it will return null
   */
  Optional<RoomPlan> findRoomPlanById(int roomPlanId);
}
