package cz.vutbr.fit.exampreparation.features.appUser;

import cz.vutbr.fit.exampreparation.common.AuthService;
import cz.vutbr.fit.exampreparation.common.RegexTester;
import cz.vutbr.fit.exampreparation.common.UserInfo;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.BadRequestException;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.ConflictException;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.NotFoundException;
import cz.vutbr.fit.exampreparation.features.appUser.dto.AccountInfoDto;
import cz.vutbr.fit.exampreparation.features.appUser.dto.EmployeeDetailsDto;
import cz.vutbr.fit.exampreparation.features.role.Role;
import cz.vutbr.fit.exampreparation.features.role.RoleName;
import cz.vutbr.fit.exampreparation.features.role.RoleService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * This service class is responsible for management of application users.
 */
@Service
@AllArgsConstructor
public class AppUserServiceImpl implements AppUserService {

  private final AppUserRepository appUserRepository;
  private final PasswordEncoder passwordEncoder;
  private final RoleService roleService;


  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseEntity<List<EmployeeDetailsDto>> getAllEmployeesList() {
    List<AppUser> employees = appUserRepository.findAllEmployeesAndAdmins();
    List<EmployeeDetailsDto> returnList = employees.stream()
      .map(employee -> new EmployeeDetailsDto(employee.getEmail(), employee.getNameWithDegrees()))
      .collect(Collectors.toList());

    return ResponseEntity.ok()
      .body(returnList);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public AppUser findUserByLogin(String login) {
    List<AppUser> appUsers = appUserRepository.findByLogin(login);
    if (appUsers.size() > 1) {
      throw new ConflictException("Uživatel s tímto loginem existuje vícekrát");
    } else if (appUsers.size() == 1) {
      return appUsers.get(0);
    } else {
      return null;
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Optional<AppUser> findByEmail(String email) {
    return appUserRepository.findByEmail(email);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public AppUser encodePasswordSetRoleAndSave(AppUser appUser) {
    RoleName role = getUserRoleFromEmail(appUser.getEmail());
    Role userRole = roleService.findRole(role)
      .orElseThrow(() -> new NotFoundException("Role \"" + role + "\" nebyla nalezena"));

    appUser.setPassword(passwordEncoder.encode(appUser.getPassword()));
    appUser.setRoles(Collections.singletonList(userRole));
    return appUserRepository.save(appUser);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public AppUser save(AppUser appUser) {
    return appUserRepository.save(appUser);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void deleteUser(AppUser appUser) {
    appUserRepository.delete(appUser);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void deleteUserById(int userId) {
    appUserRepository.deleteById(userId);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void checkAdditionalManagers(List<String> additionalManagersEmails) {
    if (additionalManagersEmails == null) {
      return;
    }
    for (String email : additionalManagersEmails) {
      Optional<AppUser> user = findByEmail(email);
      if (user.isEmpty()) {
        throw new BadRequestException("Vyučující s e-mailovou adresou '" + email + "' neexistuje");
      } else {
        for (Role role : user.get().getRoles()) {
          if (role.getRoleName() != RoleName.ADMIN && role.getRoleName() != RoleName.EMPLOYEE) {
            throw new BadRequestException("Nelze přidat uživatele s e-mailovou adresou '" + email + "'.\n" +
              "Jako dodatečného správce lze přidat pouze zaměstnance nebo administrátora");
          }
        }
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<AppUser> createManagerList(List<String> termManagersEmails) {
    List<AppUser> termManagers = new ArrayList<>();

    UserInfo userInfo = AuthService.getLoggedUserInfo();
    AppUser creator = findByEmail(userInfo.getEmail())
      .orElseThrow(() -> new BadRequestException("Uživatel neexistuje"));
    termManagers.add(creator);

    if (termManagersEmails != null) {
      termManagersEmails.forEach(email -> {
        AppUser user = findByEmail(email)
          .orElseThrow(() -> new BadRequestException("Uživatel neexistuje"));
        termManagers.add(user);
      });
    }
    return termManagers;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<AccountInfoDto> getAllAccountsInfo() {
    UserInfo userInfo = AuthService.getLoggedUserInfo();
    List<AppUser> appUsers = appUserRepository.findAll()
      .stream()
      .filter(appUser -> !appUser.getEmail().equals(userInfo.getEmail()))
      .collect(Collectors.toList());

    return appUsers.stream()
      .map(appUser -> {
          List<String> roles = appUser.getRoles()
            .stream()
            .map(role -> role.getRoleName().toString())
            .collect(Collectors.toList());
          return new AccountInfoDto(appUser.getIdAppUser(), appUser.getNameWithDegrees(), appUser.getEmail(), roles, appUser.isLocked());
        }
      )
      .collect(Collectors.toList());
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Optional<AppUser> findById(int userId) {
    return appUserRepository.findById(userId);
  }

  private RoleName getUserRoleFromEmail(String email) {
    if (RegexTester.testRegex("^.*@stud\\.(?:fit|feec|fce|fbm|fch|favu|fme|usi|fa)\\.(?:vut|vutbr)\\.cz$", email) ||
      RegexTester.testRegex("^.*@(?:vut|vutbr)\\.cz$", email)) {
      return RoleName.STUDENT;
    } else {
      return RoleName.EMPLOYEE;
    }
  }
}
