package cz.vutbr.fit.exampreparation.features.term;

import cz.vutbr.fit.exampreparation.common.MessageResponseDto;
import cz.vutbr.fit.exampreparation.features.term.dto.AllTermsInfoResponseDto;
import cz.vutbr.fit.exampreparation.features.term.dto.TermDetailDto;
import cz.vutbr.fit.exampreparation.features.term.dto.term.EditTermDto;
import cz.vutbr.fit.exampreparation.features.term.dto.term.EditTermInfoDto;
import cz.vutbr.fit.exampreparation.features.term.dto.term.NewTermDto;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * This class is responsible for managing term REST API endpoints.
 */
@Validated
@RestController
@AllArgsConstructor
public class TermController {

  private final TermService termService;

  /**
   * Gets information about all terms that can be viewed by unlogged users.
   *
   * @return response with public information about all terms
   */
  @GetMapping("/public/terms")
  public ResponseEntity<List<AllTermsInfoResponseDto>> getAllTermsInfoPublic() {
    return termService.getAllTermsInfo(true);
  }

  /**
   * Gets information about terms that cannot be viewed by unlogged users.
   *
   * @return response with private information about terms
   */
  @GetMapping("/internal/terms")
  public ResponseEntity<List<AllTermsInfoResponseDto>> getAllTermsInfoInternal() {
    return termService.getAllTermsInfo(false);
  }

  /**
   * Creates new term based on given data.
   *
   * @param newTermDto  information necessary to create new term
   * @return response with success message
   */
  @PreAuthorize("hasAnyRole('EMPLOYEE','ADMIN')")
  @PostMapping("/internal/terms")
  public ResponseEntity<MessageResponseDto> createNewTerm(@Valid @RequestBody NewTermDto newTermDto) {
    return termService.createNewTerm(newTermDto);
  }

  /**
   * Edits existing term data based on new given data.
   *
   * @param editTermDto  information about changes
   * @param termId  unique ID of the term
   * @return response with success message
   */
  @PreAuthorize("hasAnyRole('EMPLOYEE','ADMIN')")
  @PutMapping("/internal/terms/{termId:[\\d]+}")
  public ResponseEntity<MessageResponseDto> editTerm(@Valid @RequestBody EditTermDto editTermDto, @PathVariable int termId) {
    return termService.editTerm(termId, editTermDto);
  }

  /**
   * Gets information about term that can be viewed by unlogged users.
   *
   * @param termId  unique ID of the term
   * @return response with public information about term
   */
  @GetMapping("/public/terms/{termId:[\\d]+}")
  public ResponseEntity<TermDetailDto> getTermDetailsPublic(@PathVariable int termId) {
    return termService.getTermDetails(termId, true);
  }

  /**
   * Gets information about term that cannot be viewed by unlogged users.
   *
   * @param termId  unique ID of the term
   * @return response with private information about term
   */
  @PreAuthorize("hasAnyRole('EMPLOYEE','ADMIN','STUDENT')")
  @GetMapping("/internal/terms/{termId:[\\d]+}")
  public ResponseEntity<TermDetailDto> getTermDetailsInternal(@PathVariable int termId) {
    return termService.getTermDetails(termId, false);
  }

  /**
   * Deletes specified term from database.
   *
   * @param termId  unique ID of the term that should be deleted
   * @return response with success message
   */
  @PreAuthorize("hasAnyRole('EMPLOYEE','ADMIN')")
  @DeleteMapping("/internal/terms/{termId:[\\d]+}")
  public ResponseEntity<MessageResponseDto> deleteTerm(@PathVariable int termId) {
    return termService.deleteTerm(termId);
  }

  /**
   * Gets information about term that will be used to fill edit forms.
   *
   * @param termId  unique ID of the term
   * @return response with edit information about term
   */
  @PreAuthorize("hasAnyRole('EMPLOYEE','ADMIN')")
  @GetMapping("/internal/terms/{termId:[\\d]+}/edit")
  public ResponseEntity<EditTermInfoDto> getEditTermDetails(@PathVariable int termId) {
    return termService.getEditTermDetails(termId);
  }
}
