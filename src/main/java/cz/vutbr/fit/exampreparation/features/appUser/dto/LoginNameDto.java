package cz.vutbr.fit.exampreparation.features.appUser.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * Used to store information about the login and name with degrees of the logged-in user.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginNameDto {

  @NotBlank(message = "Chybějící login studenta")
  private String login;

  @NotBlank(message = "Chybějící jméno studenta")
  private String nameAndDegrees;

}
