package cz.vutbr.fit.exampreparation.features.appUser.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * Used to store information about the e-mail address of the logged-in user.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmailDto {

  @NotBlank(message = "Prosím vyplňte e-mailovou adresu")
  @Email(message = "Zkontrolujte, zda je e-mail ve správném formátu")
  @Size(max = 255, message = "Příliš dlouhý vstup")
  private String email;

}
