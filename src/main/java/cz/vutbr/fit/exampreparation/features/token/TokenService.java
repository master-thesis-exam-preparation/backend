package cz.vutbr.fit.exampreparation.features.token;

import cz.vutbr.fit.exampreparation.features.appUser.AppUser;
import cz.vutbr.fit.exampreparation.security.jwt.dto.AccessTokenDto;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface TokenService {

  /**
   * Create new token of specified type with specified expiration time after which will be deleted.
   *
   * @param appUser  user to which will token belong
   * @param tokenType  type of the new token
   * @param expireAfterHours  time after which will be created token deleted from database
   * @param remember  used with REFRESH token type - if true it will extend expiration time
   * @return created token
   */
  String generateToken(AppUser appUser, TokenType tokenType, int expireAfterHours, boolean remember);

  /**
   * Deletes all tokens of specified type that belong to specified user.
   *
   * @param userEmail  e-mail address of the user which tokens will be deleted
   * @param tokenType  type of token to be deleted
   */
  void deleteAllUserTokens(String userEmail, TokenType tokenType);

  /**
   * Deletes token that have specified token ID.
   *
   * @param tokenId  unique ID of the token which will be deleted
   */
  void deleteByTokenId(int tokenId);

  /**
   * Tries to find the token by provided token ID in database.
   *
   * @param id  unique ID of the searched token
   * @return the found {@link Token}, if not found it will return null
   */
  Optional<Token> findByTokenId(int id);

  /**
   * Generates new access token and refresh token cookie.
   *
   * @param token  current refresh token value
   * @return response with new access token and refresh token cookie
   */
  ResponseEntity<AccessTokenDto> generateNewKeys(String token);

  /**
   * Checks if the specified encoded token value is valid and if so, returns it from database.
   *
   * @param encodedToken  token to be verified
   * @return corresponding token from database
   */
  Token getTokenIfIsValid(String encodedToken);

  /**
   * Creates new refresh token cookie.
   *
   * @param userEmail  e-mail address of the user to which will cookie with token belong
   * @param rememberUser  if true, expiration time will be extended
   * @return new refresh token cookie
   */
  ResponseCookie createRefreshTokenCookie(String userEmail, boolean rememberUser);

  /**
   * Creates new refresh token cookie.
   *
   * @param appUser  user to which will cookie with token belong
   * @param rememberUser  if true, expiration time will be extended
   * @return new refresh token cookie
   */
  ResponseCookie createRefreshTokenCookie(AppUser appUser, boolean rememberUser);

  /**
   * Removes refresh token cookie from response.
   *
   * @return response cookie with empty value and zero time to live
   */
  ResponseCookie deleteRefreshTokenCookie();

  /**
   * Decides whether the provided encoded token is valid.
   *
   * @param rawTokenValue token in readable form
   * @param encodedToken hashed token value
   * @return true if token are same and therefore valid, false otherwise
   */
  boolean isTokenValid(String rawTokenValue, String encodedToken);
}
