package cz.vutbr.fit.exampreparation.features.appUser.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * Used to store information about the e-mail address of the logged-in user and his name with degrees.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeDetailsDto extends EmailDto {

  @NotBlank(message = "Chybějící jméno")
  @Size(max = 255, message = "Příliš dlouhý vstup")
  private String nameWithDegrees;

  /**
   * Creates new instance of {@link EmployeeDetailsDto}
   *
   * @param email  e-mail address of the logged-in user
   * @param nameWithDegrees  combination of degrees, name and surname of logged-in user
   */
  public EmployeeDetailsDto(String email, String nameWithDegrees) {
    super(email);
    this.nameWithDegrees = nameWithDegrees;
  }
}
