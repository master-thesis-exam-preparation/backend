package cz.vutbr.fit.exampreparation.features.room;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * This class is responsible for querying database table Room
 */
@Repository
public interface RoomRepository extends JpaRepository<Room, Integer> {
  List<Room> findAllByAbbreviationAndName(String abbreviation, String name);

  @Query("SELECT r FROM Room r JOIN r.appUserReference a " +
    "ORDER BY r.abbreviation ASC, r.name ASC, a.email ASC")
  List<Room> findAllSorted();

  @Query( "SELECT r FROM Room r " +
          "WHERE r NOT IN ( " +
            "SELECT rp.roomReference FROM RoomPlan rp LEFT JOIN rp.termRuns AS tr " +
            "WHERE " +
              "( " +
                "(tr.startDateTime > ?1 AND tr.startDateTime < ?2) OR (tr.endDateTime > ?1 AND tr.endDateTime < ?2) OR " +
                "(tr.startDateTime < ?1 AND tr.endDateTime > ?2) OR (tr.startDateTime = ?1 AND tr.endDateTime = ?2)" +
              ") " +
              "AND tr.idTermRun IS NOT NULL " +
          ") " +
          "ORDER BY r.abbreviation, r.name"
  )
  List<Room> findFreeRooms(Date startTime, Date endTime);

  @Query( "SELECT r FROM Room r " +
          "WHERE r NOT IN ( " +
            "SELECT rp.roomReference FROM RoomPlan rp LEFT JOIN rp.termRuns AS tr " +
            "WHERE " +
              "( " +
                "(tr.startDateTime > ?1 AND tr.startDateTime < ?2) OR (tr.endDateTime > ?1 AND tr.endDateTime < ?2) OR " +
                "(tr.startDateTime < ?1 AND tr.endDateTime > ?2) OR (tr.startDateTime = ?1 AND tr.endDateTime = ?2)" +
              ") " +
              "AND tr.idTermRun IS NOT NULL AND tr.termReference.idTerm <> ?3" +
          ") " +
          "ORDER BY r.abbreviation, r.name"
  )
  List<Room> findFreeRooms(Date startTime, Date endTime, int termId);
}
