package cz.vutbr.fit.exampreparation.features.appUser;

import cz.vutbr.fit.exampreparation.features.appUser.dto.AccountInfoDto;
import cz.vutbr.fit.exampreparation.features.appUser.dto.EmployeeDetailsDto;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

public interface AppUserService {

  /**
   * Tries to find the user by provided e-mail address in database.
   *
   * @param email  e-mail address of the searched user
   * @return {@link Optional} object, which will contain {@link AppUser} object, if user with specified e-mail address was found
   */
  Optional<AppUser> findByEmail(String email);

  /**
   * Encodes password attribute of the provided {@link AppUser} object and save this changes together with roles into database.
   *
   * @param appUser  object that will be saved into database
   * @return object that is now saved in database
   */
  AppUser encodePasswordSetRoleAndSave(AppUser appUser);

  /**
   * Takes provided {@link AppUser} object and saves it into database.
   *
   * @param appUser object that will be saved into database
   * @return object that is now saved in database
   */
  AppUser save(AppUser appUser);

  /**
   * Deletes provided {@link AppUser} object from database.
   *
   * @param appUser  object that will be deleted from database
   */
  void deleteUser(AppUser appUser);

  /**
   * Deletes user with provided ID from database
   *
   * @param userId ID of the user who will be removed from the database
   */
  void deleteUserById(int userId);

  /**
   * Gets list of users that have employee role.
   *
   * @return response with list of objects with information about employees
   */
  ResponseEntity<List<EmployeeDetailsDto>> getAllEmployeesList();

  /**
   * Tries to find the user by provided login in database.
   *
   * @param login  login of the searched user
   * @return the found {@link AppUser}, if not found it will return null
   */
  AppUser findUserByLogin(String login);

  /**
   * Will check that all users with e-mail addresses that are provided in list exists and have admin or employee role.
   *
   * @param additionalManagersEmails  list of e-mail addresses of the users to check
   */
  void checkAdditionalManagers(List<String> additionalManagersEmails);

  /**
   * Finds matching user records by e-mail addresses in the provided list and returns them as a list
   *
   * @param termManagersEmails  list of e-mail addresses of searched users
   * @return list of found users
   */
  List<AppUser> createManagerList(List<String> termManagersEmails);

  /**
   * Gets information about all users accounts.
   *
   * @return list of objects with information about all user accounts
   */
  List<AccountInfoDto> getAllAccountsInfo();

  /**
   * Tries to find the user by his ID in database.
   *
   * @param userId unique ID of the searched user
   * @return {@link Optional} object, which will contain {@link AppUser} object, if user with specified ID was found
   */
  Optional<AppUser> findById(int userId);
}
