package cz.vutbr.fit.exampreparation.features.assignment;

import cz.vutbr.fit.exampreparation.common.AuthService;
import cz.vutbr.fit.exampreparation.common.MessageResponseDto;
import cz.vutbr.fit.exampreparation.common.UserInfo;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.BadRequestException;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.ConflictException;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.ForbiddenException;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.NotFoundException;
import cz.vutbr.fit.exampreparation.features.appUser.AppUser;
import cz.vutbr.fit.exampreparation.features.appUser.AppUserService;
import cz.vutbr.fit.exampreparation.features.appUser.dto.EmployeeDetailsDto;
import cz.vutbr.fit.exampreparation.features.assignment.dto.*;
import cz.vutbr.fit.exampreparation.features.assignment.helpers.AssignmentGenerator;
import cz.vutbr.fit.exampreparation.features.assignment.helpers.StorageManager;
import cz.vutbr.fit.exampreparation.features.termRun.TermRun;
import cz.vutbr.fit.exampreparation.features.termRun.TermRunService;
import cz.vutbr.fit.exampreparation.features.testSchedule.TestSchedule;
import cz.vutbr.fit.exampreparation.features.testSchedule.TestScheduleService;
import lombok.AllArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class AssignmentServiceImpl implements AssignmentService {

  private final AssignmentRepository assignmentRepository;
  private final StorageManager storageManager;
  private final AssignmentGenerator assignmentGenerator;
  private final AppUserService appUserService;
  private final TermRunService termRunService;
  private final TestScheduleService testScheduleService;

  @Override
  @Transactional
  public ResponseEntity<MessageResponseDto> createNewAssignment(BasicAssignmentDto assignmentDto, MultipartFile[] assignmentFiles) {
    appUserService.checkAdditionalManagers(assignmentDto.getAdditionalAssignmentManagersEmails());

    if (assignmentRepository.findByName(assignmentDto.getName()).isPresent()) {
      throw new ConflictException("Šablona zadání s tímto názvem již existuje, vyberte jiný název");
    }

    List<AppUser> assignmentManagers = appUserService.createManagerList(assignmentDto.getAdditionalAssignmentManagersEmails());
    Assignment assignment = new Assignment(assignmentDto.getName(), AssignmentStatus.UPLOADED,
      null, assignmentManagers, null);

    try {
      assignment = assignmentRepository.save(assignment);
    } catch (Exception e) {
      throw new RuntimeException(e.getMessage());
    }

    int assignmentId = assignment.getIdAssignment();

    storageManager.saveAssignmentFiles(assignmentFiles, new ArrayList<>(), assignmentId);

    return ResponseEntity.ok()
      .body(new MessageResponseDto("Šablona úspěšně vytvořena"));
  }

  @Override
  public ResponseEntity<AssignmentInfoDto> getAssignmentDetails(int assignmentId) {
    Assignment assignment = checkIfUserIsManagerAndGetAssignment(assignmentId);

    List<FileDataDto> files = storageManager.getFilesData(assignmentId);

    List<EmployeeDetailsDto> managers = assignment.getAssignmentManagers()
      .stream()
      .map(appUser -> new EmployeeDetailsDto(appUser.getEmail(), appUser.getNameWithDegrees()))
      .collect(Collectors.toList());

    return ResponseEntity.status(HttpStatus.OK)
      .body(new AssignmentInfoDto(assignment.getIdAssignment(), assignment.getName(), assignment.getStatus(), files, managers));
  }

  @Override
  public ResponseEntity<List<BasicAssignmentInfoDto>> getAllAssignmentsInfo() {
    List<BasicAssignmentInfoDto> assignmentInfos = new ArrayList<>();

    List<Assignment> rawAssignments;
    UserInfo userInfo = AuthService.getLoggedUserInfo();
    if (userInfo.getRoles().contains("ROLE_ADMIN")) {
      rawAssignments = assignmentRepository.findAll();
    } else if (userInfo.getRoles().contains("ROLE_EMPLOYEE")) {
      rawAssignments = assignmentRepository.findAllAssignmentsWhereUserIsManager(userInfo.getEmail());
    } else {
      rawAssignments = new ArrayList<>();
    }

    for (Assignment assignment : rawAssignments) {
      assignmentInfos.add(
        new BasicAssignmentInfoDto(assignment.getIdAssignment(), assignment.getName(), assignment.getStatus())
      );
    }

    return ResponseEntity.ok()
      .body(assignmentInfos);
  }

  @Override
  public ResponseEntity<MessageResponseDto> deleteAssignment(int assignmentId) {
    Assignment assignment = checkIfUserIsManagerAndGetAssignment(assignmentId);

    if (assignment.getTermRuns().size() != 0) {
      throw new ForbiddenException("Tuto šablonu zadání nelze odstranit, jelikož existuje termín zkoušky, ke kterému je přiřazena.\n" +
        "Pokud potřebujete šablonu smazat, odeberte ji ze všech termínů zkoušek.");
    }

    try {
      assignmentRepository.deleteById(assignmentId);
    } catch (Exception e) {
      throw new BadRequestException("Šablonu se nepodařilo smazat. Ujistěte se, že tato šablona zadání existuje.");
    }

    String dirPath = storageManager.getAssignmentDirPath(assignmentId);
    storageManager.deleteDirRecursively(dirPath);

    return ResponseEntity.ok()
      .body(new MessageResponseDto("Šablona úspěšně odstraněna"));
  }

  @Override
  public Optional<Assignment> findAssignmentById(Integer assignmentId) {
    return assignmentRepository.findById(assignmentId);
  }

  @Override
  public ResponseEntity<FileContentDto> getAssignmentFileContent(int assignmentId, String filePath) {
    checkIfUserIsManagerAndGetAssignment(assignmentId);

    if (isNotEditableFile(filePath)) {
      throw new BadRequestException("Tento typ souboru není možné editovat, zkuste ho stáhnout");
    }

    String fileContent = storageManager.getAssignmentFileContent(assignmentId, filePath);
    return ResponseEntity.ok()
      .body(new FileContentDto(fileContent));
  }

  @Override
  public ResponseEntity<Resource> downloadAssignmentFile(int assignmentId, String path) {
    checkIfUserIsManagerAndGetAssignment(assignmentId);

    String assignmentDir = storageManager.getAssignmentDirPath(assignmentId);
    Path filePath = Paths.get(assignmentDir)
      .resolve(path);
    Resource file = storageManager.downloadFile(filePath);
    String probeContentType = storageManager.getResourceProbeContentType(file);

    return ResponseEntity.ok()
      .header(HttpHeaders.CONTENT_TYPE, probeContentType)
      .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
      .body(file);
  }

  @Override
  public ResponseEntity<MessageResponseDto> editAssignment(int assignmentId, EditAssignmentDto assignmentDto, MultipartFile[] assignmentFiles) {
    Assignment assignment = checkIfUserIsManagerAndGetAssignment(assignmentId);

    if (assignmentFiles != null) {
      if (assignment.getStatus() == AssignmentStatus.LOCKED) {
        throw new BadRequestException("Právě probíhá generování tohoto zadání. Nelze uložit změny");
      }

      if (assignment.getStatus() == AssignmentStatus.GENERATED) {
        assignment.setStatus(AssignmentStatus.EDITED);
      } else {
        assignment.setStatus(AssignmentStatus.UPLOADED);
      }

      List<TestSchedule> testSchedulesWithAssignment = testScheduleService.findAllTestSchedulesWithAssignment(assignmentId);
      testSchedulesWithAssignment.forEach(testSchedule -> testSchedule.setPdfPath(null));
      testScheduleService.saveAll(testSchedulesWithAssignment);
    }

    List<String> emails = assignmentDto.getAdditionalAssignmentManagersEmails()
      .stream()
      .filter(email -> !email.equals(AuthService.getLoggedUserInfo().getEmail()))
      .collect(Collectors.toList());
    appUserService.checkAdditionalManagers(emails);

    List<AppUser> assignmentManagers = appUserService.createManagerList(emails);
    assignment.setAssignmentManagers(assignmentManagers);
    assignment.setName(assignmentDto.getName());

    try {
      assignmentRepository.save(assignment);
    } catch (Exception e) {
      throw new RuntimeException(e.getMessage());
    }

    storageManager.saveAssignmentFiles(assignmentFiles, assignmentDto.getUploadedFiles(), assignmentId);

    return ResponseEntity.ok()
      .body(new MessageResponseDto("Šablona zadání úspěšně upravena."));
  }

  @Override
  public ResponseEntity<MessageResponseDto> generateAssignment(Integer termId, Integer termRunId, String roomAbbreviation, Integer testScheduleId) {
    if (termId == null && termRunId == null && roomAbbreviation == null && testScheduleId == null) {
      throw new BadRequestException("Nebyla specifikováno, kde mají být zadání generována");
    }

    if (roomAbbreviation != null && termRunId == null) {
      throw new BadRequestException("Se zkratkou místnosti musí být specifikováno i ID odpovídajícího běhu");
    }

    List<TestSchedule> testSchedules = new ArrayList<>();
    Set<Assignment> assignments = new HashSet<>();
    if (testScheduleId != null) {
      TestSchedule testSchedule = testScheduleService.findTestScheduleById(testScheduleId)
        .orElseThrow(() -> new NotFoundException("Usazení s ID " + testScheduleId + " neexistuje"));

      if (!AuthService.isUserManager(testSchedule.getTermRun().getTermReference().getTermManagers())) {
        throw new ForbiddenException("Nejste správce termínu zkoušky. Toto zadání nemůžete generovat.");
      }

      testSchedules.add(testSchedule);
      assignments.add(testSchedule.getAssignment());
    } else if (termRunId != null) {
      TermRun termRun = termRunService.findTermRunById(termRunId)
        .orElseThrow(() -> new NotFoundException("Běh s ID " + termRunId + " neexistuje"));

      if (!AuthService.isUserManager(termRun.getTermReference().getTermManagers())) {
        throw new ForbiddenException("Nejste správce termínu zkoušky. Tato zadání nemůžete generovat.");
      }
      assignments.addAll(termRun.getAssignments());
      if (roomAbbreviation != null) {
        testSchedules = testScheduleService.findAllTestSchedulesInTermRunsRoomPlan(termRunId, roomAbbreviation);
      } else {
        testSchedules = testScheduleService.findAllTestSchedulesInTermRun(termRun);
      }
    } else {
      List<TermRun> termRuns = termRunService.getTermTermRuns(termId);
      if (!AuthService.isUserManager(termRuns.get(0).getTermReference().getTermManagers())) {
        throw new ForbiddenException("Nejste správce termínu zkoušky. Tato zadání nemůžete generovat.");
      }

      testSchedules = testScheduleService.findAllTestSchedulesInTerm(termId);

      for (TermRun termRun : termRuns) {
        assignments.addAll(termRun.getAssignments());
      }
    }

    if (testSchedules.size() == 0) {
      throw new BadRequestException("Nebyla nalezena žádná usazení, pro které by bylo možné generovat zadání.");
    }
    if (assignments.size() == 0) {
      throw new BadRequestException("Nebyla nalezena žádná zadání, která by bylo možné generovat.");
    }

    termId = testSchedules.get(0).getTermRun().getTermReference().getIdTerm();
    assignmentGenerator.generateAssignments(termId, testSchedules, assignments);

    return ResponseEntity.ok()
      .body(new MessageResponseDto("Generování bylo zahájeno"));
  }

  @Override
  public ResponseEntity<MessageResponseDto> cancelAssignmentGenerating(Integer termId) {
    if (termId == null) {
      throw new BadRequestException("Není specifikováno, pro který termín zkoušky má být generování přerušeno.");
    }

    List<TermRun> termRuns = termRunService.getTermTermRuns(termId);
    if (termRuns.size() == 0) {
      throw new BadRequestException("Termín zkoušky s ID " + termId + " neexistuje.");
    }
    if (!AuthService.isUserManager(termRuns.get(0).getTermReference().getTermManagers())) {
      throw new ForbiddenException("Nejste správce termínu zkoušky. Nemůžete přerušit generování.");
    }

    assignmentGenerator.cancelAssignmentGenerating(termId);

    return ResponseEntity.ok()
      .body(new MessageResponseDto("Generování bylo zrušeno"));
  }

  private Assignment checkIfUserIsManagerAndGetAssignment(int assignmentId) {
    Assignment assignment = assignmentRepository.findById(assignmentId)
      .orElseThrow(() -> new NotFoundException("Šablona zadání nebyla nalezena"));

    boolean isUserManager = AuthService.isUserManager(assignment.getAssignmentManagers());
    if (!isUserManager) {
      throw new ForbiddenException("K této šabloně zadání nemáte přístup");
    }
    return assignment;
  }

  private boolean isNotEditableFile(String filePath) {
    List<String> editableExtensions = Arrays.asList("tex", "bib", "cls", "ptc", "fd", "bst", "dtx", "bbx", "cbx", "def", "sty", "ins");
    return editableExtensions.stream()
      .noneMatch(filePath::endsWith);
  }
}
