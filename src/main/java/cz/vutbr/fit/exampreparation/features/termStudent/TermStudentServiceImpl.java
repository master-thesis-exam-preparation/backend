package cz.vutbr.fit.exampreparation.features.termStudent;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * This service class is responsible for management of data about students who will participate in the exam.
 */
@Service
@AllArgsConstructor
public class TermStudentServiceImpl implements TermStudentService {

  private final TermStudentRepository termStudentRepository;

  /**
   * {@inheritDoc}
   */
  @Override
  public TermStudent findUserByLogin(String login) {
    return termStudentRepository.findByLogin(login);
  }
}
