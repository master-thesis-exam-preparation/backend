package cz.vutbr.fit.exampreparation.features.term.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import cz.vutbr.fit.exampreparation.features.appUser.dto.EmployeeDetailsDto;
import cz.vutbr.fit.exampreparation.features.assignment.dto.GeneratingFailDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * Used to store more specific information about term.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TermDetailDto extends BasicTermDto {

  private String description;

  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  private List<EmployeeDetailsDto> managers;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private Integer remainingGenerating;

  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  private List<GeneratingFailDto> generatingFails;

  @NotEmpty(message = "Chybí seznam běhů termínu zkoušky")
  private List<TermRunDetailsDto> termRuns;

  /**
   * Creates new instance of {@code AppUser}.
   *
   * @param courseAbbreviation  abbreviation of the course from which the term exam takes place
   * @param termName  name of the term
   * @param academicYear  academic year in which term exam takes place
   * @param termDates  list of the start times of the term runs of the term
   * @param description  text with term description
   * @param managers  list of application users that can manage this term
   * @param remainingGenerating  number of students that are still waiting for generating of their assignment
   * @param generatingFails  list of error that occurred during assignment generating
   * @param termRuns  list of terms term runs
   */
  public TermDetailDto(String courseAbbreviation, String termName, Integer academicYear, List<Long> termDates,
                       String description, List<EmployeeDetailsDto> managers, Integer remainingGenerating,
                       List<GeneratingFailDto> generatingFails, List<TermRunDetailsDto> termRuns) {
    super(courseAbbreviation, termName, academicYear, termDates);
    this.description = description;
    this.managers = managers;
    this.remainingGenerating = remainingGenerating;
    this.generatingFails = generatingFails;
    this.termRuns = termRuns;
  }
}
