package cz.vutbr.fit.exampreparation.features.appUser;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * This class is responsible for querying database table AppUser
 */
@Repository
public interface AppUserRepository extends JpaRepository<AppUser, Integer> {
  Optional<AppUser> findByEmail(String email);

  @Query("SELECT au FROM AppUser au JOIN au.roles AS r " +
    "WHERE r.roleName = 'ADMIN' OR r.roleName = 'EMPLOYEE' " +
    "ORDER BY au.lastName ASC ")
  List<AppUser> findAllEmployeesAndAdmins();

  @Query("SELECT au FROM AppUser au " +
    "WHERE au.email LIKE ?1%")
  List<AppUser> findByLogin(String login);
}
