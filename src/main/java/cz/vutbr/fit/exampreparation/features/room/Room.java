package cz.vutbr.fit.exampreparation.features.room;

import cz.vutbr.fit.exampreparation.features.appUser.AppUser;
import cz.vutbr.fit.exampreparation.features.roomPlan.RoomPlan;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.List;


/**
 * This entity class is responsible for object-relational mapping of an objects
 * with information about rooms in which exams will take place into a database table Room.
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(uniqueConstraints = {@UniqueConstraint(columnNames =
  {"abbreviation", "name"})})
public class Room {

  @Id
  @Setter(AccessLevel.PRIVATE)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int idRoom;

  @NotBlank(message = "Chybějící zkratka místnosti")
  @Size(max = 20, message = "Příliš dlouhý vstup")
  @Column(length = 20)
  private String abbreviation;

  @NotBlank(message = "Chybějící název místnosti")
  @Size(max = 255, message = "Příliš dlouhý vstup")
  private String name;

  @Size(max = 65535, message = "Příliš dlouhý vstup")
  @Column(length = 65535)
  private String description;

  @NotNull(message = "Chybějící počet řad")
  @Min(value = 1, message = "Místnost musí obsahovat alespoň 1 řadu")
  @Max(value = 150, message = "Místnost může obsahovat maximálně 150 řad")
  private Integer numberOfRows;

  @NotNull(message = "Chybějící počet míst v řadě")
  @Min(value = 1, message = "Místnost musí obsahovat alespoň 1 místo v řadě")
  @Max(value = 150, message = "Místnost může obsahovat maximálně 150 míst v řadě")
  private Integer numberOfColumns;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "idAppUser", referencedColumnName = "idAppUser")
  @OnDelete(action = OnDeleteAction.CASCADE)
  private AppUser appUserReference;

  @OneToMany(mappedBy = "roomReference", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @OnDelete(action = OnDeleteAction.CASCADE)
  private List<RoomPlan> roomPlans;

  /**
   * Creates new instance of {@code AppUser}.
   * ID attribute will be set after storing the object into database.
   *
   * @param abbreviation  room abbreviation
   * @param name  room name
   * @param description  room description text
   * @param numberOfRows  number of rows in the room plan
   * @param numberOfColumns  number of columns in the room plan
   * @param appUserReference  reference to the user who created this room
   * @param roomPlans  all 3 room plans of the given room (number of free cells between students - 0, 1, 2)
   */
  public Room(String abbreviation, String name, String description, Integer numberOfRows, Integer numberOfColumns,
              AppUser appUserReference, List<RoomPlan> roomPlans) {
    this.abbreviation = abbreviation;
    this.name = name;
    this.description = description;
    this.numberOfRows = numberOfRows;
    this.numberOfColumns = numberOfColumns;
    this.appUserReference = appUserReference;
    this.roomPlans = roomPlans;
  }

}
