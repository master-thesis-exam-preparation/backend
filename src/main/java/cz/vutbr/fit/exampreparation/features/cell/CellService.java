package cz.vutbr.fit.exampreparation.features.cell;

import cz.vutbr.fit.exampreparation.features.roomPlan.RoomPlan;
import cz.vutbr.fit.exampreparation.features.termRun.TermRun;

import java.util.List;
import java.util.Optional;

public interface CellService {

  /**
   * Gets all room plan cells and return their IDs in a list.
   *
   * @param roomPlan  room plan which cell IDs should be returned
   * @return list of IDs of room plan cells
   */
  List<Integer> getRoomPlanSeatIds(RoomPlan roomPlan);

  /**
   * Gets 2D list of {@link RoomPlan} seats is snake pattern.
   *
   * Starting from the left down corner. Everytime row contains seat, next row with will be read in reverse order.
   *
   * @param roomPlan  room plan which seats should be returned
   * @return {@link RoomPlan} seats is snake pattern
   */
  List<List<Cell>> getRoomPlanSeatsInSnakePattern(RoomPlan roomPlan);

  /**
   * Tries to find the cell by provided cell ID in database.
   *
   * @param cellId  unique ID of the searched cell
   * @return the found {@link Cell}, if not found it will return null
   */
  Optional<Cell> getCellById(Integer cellId);

  /**
   * Gets number of first row that contains cell of type seat.
   *
   * @param roomPlan  room plan in which seats should be found
   * @return number if first row that contains cell of type seat
   */
  int findFirstRowNumberWithSeatInRoomPlan(RoomPlan roomPlan);

  /**
   * Gets list of cells of type seat that has no test schedule attached to them in specific term run and room plan (no one is sitting on that seat).
   *
   * @param termRun  term run in which free seats should be found
   * @param roomPlan  room plan in which free seats should be found
   * @return list of free seats in provided term run
   */
  List<Cell> findFreeSeatsInRoomPlan(TermRun termRun, RoomPlan roomPlan);
}
