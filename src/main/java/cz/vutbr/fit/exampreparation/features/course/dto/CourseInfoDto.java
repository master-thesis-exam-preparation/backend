package cz.vutbr.fit.exampreparation.features.course.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;


/**
 * Used to store basic information about course and ID of the course.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class CourseInfoDto extends BasicCourseInfo {

  @NotNull(message = "Chybějící identifikátor předmětu")
  private Integer courseId;

  /**
   * Creates new instance of {@code CourseInfoDto}.
   *
   * @param abbreviation  course abbreviation (e.g. WAP)
   * @param name  name of the course
   * @param academicYear  academic year when course is taught (e.g. if the course is taught in years 2020-2021 use 2020)
   * @param courseId  unique ID of the course
   */
  public CourseInfoDto(String abbreviation, String name, Integer academicYear, Integer courseId) {
    super(abbreviation, name, academicYear);
    this.courseId = courseId;
  }
}
