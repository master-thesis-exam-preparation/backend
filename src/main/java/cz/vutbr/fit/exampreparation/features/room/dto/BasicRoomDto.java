package cz.vutbr.fit.exampreparation.features.room.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * Used to store basic information about room (room name and abbreviation).
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BasicRoomDto {

  @NotBlank(message = "Chybějící zkratka místnosti")
  @Size(max = 20, message = "Příliš dlouhý vstup")
  private String abbreviation;

  @NotBlank(message = "Chybějící název místnosti")
  @Size(max = 255, message = "Příliš dlouhý vstup")
  private String name;

}
