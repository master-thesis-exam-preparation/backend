package cz.vutbr.fit.exampreparation.features.course.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;

/**
 * Used to store basic information about course (course abbreviation, name, academic year).
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BasicCourseInfo {

  @NotBlank(message = "Chybějící zkratka předmětu")
  @Size(max = 50, message = "Příliš dlouhý vstup")
  private String abbreviation;

  @NotBlank(message = "Chybějící název předmětu")
  @Size(max = 255, message = "Příliš dlouhý vstup")
  private String name;

  @NotNull(message = "Chybějící akademický rok")
  @Min(value = 2010, message = "Neplatný rok (2000-2050)")
  @Max(value = 2050, message = "Neplatný rok (2000-2050)")
  private Integer academicYear;
}
