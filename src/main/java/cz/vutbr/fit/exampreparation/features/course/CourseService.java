package cz.vutbr.fit.exampreparation.features.course;

import cz.vutbr.fit.exampreparation.common.MessageResponseDto;
import cz.vutbr.fit.exampreparation.features.course.dto.BasicCourseInfo;
import cz.vutbr.fit.exampreparation.features.course.dto.CourseInfoDto;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

public interface CourseService {

  /**
   * Gets information about all courses.
   *
   * @param getOnlyCurrent  if true method will return only that courses from which the exam can take place at current time, otherwise return all courses.
   * @return response with information about all courses.
   */
  ResponseEntity<List<CourseInfoDto>> getAllCoursesInfo(boolean getOnlyCurrent);

  /**
   * Creates new ceurse based on given data.
   *
   * @param courseInfoDto  information necessary to create new course
   * @return response with information about created course
   */
  ResponseEntity<CourseInfoDto> createNewCourse(BasicCourseInfo courseInfoDto);

  /**
   * Edits existing course data based on new given data.
   *
   * @param courseId  unique ID of the course which should be edited
   * @param courseInfoDto  information about changes
   * @return response with success message
   */
  ResponseEntity<MessageResponseDto> editCourse(int courseId, BasicCourseInfo courseInfoDto);

  /**
   * Deletes specified course from database.
   *
   * @param courseId  unique ID of the course that should be deleted
   * @return response with success message
   */
  ResponseEntity<MessageResponseDto> deleteCourse(int courseId);

  /**
   * Creates multiple courses at once.
   *
   * @param courses  list of information necessary to create new courses
   * @return response with success message
   */
  ResponseEntity<MessageResponseDto> importCourses(List<BasicCourseInfo> courses);

  /**
   * Tries to find the course by provided course ID in database.
   *
   * @param courseId  unique ID of the searched assignment
   * @return the found {@link Course}, if not found it will return null
   */
  Optional<Course> findCourseById(int courseId);
}
