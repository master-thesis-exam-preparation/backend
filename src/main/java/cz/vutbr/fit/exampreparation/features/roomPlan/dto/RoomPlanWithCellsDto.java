package cz.vutbr.fit.exampreparation.features.roomPlan.dto;

import cz.vutbr.fit.exampreparation.features.cell.dto.CellDto;
import cz.vutbr.fit.exampreparation.features.roomPlan.SpacingBetweenStudents;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * Used to store information about room plan (includes 2D list of cells of room plan).
 */
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class RoomPlanWithCellsDto extends RoomPlanDto {

  @NotEmpty(message = "Plán místnosti nesmí být prázdný")
  private List<List<CellDto>> cells;

  /**
   * Creates new instance of {@link RoomPlanWithCellsDto}.
   *
   * @param roomPlanId  unique ID of the room plan
   * @param capacity  number of seats in the room plan
   * @param spacing  number of free cells between students
   * @param cells  room plan cells in 2D list
   */
  public RoomPlanWithCellsDto(Integer roomPlanId, Integer capacity, SpacingBetweenStudents spacing, List<List<CellDto>> cells) {
    super(roomPlanId, capacity, spacing);
    this.cells = cells;
  }
}
