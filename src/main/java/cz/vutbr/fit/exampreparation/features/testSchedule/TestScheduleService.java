package cz.vutbr.fit.exampreparation.features.testSchedule;

import cz.vutbr.fit.exampreparation.features.assignment.dto.FileContentDto;
import cz.vutbr.fit.exampreparation.features.room.Room;
import cz.vutbr.fit.exampreparation.features.roomPlan.RoomPlan;
import cz.vutbr.fit.exampreparation.features.term.Term;
import cz.vutbr.fit.exampreparation.features.termRun.TermRun;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

public interface TestScheduleService {

  /**
   * Gets list of test schedules in given room plan of given term run.
   *
   * @param termRun  term run for which test schedules will be searched
   * @param roomPlan  the plan of the room for which test schedules will be searched
   * @return list of test schedules in given room plan of given term run
   */
  List<TestSchedule> findAllTestSchedulesInTermRunsRoomPlan(TermRun termRun, RoomPlan roomPlan);

  /**
   * Gets list of test schedules in given room plan of given term run.
   *
   * @param termRunId  unique ID of the term run for which test schedules will be searched
   * @param roomPlanAbbreviation  the abbreviation of the plan of the room for which test schedules will be searched
   * @return list of test schedules in given room plan of given term run
   */
  List<TestSchedule> findAllTestSchedulesInTermRunsRoomPlan(int termRunId, String roomPlanAbbreviation);

  /**
   * Gets list of test schedules in all room plans of provided term run.
   *
   * @param termRun  term run for which test schedules will be searched
   * @return list of test schedules in all room plans of given term run
   */
  List<TestSchedule> findAllTestSchedulesInTermRun(TermRun termRun);

  /**
   * Gets list of test schedules to which the specified assignment was assigned.
   *
   * @param assignmentId  unique ID of the assignment
   * @return list of test schedules to which the specified assignment was assigned
   */
  List<TestSchedule> findAllTestSchedulesWithAssignment(int assignmentId);

  /**
   * Finds out if any student is seated in provided room.
   *
   * @param room  room in which students will be searched
   * @return true if any student is seated in given room, false otherwise
   */
  boolean isAnyStudentSeatedInRoom(Room room);

  /**
   * Tries to find the test schedule by provided test schedule ID in database.
   *
   * @param testScheduleId  unique ID of the searched test schedule
   * @return the found {@link TestSchedule}, if not found it will return null
   */
  Optional<TestSchedule> findTestScheduleById(int testScheduleId);

  /**
   * Sets provided path to PDF file to provided test schedule.
   *
   * @param testSchedule  test schedule to which pdf path will be set
   * @param pdfPath  path to PDF file
   */
  void updatePdfPath(TestSchedule testSchedule, String pdfPath);

  /**
   * Gets list of test schedules in all room plans in all term runs of specified term.
   *
   * @param termId  unique ID of the term in which students will be searched
   * @return list of test schedules in all room plans in all term runs of specified term
   */
  List<TestSchedule> findAllTestSchedulesInTerm(int termId);

  /**
   * Gets abbreviation of the room in which to which the provided test schedule is assigned.
   *
   * @param testSchedule test schedule for which the root directory of its term run will be searched
   * @return abbreviation of the room in which to which the provided test schedule is assigned
   */
  String getRoomAbbreviationFromTestSchedule(TestSchedule testSchedule);

  /**
   * Gets path to root directory of the term run of the provided test schedule.
   *
   * @param testSchedule  test schedule
   * @return path to root directory of specified term run
   */
  String getTermRunDir(TestSchedule testSchedule);

  /**
   * Gets path to root directory with assignments for specified room in specified term run.
   *
   * @param termRunDir  path to term runs root directory
   * @param roomAbbreviation  abbreviation of term runs room
   * @return path to root directory with assignments for specified room in specified term run
   */
  String getRoomPlanDir(String termRunDir, String roomAbbreviation);

  /**
   * Gets order number of last student in provided term.
   *
   * @param term  term in which the last order number of the student will be searched
   * @return order number of last student in provided term
   */
  int findLastOrderNumber(Term term);

  /**
   * Get URI addresses for downloading of generated assignments.
   *
   * <ul>
   *   <li>If only {@code termId} is provided generated assignments of all students will be downloaded.</li>
   *   <li>If {@code termId} and {@code termRunId} are provided only generated assignments of specified term run will be downloaded.</li>
   *   <li>If {@code termId}, {@code termRunId} and {@code roomAbbreviation} are provided only generated assignments of specified room of specified term run will be downloaded.</li>
   *   <li>If {@code testScheduleId} is provided only its generated assignment will be downloaded.</li>
   * </ul>
   *
   * @param termId  unique ID of the term
   * @param termRunId  unique ID of the term run
   * @param roomAbbreviation  term runs room abbreviation
   * @param testScheduleId  unique ID of the test schedule
   * @param pack  if set to true all PDFs will be merged into one PDF file, otherwise will be added to one archive file
   * @param doubleSided  if set to true empty list will be added after each PDF with even number of pages
   * @return response with success message
   */
  ResponseEntity<Resource> downloadPdfs(Integer termId, Integer termRunId, String roomAbbreviation,
                                        Integer testScheduleId, boolean pack, boolean doubleSided);

  void saveAll(List<TestSchedule> testSchedules);

  /**
   * Gets content of error log file of specified test schedule.
   *
   * @param testScheduleId  unique ID of the test schedule
   * @return response with content of error log file
   */
  ResponseEntity<FileContentDto> getLogFileContent(int testScheduleId);

  /**
   * Returns URI for download of list of students in .csv file in the specific room on specific term run on specific term.
   *
   * @param termId  unique ID of the term
   * @param termRunId  unique ID of the term run
   * @param roomAbbreviation  term runs room abbreviation
   * @return response with URI for download students list in .csv file
   */
  ResponseEntity<Resource> downloadStudentsCsv(Integer termId, Integer termRunId, String roomAbbreviation);

}
