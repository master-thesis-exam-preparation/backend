package cz.vutbr.fit.exampreparation.features.term;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * This class is responsible for querying database table Term
 */
@Repository
public interface TermRepository extends JpaRepository<Term, Integer> {

  @Query( "SELECT t FROM Term t JOIN t.termManagers tm " +
          "WHERE tm.email = ?1")
  List<Term> findAllTermsWhereUserIsManager(String email);

  @Query( "SELECT t FROM Term t JOIN t.termRuns tr JOIN tr.testSchedules ts " +
          "WHERE ts.student.login = ?1 ")
  List<Term> findAllTermsInWhichTheStudentAttends(String login);
}
