package cz.vutbr.fit.exampreparation.features.testSchedule;

import cz.vutbr.fit.exampreparation.features.room.Room;
import cz.vutbr.fit.exampreparation.features.roomPlan.RoomPlan;
import cz.vutbr.fit.exampreparation.features.term.Term;
import cz.vutbr.fit.exampreparation.features.termRun.TermRun;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * This class is responsible for querying database table TestSchedule
 */
@Repository
public interface TestScheduleRepository extends JpaRepository<TestSchedule, Integer> {

  @Query( "SELECT ts FROM TestSchedule ts JOIN ts.cell c " +
          "WHERE ts.termRun = ?1 AND c.roomPlanReference = ?2 " +
          "ORDER BY ts.seatNumber ASC")
  List<TestSchedule> findAllTestSchedulesInTermRunsRoomPlan(TermRun termRun, RoomPlan roomPlan);

  @Query( "SELECT ts FROM TestSchedule ts JOIN ts.cell c " +
    "WHERE ts.termRun.idTermRun = ?1 AND c.roomPlanReference.roomReference.abbreviation = ?2 " +
    "ORDER BY ts.seatNumber ASC")
  List<TestSchedule> findAllTestSchedulesInTermRunsRoomPlan(int termRunId, String roomPlanAbbreviation);

  List<TestSchedule> findAllByTermRun(TermRun termRun);

  @Query( "SELECT ts FROM TestSchedule ts JOIN ts.cell c " +
          "WHERE c.roomPlanReference.roomReference = ?1")
  List<TestSchedule> findTestSchedulesInRoom(Room room);

  List<TestSchedule> findAllByTermRun_TermReference_IdTerm(int termId);

  List<TestSchedule> findAllByAssignment_IdAssignment(int assignmentId);

  @Query( "SELECT MAX(ts.seatNumber) FROM TestSchedule ts " +
          "WHERE ts.termRun.termReference = ?1")
  Integer findTermLastSeatNumber(Term term);

  @Query( "SELECT MAX(ts.orderNumber) FROM TestSchedule ts " +
          "WHERE ts.termRun.termReference = ?1")
  Integer findTermLastOrderNumber(Term term);
}
