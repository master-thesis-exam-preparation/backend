package cz.vutbr.fit.exampreparation.features.testSchedule;

import cz.vutbr.fit.exampreparation.features.assignment.dto.FileContentDto;
import lombok.AllArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * This class is responsible for managing test schedule REST API endpoints.
 */
@Validated
@RestController
@AllArgsConstructor
public class TestScheduleController {

  private final TestScheduleService testScheduleService;

  /**
   * Get URI addresses for downloading of generated assignments.
   *
   * <ul>
   *   <li>If only {@code termId} is provided generated assignments of all students will be downloaded.</li>
   *   <li>If {@code termId} and {@code termRunId} are provided only generated assignments of specified term run will be downloaded.</li>
   *   <li>If {@code termId}, {@code termRunId} and {@code roomAbbreviation} are provided only generated assignments of specified room of specified term run will be downloaded.</li>
   *   <li>If {@code testScheduleId} is provided only its generated assignment will be downloaded.</li>
   * </ul>
   *
   * @param termId  unique ID of the term
   * @param termRunId  unique ID of the term run
   * @param roomAbbreviation  term runs room abbreviation
   * @param testScheduleId  unique ID of the test schedule
   * @param pack  if set to true all PDFs will be merged into one PDF file, otherwise will be added to one archive file
   * @param doubleSided  if set to true empty list will be added after each PDF with even number of pages
   * @return response with success message
   */
  @PreAuthorize("hasAnyRole('EMPLOYEE','ADMIN')")
  @GetMapping("/internal/test-schedules/download")
  public ResponseEntity<Resource> downloadPdf(@RequestParam(required = false) Integer termId,
                                              @RequestParam(required = false) Integer termRunId,
                                              @RequestParam(required = false) String roomAbbreviation,
                                              @RequestParam(required = false) Integer testScheduleId,
                                              @RequestParam boolean pack,
                                              @RequestParam boolean doubleSided) {
    return testScheduleService.downloadPdfs(termId, termRunId, roomAbbreviation, testScheduleId, pack, doubleSided);
  }

  /**
   * Gets content of error log file of specified test schedule.
   *
   * @param testScheduleId  unique ID of the test schedule
   * @return response with content of error log file
   */
  @PreAuthorize("hasAnyRole('EMPLOYEE','ADMIN')")
  @GetMapping("/internal/test-schedules/{testScheduleId:[\\d]+}/log-file-content")
  public ResponseEntity<FileContentDto> getLogFileContent$(@PathVariable int testScheduleId) {
    return testScheduleService.getLogFileContent(testScheduleId);
  }

  /**
   * Returns URI for download of list of students in .csv file in the specific room on specific term run on specific term.
   *
   * @param termId  unique ID of the term
   * @param termRunId  unique ID of the term run
   * @param roomAbbreviation  term runs room abbreviation
   * @return response with URI for download students list in .csv file
   */
  @PreAuthorize("hasAnyRole('EMPLOYEE','ADMIN')")
  @GetMapping("/internal/test-schedules/download-students-csv")
  public ResponseEntity<Resource> downloadStudentsCsv(@RequestParam(required = false) Integer termId,
                                                      @RequestParam(required = false) Integer termRunId,
                                                      @RequestParam(required = false) String roomAbbreviation) {
    return testScheduleService.downloadStudentsCsv(termId, termRunId, roomAbbreviation);
  }
}
