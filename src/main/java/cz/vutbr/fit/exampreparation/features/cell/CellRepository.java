package cz.vutbr.fit.exampreparation.features.cell;

import cz.vutbr.fit.exampreparation.features.roomPlan.RoomPlan;
import cz.vutbr.fit.exampreparation.features.termRun.TermRun;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * This class is responsible for querying database table Cell
 */
@Repository
public interface CellRepository extends JpaRepository<Cell, Integer> {
  int countByCellTypeAndRoomPlanReference(CellType cellType, RoomPlan roomPlan);

  @Query( "SELECT c FROM Cell c " +
          "WHERE c.roomPlanReference = ?1 AND c.cellType = ?2")
  List<Cell> findCellsByRoomPlanAndCellType(RoomPlan roomPlan, CellType cellType);


  @Query( "SELECT MIN (c.rowNumber) " +
          "FROM Cell c " +
          "WHERE c.cellType = 'SEAT' AND c.roomPlanReference = ?1")
  int findFirstRowNumberWithSeatInRoomPlan(RoomPlan roomPlan);

  @Query( "SELECT c " +
    "FROM Cell c JOIN c.roomPlanReference AS rp " +
    "WHERE c.cellType = 'SEAT' AND c.roomPlanReference = ?2 AND c NOT IN ( " +
    "    SELECT c FROM TestSchedule ts JOIN ts.cell AS c JOIN c.roomPlanReference " +
    "    WHERE c.cellType = 'SEAT' AND rp = ?2 AND ts.termRun = ?1 " +
    ") ORDER BY c.idCell")
  List<Cell> findFreeSeatsInTermRunsRoomPlan(TermRun termRun, RoomPlan roomPlan);
}
