package cz.vutbr.fit.exampreparation.features.assignment.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Used to store basic information about assignment and list of information about assignments files.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EditAssignmentDto extends BasicAssignmentDto {
  private List<FileDataDto> uploadedFiles;
}
