package cz.vutbr.fit.exampreparation.features.role;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * This service class is responsible for management of all user roles.
 */
@Service
@AllArgsConstructor
public class RoleServiceImpl implements RoleService {

  private final RoleRepository roleRepository;

  /**
   * {@inheritDoc}
   */
  @Override
  public Optional<Role> findRole(RoleName roleName) {
    return roleRepository.findByRoleName(roleName);
  }

}
