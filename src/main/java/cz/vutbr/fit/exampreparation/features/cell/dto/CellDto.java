package cz.vutbr.fit.exampreparation.features.cell.dto;

import cz.vutbr.fit.exampreparation.features.cell.CellType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * Used to store information about cell ID and cell type.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CellDto {

  @NotNull(message = "Chybějící identifikátor místnosti")
  private Integer cellId;

  @NotNull(message = "Buňka musí mít typ")
  private CellType cellType;

}
