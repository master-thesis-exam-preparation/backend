package cz.vutbr.fit.exampreparation.features.term.dto.term;

import cz.vutbr.fit.exampreparation.features.appUser.dto.EmployeeDetailsDto;
import cz.vutbr.fit.exampreparation.features.appUser.dto.LoginNameDto;
import cz.vutbr.fit.exampreparation.features.roomPlan.SpacingBetweenStudents;
import cz.vutbr.fit.exampreparation.features.termRun.Distribution;
import cz.vutbr.fit.exampreparation.features.termRun.dto.TermRunEditInfoDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * Used to store information to fill edit term forms.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
public class EditTermInfoDto extends BasicTermDto {

  @NotEmpty(message = "Chybí seznam studentů, kteří se mají účastnit termínu zkoušky")
  private List<@Valid LoginNameDto> studentsList;

  private List<EmployeeDetailsDto> managers;

  @NotEmpty(message = "Chybí seznam běhů termínů zkoušky")
  private List<@Valid TermRunEditInfoDto> termRuns;

  /**
   * Creates new instance of {@code EditTermInfoDto}.
   * ID attribute will be set after storing the object into database.
   *
   * @param termName  name of the term
   * @param courseId  id of the course from which the exam takes place
   * @param studentsList  list of students that will attend exam of this term
   * @param spacingBetweenStudents  selected number of free cells between students
   * @param termRuns  list of term runs of this term
   * @param distribution  selected type of distribution of students
   * @param managers  list of application users that can manage this term
   * @param informationText  text with term description
   */
  public EditTermInfoDto(String termName, Integer courseId, List<LoginNameDto> studentsList,
                         SpacingBetweenStudents spacingBetweenStudents, List<TermRunEditInfoDto> termRuns,
                         Distribution distribution, List<EmployeeDetailsDto> managers, String informationText) {
    super(termName, courseId, spacingBetweenStudents, distribution, informationText);
    this.termRuns = termRuns;
    this.studentsList = studentsList;
    this.managers = managers;
  }
}
