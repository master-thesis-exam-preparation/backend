package cz.vutbr.fit.exampreparation.features.roomPlan;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * This service class is responsible for management of room plans.
 */
@Service
@AllArgsConstructor
public class RoomPlanServiceImpl implements RoomPlanService {

  private final RoomPlanRepository roomPlanRepository;

  /**
   * {@inheritDoc}
   */
  @Override
  public void deleteRoomPlans(List<RoomPlan> roomPlans) {
    roomPlanRepository.deleteInBatch(roomPlans);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public RoomPlan findRoomPlanByRoomIdAndSpacing(Integer roomId, SpacingBetweenStudents spacing) {
    return roomPlanRepository.findRoomPlanByRoomReference_IdRoomAndSpacing(roomId, spacing);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Optional<RoomPlan> findRoomPlanById(int roomPlanId) {
    return roomPlanRepository.findById(roomPlanId);
  }
}
