package cz.vutbr.fit.exampreparation.features.appUser;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import cz.vutbr.fit.exampreparation.features.assignment.Assignment;
import cz.vutbr.fit.exampreparation.features.role.Role;
import cz.vutbr.fit.exampreparation.features.room.Room;
import cz.vutbr.fit.exampreparation.features.term.Term;
import cz.vutbr.fit.exampreparation.features.token.Token;
import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * This entity class is responsible for object-relational mapping of an objects
 * with information about application users into a database table AppUser.
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AppUser {

  @Id
  @Setter(AccessLevel.PRIVATE)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int idAppUser;

  @NotBlank(message = "Prosím vyplňte e-mailovou adresu")
  @Email(message = "Zkontrolujte, zda je e-mail ve správném formátu")
  @Size(max = 255, message = "Příliš dlouhý vstup")
  @Column(unique = true)
  private String email;

  @NotBlank(message = "Prosím vyplňte jméno")
  @Size(max = 255, message = "Příliš dlouhý vstup")
  private String firstName;

  @NotBlank(message = "Prosím vyplňte příjmení")
  @Size(max = 255, message = "Příliš dlouhý vstup")
  private String lastName;

  @Size(max = 255, message = "Příliš dlouhý vstup")
  private String degreesBeforeName;

  @Size(max = 255, message = "Příliš dlouhý vstup")
  private String degreesBehindName;

  private String password;

  private boolean verified;

  private boolean locked;

  @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
  @JoinTable(
    name = "user_roles",
    joinColumns = {@JoinColumn(name = "id_app_user")},
    inverseJoinColumns = {@JoinColumn(name = "role_name")}
  )
  @JsonManagedReference
  private List<Role> roles;

  @ManyToMany(mappedBy = "termManagers", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
  private List<Term> terms;

  @ManyToMany(mappedBy = "assignmentManagers", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
  private List<Assignment> assignments;

  @OneToMany(mappedBy = "appUserReference", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
  private List<Room> rooms;

  @OneToMany(mappedBy = "appUserReference", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @OnDelete(action = OnDeleteAction.CASCADE)
  private List<Token> userTokens;

  /**
   * Creates new instance of {@code AppUser}.
   * ID attribute will be set after storing the object into database.
   *
   * @param email  users e-mail address
   * @param firstName  users first name
   * @param lastName  users last name
   * @param degreesBeforeName  users degrees that is written before name, separated by comma
   * @param degreesBehindName  users degrees that is written behind name, separated by comma
   * @param password  users hashed password
   * @param verified  information on whether the user's account is verified
   * @param locked  information on whether the user's account is blocked
   * @param roles  list of users roles
   * @param terms  list of terms that user creates
   * @param rooms  list of rooms that user creates
   * @param userTokens  list of users tokens
   */
  public AppUser(String email, String firstName, String lastName, String degreesBeforeName, String degreesBehindName,
                 String password, boolean verified, boolean locked, List<Role> roles, List<Term> terms, List<Room> rooms,
                 List<Token> userTokens) {
    this.email = email;
    this.firstName = firstName;
    this.lastName = lastName;
    this.degreesBeforeName = degreesBeforeName;
    this.degreesBehindName = degreesBehindName;
    this.password = password;
    this.verified = verified;
    this.locked = locked;
    this.roles = roles;
    this.terms = terms;
    this.rooms = rooms;
    this.userTokens = userTokens;
  }

  /**
   * Combines name, surname and degrees of the user into the one String
   *
   * @return users name and degrees
   */
  public String getNameWithDegrees() {
    StringBuilder s = new StringBuilder();
    if (this.getDegreesBeforeName() != null) {
      s.append(this.getDegreesBeforeName());
      s.append(" ");
    }
    s.append(this.getFirstName());
    s.append(" ");
    s.append(this.getLastName());
    if (this.getDegreesBehindName() != null) {
      s.append(" ");
      s.append(this.getDegreesBehindName());
    }
    return s.toString();
  }
}
