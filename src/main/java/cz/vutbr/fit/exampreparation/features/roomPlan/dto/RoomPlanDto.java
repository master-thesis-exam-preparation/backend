package cz.vutbr.fit.exampreparation.features.roomPlan.dto;

import cz.vutbr.fit.exampreparation.features.roomPlan.SpacingBetweenStudents;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Used to store information about room plan (unique ID, capacity and spacing between students).
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoomPlanDto {

  @NotNull(message = "Chybějící identifikátor plánu místnosti")
  private Integer roomPlanId;

  @NotNull(message = "Chybějící kapacita plánu místnosti")
  @Min(value = 0, message = "Kapacita místnosti nemůže být záporná")
  private Integer capacity;

  @NotNull(message = "Chybějící údaj o počtu volných míst mezi studenty")
  @Enumerated(EnumType.STRING)
  private SpacingBetweenStudents spacing;

}
