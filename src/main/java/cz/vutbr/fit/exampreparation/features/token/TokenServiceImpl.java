package cz.vutbr.fit.exampreparation.features.token;

import cz.vutbr.fit.exampreparation.common.Base64EncoderDecoder;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.BaseException;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.NotFoundException;
import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.UnauthorizedException;
import cz.vutbr.fit.exampreparation.features.appUser.AppUser;
import cz.vutbr.fit.exampreparation.features.appUser.AppUserService;
import cz.vutbr.fit.exampreparation.security.jwt.JwtTokenUtil;
import cz.vutbr.fit.exampreparation.security.jwt.dto.AccessTokenDto;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;

/**
 * This service class is responsible for management of tokens of all application users.
 */
@Service
@AllArgsConstructor
@Transactional
public class TokenServiceImpl implements TokenService {

  private final TokenRepository tokenRepository;
  private final AppUserService appUserService;
  private final JwtTokenUtil jwtTokenUtil;
  private final PasswordEncoder passwordEncoder;

  /**
   * {@inheritDoc}
   */
  @Override
  @Transactional
  public String generateToken(AppUser appUser, TokenType tokenType, int expireAfterHours, boolean remember) {
    String tokenValue = UUID.randomUUID().toString();
    String encodedTokenValue = passwordEncoder.encode(tokenValue);

    Token token = new Token();
    token.setToken(encodedTokenValue);
    token.setAppUserReference(appUser);
    token.setExpireAt(Instant.now().plus(expireAfterHours, ChronoUnit.HOURS));
    token.setTokenType(tokenType);
    token.setRemember(remember);
    Token newToken = tokenRepository.save(token);

    return Base64EncoderDecoder.encode(tokenValue + "&" + newToken.getIdToken());
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void deleteAllUserTokens(String userEmail, TokenType tokenType) {
    tokenRepository.deleteAllByAppUserReference_EmailAndTokenType(userEmail, tokenType);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Optional<Token> findByTokenId(int id) {
    return tokenRepository.findById(id);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void deleteByTokenId(int tokenId) {
    tokenRepository.deleteByIdToken(tokenId);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseEntity<AccessTokenDto> generateNewKeys(String refreshToken) {
    if (refreshToken == null) {
      throw new UnauthorizedException("Uživatel není přihlášen");
    }

    Token oldRefreshToken;
    try {
      oldRefreshToken = getTokenIfIsValid(refreshToken);
    } catch (BaseException e) {
      throw new UnauthorizedException("Token neexistuje nebo vypršela jeho platnost");
    }

    AppUser appUserReference = oldRefreshToken.getAppUserReference();
    if (appUserReference.isLocked()) {
      throw new UnauthorizedException("Učet je zablokován. Kontaktujte administrátora systému.");
    }

    if (!appUserReference.isVerified()) {
      throw new UnauthorizedException("Účet není aktivován. Aktivujte ho pomocí odkazu v obdržené e-mailové zprávě.");
    }

    AccessTokenDto accessToken = new AccessTokenDto();
    accessToken.setAccessToken(jwtTokenUtil.generateAccessToken(appUserReference.getEmail()));

    tokenRepository.delete(oldRefreshToken);

    ResponseCookie refreshTokenCookie = createRefreshTokenCookie(appUserReference, oldRefreshToken.isRemember());

    return ResponseEntity.ok()
      .header(HttpHeaders.SET_COOKIE, refreshTokenCookie.toString())
      .body(accessToken);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Token getTokenIfIsValid(String encodedToken) {
    Map<String, Object> tokenParts = getTokenParts(encodedToken);
    Token token = findByTokenId(Integer.parseInt((String) tokenParts.get("id")))
      .orElseThrow(() -> new NotFoundException("Zadaný token neexistuje"));

    if (isTokenValid((String) tokenParts.get("value"), token.getToken())) {
      return token;
    } else {
      throw new UnauthorizedException("Zadaný token není validní");
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseCookie createRefreshTokenCookie(String email, boolean rememberUser) {
    AppUser user = appUserService.findByEmail(email)
      .orElseThrow(() -> new NotFoundException("Uživatel s e-mailem \"" + email + "\" nebyl nalezen"));
    return createRefreshTokenCookieWithExpiration(user, rememberUser);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseCookie createRefreshTokenCookie(AppUser appUser, boolean rememberUser) {
    return createRefreshTokenCookieWithExpiration(appUser, rememberUser);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseCookie deleteRefreshTokenCookie() {
    return createRefreshTokenCookie("", 0);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isTokenValid(String rawToken, String encodedToken) {
    return passwordEncoder.matches(rawToken, encodedToken);
  }

  @Scheduled(cron = "0 * * * * *")
  public void deleteExpiredTokensEveryMinute() {
    List<Token> expiredTokens = tokenRepository.findAllByExpireAtLessThan(Instant.now());
    for (Token token : expiredTokens) {
      AppUser appUser = token.getAppUserReference();
      if (token.getTokenType() == TokenType.VERIFICATION && !appUser.isVerified()) {
        appUserService.deleteUser(appUser);
      }
      tokenRepository.delete(token);
    }
  }

  private Map<String, Object> getTokenParts(String encodedToken) {
    try {
      String decodedToken = Base64EncoderDecoder.decode(encodedToken);
      return splitTokenToValueAndId(decodedToken);
    } catch (Exception e) {
      throw new UnauthorizedException("Token je ve špatném formátu");
    }
  }

  private Map<String, Object> splitTokenToValueAndId(String rawToken) throws IndexOutOfBoundsException {
    String[] parts = rawToken.split("&");
    Map<String, Object> tokenParts = new HashMap<>();
    tokenParts.put("value", parts[0]);
    tokenParts.put("id", parts[1]);
    return tokenParts;
  }

  private ResponseCookie createRefreshTokenCookieWithExpiration(AppUser appUser, boolean rememberUser) {
    int expireAfterHours;
    if (rememberUser) {
      expireAfterHours = 14 * 24;
    } else {
      expireAfterHours = 2;
    }
    return createRefreshTokenCookie(generateToken(appUser, TokenType.REFRESH, expireAfterHours, rememberUser), expireAfterHours);
  }

  private ResponseCookie createRefreshTokenCookie(String value, int expireAfterHours) {
    return ResponseCookie.from("refreshToken", value)
      .httpOnly(true)
      .maxAge((long) expireAfterHours * 60 * 60)
      .path("/auth")
      .sameSite("Strict")
//      .secure(true)
      .build();
  }
}
