package cz.vutbr.fit.exampreparation.features.assignment;

import cz.vutbr.fit.exampreparation.common.MessageResponseDto;
import cz.vutbr.fit.exampreparation.features.assignment.dto.*;
import lombok.AllArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

/**
 * This class is responsible for managing assignment REST API endpoints.
 */
@Validated
@RestController
@AllArgsConstructor
public class AssignmentController {

  private final AssignmentService assignmentService;

  /**
   * Gets information about all assignments.
   *
   * @return response with information about all assignments
   */
  @PreAuthorize("hasAnyRole('EMPLOYEE','ADMIN')")
  @GetMapping("/internal/assignments")
  public ResponseEntity<List<BasicAssignmentInfoDto>> getAllAssignmentsInfo() {
    return assignmentService.getAllAssignmentsInfo();
  }

  /**
   * Gets more specific information about provided assignment.
   *
   * @param assignmentId  unique ID of the assignment
   * @return response with information about assignment
   */
  @PreAuthorize("hasAnyRole('EMPLOYEE','ADMIN')")
  @GetMapping("/internal/assignments/{id}")
  public ResponseEntity<AssignmentInfoDto> getAssignmentDetails(@PathVariable(value = "id") int assignmentId) {
    return assignmentService.getAssignmentDetails(assignmentId);
  }

  /**
   * Creates new assignment based on given data.
   *
   * @param assignmentDto  information necessary to create new assignment
   * @param assignmentFiles  list of assignment files
   * @return response with success message
   */
  @PreAuthorize("hasAnyRole('EMPLOYEE','ADMIN')")
  @PostMapping("/internal/assignments")
  public ResponseEntity<MessageResponseDto> createNewAssignment(@RequestPart("info") @Valid BasicAssignmentDto assignmentDto,
                                                                @RequestPart("files") MultipartFile[] assignmentFiles) {
    return assignmentService.createNewAssignment(assignmentDto, assignmentFiles);
  }

  /**
   * Edits existing assignment data based on new given data.
   *
   * @param assignmentId  unique ID of the assignment that should be edited
   * @param assignmentDto  information about changes
   * @param assignmentFiles  new assignment files
   * @return response with success message
   */
  @PreAuthorize("hasAnyRole('EMPLOYEE','ADMIN')")
  @PutMapping("/internal/assignments/{id}")
  public ResponseEntity<MessageResponseDto> editAssignment(@PathVariable(value = "id") int assignmentId,
                                                           @RequestPart("info") @Valid EditAssignmentDto assignmentDto,
                                                           @RequestPart(value = "files", required = false) MultipartFile[] assignmentFiles) {
    return assignmentService.editAssignment(assignmentId, assignmentDto, assignmentFiles);
  }

  /**
   * Deletes specified assignment from database.
   *
   * @param assignmentId  unique ID of the assignment that should be deleted
   * @return response with success message
   */
  @PreAuthorize("hasAnyRole('EMPLOYEE','ADMIN')")
  @DeleteMapping("/internal/assignments/{assignmentId:[\\d]+}")
  public ResponseEntity<MessageResponseDto> deleteAssignment(@PathVariable int assignmentId) {
    return assignmentService.deleteAssignment(assignmentId);
  }

  /**
   * Reads content of specified assignment file
   *
   * @param assignmentId  unique ID of the assignment to which the file belong
   * @param filePath  path to file which should be read
   * @return response with content of a file as String
   */
  @PreAuthorize("hasAnyRole('EMPLOYEE','ADMIN')")
  @GetMapping("/internal/assignments/{assignmentId:[\\d]+}/file")
  public ResponseEntity<FileContentDto> getAssignmentFileContent(@PathVariable int assignmentId,
                                                                 @RequestParam String filePath) {
    return assignmentService.getAssignmentFileContent(assignmentId, filePath);
  }

  /**
   * Gets URI address of specified file path.
   *
   * @param assignmentId  unique ID of the assignment to which the file belong
   * @param filePath  path to file which should be downloaded
   * @return response with URI to download the file
   */
  @PreAuthorize("hasAnyRole('EMPLOYEE','ADMIN')")
  @GetMapping("/internal/assignments/{assignmentId:[\\d]+}/file/download")
  @ResponseBody
  public ResponseEntity<Resource> downloadFile(@PathVariable int assignmentId, @RequestParam String filePath) {
    return assignmentService.downloadAssignmentFile(assignmentId, filePath);
  }

  /**
   * Starts generating of specified assignments.
   *
   * <ul>
   *   <li>If only {@code termId} is provided assignments for all students will be generated.</li>
   *   <li>If {@code termId} and {@code termRunId} are provided only assignments of specified term run will be generated.</li>
   *   <li>If {@code termId}, {@code termRunId} and {@code roomAbbreviation} are provided only assignments of specified room of specified term run will be generated.</li>
   *   <li>If {@code testScheduleId} is provided only its assignment will be generated.</li>
   * </ul>
   *
   * @param termId  unique ID of the term
   * @param termRunId  unique ID of the term run
   * @param roomAbbreviation  term runs room abbreviation
   * @param testScheduleId  unique ID of the test schedule
   * @return response with success message
   */
  @PreAuthorize("hasAnyRole('EMPLOYEE','ADMIN')")
  @GetMapping("/internal/assignments/generate")
  public ResponseEntity<MessageResponseDto> generateAssignment(@RequestParam(required = false) Integer termId,
                                                               @RequestParam(required = false) Integer termRunId,
                                                               @RequestParam(required = false) String roomAbbreviation,
                                                               @RequestParam(required = false) Integer testScheduleId) {
    return assignmentService.generateAssignment(termId, termRunId, roomAbbreviation, testScheduleId);
  }

  /**
   * Cancels all generating processes of given term.
   *
   * @param termId  unique ID of the term which generating processes should be canceled
   * @return response with success message
   */
  @PreAuthorize("hasAnyRole('EMPLOYEE','ADMIN')")
  @GetMapping("/internal/assignments/cancel-generating")
  public ResponseEntity<MessageResponseDto> cancelAssignmentGenerating(@RequestParam(required = false) Integer termId) {
    return assignmentService.cancelAssignmentGenerating(termId);
  }
}
