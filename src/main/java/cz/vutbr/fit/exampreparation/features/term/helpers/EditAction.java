package cz.vutbr.fit.exampreparation.features.term.helpers;

/**
 * There are multiple scenarios which can happen after rooms of term runs edit.
 *
 * <ul>
 *   <li>ADD - just add new term runs or room plans to array.</li>
 *   <li>REMOVE - just remove existing term runs or room plans to array.</li>
 *   <li>NEW - significant changes were made, create term runs or room plan again.</li>
 *   <li>NOTHING - no changes were made, do nothing.</li>
 * </ul>
 */
public enum EditAction {
  ADD,
  REMOVE,
  NEW,
  NOTHING
}
