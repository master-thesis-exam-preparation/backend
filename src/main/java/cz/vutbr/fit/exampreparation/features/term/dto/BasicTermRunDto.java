package cz.vutbr.fit.exampreparation.features.term.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Used to store basic information about terms term run.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BasicTermRunDto {

  @NotNull(message = "Chybí pomocný identifikátor běhu termínu zkoušky")
  private Integer id;

  @NotNull(message = "Chybí čas začátku termínu zkoušky")
  private Long startTime;

  @NotNull(message = "Chybí čas konce běhu termínu zkoušky")
  private Long endTime;

  private List<Integer> assignments;

}
