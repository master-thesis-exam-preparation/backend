package cz.vutbr.fit.exampreparation.features.cell;

import cz.vutbr.fit.exampreparation.features.roomPlan.RoomPlan;
import cz.vutbr.fit.exampreparation.features.testSchedule.TestSchedule;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * This entity class is responsible for object-relational mapping of an objects
 * with information about room plan cell into a database table Cell.
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
public class Cell {

  @Id
  @Setter(AccessLevel.PRIVATE)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int idCell;

  @NotNull(message = "Chybějící typ buňky (sedadlo|katedra|ulička")
  @Enumerated(EnumType.STRING)
  private CellType cellType;

  @NotNull(message = "Chybějící číslo pozice v řadě")
  @Min(value = 1, message = "Pozice v řadě musí být větší než 0")
  @Max(value = 150, message = "Pozice v řadě musí být menší než 150")
  private Integer columnNumber;

  @NotNull(message = "Chybějící číslo řady")
  @Min(value = 1, message = "Číslo řady musí být větší než 0")
  @Max(value = 150, message = "Číslo řady musí být menší než 150")
  private Integer rowNumber;

  @Min(value = 1, message = "Pozice v řadě musí být větší než 0")
  @Max(value = 150, message = "Pozice v řadě musí být menší než 150")
  private Integer seatColumnNumber;

  @Min(value = 1, message = "Číslo řady musí být větší než 0")
  @Max(value = 150, message = "Číslo řady musí být menší než 150")
  private Integer seatRowNumber;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "idRoomPlan", referencedColumnName = "idRoomPlan")
  @OnDelete(action = OnDeleteAction.CASCADE)
  private RoomPlan roomPlanReference;

  @OneToMany(mappedBy = "cell", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
  private List<TestSchedule> testSchedules;

  /**
   * Creates new instance of {@code Cell}.
   * ID attribute will be set after storing the object into database.
   *
   * @param cellType  cell type (seat, desk etc.)
   * @param rowNumber  row number in which the cell is localed
   * @param columnNumber  column number in which the cell is located
   * @param roomPlanReference  {@link RoomPlan} in which the cell is located
   * @param testSchedules  list of {@link TestSchedule} that are attached to this cell
   * @param seatRowNumber  if the {@link CellType} is not a seat then set this to null, otherwise row number, which contains only the seats
   * @param seatColumnNumber if the {@link CellType} is not a seat then set this to null, otherwise column number, which contains only the seats
   */
  public Cell(CellType cellType, Integer rowNumber, Integer columnNumber, RoomPlan roomPlanReference,
              List<TestSchedule> testSchedules, Integer seatRowNumber, Integer seatColumnNumber) {
    this.cellType = cellType;
    this.columnNumber = columnNumber;
    this.rowNumber = rowNumber;
    this.roomPlanReference = roomPlanReference;
    this.testSchedules = testSchedules;
    this.seatRowNumber = seatRowNumber;
    this.seatColumnNumber = seatColumnNumber;
  }
}
