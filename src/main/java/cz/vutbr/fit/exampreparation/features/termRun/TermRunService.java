package cz.vutbr.fit.exampreparation.features.termRun;

import java.util.List;
import java.util.Optional;

public interface TermRunService {

  /**
   * Finds all terms of specified term.
   *
   * @param termId  unique ID of the term
   * @return list of term runs of specified term
   */
  List<TermRun> getTermTermRuns(int termId);

  /**
   * Tries to find the term run by provided term run ID in database.
   *
   * @param termRunId  unique ID of the searched  term run
   * @return the found {@link TermRun}, if not found it will return null
   */
  Optional<TermRun> findTermRunById(int termRunId);
}
