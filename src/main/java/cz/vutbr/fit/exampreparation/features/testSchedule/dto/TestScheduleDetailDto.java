package cz.vutbr.fit.exampreparation.features.testSchedule.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import cz.vutbr.fit.exampreparation.features.appUser.dto.LoginNameDto;
import cz.vutbr.fit.exampreparation.features.assignment.Assignment;
import cz.vutbr.fit.exampreparation.features.assignment.dto.BasicAssignmentInfoDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * Used to store information about where who sits, what is their assignment etc.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TestScheduleDetailDto extends LoginNameDto {

  @NotNull(message = "Chybí identifikátor usazení")
  private Integer testScheduleId;

  @NotNull(message = "Chybí číslo sedadla")
  private Integer seatNumber;

  @NotNull(message = "Chybí pořadové číslo studenta")
  private Integer orderNumber;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private BasicAssignmentInfoDto assignment;

  private boolean isGenerated;

  /**
   * Creates new instance of {@code TestScheduleDetailDto}.
   *
   * @param login  login of the student
   * @param nameAndDegrees  name and degrees of the student
   * @param testScheduleId  unique ID of the test schedule
   * @param seatNumber  order number of seat on which will be student seated
   * @param orderNumber  student order in alphabetical order
   * @param assignment  assignment assigned to the student
   * @param isGenerated  set to true if assignment for this student was already generated
   */
  public TestScheduleDetailDto(String login, String nameAndDegrees, Integer testScheduleId, Integer seatNumber,
                               Integer orderNumber, Assignment assignment, boolean isGenerated) {
    super(login, nameAndDegrees);
    this.testScheduleId = testScheduleId;
    this.seatNumber = seatNumber;
    this.orderNumber = orderNumber;
    if (assignment == null) {
      this.assignment = null;
    } else {
      this.assignment = new BasicAssignmentInfoDto(assignment.getIdAssignment(), assignment.getName(), null);
    }
    this.isGenerated = isGenerated;
  }
}
