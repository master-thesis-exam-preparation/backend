package cz.vutbr.fit.exampreparation.features.term.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Used to store basic information about term.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BasicTermDto {
  @NotBlank(message = "Chybějící zkratka předmětu")
  private String courseAbbreviation;

  @NotBlank(message = "Chybějící název termínu zkoušky")
  private String termName;

  @NotNull(message = "Chybějící akademický rok")
  private Integer academicYear;

  @NotEmpty(message = "Chybějící datumy termínu zkoušek")
  private List<Long> termDates;
}
