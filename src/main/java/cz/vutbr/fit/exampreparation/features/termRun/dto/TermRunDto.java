package cz.vutbr.fit.exampreparation.features.termRun.dto;

import cz.vutbr.fit.exampreparation.features.term.dto.BasicTermRunDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * Used to store basic information about term run and list of ids of term runs selected rooms.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class TermRunDto extends BasicTermRunDto {

  @NotEmpty(message = "Chybí seznam místností u běhu termínu zkoušky")
  private List<Integer> rooms;

}
