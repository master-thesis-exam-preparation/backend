package cz.vutbr.fit.exampreparation.features.assignment.helpers;

import cz.vutbr.fit.exampreparation.common.exceptionHandling.exceptions.BadRequestException;
import cz.vutbr.fit.exampreparation.features.assignment.dto.FileDataDto;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


/**
 * This service class is responsible for saving data to disk and read them back from it.
 */
@Service
@PropertySource(value = "application.properties")
public class StorageManager {

  @Value("${assignments.path:${user.dir}${file.separator}assignments}")
  private String assignmentDirPath;

  /**
   * Creates directories for assignment files on application startup
   */
  @EventListener(ApplicationReadyEvent.class)
  public void createDirForAssignments() {
    new File(assignmentDirPath + File.separator + "templates").mkdirs();
    new File(assignmentDirPath + File.separator + "terms").mkdirs();
    new File(assignmentDirPath + File.separator + "backups").mkdirs();
  }

  /**
   * Save given assignment files to disk. Path to file will be determined from given assignment ID.
   * All files, that are not in {@code uploadedFiles} list will be deleted from directory.
   *
   * @param files  files to be saved to disk
   * @param uploadedFiles  list of files that should be kept in assignment directory
   * @param assignmentId  unique ID of the assignment to which the stored files belong
   */
  public void saveAssignmentFiles(MultipartFile[] files, List<FileDataDto> uploadedFiles, int assignmentId) {
    String assignmentDirPath = getAssignmentDirPath(assignmentId);
    String backupDirPath = getBackupDirPath(assignmentId);
    copyDirectory(assignmentDirPath, backupDirPath);

    if (uploadedFiles.size() == 0) {
      createOrClearDir(assignmentDirPath);
    } else {
      List<String> filePaths = uploadedFiles.stream()
        .map(file -> assignmentDirPath + File.separator + file.getName())
        .collect(Collectors.toList());
      deleteFilesNotInList(filePaths, assignmentDirPath);
    }
    if (files == null || files.length == 1 && Objects.equals(files[0].getOriginalFilename(), "")) {
      if (uploadedFiles.size() == 0) {
        revertAssignment(backupDirPath, assignmentDirPath);
        throw new BadRequestException("Šablona zadání neobsahuje žádný soubor");
      }
    }
    if (files != null) {
      for (MultipartFile file : files) {
        String filePath = storeFile(assignmentDirPath, file);
        if (isFileArchive(filePath)) {
          if (files.length > 1) {
            revertAssignment(backupDirPath, assignmentDirPath);
            throw new BadRequestException("Archiv lze nahrát pouze jako samostatný soubor. Vložte i ostatní soubory do daného archivu.");
          }
          extractArchive(filePath, assignmentDirPath);
        }
      }
    }

    try {
      checkIfThereIsTexFileInRootDir(assignmentDirPath);
    } catch (RuntimeException e) {
      revertAssignment(backupDirPath, assignmentDirPath);
      throw e;
    }
    deleteDirRecursively(backupDirPath);
  }

  /**
   * Copy all files that does not have .tex extension from source to destination directory.
   *
   * @param srcDirPath  path to source directory
   * @param destDirPath  path to destination directory
   */
  public void copyNonTexFiles(String srcDirPath, String destDirPath) {
    Path src = Paths.get(srcDirPath);
    Path dest = Paths.get(destDirPath);
    try {
      // copy all non tex files and folders from `src` to `dest`
      Files.walk(src)
        .filter(path -> !path.getFileName().toString().toLowerCase().endsWith(".tex"))
        .forEach(file -> {
          try {
            Files.copy(file, dest.resolve(src.relativize(file)), StandardCopyOption.REPLACE_EXISTING);
          } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Chyba při kopírování souborů");
          }
        });
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException("Chyba při procházení souborů");
    }
  }

  /**
   * Gets information about all files of given assignment.
   *
   * @param assignmentId  unique ID of the assignment
   * @return list of objects with information about assignment files
   */
  public List<FileDataDto> getFilesData(int assignmentId) {
    String assignmentDir = getAssignmentDirPath(assignmentId);

    try {
      Path root = Paths.get(assignmentDir);
      if (Files.exists(root)) {
        return Files.walk(root)
          .filter(Files::isRegularFile)
          .filter(path -> !path.equals(root))
          .collect(Collectors.toList())
          .stream()
          .map(path -> pathToFileData(path, assignmentDir))
          .collect(Collectors.toList());
      }
      return Collections.emptyList();
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException("Chyba při prohledávání souborů");
    }
  }

  /**
   * Reads given file and return his content as String.
   *
   * @param filePath path to file
   * @return file content
   */
  public String loadAsString(String filePath) {
    try {
      return new String(Files.readAllBytes(Paths.get(filePath)));
    } catch (IOException e) {
      e.printStackTrace();
      String message = "Chyba při čtení souboru. Ujistěte se, že soubor existuje." +
        "\nOpakujte pokus nebo kontaktujte administrátora.";
      throw new RuntimeException(message);
    }
  }

  /**
   * Creates {@link Resource} object from given file that can be send in HTTP body and used to download given file on client side.
   *
   * @param file  path to file to be downloaded
   * @return provided {@link Resource} object with URI address to download
   */
  public Resource downloadFile(Path file) {
    try {
      Resource resource = new UrlResource(file.toUri());
      if (resource.exists() || resource.isReadable()) {
        return resource;
      } else {
        throw new RuntimeException("Chyba, soubor neexistuje nebo není čitelný");
      }
    } catch (MalformedURLException e) {
      e.printStackTrace();
      String message = "Chyba při čtení souboru. Ujistěte se, že soubor existuje." +
        "\nOpakujte pokus nebo kontaktujte administrátora.";
      throw new RuntimeException(message);
    }
  }

  /**
   * Gets content type of given {@link Resource file}.
   *
   * @param file  file to examine
   * @return content type of provided file
   */
  public String getResourceProbeContentType(Resource file) {
    Path path;
    try {
      path = file.getFile().toPath();
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException("Chyba při zpracovávání stahovaného souboru");
    }

    try {
      return Files.probeContentType(path);
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException("Chyba při zpracovávání stahovaného souboru");
    }
  }

  /**
   * Finds path of the root directory of the given assignment.
   *
   * @param assignmentId  unique ID of the assignment
   * @return path to assignment root directory
   */
  public String getAssignmentDirPath(int assignmentId) {
    return this.assignmentDirPath + File.separator + "templates" + File.separator + assignmentId;
  }

  /**
   * Finds path of the root directory of the given term.
   *
   * @param termId  unique ID of the term
   * @return path to term root directory
   */
  public String getTermDirPath(int termId) {
    return this.assignmentDirPath + File.separator + "terms" + File.separator + termId;
  }

  /**
   * Finds path of the root directory of the given assignment backup.
   *
   * @param assignmentId  unique ID of the term
   * @return path to assignment backup root directory
   */
  public String getBackupDirPath(int assignmentId) {
    return this.assignmentDirPath + File.separator + "backups" + File.separator + assignmentId;
  }

  /**
   * Creates directory from given path. If this directory already exist removes all files in it.
   *
   * @param dirPath  path to directory
   */
  public void createOrClearDir(String dirPath) {
    if (!new File(dirPath).mkdirs()) {
      deleteDirRecursively(dirPath);
      if (!new File(dirPath).mkdirs()) {
        throw new RuntimeException("Chyba při vytváření pracovní složky");
      }
    }
  }

  /**
   * Creates directory from given path.
   *
   * @param dirPath  path to directory
   */
  public void createDir(String dirPath) {
    File directory = new File(dirPath);
    if (!directory.exists()) {
      if (!new File(dirPath).mkdirs()) {
        throw new RuntimeException("Chyba při vytváření pracovní složky");
      }
    }
  }

  /**
   * Deletes directory with all of its subdirectories and files.
   *
   * @param path  path do directory
   */
  public void deleteDirRecursively(String path) {
    FileSystemUtils.deleteRecursively(new File(path));
  }

  /**
   * Creates directories for each given path.
   *
   * @param dirs  list of paths to directories
   * @param clear  true if all files in directory should be deleted if that directory already exists, false otherwise
   */
  public void createTestScheduleDirs(Set<String> dirs, boolean clear) {
    dirs.forEach(dirPath -> {
      if (clear) {
        createOrClearDir(dirPath);
      } else {
        createDir(dirPath);
      }
    });
  }

  /**
   * Reads content of given assignment file.
   *
   * @param assignmentId  unique ID of the assignment to which the file belong
   * @param filePath  path to assignment file
   * @return file content as string
   */
  public String getAssignmentFileContent(int assignmentId, String filePath) {
    String path = getAssignmentDirPath(assignmentId) + File.separator + filePath;
    return loadAsString(path);
  }

  /**
   * Pack all given PDF files into archive file (.zip).
   *
   * @param filePaths  list of paths to PDF files that should be added to archive
   * @param zipFileName  name of the result archive file
   * @param destinationDir  path of directory into which should be final archive file saved
   * @return path to final archive file
   */
  public String zipFiles(List<String> filePaths, String zipFileName, String destinationDir) {
    try {
      String zipFilePath = destinationDir + File.separator + zipFileName.concat(".zip");
      FileOutputStream fos = new FileOutputStream(zipFilePath);
      ZipOutputStream zos = new ZipOutputStream(fos);
      for (String filePath : filePaths) {
        zos.putNextEntry(new ZipEntry(new File(filePath).getName()));

        byte[] bytes = Files.readAllBytes(Paths.get(filePath));
        zos.write(bytes, 0, bytes.length);
        zos.closeEntry();
      }
      zos.close();
      return zipFilePath;
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException("Chyba při vytváření archivu se zadáními." +
        "\nOpakujte pokus nebo kontaktujte administrátora.");
    }
  }

  /**
   * Merge all given PDF files into one final PDF file.
   *
   * @param filePaths  list of paths to PDF files that should be added to final PDF file
   * @param finalFileName  name of the result PDF file
   * @param destinationDir  path of directory into which should be final PDF file saved
   * @param doubleSided  true if empty pages should be added after PDF file with odd number of pages, false otherwise
   * @return path to final PDF file
   */
  public String mergePdfsIntoOne(List<String> filePaths, String finalFileName, String destinationDir, boolean doubleSided) {
    try {
      String finalPdfFilePath = destinationDir + File.separator + finalFileName + ".pdf";
      PDDocument finalDocument = new PDDocument();

      PDFMergerUtility PDFmerger = new PDFMergerUtility();
      for (String filePath : filePaths) {
        File pdfFile = new File(filePath);
        PDDocument document = PDDocument.load(pdfFile);
        PDFmerger.appendDocument(finalDocument, document);
        if (doubleSided) {
          if (document.getNumberOfPages() % 2 == 1) {
            finalDocument.addPage(new PDPage());
          }
        }
        document.close();
      }

      finalDocument.save(finalPdfFilePath);
      finalDocument.close();
      return finalPdfFilePath;
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException("Chyba při vytváření PDF se zadáními." +
        "\nOpakujte pokus nebo kontaktujte administrátora.");
    }
  }

  private void revertAssignment(String srcDir, String destDir) {
    copyDirectory(srcDir, destDir);
    deleteDirRecursively(srcDir);
  }

  private void checkIfThereIsTexFileInRootDir(String assignmentDirPath) {
    File f = new File(assignmentDirPath);
    FilenameFilter filter = (f1, name) -> name.endsWith(".tex");
    String[] texFiles = f.list(filter);
    if (texFiles == null || texFiles.length == 0) {
      throw new BadRequestException("Kořenový adresář šablony zadání neobsahuje žádný .tex soubor");
    }
  }

  private FileDataDto pathToFileData(Path path, String rootDirPath) {
    FileDataDto fileData = new FileDataDto();
    String filename = path.toString().replace(rootDirPath + File.separator, "");
    fileData.setName(filename);

    try {
      fileData.setType(Files.probeContentType(path));
      fileData.setSize(Files.size(path));
    } catch (IOException e) {
      throw new RuntimeException("Chyba při zpracování souboru");
    }

    return fileData;
  }

  private String storeFile(String dirPath, MultipartFile file) {
    try {
      String filePath = dirPath + File.separator + file.getOriginalFilename();
      file.transferTo(new File(filePath));
      return filePath;
    } catch (IOException e) {
      e.printStackTrace();
      String message = "Chyba při transformaci vstupních souborů." +
        "\nOpakujte pokus nebo kontaktujte administrátora.";
      throw new RuntimeException(message);
    }
  }

  private boolean isFileArchive(String filePath) {
    try {
      Process p = new ProcessBuilder("7z", "t", filePath).start();
      return p.waitFor() == 0;
    } catch (InterruptedException | IOException e) {
      e.printStackTrace();
      String message = "Chyba při práci s archivem." +
        "\nOpakujte pokus nebo kontaktujte administrátora.";
      throw new RuntimeException(message);
    }
  }

  private void extractArchive(String filePath, String dirPath) {
    try {
      Process p = new ProcessBuilder("7z", "x", filePath, "-o" + dirPath).start();
      p.waitFor();
    } catch (InterruptedException | IOException e) {
      e.printStackTrace();
      String message = "Chyba při rozbalování archivu." +
        "\nOpakujte pokus nebo kontaktujte administrátora.";
      throw new RuntimeException(message);
    }

    deleteFile(filePath);
  }

  private void copyDirectory(String sourceDirectoryLocation, String destinationDirectoryLocation) {
    deleteDirRecursively(destinationDirectoryLocation);
    try {
      Path root = Paths.get(sourceDirectoryLocation);
      if (Files.exists(root)) {
        Files.walk(Paths.get(sourceDirectoryLocation))
          .forEach(source -> {
            Path destination = Paths.get(destinationDirectoryLocation, source.toString()
              .substring(sourceDirectoryLocation.length()));
            try {
              Files.copy(source, destination, StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
              e.printStackTrace();
              String message = "Chyba při zálohování šablony zadání." +
                "\nOpakujte pokus nebo kontaktujte administrátora.";
              throw new RuntimeException(message);
            }
          });
      }
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException("Chyba při procházení souborů při zálohování šablony zadání." +
        "\nOpakujte pokus nebo kontaktujte administrátora.");
    }
  }

  private void deleteFile(String filePath) {
    if (!new File(filePath).delete()) {
      throw new RuntimeException("Chyba při mazání souboru" +
        "\nOpakujte pokus nebo kontaktujte administrátora.");
    }
  }

  private void deleteFilesNotInList(List<String> filePaths, String dirPath) {
    Path root = Paths.get(dirPath);
    try {
      Files.walk(root)
        .filter(path -> !path.equals(root) && !new File(path.toString()).isDirectory())
        .collect(Collectors.toList())
        .forEach(path -> {
          String filePath = path.toString();
          if (!filePaths.contains(filePath)) {
            deleteFile(filePath);
          }
        });
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException("Chyba při promazávání souborů." +
        "\nOpakujte pokus nebo kontaktujte administrátora.");
    }
  }
}
