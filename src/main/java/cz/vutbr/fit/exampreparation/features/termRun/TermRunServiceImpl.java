package cz.vutbr.fit.exampreparation.features.termRun;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * This service class is responsible for management of all term runs.
 */
@Service
@AllArgsConstructor
public class TermRunServiceImpl implements TermRunService {

  private final TermRunRepository termRunRepository;

  /**
   * {@inheritDoc}
   */
  @Override
  public List<TermRun> getTermTermRuns(int termId) {
    return termRunRepository.findByTermReference_IdTerm(termId);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Optional<TermRun> findTermRunById(int termRunId) {
    return termRunRepository.findById(termRunId);
  }
}
