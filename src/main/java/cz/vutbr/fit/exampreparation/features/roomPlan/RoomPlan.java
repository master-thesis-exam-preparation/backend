package cz.vutbr.fit.exampreparation.features.roomPlan;

import cz.vutbr.fit.exampreparation.features.cell.Cell;
import cz.vutbr.fit.exampreparation.features.room.Room;
import cz.vutbr.fit.exampreparation.features.termRun.TermRun;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * This entity class is responsible for object-relational mapping of an objects
 * with information about room plans into a database table RoomPlan.
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
public class RoomPlan {

  @Id
  @Setter(AccessLevel.PRIVATE)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int idRoomPlan;

  @NotNull(message = "Chybějící kapacita místnosti")
  @Min(value = 0, message = "Kapacita místnosti nemůže být záporná")
  private int capacity;

  @NotNull(message = "Chybějící údaj o počtu volných míst mezi studenty")
  @Enumerated(EnumType.STRING)
  private SpacingBetweenStudents spacing;

  @ManyToMany(mappedBy = "roomPlans", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
  private List<TermRun> termRuns;

  @OneToMany(mappedBy = "roomPlanReference", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @OnDelete(action = OnDeleteAction.CASCADE)
  private List<Cell> cells;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "idRoom", referencedColumnName = "idRoom")
  @OnDelete(action = OnDeleteAction.CASCADE)
  private Room roomReference;

  /**
   * Creates new instance of {@code RoomPlan}.
   * ID attribute will be set after storing the object into database.
   *
   * @param capacity  number of seats in room plan
   * @param spacing  number of free cells between students in room plan
   * @param termRuns  list of term runs in which this room plan was selected
   * @param cells  list of cells
   * @param roomReference  reference to the room to which this room plan belongs
   */
  public RoomPlan(@NotNull(message = "Chybějící kapacita místnosti") @Min(value = 0, message = "Kapacita místnosti nemůže být záporná") int capacity,
                  @NotNull(message = "Chybějící údaj o počtu volných míst mezi studenty") SpacingBetweenStudents spacing,
                  List<TermRun> termRuns,
                  List<Cell> cells,
                  Room roomReference) {
    this.capacity = capacity;
    this.spacing = spacing;
    this.termRuns = termRuns;
    this.cells = cells;
    this.roomReference = roomReference;
  }

  /**
   * Creates new instance of {@code RoomPlan}, but only with spacing between students and reference to parent room.
   *
   * @param spacing  number of free cells between students in room plan
   * @param roomReference  reference to the room to which this room plan belongs
   */
  public RoomPlan(@NotNull(message = "Chybějící údaj o počtu volných míst mezi studenty") SpacingBetweenStudents spacing,
                  Room roomReference) {
    this.spacing = spacing;
    this.roomReference = roomReference;
  }
}
