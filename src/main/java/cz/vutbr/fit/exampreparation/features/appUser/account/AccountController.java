package cz.vutbr.fit.exampreparation.features.appUser.account;

import cz.vutbr.fit.exampreparation.common.MessageResponseDto;
import cz.vutbr.fit.exampreparation.features.appUser.dto.AccountInfoDto;
import cz.vutbr.fit.exampreparation.features.appUser.dto.ChangePasswordDto;
import cz.vutbr.fit.exampreparation.features.appUser.dto.NewRoleDto;
import cz.vutbr.fit.exampreparation.features.appUser.dto.UserDataDto;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * This class is responsible for managing account REST API endpoints.
 */
@Validated
@RestController
@RequestMapping("/internal/accounts")
@AllArgsConstructor
public class AccountController {

  private final AccountService accountService;

  /**
   * Changes current password of logged-in user.
   *
   * @param changePasswordDto  object with old password to confirmation and new password to set
   * @return response with success message
   */
  @PreAuthorize("hasAnyRole('STUDENT','EMPLOYEE','ADMIN')")
  @PostMapping("/change-password")
  public ResponseEntity<MessageResponseDto> changePassword(@Valid @RequestBody ChangePasswordDto changePasswordDto) {
    return accountService.changePassword(changePasswordDto);
  }

  /**
   * Gets information about currently logged-in user.
   *
   * @return response with information about logged-in user
   */
  @PreAuthorize("hasAnyRole('STUDENT','EMPLOYEE','ADMIN')")
  @GetMapping("/change-user-details")
  public ResponseEntity<UserDataDto> getUserData() {
    return accountService.getUserData();
  }

  /**
   * Changes current information of logged-in user.
   *
   * @param userDataDto  object with new information about user (name, degrees etc.)
   * @return response with success message
   */
  @PreAuthorize("hasAnyRole('STUDENT','EMPLOYEE','ADMIN')")
  @PostMapping("/change-user-details")
  public ResponseEntity<MessageResponseDto> changeUserDetails(@Valid @RequestBody UserDataDto userDataDto) {
    return accountService.changeUserDetails(userDataDto);
  }

  /**
   * Gets information about all users accounts.
   *
   * @return response with information about all user accounts
   */
  @PreAuthorize("hasRole('ADMIN')")
  @GetMapping("")
  public ResponseEntity<List<AccountInfoDto>> getAllUsersData() {
    return accountService.getAllUsersData();
  }

  /**
   * Changes role of a specific user to new one.
   *
   * @param userId  unique ID of the user whose account should get different role
   * @param newRoleDto  object with name of the new role
   * @return response with success message
   */
  @PreAuthorize("hasRole('ADMIN')")
  @PostMapping("/change-role/{userId}")
  public ResponseEntity<MessageResponseDto> changeAccountRole(@PathVariable int userId, @Valid @RequestBody NewRoleDto newRoleDto) {
    return accountService.changeAccountRole(newRoleDto, userId);
  }

  /**
   * Deletes account of a specific user.
   *
   * @param userId  unique ID of the user whose account is to be deleted
   * @return response with success message
   */
  @PreAuthorize("hasRole('ADMIN')")
  @DeleteMapping("/delete/{userId}")
  public ResponseEntity<MessageResponseDto> deleteAccount(@PathVariable int userId) {
    return accountService.deleteAccount(userId);
  }

  /**
   * Locks account of a blocked user.
   *
   * @param userId unique ID of the user whose account is to be locked
   * @return response with success message
   */
  @PreAuthorize("hasRole('ADMIN')")
  @GetMapping("/lock/{userId}")
  public ResponseEntity<MessageResponseDto> lockAccount(@PathVariable int userId) {
    return accountService.lockAccount(userId);
  }

  /**
   * Unlocks account of a blocked user.
   *
   * @param userId  unique ID of the user whose account is to be unlocked
   * @return response with success message
   */
  @PreAuthorize("hasRole('ADMIN')")
  @GetMapping("/unlock/{userId}")
  public ResponseEntity<MessageResponseDto> unlockAccount(@PathVariable int userId) {
    return accountService.unlockAccount(userId);
  }
}
