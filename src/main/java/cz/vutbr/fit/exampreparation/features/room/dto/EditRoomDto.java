package cz.vutbr.fit.exampreparation.features.room.dto;

import cz.vutbr.fit.exampreparation.features.cell.CellType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Used to store information necessary for room edit.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class EditRoomDto extends BasicRoomDto {

  private String description;

  @NotNull(message = "Chybějící počet řad")
  @Min(value = 1, message = "Místnost musí obsahovat alespoň 1 řadu")
  @Max(value = 150, message = "Místnost může obsahovat maximálně 150 řad")
  private Integer numberOfRows;

  @NotNull(message = "Chybějící počet míst v řadě")
  @Min(value = 1, message = "Místnost musí obsahovat alespoň 1 místo v řadě")
  @Max(value = 150, message = "Místnost může obsahovat maximálně 150 míst v řadě")
  private Integer numberOfColumns;

  @NotEmpty(message = "Plán místnosti nesmí být prázdný")
  private List<List<CellType>> cells;

  /**
   * Creates new instance of {@code EditRoomDto}.
   *
   * @param abbreviation  new room abbreviation
   * @param name  new room name
   * @param description  new room description text
   * @param numberOfRows  new number of rows in the room plan
   * @param numberOfColumns  new number of columns in the room plan
   * @param cells  new room plan cells (with cell types etc.)
   */
  public EditRoomDto(String abbreviation, String name, String description, Integer numberOfRows, Integer numberOfColumns,
                     List<List<CellType>> cells) {
    super(abbreviation, name);
    this.description = description;
    this.numberOfRows = numberOfRows;
    this.numberOfColumns = numberOfColumns;
    this.cells = cells;
  }
}
