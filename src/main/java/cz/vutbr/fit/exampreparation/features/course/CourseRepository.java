package cz.vutbr.fit.exampreparation.features.course;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * This class is responsible for querying database table Course
 */
@Repository
public interface CourseRepository extends JpaRepository<Course, Integer> {
  List<Course> findAllByAbbreviationAndNameAndAcademicYear(String abbreviation, String name, int academicYear);

  @Query("SELECT c FROM Course c " +
    "ORDER BY c.academicYear DESC, c.abbreviation ASC, c.name ASC")
  List<Course> findAllSorted();

  @Query("SELECT c FROM Course c " +
    "WHERE c.academicYear >= ?1 AND c.academicYear <= ?2 " +
    "ORDER BY c.academicYear DESC, c.abbreviation ASC, c.name ASC")
  List<Course> findAllCurrentAndSorted(int startYear, int endYear);
}
