package cz.vutbr.fit.exampreparation.features.appUser.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * Used to store information about users old and new password.
 */
@Data
@AllArgsConstructor
public class ChangePasswordDto {

  @NotBlank(message = "Chybějící heslo")
  private String currentPassword;

  @NotBlank(message = "Chybějící nové heslo")
  private String newPassword;

}
