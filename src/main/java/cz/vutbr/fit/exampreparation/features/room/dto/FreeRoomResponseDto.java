package cz.vutbr.fit.exampreparation.features.room.dto;

import cz.vutbr.fit.exampreparation.features.roomPlan.dto.RoomPlanDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Used to store information about free rooms.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FreeRoomResponseDto extends BasicRoomDto {

  @NotNull(message = "Identifikátor místnosti je povinný")
  private Integer roomId;

  @NotEmpty(message = "Seznam plánů místností nesmí být prázdný")
  private List<RoomPlanDto> roomPlans;

}
