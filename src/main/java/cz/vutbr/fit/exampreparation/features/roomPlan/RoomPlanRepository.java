package cz.vutbr.fit.exampreparation.features.roomPlan;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * This class is responsible for querying database table RoomPlan
 */
@Repository
public interface RoomPlanRepository extends JpaRepository<RoomPlan, Integer> {
  RoomPlan findRoomPlanByRoomReference_IdRoomAndSpacing(Integer roomId, SpacingBetweenStudents spacing);
}
