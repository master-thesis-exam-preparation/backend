package cz.vutbr.fit.exampreparation.features.appUser.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * Used to store information necessary to log-in user to application (e-mail address, password and remember me option value).
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginUserDto extends EmailPasswordDto {

  @NotNull(message = "Chybějící volba")
  private boolean rememberMe;

}
