package cz.vutbr.fit.exampreparation.features.termRun.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import cz.vutbr.fit.exampreparation.features.assignment.dto.BasicAssignmentInfoDto;
import cz.vutbr.fit.exampreparation.features.room.dto.BasicRoomDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * Used to store basic information about term run and information about selected rooms and assignments.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TermRunBasicRoomDto extends BasicTermRun {

  @NotEmpty(message = "Chybí seznam místností u běhu termínu zkoušky")
  private List<BasicRoomDto> rooms;

  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  private List<BasicAssignmentInfoDto> assignments;

  /**
   * Creates new instance of {@link TermRunBasicRoomDto}
   *
   * @param id  unique ID of the term run
   * @param startTime  start date time of the term run
   * @param endTime  end date time of the term run
   * @param rooms  list of selected rooms
   * @param assignments  list of selected assignments
   */
  public TermRunBasicRoomDto(Integer id, Long startTime, Long endTime, List<BasicRoomDto> rooms,
                             List<BasicAssignmentInfoDto> assignments) {
    super(id, startTime, endTime);
    this.rooms = rooms;
    this.assignments = assignments;
  }
}
