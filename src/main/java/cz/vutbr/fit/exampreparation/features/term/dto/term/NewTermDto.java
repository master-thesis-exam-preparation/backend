package cz.vutbr.fit.exampreparation.features.term.dto.term;

import cz.vutbr.fit.exampreparation.features.appUser.dto.LoginNameDto;
import cz.vutbr.fit.exampreparation.features.roomPlan.SpacingBetweenStudents;
import cz.vutbr.fit.exampreparation.features.term.dto.StudentsPlacementDto;
import cz.vutbr.fit.exampreparation.features.termRun.Distribution;
import cz.vutbr.fit.exampreparation.features.termRun.dto.TermRunDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Used to store basic information about term and other data necessary to create new term.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NewTermDto extends BasicTermDto {

  @NotEmpty(message = "Chybí seznam studentů, kteří se mají účastnit termínu zkoušky")
  private List<@Valid LoginNameDto> studentsList;

  private List<String> additionalTermManagersEmails;

  @NotEmpty(message = "Chybí seznam běhů termínů zkoušky")
  private List<@Valid TermRunDto> termRuns;

  private List<@Valid StudentsPlacementDto> studentsPlacements;

  private boolean sendEmail;

  @Size(max = 65535, message = "Příliš dlouhý vstup")
  private String emailMessage;

  /**
   *
   * @param termName  name of the term
   * @param courseId  id of the course from which the exam takes place
   * @param spacingBetweenStudents  selected number of free cells between students
   * @param termRuns  list of term runs of this term
   * @param studentsPlacements  list with information about manually placed students
   * @param distribution  selected type of distribution of students
   * @param informationText  text with term description
   * @param studentsList  list of students that will attend exam of this term
   * @param additionalTermManagersEmails  list of application users that can manage this term
   * @param sendEmail  if true e-mail message with notification will be sent to students, otherwise no message will be send
   * @param emailMessage  e-mail message text that will be sent to students
   */
  public NewTermDto(String termName, Integer courseId, SpacingBetweenStudents spacingBetweenStudents,
                    List<TermRunDto> termRuns, List<StudentsPlacementDto> studentsPlacements, Distribution distribution,
                    String informationText, List<LoginNameDto> studentsList, List<String> additionalTermManagersEmails,
                    boolean sendEmail, String emailMessage) {
    super(termName, courseId, spacingBetweenStudents, distribution, informationText);
    this.studentsList = studentsList;
    this.additionalTermManagersEmails = additionalTermManagersEmails;
    this.termRuns = termRuns;
    this.studentsPlacements = studentsPlacements;
    this.sendEmail = sendEmail;
    this.emailMessage = emailMessage;
  }
}
