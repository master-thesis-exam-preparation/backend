package cz.vutbr.fit.exampreparation.features.term.helpers;

import cz.vutbr.fit.exampreparation.features.cell.Cell;
import cz.vutbr.fit.exampreparation.features.roomPlan.RoomPlan;
import cz.vutbr.fit.exampreparation.features.termRun.TermRun;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * Used to store information about list of cells of type seat that have no students assigned
 * and they are at the end of specified room plan of specified term run.
 */
@Data
@AllArgsConstructor
public class FreeSeatsAtTheEndOfRoomPlan {

  private TermRun termRun;
  private RoomPlan roomPlan;
  private List<Cell> FreeSeatsAtTheEndOfRoom;
}
