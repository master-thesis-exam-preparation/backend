package cz.vutbr.fit.exampreparation.features.room;

import cz.vutbr.fit.exampreparation.common.MessageResponseDto;
import cz.vutbr.fit.exampreparation.features.room.dto.*;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

public interface RoomService {

  /**
   * Creates new room based on given data.
   *
   * @param newRoomDto  information necessary to create new room
   * @return response with information about newly created room
   */
  ResponseEntity<RoomInfoDto> createNewRoom(NewRoomDto newRoomDto);

  /**
   * Gets information about all rooms.
   *
   * @return response with information about all rooms
   */
  ResponseEntity<List<RoomInfoDto>> getAllRoomsInfo();

  /**
   * Gets information about room that can be viewed by users.
   *
   * @param roomId  unique ID of the room
   * @param isPublic  if set to false this method return only that information that can be accessed by logged-in users
   * @return response with information about room
   */
  ResponseEntity<RoomDetailDto> getRoomDetails(int roomId, boolean isPublic);

  /**
   * Gets information about room that will be used to fill form.
   *
   * @param roomId  unique ID of the room
   * @param isEditForm  if true it will return data for edit form
   * @return response with edit information about room
   */
  ResponseEntity<EditRoomDto> getEditRoomDetails(int roomId, boolean isEditForm);

  /**
   * Edits existing room data based on new given data.
   *
   * @param roomId  unique ID of the room
   * @param newRoomDto  information about changes
   * @return response with success message
   */
  ResponseEntity<MessageResponseDto> editRoom(int roomId, NewRoomDto newRoomDto);

  /**
   * Deletes specified room, its room plan and cells from database.
   *
   * @param roomId  unique ID of the room that should be deleted
   * @return response with success message
   */
  ResponseEntity<MessageResponseDto> deleteRoom(int roomId);

  /**
   * Gets list of room that does not have any exam in it at given time.
   *
   * @param freeRoomRequestDto  information about start time, length etc.
   * @return response with list of free rooms
   */
  ResponseEntity<List<FreeRoomResponseDto>> getFreeRooms(FreeRoomRequestDto freeRoomRequestDto);

  /**
   * Tries to find the room by provided room ID in database.
   *
   * @param id  unique ID of the searched assignment
   * @return the found {@link Room}, if not found it will return null
   */
  Optional<Room> findRoomById(int id);

  /**
   * Finds all free rooms in specified time window.
   *
   * @param startTime  start time of the time window
   * @param endTime  end time of the time window
   * @param termId  unique ID of the term, if not null it will compare only with exams of specified term
   * @return list of rooms that does not have any exam in it in specified time window
   */
  List<Room> findFreeRooms(long startTime, long endTime, Integer termId);
}
